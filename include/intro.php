<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<h1 class="text-center intro"><?=$APPLICATION->ShowTitle('h1',false);?> </h1>
<p class="text-center">Специалисты компании Первый БИТ помогут подобрать оптимальную CRM-систему, идеально соответствуюшую вашим потребностям. Мы работаем как с облачными, так и с коробочными версиями продуктов. </p>

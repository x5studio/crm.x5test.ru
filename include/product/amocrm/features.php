<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="container clearfix product-features-wrap">

	<div class="row">
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Простой и понятный интерфейс</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Автоматическая фиксация заявок</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Мобильная CRM</span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Полная интеграция с e-mail</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Интеграция с вашей телефонией</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Совместная работа</span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Автоматическая воронка продаж</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Воронка периодических покупок</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>API и расширения</span>
			</div>
		</div>
	</div>

</div>

<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>


<img style="position: absolute;bottom: -30px;" class="hidden-xs" src="/local/templates/main2017/assets/css/../img/products/bg-image_3.svg" />


<div class="container clearfix product-features-wrap">
	<div class="row">
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Бесшовная интеграция с любой 1С</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Бизнес-конструктор из различных модулей</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Интеграция с MS Exchange, мобильными приложениями и online-сервисами</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Интеграция с АТС. Запись и анализ звонков</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Наглядная аналитика в формате настраиваемых дашбордов</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Работа с любыми облачными решениями</span>
			</div>
		</div>
	</div>
</div>
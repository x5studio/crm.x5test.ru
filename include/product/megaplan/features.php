<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="row features-block">
	<div class="col-sm-6 col-md-5">
		<div class="single-feature">
			<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
			<span>Быстрый старт</span>
		</div>
	</div>
	<div class="col-sm-6 col-md-5">
		<div class="single-feature">
			<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
			<span>Полная история переговоров с клиентами</span>
		</div>
	</div>
	<div class="col-sm-6 col-md-5">
		<div class="single-feature">
			<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
			<span>Бесплатные обновления раз в месяц</span>
		</div>
	</div>

</div>

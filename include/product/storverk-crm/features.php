<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>


<img style="position: absolute;bottom: -30px;" class="hidden-xs" src="/local/templates/main2017/assets/css/../img/products/bg-image_3.svg" />


<div class="container clearfix product-features-wrap">
	<div class="row">
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Максимально обширный функционал без дорогостоящих доработок</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Безопасность ваших данных</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Без ежемесячных оплат</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Мультиинтеграция с продуктами 1С</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Интеграции с любыми сервисами через API</span>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="single-feature">
				<div class="pull-left wrap-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bit/agree.png" alt=""></div>
				<span>Интеграция с ATC. Запись и анализ звонков</span>
			</div>
		</div>
	</div>
</div>
<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<h2 class="col-sm-4">Цены на STORVERK CRM</h2>
<div class="col-sm-4">
	<div class="crm-package first-package">
 <span class="package-name">Основная поставка <br>
		 для «1С: Управление торговлей 11.4»</span> <span class="package-price">54 000 руб.</span>
	</div>
</div>
<div class="col-sm-4">
	<div class="crm-package">
 <span class="package-name">Основная поставка <br>
		 для «1С: ERP 2.4»</span> <span class="package-price">84 000 руб.</span>
	</div>
</div>
 <br>
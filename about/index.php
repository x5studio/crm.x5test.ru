<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("description", "Крупнейший партнер 1С, лидер отрасли автоматизации – компания Первый БИТ. Полная консультация по CRM, внедрению и настройке. Круглосуточная поддержка.");
$APPLICATION->SetPageProperty("title", "О компании Первый БИТ");
$APPLICATION->SetTitle("О компании");
?><style>
    .text-content{
        padding: 30px 18px;
    }

	.width{
		margin: 0 auto;
		width: 959px;
		border: 1px solid #ccc;
		box-shadow: 0 1px 3px rgba(0,0,0,.3);
		text-align: left;
	}

	.width img{
		max-width: 100%;
    }

	@media (max-width: 991px) and (min-width: 768px){
		.width {
			width: 720px;
		}
	}

	@media (max-width: 767px){
		.width {
			width: 100%;
		}
	}

    /* Start:/company/about/about.min.css?15187916608129*/
	.about-header{padding-left:55px;margin-bottom:0;font:40px/1.25em Arial,Helvetica,sans-serif}.about-history{padding:28px 0 38px 49px;color:#a2a2aa;text-shadow:0 1px 0 #fff,0 -1px 0 #59595f}.about-block1{margin-bottom:-14px;position:relative;height:214px;background:url(/bitrix/templates/main/img/about-block1-bg.png) no-repeat 0 0}p.volume{position:relative;padding:46px 160px 46px 150px;text-align:center;font:bold 42px/1.25em Arial,Helvetica,sans-serif;color:#323694;z-index:5}.about-block1 a{text-decoration:none}p.volume-under{padding:46px 160px 46px 150px;text-align:center;font:bold 42px/1.25em Arial,Helvetica,sans-serif;color:#181947;position:absolute;top:-1px;z-index:0}.pink-char{color:#d1088e!important}.about-block2{background:#d9dcdc;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPHJhZGlhbEdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iNzUlIj4KICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNkOWRjZGMiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI5OCUiIHN0b3AtY29sb3I9IiNkOWRiZGIiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI5OSUiIHN0b3AtY29sb3I9IiNkN2RhZGEiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjZDFkM2Q0IiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2NkY2ZkMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjMmMyYzIiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjZmZmZmZmIiBzdG9wLW9wYWNpdHk9IjEiLz4KICA8L3JhZGlhbEdyYWRpZW50PgogIDxyZWN0IHg9Ii01MCIgeT0iLTUwIiB3aWR0aD0iMTAxIiBoZWlnaHQ9IjEwMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background:-moz-radial-gradient(center,ellipse cover,#d9dcdc 0,#d9dbdb 98%,#d7dada 99%,#d1d3d4 100%,#cdcfd0 100%,#c2c2c2 100%,#fff 100%);background:-webkit-gradient(radial,center center,0,center center,100%,color-stop(0,#d9dcdc),color-stop(98%,#d9dbdb),color-stop(99%,#d7dada),color-stop(100%,#d1d3d4),color-stop(100%,#cdcfd0),color-stop(100%,#c2c2c2),color-stop(100%,#fff));background:-webkit-radial-gradient(center,ellipse cover,#d9dcdc 0,#d9dbdb 98%,#d7dada 99%,#d1d3d4 100%,#cdcfd0 100%,#c2c2c2 100%,#fff 100%);background:-o-radial-gradient(center,ellipse cover,#d9dcdc 0,#d9dbdb 98%,#d7dada 99%,#d1d3d4 100%,#cdcfd0 100%,#c2c2c2 100%,#fff 100%);background:-ms-radial-gradient(center,ellipse cover,#d9dcdc 0,#d9dbdb 98%,#d7dada 99%,#d1d3d4 100%,#cdcfd0 100%,#c2c2c2 100%,#fff 100%);background:radial-gradient(ellipse at center,#d9dcdc 0,#d9dbdb 98%,#d7dada 99%,#d1d3d4 100%,#cdcfd0 100%,#c2c2c2 100%,#fff 100%)}.about-block2 p{margin:0;padding:20px}.gradient{background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPHJhZGlhbEdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgY3g9IjUwJSIgY3k9IjUwJSIgcj0iNzUlIj4KICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNmZmZmZmYiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjZmZmZmZmIiBzdG9wLW9wYWNpdHk9IjAiLz4KICA8L3JhZGlhbEdyYWRpZW50PgogIDxyZWN0IHg9Ii01MCIgeT0iLTUwIiB3aWR0aD0iMTAxIiBoZWlnaHQ9IjEwMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background:-moz-radial-gradient(center,ellipse cover,rgba(255,255,255,1) 0,rgba(255,255,255,0) 100%);background:-webkit-gradient(radial,center center,0,center center,100%,color-stop(0,rgba(255,255,255,1)),color-stop(100%,rgba(255,255,255,0)));background:-webkit-radial-gradient(center,ellipse cover,rgba(255,255,255,1) 0,rgba(255,255,255,0) 100%);background:-o-radial-gradient(center,ellipse cover,rgba(255,255,255,1) 0,rgba(255,255,255,0) 100%);background:-ms-radial-gradient(center,ellipse cover,rgba(255,255,255,1) 0,rgba(255,255,255,0) 100%);background:radial-gradient(ellipse at center,rgba(255,255,255,1) 0,rgba(255,255,255,0) 100%)}.gradient2{background:url(/bitrix/templates/main/img/gradient.png) repeat-x 0 100%}.about-book{margin-left:8px}.about-book p{padding:0;margin-bottom:15px;font-size:15px;color:#414141}.book-top{height:20px;background:url(/bitrix/templates/main/img/book-top.png) no-repeat 0 0}.book-middle{min-height:354px;padding:41px 65px 20px;overflow:hidden;background:url(/bitrix/templates/main/img/book-middle.png) repeat-y 0 0}.book-bottom{height:60px;background:url(/bitrix/templates/main/img/book-bottom.png) no-repeat 0 0}.book-col{width:355px}.book-col-l{float:left}.book-col-r{float:right}
.about-block3{position:relative}.about-tree{position:absolute;left:50%;top:195px;bottom:170px;width:4px;background-color:#323694}
.about-block3 .inner{overflow:hidden;padding:0 43px 138px}.about-block3 h1{padding:31px 49px 20px; margin-top: 0;}
.about-col{width:392px}.about-col-l{float:left}
.about-col-r{float:right}.about-col ul{list-style:none;list-style-image:none;margin:0;padding:0}.about-col ul li{background:0 0;margin:0;padding:0}li.leaf{position:relative;border:1px solid #323694;font:15px/1.25em Arial,Helvetica,sans-serif;color:#414141;zoom:1}.leaf-arrow{position:absolute;top:40px;width:14px;height:20px;background-image:url(/bitrix/templates/main/img/leaf-arrow.png)}.leaf-arrow-l{background-position:0 0;right:-11px}.leaf-arrow-r{background-position:100% 0;left:-11px}
.leaf-year{position:absolute;top:25px;width:50px;height:50px;background-image:url(/about/leaf-year-pink.png)}.leaf-year-l{right:-73px}.leaf-year-r{left:-68px}.leaf-year span{display:block;color:#fff;padding:16px 0 0 9px}li.leaf ul{padding:18px 20px 18px 21px}li.leaf ul li{border-top:1px dotted #323694;padding:10px 7px}li.leaf ul li:first-child{padding-top:0;border-top:1px dotted transparent}.about-press{width:100%;height:290px;background:url(/bitrix/templates/main/img/about-pc-bg.jpg) no-repeat}.about-press .h2{font-weight:bold;font-size:120px;font-family:Arial,Helvetica,sans-serif;color:#fff;display:inline-block;line-height:90px;float:right;margin:45px 50px 0;text-align:right}.about-press .h2 span{font-size:49px;display:block;line-height:50px}.about-press a{text-decoration:underline;color:#fff;font-size:18px;font-family:"Myriad Pro";margin:45px 50px 0 0;float:right;font-family:Arial}.about-press a:hover{text-decoration:none;}@media (min-width:768px) and (max-width:991px){.about-header{padding:0;font-size:30px}.about-block1 p.volume{padding:46px 20px;font-size:35px}.about-block1 p.volume-under{padding:46px 20px;font-size:35px}.about-press{background-size:100% auto;height:210px}.about-press .h2{font-size:80px;line-height:70px;margin:35px 30px 0 0}.about-press .h2 span{font-size:44px;line-height:40px}.about-press a{font-size:16px;margin:25px 30px 0 0}.book-top{background:0 0}.book-middle{padding:0;background:0 0}.book-col{width:100%}.book-bottom{display:none}.about-block3 h1{padding-left:20px}.about-tree{left:36px}.about-block3 .leaf-year-l{left:-71px}.about-block3 .about-col-l,.about-block3 .about-col-r{float:none;margin-left:40px;width:auto}.about-block3 .leaf-arrow-l{right:auto;left:-11px;background-position:100% 0}.about-block3 .inner{padding:0 18px 138px 43px}}@media (max-width:767px){.about-header{padding:0;font-size:30px}.about-block1{background-size:165% auto;height:auto}.about-block1 p.volume-under{padding:37px 10px 43px;margin-bottom:-15px;font-size:16px;line-height:17px;text-align:center;position:relative}.about-block1 p.volume{display:none}.about-press{background-size:160%;height:150px}.about-press .h2{font-size:50px;line-height:50px;margin:25px 20px 0 0}.about-press .h2 span{font-size:24px;line-height:24px}.about-press a{font-size:12px;margin:18px 20px 0 0}.book-top{background:0 0}.book-middle{padding:0;background:0 0}.book-col{width:100%}.book-bottom{display:none}.about-block3 h1{padding:0 0 20px}.about-tree{left:36px}.about-block3 .leaf-year-l{left:-71px}.about-block3 .about-col-l,.about-block3 .about-col-r{float:none;margin-left:40px;width:auto}.about-block3 .leaf-arrow-l{right:auto;left:-11px;background-position:100% 0}.about-block3 .inner{padding:0 18px 138px 43px}
        .leaf-year span {
            display: block;
            color: #fff;
            padding: 16px 0 0 9px;
        }

    /* End */
    /* /company/about/about.min.css?15187916608129 */





</style> <section>
<div class="container">
	<div class="width">
		<div class="margin-0-xs">
			<div class="about-crm clearfix">
				<div class="row clearfix">
					<div class="col-sm-12 col-xs-12">
						<div class="text-content">
							<h1 class="title"><?=$APPLICATION->ShowTitle(false);?></h1>
						</div>
 <img src="https://www.1cbit.ru/bitrix/templates/main/img/new-about-company.png" alt="" usemap="#navigation">
						<div id="navigation" name="navigation">
						</div>
						 <!--div class="porebrik1"></div-->
						<div class="about-block1">
							<p class="volume-under">
								Мы поддерживаем ваш бизнес <span class="pink-char">24</span> часа <span class="pink-char">7</span> дней в неделю
							</p>
							<p class="volume">
								Мы поддерживаем ваш бизнес <span class="pink-char">24</span> часа <span class="pink-char">7</span> дней в неделю
							</p>
						</div>
						 <!--div class="porebrik2"></div--> <?php /* <div class="about-press">
                             <div class="h2">ПРЕСС<span>центр</span></div>
                             <div class="clearfix"></div>
                             <a href="/company/press/" target="_blank">Подробнее</a>
                      </div> */ ?>
						<div class="about-block2">
							<div class="gradient">
								<div class="gradient2">
									<div class="text-content">
										<h1 class="about-header about-history">История</h1>
										<div class="about-book">
											<div class="book-top">
											</div>
											<div class="book-middle">
												<div class="book-col book-col-l">
													<p>
														История создания компании Первый Бит уходит корнями в 1997 год.
													</p>
													<p>
														Энергичная команда специалистов по прикладной математике, физике, экономике из ведущих вузов России приняла решение - развивать бизнес на основе новейших интеллектуальных технологий.
													</p>
													<p>
														Их хорошая научная подготовка и предпринимательские способности стали основой быстрого роста компании.
													</p>
													<p>
													</p>
													<p>
														Логичным партнером для компании Первый Бит, конечно, явилась фирма «1С» - ведущий российский производитель программных продуктов экономического назначения.
													</p>
												</div>
												<div class="book-col book-col-r">
													<p>
														С момента основания компания становится франчайзи фирмы «1С», получив права на распространение продуктов 1С.
													</p>
													<p>
														Позже к продаже и сопровождению программных продуктов добавилось обучение клиентов работе с программами «1С»; БИТ является Центром Сертифицированного Обучения Фирмы 1С.
													</p>
													<p>
														В 2000 году группа специалистов, стоявшая у истоков основания компании, взяла курс на построение широкой сети региональных представительств. А позднее - на международную экспансию.
													</p>
													<p>
														Офисы компании открыты в восьми странах: Россия, Украина, Казахстан, ОАЭ, Канада, Испания, Чехия, Андорра.
													</p>
												</div>
											</div>
											<div class="book-bottom">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="about-block3">
							<div class="text-content">
								<h1 class="about-header">Хронология событий</h1>
							</div>
							<div class="about-tree">
							</div>
							<div class="inner">
								<div class="about-col about-col-l">
									<ul>
										<li class="leaf"> <span class="leaf-arrow leaf-arrow-l"></span>
										<div class="leaf-year leaf-year-l">
											2019
										</div>
										<ul>
											<li>Первый Бит открыл офис в Хабаровске</li>
											<li>Открыты офисы в Испании, Чехии и Андорре</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-l"></span>
										<div class="leaf-year leaf-year-l">
											2017
										</div>
										<ul>
											<li>Открыты филиалы в Набережных Челнах, Чебоксарах и Ульяновске.</li>
											<li>Открыты филиалы в Липецке, Саранске, Астрахани.</li>
											<li>«Первый БИТ» вывел на рынок Ближнего Востока решение FirstBIT Accounting.</li>
											<li>«Первый БИТ» запустил акселератор FirstBITLab - корпоративную экосистему запуска и поддержки стартапов. Вместе с Фондом развития интернет-инициатив (ФРИИ) открыта акселерационная программа. Первыми инвестируемыми проектами стали стартапы Let’s Taxi и DocsInBox.</li>
											<li>Веб-студия «Первого БИТа» получила компетенцию от 1С-Битрикс «Крупные корпоративные внедрения».</li>
											<li>Веб-студия «Первый БИТ» запустила новое направление БИТ.Digital, которое специализируется на интернет-маркетинге.</li>
											<li>«Первый БИТ» и AXELOT создали совместное предприятие GRADUM для работы с крупными корпоративными клиентами.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-l"></span>
										<div class="leaf-year leaf-year-l">
											2015
										</div>
										<ul>
											<li>Открылся офис «Первого БИТа» в Тольятти.</li>
											<li>К «Первому БИТу» присоединилась компания DRV в Калининграде.</li>
											<li>Компания выпустила отраслевые решения:
											<ul>
												<li>Для автоматизации основных операций складского учета – программа БИТ.ЛИС.</li>
												<li>Для клиентов коммерческих медицинских организаций – мобильное приложение БИТ.Мед.</li>
											</ul>
 </li>
											<li>Запущены сервисы «Клик.Поиск», «Сверка НДС» и таск-менеджер Bitask.</li>
											<li>«Первый БИТ» выпустил мобильное приложение для модуля «Снабжение и склад» ТОРа БИТ.СТРОИТЕЛЬСТВО.</li>
											<li>Фирма «1С» присвоила Центральному офису «Первого БИТа» статус «Центр компетенции «1С:КОРП».</li>
											<li>Сервис «Онлайн-помощник» получил статус Kaspersky Trusted (сертификат безопасности от вирусов) и внесен в глобальный белый список Лаборатории Касперского.</li>
											<li>Наши отраслевые решения БИТ.ЖКХ и БИТ.ФИНАНС подтверждены сертификатами «1С:Совместимо!».</li>
											<li>«Первый БИТ» очередной раз подтвердил статус Elite Solution Provider – наивысший партнерский статус компании QlikTech.</li>
											<li>Компания вошла в первую тройку рейтинга Центров компетенции по документообороту фирмы «1С».</li>
											<li>Популярный ресурс Superjob.ru присвоил «Первому БИТу» статус «Привлекательный работодатель-2015».</li>
											<li>Компания «1С-Битрикс» присвоила web-студии «Первый БИТ» статус «Крупнейший федеральный партнер «1С-Битрикс».</li>
											<li>«Первый БИТ» признан лидером продаж облачного сервиса и коробочного продукта «1С-Битрикс24» за 2015 год.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-l"></span>
										<div class="leaf-year leaf-year-l">
											2013
										</div>
										<ul>
											<li>К команде «Первого БИТа» присоединилась канадская компания Valmax Solutions Inc., казахстанский «IT Бизнес Центр», санкт-петербургские «Альфа-Проф» и «Экос», владимирская «Центр сопровождения Синтез», новосибирская «Поли-Софт» и ижевская «Решения для бизнеса».</li>
											<li>Открыт филиал в городе Дубае.</li>
											<li>Реализованы масштабные проекты для компаний «Технониколь», «Комбинат “КМАруда”», ОАО «Русбурмаш» и других.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 80px;"> <span class="leaf-arrow leaf-arrow-l"></span>
										<div class="leaf-year leaf-year-l">
											2011
										</div>
										<ul>
											<li>Частью «1С:Бухучет и Торговля» (БИТ) стала московская компания «Трейд Софт».</li>
											<li>В состав «1С:БИТ» вошел один из лидеров петербургского рынка «Софт-Маркет».</li>
											<li>В состав воронежского филиала «1С:БИТ» вошла компания «Москва-Сервис».</li>
											<li>«Софт-Мастер Групп» стала частью петербургского подразделения компании БИТ.</li>
											<li>Открытие филиалов в Кемерово и Астане.</li>
											<li>Открытие офиса «Первое решение» в Москве.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-l"></span>
										<div class="leaf-year leaf-year-l">
											2009
										</div>
										<ul>
											<li>Присоединение подразделения нижегородской компании «Браво софт», занимающегося автоматизацией на базе «1С».</li>
											<li>Открытие филиала в Тюмени.</li>
											<li>Открытие Центра по бюджетному учету, ставшего 11-м офисом «БИТ» в Москве.</li>
											<li>В состав «БИТ» вошел красноярский «Внедренческий центр Сарычева».</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-l"></span>
										<div class="leaf-year leaf-year-l">
											2007
										</div>
										<ul>
											<li>Открытие филиалов в Волгограде, Томске, Воронеже, Сочи, Абакане, двух филиалов<br>
											в Нижнем Новгороде.</li>
											<li>Открыт еще один филиал в Санкт-Петербурге.</li>
											<li>Открыт еще один филиал в Красноярске.</li>
											<li>Центральный офис переехал на м. Пролетарская.</li>
											<li>В состав компании БИТ вошел Центр автоматизации «Олимп».</li>
											<li>Известный разработчик отраслевых решений «ПиБи» стал частью компании БИТ.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 140px;"> <span class="leaf-arrow leaf-arrow-l" style="top: 23px;"></span>
										<div class="leaf-year leaf-year-l" style="top: 6px;">
											2003
										</div>
										<ul>
											<li>Открыт первый офис компании в Красноярске.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 120px;"> <span class="leaf-arrow leaf-arrow-l" style="top: 23px;"></span>
										<div class="leaf-year leaf-year-l" style="top: 6px;">
											2002
										</div>
										<ul>
											<li>Открылись филиалы компании БИТ в Самаре <br>
											и Казани.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 190px;"> <span class="leaf-arrow leaf-arrow-l" style="top: 33px;"></span>
										<div class="leaf-year leaf-year-l" style="top: 18px;">
											1997
										</div>
										<ul>
											<li>Образование компании «1С:Бухучет и Торговля» (БИТ) в Москве.</li>
										</ul>
 </li>
									</ul>
								</div>
								<div class="about-col about-col-r">
									<ul>
										<li class="leaf" style="margin-top: 60px;"> <span class="leaf-arrow leaf-arrow-r"></span>
										<div class="leaf-year leaf-year-r">
											2018
										</div>
										<ul>
											<li>Открыт новый офис в Тамбове</li>
											<li>Открыты дополнительные офисы в городах присутствия: Алматы, Москве, Воронеже, Ростове–на-Дону</li>
											<li>На международной выставке «Иннопром» эксперты компании «Первый БИТ» представили решение 1С с английским интерфейсом</li>
											<li>«Первый БИТ» открыл представительство в Испании</li>
											<li>«Первый БИТ» провел ребрендинг</li>
											<li>Разработаны новые продукты: Бит.Адаптер и Бит.Магазин</li>
											<li>«Первый БИТ» заключил договор о стратегическом партнёрстве с компанией «Серебряная Пуля» и веб-студией «PINKMAN»</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r"></span>
										<div class="leaf-year leaf-year-r">
											2016
										</div>
										<ul>
											<li>«Первый БИТ» открыл офисы в Балашихе, Симферополе, Королеве, Люберцах и Подольске.</li>
											<li>«Первый БИТ» попал в пятёрку лучших разработчиков интернет-магазинов по версии проекта «Рейтинг Рунета».</li>
											<li>«Первый БИТ» вступил в Ассоциацию IT-компаний «ПС СОФТ».</li>
											<li>«Первому БИТу» присвоен статус Премиум партнер компании АТОЛ.</li>
											<li>Компании «Первый БИТ» присвоен статус «1С:Центр разработки».</li>
											<li>Компания получила представительские статусы:
											<ul>
												<li>авторизованный партнер компании RRC и официальный реселлер производителей Zebra, Honeywell, Datalogic, Entrust Datacard, Citizen, Toshiba и других;</li>
												<li>официальный партнер DORS по продаже и продвижению продукции марок DORS, MAGNER и TISF;</li>
												<li>официальный дилер оборудования CAS на территории РФ и Беларуси.</li>
											</ul>
 </li>
											<li>«Первый БИТ» – крупнейший федеральный партнер «1С-Битрикс» по итогам 2016 года.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r"></span>
										<div class="leaf-year leaf-year-r">
											2014
										</div>
										<ul>
											<li>Компания вышла на рынок ИТ-услуг в Севастополе и Ярославле.</li>
											<li>К команде «Первого БИТа» присоединилась ростовская группа компаний «ЛЕММА».</li>
											<li>Проект автоматизации клинического центра ПМПГУ им. Сеченова (800 АРМ) признан «Лучшим проектом автоматизации медицинского учреждения» на конкурсе «Лучшие 10 ИТ-проектов для госсектора», учреждённого Минкомсвязи России и группой ComNews.</li>
											<li>Разработаны типовые отраслевые решения БИТ.Стоматология, БИТ.Аптека, БИТ.Складская логистика, БИТ.Автосервис, БИТ.Ломбард, БИТ.Контрактный управляющий.</li>
											<li>По итогам года компания в очередной раз возглавила рейтинг интеграторов ERP-систем, составленный аналитическим агентством TAdviser.</li>
											<li>Офис «Первого БИТа» в г. Астане (Казахстан) получил звание «ЛИДЕР ОТРАСЛИ 2014 года».</li>
											<li>Офис «Первого БИТа» в Томске вступил в Томскую торгово-промышленную палату.</li>
											<li>«Первый БИТ» получил награду за сотрудничество с компанией ESET и статус «Почетный участник клуба доверенных партнеров ESET».</li>
											<li>Web-студия «Первый БИТ» объявлена «Лидером продаж» по итогам 2014 года, войдя в число победителей премии «Золотой инфоблок» от «1С-Битрикс».</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r"></span>
										<div class="leaf-year leaf-year-r">
											2012
										</div>
										<ul>
											<li>С 1 октября 2012 года мы называемся<br>
											«Первый БИТ».</li>
											<li>Компания «Интеллектуальные технологии» г. Йошкар-Ола<br>
											вошла в состав Первого БИТа.</li>
											<li>Новороссийская компания VVT group вошла<br>
											в состав «1С:Бухучет и Торговля» (БИТ).</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r"></span>
										<div class="leaf-year leaf-year-r">
											2010
										</div>
										<ul>
											<li>В состав «БИТ» включена компания<br>
											«АВРО-БУС», ставшая 12-м московским<br>
											филиалом.</li>
											<li>Открыт офис «БИТ:ERP», занимающийся<br>
											реализацией масштабных<br>
											проектов корпоративной автоматизации<br>
											в производственной отрасли.</li>
											<li>В состав «1С:Бухучет и Торговля» (БИТ) вошла<br>
											воронежская компания «АРТ-КОМ».</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r"></span>
										<div class="leaf-year leaf-year-r">
											2008
										</div>
										<ul>
											<li>Открытие филиалов в Омске,<br>
											Оренбурге, Пензе.</li>
											<li>В результате слияния компании «БИТ»<br>
											и компании «Аванте Систем» открыт<br>
											офис в Новороссийске.</li>
											<li>Открыт еще один филиал в Новосибирске.</li>
											<li>Известный разработчик отраслевых решений<br>
											для автоматизации транспортной логистики<br>
											компания «Нова-АйТи» стала частью компании<br>
											«БИТ».</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r"></span>
										<div class="leaf-year leaf-year-r">
											2006
										</div>
										<ul>
											<li>Открыты офисы в Екатеринбурге, Краснодаре, Саратове, Иркутске, Уфе и Челябинске.</li>
											<li>«1С:Бухучет и Торговля» выходит на рынок Казахстана, открыт первый офис в Алматы.</li>
											<li>Открыты три дополнительных филиала в Красноярском крае.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r"></span>
										<div class="leaf-year leaf-year-r">
											2005
										</div>
										<ul>
											<li>Открытие филиалов в Ростове-на-Дону <br>
											и Новосибирске.</li>
											<li>Компания БИТ выходит на рынок Украины, открыт офис в Киеве </li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r" style="top: 32px;"></span>
										<div class="leaf-year leaf-year-r" style="top: 17px;">
											2000
										</div>
										<ul>
											<li>Открытие первого филиала в Санкт-Петербурге.</li>
										</ul>
 </li>
										<li class="leaf" style="margin-top: 55px;"> <span class="leaf-arrow leaf-arrow-r" style="top: 23px;"></span>
										<div class="leaf-year leaf-year-r" style="top: 8px;">
											1998
										</div>
										<ul>
											<li>Открытие второго офиса в Москве.</li>
										</ul>
 </li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <br><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
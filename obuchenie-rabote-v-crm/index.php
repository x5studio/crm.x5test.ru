<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Обучение работе в CRM для сотрудников и руководства компании. Курсы по работе в CRM-системах от специалистов компании Первый Бит. ☎ +7(495)748-33-02");
$APPLICATION->SetTitle("Обучение CRM, курсы по работе в CRM-системе");
?>

<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/promo.php');?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/education.php');?>

<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/results.php');?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/price.php');?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/faq.php');?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
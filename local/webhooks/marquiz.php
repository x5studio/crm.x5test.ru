<?
$data = json_decode(file_get_contents('php://input'), true);

file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] RAW -- '.print_r($data, true)."\n\n", FILE_APPEND);


if(!empty($data) && isset($data['answers']) && isset($data['contacts']) && isset($data['extra'])){

    $answers = $data['answers'];
    $phone = $data['contacts']['phone'];
    $email = $data['contacts']['email'];
    $name = $data['contacts']['name'];
    $createdDate = strtotime($data['created']);
    $quiz_url = $data['extra']['href'];
    $quiz_name = $data['quiz']['name'];
    $utm = $data['extra']['utm'];
    $cookies = $data['extra']['cookies'];
    $quiz = $data['quiz'];


    $comment = '';

    foreach ($answers as $key => $value) {

        $text = is_array($value['a']) ? implode(', ', $value['a']) : $value['a'];

        $comment .= $value['q']. ': '.$text.";\n";
    }


    foreach ($utm  as $key => $value) {
        $comment .= 'utm_'.$key. ': '.$value.";\n";
    }

    $calltouchUrl = 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/47955/register/';

    $calltouchID = isset($cookies['roistat_visit']) ? $cookies['roistat_visit'] : '';

    if(!$calltouchID ) {

        parse_str(parse_url($quiz_url, PHP_URL_QUERY), $output);

        if(isset($output['_ct_session_id']) && !empty($output['_ct_session_id'])) $calltouchID = (int) $output['_ct_session_id'];

    }

    $request = array(
        'subject' => 'Заявка с Marquiz ('.$quiz['name'].')',
        'requestDate' => date("d.m.Y H:i:s", $createdDate),
        'sessionId' => $calltouchID,
        'fio' => $name,
        'phoneNumber' => $phone,
        'email' => $email,
        'comment' => $comment
    );

    $fullUrl = $calltouchUrl.'?'.http_build_query($request);

    file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] SEND -- '.print_r($request, true)."\n\n", FILE_APPEND);


    if ($ch = @curl_init())
    {
        @curl_setopt($ch, CURLOPT_URL, $fullUrl);
        @curl_setopt($ch, CURLOPT_HEADER, false);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Выполнение запроса
        $dataAnswer = @curl_exec($ch);

        @curl_close($ch);
    }



    // подготовка данных для прелида

    $cityName = '';

    foreach ($answers as $key => $value) {

        if(strpos(strtolower($value['q']), strtolower('город')) !== false){
            $cityName = $value['a'];
        }

    }

    $preparedUTMs = [];

    foreach ($utm  as $key => $value) {
        $preparedUTMs['utm_'.$key] = $value;
    }

    //file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] TO_FORM -- '.print_r($formData, true)."\n\n", FILE_APPEND);
    
    
    //if(isset($_GET['TEST'])) {
        include(__DIR__.'/webform.php');
    //}
}

die();
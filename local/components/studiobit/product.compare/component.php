<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

$arResult = array();

if(!CModule::IncludeModule('iblock')){
    throw new Exception('iblock module not installed');
}

if(isset($arParams['SECTION'])) {

	$arFilter = array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'ACTIVE' => 'Y'
	);

	if (is_numeric($arParams['SECTION'])) {
		$arFilter['ID'] = $arParams['SECTION'];
	} else {
		$arFilter['CODE'] = $arParams['SECTION'];
	}

	$arSection = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME","LEFT_MARGIN","RIGHT_MARGIN","DEPTH_LEVEL"))->GetNext();
	$arResult['PRODUCT_NAME'] = "";

	if (count($arSection) > 0) {
		$arResult['PRODUCT_ROOT'] = $arSection['ID'];
		$arResult['PRODUCT_NAME'] = $arSection['NAME'];

		$arFilter3 = array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'ACTIVE' => 'Y',
			'SECTION_ID' => $arSection['ID'],
			'INCLUDE_SUBSECTIONS' => 'Y',
		);

		$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter3, false, false, array("ID", "NAME", "PROPERTY_VALUE", "IBLOCK_SECTION_ID"));

		while ($arElement = $rsElement->Fetch()) {
			$arValue[$arElement['IBLOCK_SECTION_ID']][] = array(
				"NAME" => $arElement['NAME'],
				"VALUE" => $arElement['PROPERTY_VALUE_VALUE'],
			);
		}

		$arFilter2 = array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'ACTIVE' => 'Y',
			//'SECTION_ID' => $arSection['ID'],
			'>LEFT_MARGIN' => $arSection['LEFT_MARGIN'],
			'<RIGHT_MARGIN' => $arSection['RIGHT_MARGIN'],
			'>DEPTH_LEVEL' => $arSection['DEPTH_LEVEL']
		);
		$rsSection2 = CIBlockSection::GetList(array("SORT"=>"ASC"), $arFilter2, false, array("ID", "NAME", "CODE", "IBLOCK_SECTION_ID"));
		while ($arSection2 = $rsSection2->Fetch()) {

			if ($arSection2['IBLOCK_SECTION_ID'] !== false) {
				$arSection2['ROW'] = array();
				if ($arValue[$arSection2['ID']]) {
					$arSection2['ROW'] = $arValue[$arSection2['ID']];
				}
				$arResult['SECTION'][$arSection2['IBLOCK_SECTION_ID']][] = $arSection2;
			}

		}

	}



	$this->IncludeComponentTemplate();
}




<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
$this->setFrameMode(false);
?>
<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH;?>/assets/css/compare_bitrix_editions.css">
<section class="editions_bitrix_24_wrap">
    <div class="container">
        <div class="row section_title_wrap clearfix">
            <h1>Сравнений редакций <?=$arResult['PRODUCT_NAME'];?></h1>
            <div class="trigger_version_editions clearfix">
                <?$i = 1;?>
                <?foreach ($arResult['SECTION'][$arResult['PRODUCT_ROOT']] as $arSection):?>
                <div class="single_version<?if($i == 2):?> active<?endif;?>" data-version="<?=$arSection['CODE'];?>"><span><?=$arSection['NAME'];?></span></div>
                    <?$i++;?>
                <? endforeach;?>
            </div>
        </div>
        <div class="row">
			<?$i = 1;?>
			<?foreach ($arResult['SECTION'][$arResult['PRODUCT_ROOT']] as $arSection):?>
                <div class="compare_bitrix_editions" data-version="<?=$arSection['CODE'];?>">
                    <table<?if($i == 1):?> data-tablesaw-mode="swipe"<?else:?>  class="hidden-xs hidden-sm"<?endif;?>>
                        <thead>
                        <?if ($arSection['CODE'] == "cloud"):?>
							<?
							$arSection['COL'] = count($arSection['ROW'][0]['VALUE']);
							?>
                        <tr>
                            <th class="edition_title column_1" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist"></th>
							<?foreach ($arSection['ROW'][0]['VALUE'] as $name):?>
                            <th class="edition_title column_2" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="1"><span><?=$name;?></span></th>
							<? endforeach;?>
                        </tr>
                        <tr>
                            <th class="column_1"></th>
							<?foreach ($arSection['ROW'][1]['VALUE'] as $key => $price):?>
                            <th class="column_<?=$key+2?>">
                                <div class="try_offer_block">
                                    <span class="price">
                                        <? if("бесплатно" == mb_strtolower(trim($price))):?>
                                            <?=$price?>
										<? else:?>
                                            от <?=$price?> руб.
                                        <?endif;?>
                                    </span>
                                    <span class="limit">
                                        <? if("бесплатно" == mb_strtolower(trim($price))):?>
                                            &nbsp;
										<? else:?>
                                            в месяц
										<?endif;?>
                                    </span>
									<? if("бесплатно" == mb_strtolower(trim($price))):?>
                                        <button class="btn_modal_request_crm" data-form-name="Попробовать CRM">Попробовать</button>
									<? else:?>
                                        <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
									<?endif;?>
                                </div>
                            </th>
							<? endforeach;?>
                        </tr>
						<?endif;?>
						<?if ($arSection['CODE'] == "box"):?>
						<?foreach ($arSection['ROW'] as $row):?>
                            <?
                            $loc = false;
                                if ($row['NAME'] == "Тарифы") {
                                    foreach ($row['VALUE'] as $value) {

										similar_text("Корпоративный портал", $value,$percent);
                                        if ($percent > 90) {
                                            if ($loc === false) {
												$arTariffs[] = "Корпоративный портал";
												$loc = true;
                                            }
                                        } else {
											$arTariffs[] = $value;
                                        }
                                    }
                                }
                            ?>
                            <?/*
                                <pre><?print_r($row);?></pre>
                            */?>
                            <?endforeach;?>
                        <?
							$arSection['COL'] = 3;
							?>
                            <tr>
                                <th class="edition_title column_1" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist"></th>
								<?foreach ($arTariffs as $key => $tariff):?>
                                    <th class="edition_title column_<?=$key+2?>" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="<?=$key + 1?>"><span><?=$tariff?></span></th>
								<?endforeach;?>
                            </tr>
                            <tr>
                                <th class="column_1"></th>
                                <th class="column_2">
                                    <div class="try_offer_block">
                                        <span class="price"><?=$arSection['ROW'][2]['VALUE'][0]?></span>
                                        <span class="limit"><?=$arSection['ROW'][1]['VALUE'][0]?> пользователей</span>
                                        <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                                    </div>
                                </th>
                                <th class="column_3">
                                    <div class="try_offers_block_wrap">
                                        <div class="try_offer_block">
                                            <span class="price"><?=$arSection['ROW'][2]['VALUE'][1]?></span>
                                            <span class="limit"><?=$arSection['ROW'][1]['VALUE'][1]?>  пользователей</span>
                                            <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                                        </div>
                                        <div class="try_offer_block">
                                            <span class="price"><?=$arSection['ROW'][2]['VALUE'][2]?></span>
                                            <span class="limit"><?=$arSection['ROW'][1]['VALUE'][2]?> пользователей</span>
                                            <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                                        </div>
                                        <div class="try_offer_block">
                                            <span class="price"><?=$arSection['ROW'][2]['VALUE'][3]?></span>
                                            <span class="limit"><?=$arSection['ROW'][1]['VALUE'][3]?> пользователей</span>
                                            <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                                        </div>
                                        <div class="try_offer_block">
                                            <span class="price"><?=$arSection['ROW'][2]['VALUE'][4]?></span>
                                            <span class="limit"><?=$arSection['ROW'][1]['VALUE'][4]?> пользователей</span>
                                            <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                                        </div>
                                    </div>
                                </th>
                                <th class="column_4">
                                    <div class="try_offer_block">
                                        <span class="price"><?=$arSection['ROW'][2]['VALUE'][5]?></span>
                                        <span class="limit"><?=$arSection['ROW'][1]['VALUE'][5]?> пользователей</span>
                                        <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                                    </div>
                                </th>
                            </tr>
                        <?endif;?>
                        </thead>
                        <tbody>
						<?foreach ($arResult['SECTION'][$arSection['ID']] as $arSection2):?>
                            <tr class="for_chapter_title">
                                <td class="column_1"><span class="chapter_title_grey"><?=$arSection2['NAME']?></span></td>
                                <?for($i = 2; $i <= ($arSection['COL'] + 1); $i++):?>
                                <td class="column_<?=$i;?>"></td>
                                <?endfor;?>
                            </tr>
							<?foreach ($arSection2['ROW'] as $row):?>
                                <tr>
                                    <td class="column_1"><span><?=$row['NAME']?></span></td>
									<?foreach ($row['VALUE'] as $key => $name):?>
                                        <td class="column_<?=$key + 2; ?>">
											<? if("нет" == mb_strtolower(trim($name))):?>
                                                <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
											<?elseif("да" == mb_strtolower(trim($name))):?>
                                                <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
											<?elseif("неограничено" == mb_strtolower(trim($name)) || "неограниченно" == mb_strtolower(trim($name))):?>
                                                <span class="status_infinite">&infin;</span>
											<?else:?>
												<?=$name;?>
											<?endif;?>
                                        </td>
									<? endforeach;?>
                                </tr>
							<? endforeach;?>
							<?foreach ($arResult['SECTION'][$arSection2['ID']] as $arSection3):?>
                                <tr class="for_chapter_title">
                                    <td class="column_1"><span class="chapter_title_grey"><?=$arSection3['NAME']?></span></td>
									<?for($i = 2; $i <= ($arSection['COL'] + 1); $i++):?>
                                        <td class="column_<?=$i;?>"></td>
									<?endfor;?>
                                </tr>
								<?foreach ($arSection3['ROW'] as $row):?>
                                    <tr>
                                        <td class="column_1"><span><?=$row['NAME']?></span></td>
									    <?foreach ($row['VALUE'] as $key => $name):?>
                                            <td class="column_<?=$key + 2; ?>">
                                                <? if("нет" == mb_strtolower(trim($name))):?>
                                                <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                <?elseif("да" == mb_strtolower(trim($name))):?>
                                                    <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
												<?elseif("неограничено" == mb_strtolower(trim($name)) || "неограниченно" == mb_strtolower(trim($name))):?>
                                                    <span class="status_infinite">&infin;</span>
												<?else:?>
                                                <?=$name;?>
                                                <?endif;?>
                                            </td>
										<? endforeach;?>
                                    </tr>
								<? endforeach;?>
							<? endforeach;?>
						<? endforeach;?>
                        </tbody>
                    </table>
                </div>
				<?$i++;?>
			<? endforeach;?>


<? /*?>



            <div class="compare_bitrix_editions"  data-version="box">
                <table class="hidden-xs hidden-sm">
                    <thead>
                    <tr>
                        <th class="edition_title column_1" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist"></th>
                        <th class="edition_title column_2" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="1"><span>CRM</span></th>
                        <th class="edition_title column_3" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2"><span>Корпоративный портал</span></th>
                        <th class="edition_title column_4" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3"><span>Энтерпрайз</span></th>
                    </tr>
                    <tr>
                        <th class="column_1"></th>
                        <th class="column_2">
                            <div class="try_offer_block">
                                <span class="price">59 000 руб.</span>
                                <span class="limit">12 пользователей</span>
                                <button class="btn_modal_request_crm">Попробовать</button>
                            </div>
                        </th>
                        <th class="column_3">
                            <div class="try_offers_block_wrap">
                                <div class="try_offer_block">
                                    <span class="price">139 000 руб.</span>
                                    <span class="limit">50 пользователей</span>
                                    <button class="btn_modal_request_crm">Попробовать</button>
                                </div>
                                <div class="try_offer_block">
                                    <span class="price">199 000 руб.</span>
                                    <span class="limit">100 пользователей</span>
                                    <button class="btn_modal_request_crm">Попробовать</button>
                                </div>
                                <div class="try_offer_block">
                                    <span class="price">299 000 руб.</span>
                                    <span class="limit">500 пользователей</span>
                                    <button class="btn_modal_request_crm">Попробовать</button>
                                </div>
                                <div class="try_offer_block">
                                    <span class="price">399 000 руб.</span>
                                    <span class="limit">500 пользователей</span>
                                    <button class="btn_modal_request_crm">Попробовать</button>
                                </div>
                            </div>

                        </th>
                        <th class="column_4">
                            <div class="try_offer_block">
                                <span class="price">699 000 руб.</span>
                                <span class="limit">1000 пользователей</span>
                                <button class="btn_modal_request_crm">Попробовать</button>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="for_chapter_title">
                        <td class="column_1"><span class="chapter_title_grey">Основные</span></td>
                        <td class="column_2"></td>
                        <td class="column_3"></td>
                        <td class="column_4"></td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Контроль задач и сроков</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Дисковое пространство</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Бизнес-чат</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Обмен документами</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Гибкая настройка прав сотрудников</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Отчеты</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_4">
                            <span class="status_infinite">&infin;</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Рабочий стол</span></td>
                        <td class="column_2">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr class="for_chapter_title">
                        <td class="column_1"><span class="chapter_title_grey">Дополнительные</span></td>
                        <td class="column_2"></td>
                        <td class="column_3"></td>
                        <td class="column_4"></td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Количество сотрудников</span></td>
                        <td class="column_2">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Количество клиентов</span></td>
                        <td class="column_2">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_4">
                            <span class="status_infinite">&infin;</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Воронка продаж</span></td>
                        <td class="column_2">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <table data-tablesaw-mode="swipe" class="visible-xs visible-sm">
                    <thead>
                    <tr>
                        <th class="edition_title column_1" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist"></th>
                        <th class="edition_title column_2" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="1"><span>CRM</span></th>
                        <th class="edition_title column_3" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="2"><span>Корпоративный портал</span></th>
                        <th class="edition_title column_3" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3"><span>Корпоративный портал</span></th>
                        <th class="edition_title column_3" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="4"><span>Корпоративный портал</span></th>
                        <th class="edition_title column_3" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="5"><span>Корпоративный портал</span></th>
                        <th class="edition_title column_4" scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="6"><span>Энтерпрайз</span></th>
                    </tr>
                    <tr>
                        <th class="column_1"></th>

                        <th class="column_2">
                            <div class="try_offer_block">
                                <span class="price">59 000 руб.</span>
                                <span class="limit">12 пользователей</span>
                                <button class="btn_modal_request_crm">Попробовать</button>
                            </div>
                        </th>

                        <th class="column_3">
                            <div class="try_offer_block">
                                <span class="price">139 000 руб.</span>
                                <span class="limit">50 пользователей</span>
                                <button class="btn_modal_request_crm">Попробовать</button>
                            </div>
                        </th>

                        <th class="column_3">
                            <div class="try_offer_block">
                                <span class="price">199 000 руб.</span>
                                <span class="limit">100 пользователей</span>
                                <button class="btn_modal_request_crm">Попробовать</button>
                            </div>
                        </th>

                        <th class="column_3">
                            <div class="try_offer_block">
                                <span class="price">299 000 руб.</span>
                                <span class="limit">500 пользователей</span>
                                <button class="btn_modal_request_crm">Попробовать</button>
                            </div>
                        </th>

                        <th class="column_3">
                            <div class="try_offer_block">
                                <span class="price">399 000 руб.</span>
                                <span class="limit">500 пользователей</span>
                                <button class="btn_modal_request_crm">Попробовать</button>
                            </div>
                        </th>

                        <th class="column_4">
                            <div class="try_offer_block">
                                <span class="price">699 000 руб.</span>
                                <span class="limit">1000 пользователей</span>
                                <button class="btn_modal_request_crm">Попробовать</button>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="for_chapter_title">
                        <td class="column_1"><span class="chapter_title_grey">Основные</span></td>
                        <td class="column_2"></td>
                        <td class="column_3"></td>
                        <td class="column_3"></td>
                        <td class="column_3"></td>
                        <td class="column_3"></td>
                        <td class="column_4"></td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Контроль задач и сроков</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Дисковое пространство</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Бизнес-чат</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Обмен документами</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Гибкая настройка прав сотрудников</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Отчеты</span></td>
                        <td class="column_2">
                            <span class="status_disabled"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_4">
                            <span class="status_infinite">&infin;</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Рабочий стол</span></td>
                        <td class="column_2">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr class="for_chapter_title">
                        <td class="column_1"><span class="chapter_title_grey">Дополнительные</span></td>
                        <td class="column_2"></td>
                        <td class="column_3"></td>
                        <td class="column_3"></td>
                        <td class="column_3"></td>
                        <td class="column_3"></td>
                        <td class="column_4"></td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Количество сотрудников</span></td>
                        <td class="column_2">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Количество клиентов</span></td>
                        <td class="column_2">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_3">
                            <span class="status_infinite">&infin;</span>
                        </td>
                        <td class="column_4">
                            <span class="status_infinite">&infin;</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="column_1"><span>Воронка продаж</span></td>
                        <td class="column_2">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_3">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                        <td class="column_4">
                            <span class="status_included"><i class="fa fa-check" aria-hidden="true"></i></span>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
			<?*/ ?>


        </div>
    </div>

</section>
$(document).ready( function(){
    require(['tablesawJquery'], function () {
        Tablesaw.init();
    });

    setTimeout(function() { $('.compare_bitrix_editions[data-version="cloud"]').addClass('hidden'); }, 1000);

    $('body').on('click', '.single_version:not(".active")', function(){
        $('.compare_bitrix_editions').addClass('hidden');
        $('.compare_bitrix_editions[data-version="' + $(this).attr('data-version') + '"]' ).removeClass('hidden');
        $('.single_version').removeClass('active');

        // $('.compare_bitrix_editions[data-version="' + $(this).attr('data-version') + '"] table[data-tablesaw-mode="swipe"]').trigger( "enhance.tablesaw" );
        // $('table[data-tablesaw-mode="swipe"]').tablesaw().data( "tablesaw" ).refresh();
        $(this).addClass('active');

        setTimeout(function() { $('table[data-tablesaw-mode="swipe"]').tablesaw().data( "tablesaw" ).refresh(); }, 10);

    });
});
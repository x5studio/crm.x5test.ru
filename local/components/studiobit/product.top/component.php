<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

$arResult = array();
$arResult['TEXT'] = "";

if(!CModule::IncludeModule('iblock')){
    throw new Exception('iblock module not installed');
}

    if(isset($arParams['SECTION'])){

        $filter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],
                        'ACTIVE' => 'Y');

        if(is_numeric($arParams['SECTION'])){
            $filter['ID'] = $arParams['SECTION'];
        }
        else {
            $filter['CODE'] = $arParams['SECTION'];
        }

        $arSection = CIBlockSection::GetList(array(), $filter, false, array("ID","NAME","PICTURE","DESCRIPTION", "UF_DESCRIPTION", "UF_ICO"))->GetNext();

        $arResult['NAME'] = $arSection['NAME'];
        $arResult['TEXT'] = $arSection['DESCRIPTION'];
		$arResult['DESCRIPTION'] = $arSection['UF_DESCRIPTION'];

        if(isset($arSection["PICTURE"])) {
            $arResult["PICTURE"] = (0 < $arSection["PICTURE"] ? CFile::GetFileArray($arSection["PICTURE"]) : false);
            if ($arResult["PICTURE"]) {
                $arResult["PICTURE"]["ALT"] = $arResult["NAME"];
                $arResult["PICTURE"]["TITLE"] = $arResult["NAME"];
            }
        }

		if(isset($arSection["UF_ICO"])) {
			$arResult["ICON"] = (0 < $arSection["UF_ICO"] ? CFile::GetFileArray($arSection["UF_ICO"]) : false);
			if ($arResult["ICON"]) {
				$arResult["ICON"]["ALT"] = $arResult["NAME"];
				$arResult["ICON"]["TITLE"] = $arResult["NAME"];
			}
		}

        $this->IncludeComponentTemplate();
    }
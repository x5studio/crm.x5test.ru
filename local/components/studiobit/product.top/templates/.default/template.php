<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
$this->setFrameMode(false);
?>
<? if (isset($arResult["DETAIL_PICTURE"]["SRC"])):?>
    <div><img
        border="0"
        src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
        alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
        title="<?=$arResult["PREVIEW_PICTURE"]["TITLE"]?>"
        align="center"
        />
        </div>
<?endif;?>
<?=$arResult['TEXT'];?>
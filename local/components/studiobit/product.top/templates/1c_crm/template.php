<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
$this->setFrameMode(false);
?>
<div class="wrap_products_2bg">
    <section class="product_intro">

        <div class="container visible-xs">
            <h2 class="title">1С:CRM</h2>
            <p class="subtitle">Организуйте эффективную работу отделов продаж, маркетинга и сервисного обслуживания на всех этапах взаимодействия с клиентом</p>
        </div>

        <div class="wrap_product_img">
            <img src="<?=$arResult["PICTURE"]["SRC"]?>" alt="" class="product_screen">
            <img src="<?=$arResult["ICON"]["SRC"]?>" alt="" style="top: -10px" class="product_logo hidden-xs">
        </div>

        <div class="container clearfix">
            <div class="super-row">
                <div class="col-sm-7 about_product">
                    <h1 class="title hidden-xs"><?=$arResult['NAME'];?></h1>
                    <p class="subtitle hidden-xs"><?=$arResult['DESCRIPTION'];?></p>
                    <p class="product_desc"><?=$arResult['TEXT'];?></p>
                    <div class="wrap_for_btn">
                        <button class="btn_modal_request_crm" data-form-name="Заказать консультацию">ЗАЯВКА НА КОНСУЛЬТАЦИЮ</button>
                        <button class="btn_modal_request_crm transparent-btn pink-color" data-form-name="Заказать демонстрацию">ЗАКАЗАТЬ ДЕМОНСТРАЦИЮ</button>
                    </div>
                </div>
            </div>
        </div>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			array(
				"AREA_FILE_SHOW"      => "file",
				"PATH"                => SITE_DIR."include/product/1c-crm/features.php",
				"AREA_FILE_RECURSIVE" => "N",
				"EDIT_MODE"           => "html",
			),
			false,
			array('HIDE_ICONS' => 'N')
		);?>

    </section>
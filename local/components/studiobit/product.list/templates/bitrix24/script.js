$(window).load(function(){
    require(['tablesawJquery'], function () {
        Tablesaw.init();
    });
});
$(document).ready( function(){


    $('body').on('click', '.single_version:not(".active")', function(){
        $('.editions-wrap').addClass('hidden');
        $('.editions-wrap[data-version="' + $(this).attr('data-version') + '"]' ).removeClass('hidden');
        $('.single_version').removeClass('active');

        $(this).addClass('active');

    });
});
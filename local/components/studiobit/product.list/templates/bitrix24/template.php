<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
$this->setFrameMode(false);
?>
<section class="prices_bitrix24">
    <div class="container">

        <div class="row">
            <h2 class="text-left pull-left">Цены на Битрикс24 </h2>
            <a href="/products/bitriks-24/compare/" class="compare_editions hidden-xs">Сравнить редакции</a>
        </div>

        <div class="trigger_version_editions clearfix">
            <div class="single_version  active" data-version="cloud"><span>Облачная версия</span></div>
            <div class="single_version" data-version="box"><span>Коробочная версия</span></div>
        </div>

        <div class="row clearfix editions-wrap" data-version="cloud">

            <?foreach ($arResult['SECTION'][0]['TARIF'] as $arTarif):?>

            <div class="col-sm-6 col-md-3 col-xs-12">
                <a href="#" class="single-edition">
                    <div class="edition-content">
                        <span class="title"><?=$arTarif[0];?></span>
                        <table class="list-items">
							<?foreach ($arTarif as $key => $value):?>
								<?if ($key > 1 && trim($value) != ""):?>
                                    <tr>
                                        <td class="wrap_for_ico">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td><span><?=$value;?></span></td>
                                    </tr>
								<?endif;?>
							<?endforeach;?>
                        </table>
                        <span class="price"><?=$arTarif[1];?><?if ($arTarif[1] != "Бесплатно"):?> руб.<?endif;?></span>
                        <span class="for_single_user"></span>

                    </div>
                    <?if ($arTarif[1] == "Бесплатно"):?>
                    <button class="btn_modal_request_crm" data-form-name="Купить CRM">Попробовать</button>
                    <?else:?>
                        <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                    <?endif;?>
                </a>
            </div>
            <?endforeach;?>

        </div>

        <div class="row clearfix editions-wrap hidden" data-version="box">

            <?

			// TODO переделать!!!
           /* $arTarif = $arResult['SECTION'][1]['TARIF'][0];
            ?>
            <div class="col-sm-6 col-md-3 col-xs-12">
                    <a href="#" class="single-edition">
                        <div class="edition-content">
                            <span class="title"><?=$arTarif[0];?></span>
                            <table class="list-items">
								<?foreach ($arTarif as $key => $value):?>
									<?if ($key > 2 && trim($value) != ""):?>
                                        <tr>
                                            <td class="wrap_for_ico">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </td>
                                            <td><span><?=$value;?></span></td>
                                        </tr>
									<?endif;?>
								<?endforeach;?>
                            </table>
                            <span class="price"><?=$arTarif[2];?></span>
                            <span class="for_single_user"><?=$arTarif[1];?> пользователей</span>
                        </div>
                            <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                    </a>
            </div>*/?>
			<div class="col-sm-6 col-md-3 col-xs-12">
				<a href="#" class="single-edition">
					<div class="edition-content">
						<span class="title">Интернет-магазин + CRM</span>
						<table class="list-items">
							<tr>
								<td class="wrap_for_ico">
									<i class="fa fa-check" aria-hidden="true"></i>
								</td>
								<td><span>Экстранет</span></td>
							</tr>
						<?/*	<tr>
								<td class="wrap_for_ico">
									<i class="fa fa-check" aria-hidden="true"></i>
								</td>
								<td><span>Многодепартаментность</span></td>
							</tr>
							<tr>
								<td class="wrap_for_ico">
									<i class="fa fa-check" aria-hidden="true"></i>
								</td>
								<td><span>Веб-кластер</span></td>
							</tr> */?>
							<tr>
								<td class="wrap_for_ico">
									<i class="fa fa-check" aria-hidden="true"></i>
								</td>
								<td><span>eCommerce-платформа</span></td>
							</tr>
						</table>
						<span class="price">99 000 руб.</span>
						<span class="for_single_user">12 пользователей</span>
					</div>
					<button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
				</a>
			</div>
			<?
			$arTarif = $arResult['SECTION'][1]['TARIF'];
			?>
            <div class="col-sm-6 col-md-3 col-xs-12">
                <a href="#" class="single-edition">
                    <div class="edition-content">
                        <span class="title">Корпоративный портал</span>
                        <table class="list-items">
							<?foreach ($arTarif[1] as $key => $value):?>
								<?if ($key > 2 && trim($value) != ""):?>
                                    <tr>
                                        <td class="wrap_for_ico">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td><span><?=$value;?></span></td>
                                    </tr>
								<?endif;?>
							<?endforeach;?>
                        </table>
                        <span class="price">от <?=$arTarif[1][2];?> до <?=$arTarif[4][2];?></span>
                        <span class="for_single_user">от <?=$arTarif[1][1];?> до <?=$arTarif[4][1];?> пользователей</span>
                    </div>
                    <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                </a>
            </div>
			<?
			$arTarif = $arResult['SECTION'][1]['TARIF'][5];
			?>
            <div class="col-sm-6 col-md-3 col-xs-12">
                <a href="#" class="single-edition">
                    <div class="edition-content">
                        <span class="title"><?=$arTarif[0];?></span>
                        <table class="list-items">
							<?foreach ($arTarif as $key => $value):?>
								<?if ($key > 2 && trim($value) != ""):?>
                                    <tr>
                                        <td class="wrap_for_ico">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td><span><?=$value;?></span></td>
                                    </tr>
								<?endif;?>
							<?endforeach;?>
                        </table>
                        <span class="price"><?=$arTarif[2];?></span>
                        <span class="for_single_user"><?=$arTarif[1];?> пользователей</span>
                    </div>
                    <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                </a>
            </div>
        </div>

    </div>
</section>
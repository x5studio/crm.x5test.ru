<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
$this->setFrameMode(false);
?>
<section class="prices_bitrix24">
    <div class="container">

        <div class="row">
            <h2 class="text-left pull-left">Цены на Мегаплан </h2>
            <a href="/products/megaplan/compare/" class="compare_editions hidden-xs">Сравнить редакции</a>
        </div>

        <div class="trigger_version_editions clearfix">
            <div class="single_version  active" data-version="cloud"><span>Облачная версия</span></div>
            <div class="single_version" data-version="box"><span>Коробочная версия</span></div>
        </div>

        <div class="row clearfix editions-wrap" data-version="cloud">

            <?foreach ($arResult['SECTION'][0]['TARIF'] as $arTarif):?>

            <div class="col-sm-6 col-md-3 col-xs-12">
                <a href="#" class="single-edition">
                    <div class="edition-content">
                        <span class="title"><?=$arTarif[0];?></span>
                        <table class="list-items">
							<?foreach ($arTarif as $key => $value):?>
							<?if ($key > 1 && trim($value) != ""):?>
                                <tr>
                                    <td class="wrap_for_ico">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </td>
                                    <td><span><?=$value;?></span></td>
                                </tr>
							<?endif;?>

							<?endforeach;?>
                        </table>
                        <span class="price"><?=$arTarif[1];?> руб.</span>
                        <span class="for_single_user">За лицензию в месяц</span>
                    </div>


                        <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                </a>
            </div>
            <?endforeach;?>

        </div>

        <div class="row clearfix editions-wrap hidden" data-version="box">

			<?foreach ($arResult['SECTION'][1]['TARIF'] as $key => $arTarif):?>

                <div class="col-sm-6 col-md-3 col-xs-12">
                    <a href="#" class="single-edition">
                        <div class="edition-content">
                            <span class="title"><?=$arTarif[0];?></span>
                            <table class="list-items">
								<?foreach ($arTarif as $key => $value):?>
								<?if ($key > 1 && trim($value) != ""):?>
                                    <tr>
                                        <td class="wrap_for_ico">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td><span><?=$value;?></span></td>
                                    </tr>
								<?endif;?>
								<?endforeach;?>
                            </table>
                            <span class="price"><?=$arTarif[1];?> руб.</span>
                            <span class="for_single_user">За 10 лицензий</span>
                        </div>
                            <button class="btn_modal_request_crm" data-form-name="Купить CRM">Купить</button>
                    </a>
                </div>

			<?endforeach;?>

        </div>

    </div>
</section>
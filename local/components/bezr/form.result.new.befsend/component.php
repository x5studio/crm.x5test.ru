<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \PB\Main\Loc;
use Bitrix\Main\Config\Option;

//предустановленные параметры для БитЛид
if (!empty($arParams['BITLEAD_OFFICE'])) {
    $arParams['BITLEAD_OFFICE'] = trim($arParams['BITLEAD_OFFICE']);
}elseif(defined('BITLEAD_OFFICE') ) {
    $arParams['BITLEAD_OFFICE'] = BITLEAD_OFFICE;
}
if (!empty($arParams['BITLEAD_COMPETENCE'])) {
    $arParams['BITLEAD_COMPETENCE'] = trim($arParams['BITLEAD_COMPETENCE']);
}elseif(defined('BITLEAD_COMPETENCE') ) {
    $arParams['BITLEAD_COMPETENCE'] = BITLEAD_COMPETENCE;
}
if (!empty($arParams['BITLEAD_SOURCE'])) {
    $arParams['BITLEAD_SOURCE'] = trim($arParams['BITLEAD_SOURCE']);
}elseif(defined('BITLEAD_SOURCE') ) {
    $arParams['BITLEAD_SOURCE'] = BITLEAD_SOURCE;
}

/**
 * reCaptcha
 * 0 - выключена
 * v2 - v2
 * v3 - v3
 * */
$reCaptchaVersion = Option::get('pb.main', 'RECAPTCHA');

$siteKey = Option::get('pb.main', 'SITE_KEY');
$secretKeyFromModule = Option::get('pb.main', 'SECRET_KEY');


$arParams['USE_RECATPCHA'] = false;

// на всех проектах установлены в dbconn.php
if(defined('G_RECAPTCHA_SITE_KEY') && defined('G_RECAPTCHA_SECRET_KEY')) {
	$arParams['USE_RECATPCHA'] = true;
	$secretKey = G_RECAPTCHA_SECRET_KEY;
}

// из настроек модуля pb.main
if (!empty($secretKeyFromModule)) {
	$secretKey = $secretKeyFromModule;
}

if ($reCaptchaVersion !== '0'
	&& strlen($siteKey) > 0
	&& strlen($secretKey) > 0) {
	$arParams['USE_RECATPCHA'] = true;
}

if (!isset($arParams['NEED_SCROLL_OFFICE'])) {
    if (defined('FORM_NEED_SCROLL_OFFICE')) {
        $arParams['NEED_SCROLL_OFFICE'] = FORM_NEED_SCROLL_OFFICE;
    } else {
        $arParams['NEED_SCROLL_OFFICE'] = true;
    }
}

if (!empty($arParams['PROPERTY_OFFICE_EMAIL'])) {
    $arParams['PROPERTY_OFFICE_EMAIL'] = trim($arParams['PROPERTY_OFFICE_EMAIL']);
} elseif (defined('DEFAULT_FORM_OFFICE_EMAIL') && is_string(DEFAULT_FORM_OFFICE_EMAIL)) {
    $arParams['PROPERTY_OFFICE_EMAIL'] = DEFAULT_FORM_OFFICE_EMAIL;
} else {
    $arParams['PROPERTY_OFFICE_EMAIL'] = "email";
}

$arParams['USE_ANTISPAM'] = false;
if(defined('USE_ANTISPAM') ) {
    $arParams['USE_ANTISPAM'] = true;
}

if (!is_array($arParams['IGNORE_FIELDS'])) {
    $arParams['IGNORE_FIELDS'] = Array();
}

if(!isset($arParams["HIDE_OFFICE_TEXT_AFTER_SELECT"])){
    $arParams["HIDE_OFFICE_TEXT_AFTER_SELECT"] = "N";
    $arParams["~HIDE_OFFICE_TEXT_AFTER_SELECT"] = "N";
}
else {
    $arParams["~HIDE_OFFICE_TEXT_AFTER_SELECT"] = $arParams["HIDE_OFFICE_TEXT_AFTER_SELECT"];
}

if(!isset($arParams['HIDE_PRIVACY_POLICE'])){
    if($policyRegion = COption::GetOptionString('pb.main', 'form_policy_country', '')) {
        $policyRegion = explode(',', $policyRegion);

        $arParams['HIDE_PRIVACY_POLICE'] = (!in_array($_SESSION['REGION']['COUNTRY_CODE'], $policyRegion)) ? 'Y' : 'N';
    }else
        $arParams['HIDE_PRIVACY_POLICE'] = 'Y';
}

$arParams["FANCYBOX_FALSE"] = true;
if (COption::GetOptionString('pb.main', 'thanks-page', '')){
    $arParams["SUCCESS_URL"] = COption::GetOptionString('pb.main', 'thanks-page', '');
    $arParams["FANCYBOX_FALSE"] = false;
}

$arParams["CITY_QUESTION_CODE"] = $arParams["CITY_QUESTION_CODE"] ? : 'CITY';
$arParams["OFFICE_QUESTION_CODE"] = $arParams["OFFICE_QUESTION_CODE"] ? : 'OFFICE';
$arParams["PRODUCT_QUESTION_CODE"] = $arParams["PRODUCT_QUESTION_CODE"] ? : 'PRODUCT';
$arParams["TO_QUESTION_CODE"] = $arParams["TO_QUESTION_CODE"] ? : 'TO';

if ($_REQUEST["WEB_FORM_ID"] && !$_REQUEST["web_form_submit"]) $_REQUEST["web_form_submit"] = "submit";
if (CModule::IncludeModule("form")) {
    $GLOBALS['strError'] = '';

    $arDefaultComponentParameters = array(
        "WEB_FORM_ID" => $_REQUEST["WEB_FORM_ID"],
        "SEF_MODE" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "CACHE_TIME" => "3600",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",

    );

    $arParams["FORM_RESULT_ADDOK"] = !empty($arParams["FORM_RESULT_ADDOK"]) ? $arParams["FORM_RESULT_ADDOK"] : "ADDOK";

    $arResult["SOULT_FORM"] = $arParams['WEB_FORM_ID'] . $arParams["COMPONENT_MARKER"];

    foreach ($arDefaultComponentParameters as $key => $value) {
        if (!is_set($arParams, $key)) {
            $arParams[$key] = $value;
        }
    }

    $arDefaultUrl = array(
        'LIST' => $arParams["SEF_MODE"] == "Y" ? "list/" : "result_list.php",
        'EDIT' => $arParams["SEF_MODE"] == "Y" ? "edit/#RESULT_ID#/" : "result_edit.php"
    );

    foreach ($arDefaultUrl as $action => $url) {
        if (!is_set($arParams, $action . '_URL')) {
            if (!is_set($arParams, 'SHOW_' . $action . '_PAGE') || $arParams['SHOW_' . $action . '_PAGE'] == 'Y')
                $arParams[$action . '_URL'] = $url;
        }
    }


    if (isset($arParams['RESULT_ID'])) {
        unset($arParams['RESULT_ID']);
    }

    //  insert chain item
    if (strlen($arParams["CHAIN_ITEM_TEXT"]) > 0) {
        $APPLICATION->AddChainItem($arParams["CHAIN_ITEM_TEXT"], $arParams["CHAIN_ITEM_LINK"]);
    }

    // check whether cache using needed
    $bCache = !(
            $_SERVER["REQUEST_METHOD"] == "POST"
            &&
            (
                !empty($_REQUEST["web_form_submit"])
                ||
                !empty($_REQUEST["web_form_apply"])
            )
            ||
            $_REQUEST['formresult'] == $arParams["FORM_RESULT_ADDOK"]
        )
        &&
        !(
            $arParams["CACHE_TYPE"] == "N"
            ||
            (
                $arParams["CACHE_TYPE"] == "A"
                &&
                COption::GetOptionString("main", "component_cache_on", "Y") == "N"
            )
            ||
            (
                $arParams["CACHE_TYPE"] == "Y"
                &&
                intval($arParams["CACHE_TIME"]) <= 0
            )
        );

    // проверяем настройки еспутника для вывода чекбокса подтерждения подписки в формах
    if (IsModuleInstalled("pb.esputnik") && Option::get('pb.esputnik', 'ESPUTNIK_CHECKBOX_IN_FORMS') == 'Y'){
        // получаем массив исключенных форм из настроек модуля
        $disabledOnForms = Option::get('pb.esputnik', 'ESPUTNIK_ENABLED_ON_FORMS');
        $disabledOnForms = array_filter(explode( ',', $disabledOnForms));
        $forceForms = Option::get('pb.esputnik', 'ESPUTNIK_FORCE_SUBSCRIBE');
        $forceForms = array_filter(explode( ',', $forceForms));
        $subscribeCheckText = Option::get('pb.esputnik', 'ESPUTNIK_TEXT_SUBSCRIBE');
        $arParams["POPUP_TEXT"] = Option::get('pb.esputnik', 'ESPUTNIK_POPUP_TEXT');
        if (!in_array($arParams["WEB_FORM_ID"], $disabledOnForms) && !in_array($arParams["WEB_FORM_ID"], $forceForms)) {
            $arParams['ESPUTNIK_SUBSCRIBE_ON'] = "Y";
            $arParams['ESPUTNIK_TEXT_SUBSCRIBE'] = $subscribeCheckText;
        }
    }

    // start caching
    if ($bCache) {
        // append arParams to cache ID;
        $arCacheParams = array();
        foreach ($arParams as $key => $value) if (substr($key, 0, 1) != "~") $arCacheParams[$key] = $value;
        // create CPHPCache class instance
        $obFormCache = new CPHPCache;
        // create cache ID and path
        $CACHE_ID = SITE_ID . "|" . $componentName . "|" . md5(serialize($arCacheParams)) . "|" . $USER->GetGroups();
        $CACHE_PATH = "/" . SITE_ID . CComponentEngine::MakeComponentPath($componentName);
    }

    // initialize cache
    if ($bCache && $obFormCache->InitCache($arParams["CACHE_TIME"], $CACHE_ID, $CACHE_PATH)) {
        // if cache already exists - get vars
        $arCacheVars = $obFormCache->GetVars();
        $bVarsFromCache = true;

        $arResult = $arCacheVars["arResult"];

        if ($arParams["IGNORE_CUSTOM_TEMPLATE"] == "N" && $arResult["arForm"]["USE_DEFAULT_TEMPLATE"] == "N" && strlen($arResult["arForm"]["FORM_TEMPLATE"]) > 0) {
            $FORM = $arCacheVars["FORM"];
            if (!$FORM) $bVarsFromCache = false;
        }
        $arResult['FORM_NOTE'] = '';
        $arResult['isFormNote'] = 'N';
    } else {
        /*************************************************************************************************/
        $bVarsFromCache = false;

        $arResult["bSimple"] = COption::GetOptionString("form", "SIMPLE", "Y") == "N" ? "N" : "Y";
        $arResult["bAdmin"] = defined("ADMIN_SECTION") && ADMIN_SECTION === true ? "Y" : "N";

        // if form taken from admin interface - check rights to form module
        if ($arResult["bAdmin"] == "Y") {
            $FORM_RIGHT = $APPLICATION->GetGroupRight("form");
            if ($FORM_RIGHT <= "D") $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
        }

        if (intval($arParams['WEB_FORM_ID']) <= 0 && strlen($arParams['WEB_FORM_ID']) > 0) {
            $obForm = CForm::GetBySID($arParams['WEB_FORM_ID']);
            if ($arForm = $obForm->Fetch()) {
                $arParams['WEB_FORM_ID'] = $arForm['ID'];
            }
        }


        // check WEB_FORM_ID and get web form data
        $arParams["WEB_FORM_ID"] = CForm::GetDataByID($arParams["WEB_FORM_ID"], $arResult["arForm"], $arResult["arQuestions"], $arResult["arAnswers"], $arResult["arDropDown"], $arResult["arMultiSelect"], $arResult["bAdmin"] == "Y" || $arParams["SHOW_ADDITIONAL"] == "Y" || $arParams["EDIT_ADDITIONAL"] == "Y" ? "ALL" : "N");

        $arResult["WEB_FORM_NAME"] = $arResult["arForm"]["SID"];

        // if wrong WEB_FORM_ID return error;
        if ($arParams["WEB_FORM_ID"] > 0) {
            // check web form rights;
            $arResult["F_RIGHT"] = intval(CForm::GetPermission($arParams["WEB_FORM_ID"]));

            // in no form access - return error
            if ($arResult["F_RIGHT"] < 10) {
                $arResult["ERROR"] = "FORM_ACCESS_DENIED";
            }

            // ================= A N T I S P A M =========================
            include 'antispam.php';
            // ===========================================================

        } else {
            $arResult["ERROR"] = "FORM_NOT_FOUND";
        }

    }

    if (strlen($arResult["ERROR"]) <= 0) {
        // ************************************************************* //
        //                                             get/post processing                                             //
        // ************************************************************* //

        $arResult["arrVALUES"] = array();

        if (
        	(empty($arParams['COMPONENT_MARKER'])
				||
				$_POST['COMPONENT_MARKER'] == $arParams['COMPONENT_MARKER'])
			&& ($_POST['WEB_FORM_ID'] == $arParams['WEB_FORM_ID']
				|| $_POST['WEB_FORM_ID'] == $arResult['arForm']['SID'])
			&& (strlen($_REQUEST["web_form_submit"]) > 0
				|| strlen($_REQUEST["web_form_apply"]) > 0)
		) {
            if ($GLOBALS['IS_APPEND_ACTION_PAGE']) {
                $_POST['form_text_72'] .= ' - Акция Битрикс';
                $_REQUEST['form_text_72'] .= ' - Акция Битрикс';

                $_POST['form_text_61'] .= ' - Акция Битрикс';
                $_REQUEST['form_text_61'] .= ' - Акция Битрикс';

                if ($_POST['form_text_37']) {
                    $_POST['form_text_37'] .= ' - Акция Битрикс';
                    $_REQUEST['form_text_37'] .= ' - Акция Битрикс';
                } else {
                    $_POST['form_text_37'] = $_POST['form_text_72'];
                    $_REQUEST['form_text_37'] = $_REQUEST['form_text_72'];
                }

                if ($_POST['form_text_26']) {
                    $_POST['form_text_26'] .= ' - Акция Битрикс';
                    $_REQUEST['form_text_26'] .= ' - Акция Битрикс';
                } else {
                    $_POST['form_text_26'] = $_POST['form_text_61'];
                    $_REQUEST['form_text_26'] = $_REQUEST['form_text_61'];
                }
            }

        	if ($arParams['USE_RECATPCHA'] && $arResult["arForm"]["USE_CAPTCHA"] == 'Y') {
                $reCaptchaVerify = new \ReCaptcha\ReCaptcha($secretKey);

                if ($reCaptchaVersion === 'v3') {
					$responseReCaptcha = $reCaptchaVerify->setExpectedAction($arResult['SOULT_FORM'])
						//->setScoreThreshold(0.5)
						->verify($_POST["token"], $_SERVER['REMOTE_ADDR']);

				// на всех проектах стоят ключи от v2 invisible
				} else {
					$responseReCaptcha = $reCaptchaVerify->verify($_REQUEST["g-recaptcha-response"], $_SERVER['REMOTE_ADDR']);
				}

                $_REQUEST['captcha_word'] = var_export($responseReCaptcha->isSuccess(), true);

            }

            $arResult["arrVALUES"] = $_REQUEST;


            // check errors
            $arResult["FORM_ERRORS"] = CForm::Check($arParams["WEB_FORM_ID"], $arResult["arrVALUES"], false, "Y", $arParams['USE_EXTENDED_ERRORS']);

            include(__DIR__ . '/include/functions.php');

            $phone = $arResult['arrVALUES']['form_' . $arResult['arAnswers']['PHONE'][0]['FIELD_TYPE'] . '_' .  $arResult['arAnswers']['PHONE'][0]['ID']];
            if (!empty($phone) && $arResult['arQuestions']['PHONE']['REQUIRED'] === 'Y') {
                if (!validatePhone($phone)) {
                    $arResult['FORM_ERRORS']['INVALID_PHONE'] = Loc::getMessage('PB_MAIN_BEFSEND_INVALID_PHONE');
                }

            }

            $email = $arResult['arrVALUES']['form_' . $arResult['arAnswers']['EMAIL'][0]['FIELD_TYPE'] . '_' .  $arResult['arAnswers']['EMAIL'][0]['ID']];
            if (!empty($email)) {
                if (!validateEmail($email)) {
                    $arResult['FORM_ERRORS']['INVALID_EMAIL'] = Loc::getMessage('PB_MAIN_BEFSEND_INVALID_EMAIL');
                }
            }

            if($arParams['USE_ANTISPAM']  == 'Y' && strlen($arResult["arrVALUES"]['form_text_'.$ANTISPAM_ID])>0) {
                $arResult["FORM_ERRORS"]["ANTISPAM"] = Loc::getMessage("PB_MAIN_BEFSEND_FANTOME_ANSWER");
            }
            if (
                $arParams['USE_EXTENDED_ERRORS'] == 'Y' && (!is_array($arResult["FORM_ERRORS"]) || count($arResult["FORM_ERRORS"]) <= 0)
                ||
                $arParams['USE_EXTENDED_ERRORS'] != 'Y' && strlen($arResult["FORM_ERRORS"]) <= 0
            ) {
                // check user session
                if (check_bitrix_sessid()) {
                    $return = false;

                    $arResult['arrOrigVALUES'] = $arResult['arrVALUES'];

                    $templateName = empty($this->__templateName) ? ".default" : $this->__templateName;
                    $pathDefaultTmpl = getLocalPath("templates/.default/components" . $this->__relativePath . "/" . $templateName . "/before_result.php");

                    if(is_object($parent = $this->__parent) && file_exists($befResPath = $_SERVER["DOCUMENT_ROOT"].$parent->getTemplate()->GetFolder().$this->__relativePath . "/" . $templateName . "/before_result.php")){
                        include $befResPath; unset($befResPath);
                    }elseif (file_exists($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . "/components" . $this->__relativePath . "/" . $templateName . "/before_result.php")) {
                        include($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . "/components" . $this->__relativePath . "/" . $templateName . "/before_result.php");
                    } elseif (strlen($pathDefaultTmpl) > 0 && file_exists($_SERVER["DOCUMENT_ROOT"] . $pathDefaultTmpl)) {
                        include($_SERVER["DOCUMENT_ROOT"] . $pathDefaultTmpl);
                    } elseif (file_exists($_SERVER["DOCUMENT_ROOT"] . $this->__path . "/templates/" . $templateName . "/before_result.php")) {
                        include($_SERVER["DOCUMENT_ROOT"] . $this->__path . "/templates/" . $templateName . "/before_result.php");
                    }

                    $vCity = $arResult['arrOrigVALUES']["form_{$arResult["arAnswers"]["CITY"][0]['FIELD_TYPE']}_{$arResult["arAnswers"]["CITY"][0]['ID']}"];
                    $vOffice = $arResult['arrOrigVALUES']["form_{$arResult["arAnswers"]["OFFICE"][0]['FIELD_TYPE']}_{$arResult["arAnswers"]["OFFICE"][0]['ID']}"];

                    //подписка на esputnik  через чекбокс в формах
                    if ($arParams['ESPUTNIK_SUBSCRIBE_ON'] == "Y" && isset($_REQUEST["esputnik-subscribe-checkbox"]) && $_REQUEST["esputnik-subscribe-checkbox"] == "on"){
                        $_SESSION["ESPUTNIK_SUBSCRIBE"] = "Y";
                        setcookie("ESPUTNIK_SUBSCRIBED", "Y", null, "/");
                    }

                    // add result
                    if ($RESULT_ID = CFormResult::Add($arParams["WEB_FORM_ID"], $arResult["arrVALUES"])) {
						//отправка данных на БитЛид
						if (CModule::IncludeModule("pb.union2")) {
							$api = new PB\Union2\Api;
							
							if (!empty($arParams['BITLEAD_OFFICE']))
								$_SESSION['BITLEAD_OFFICE'] = $api->getOffice($arParams['BITLEAD_OFFICE']);
							
							if (!empty($arParams['BITLEAD_SOURCE']))
								$_SESSION['BITLEAD_SOURCE'] = $api->getSource($arParams['BITLEAD_SOURCE']);

							if (!empty($arParams['BITLEAD_COMPETENCE']))
								$_SESSION['BITLEAD_COMPETENCE'] = $api->getCompetence($arParams['BITLEAD_COMPETENCE']);
						}
	
                        //$arResult["FORM_NOTE"] = GetMessage("FORM_DATA_SAVED1").$RESULT_ID.GetMessage("FORM_DATA_SAVED2");
                        $arResult["FORM_RESULT"] = $arParams["FORM_RESULT_ADDOK"];

                        // send email notifications
                        CFormResult::SetEvent($RESULT_ID);

                        if (CModule::IncludeModule("pb.union") && \Union\Form\FormResult::checkSendCrm($arResult)) {
                            \Union\Form\FormResult::Mail($RESULT_ID, $arResult['SEND_MAIL_ONLY'], $arParams["WEB_FORM_ID"], $vCity, $APPLICATION->GetCurDir(), $arResult["arAnswers"]["UNIONRESULT"][0]["ID"]);
                        } else {
                            if (!empty($arResult['SEND_MAIL_ONLY'])) {
                                foreach ($arResult['SEND_MAIL_ONLY'] AS $mailTmplItem) {
                                    CFormResult::Mail($RESULT_ID, $mailTmplItem);
                                }
                            } else {
                                CFormResult::Mail($RESULT_ID);
                            }
                        }

                        setcookie("WEBFORM_" . $arResult["SOULT_FORM"], "Y", null, "/");
                        $contMng = ContactManager::getInstance();
                        global $obOfficeFilter;
                        $arFilterSid = array("IBLOCK_SECTION_ID" => $vCity);
                        $sid = $contMng->getQueueInstance()->getQueueSID($obOfficeFilter, $arFilterSid);
                        $cursor = ($sid)?$contMng->getQueueInstance()->getCursorQueue($sid):"";

                        \CEventLog::Add(array(
                                "SEVERITY" => "UNKNOWN",
                                "AUDIT_TYPE_ID" => "FORM_SEND_DATA",
                                "MODULE_ID" => "pb.main",
                                "ITEM_ID" => "",
                                "DESCRIPTION" => json_encode(array(
                                    'FORM_ID' => $arParams["WEB_FORM_ID"],
                                    'RESULT_ID' => $RESULT_ID,
                                    'QUEUE' => $sid,
                                    'CURSOR' => $cursor
                                ), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                        );

                        // choose type of user redirect and do it
                        if ($arResult["F_RIGHT"] >= 15) {
                            if (strlen($_REQUEST["web_form_submit"]) > 0 && strlen($arParams["LIST_URL"]) > 0) {
                                if ($arParams["SEF_MODE"] == "Y") {
                                    //LocalRedirect($arParams["LIST_URL"]."?strFormNote=".urlencode($arResult["FORM_NOTE"]));
                                    LocalRedirect(
                                        str_replace(
                                            array('#WEB_FORM_ID#', '#RESULT_ID#'),
                                            array($arParams['WEB_FORM_ID'], $RESULT_ID),
                                            $arParams["LIST_URL"]
                                        ) . "?formresult=" . urlencode($arResult["FORM_RESULT"])
                                        . (!empty($_POST['COMPONENT_MARKER']) ? "&marker=" . $_POST['COMPONENT_MARKER'] : "")
                                        . (!empty($_POST['OFFSET']) ? "&offset=" . $_POST['OFFSET'] : "")
                                    );
                                } else {
                                    //LocalRedirect($arParams["LIST_URL"].(strpos($arParams["LIST_URL"], "?") === false ? "?" : "&")."WEB_FORM_ID=".$arParams["WEB_FORM_ID"]."&RESULT_ID=".$RESULT_ID."&strFormNote=".urlencode($arResult["FORM_NOTE"]));
                                    LocalRedirect(
                                        $arParams["LIST_URL"]
                                        . (strpos($arParams["LIST_URL"], "?") === false ? "?" : "&")
                                        . "WEB_FORM_ID=" . $arParams["WEB_FORM_ID"]
                                        . "&RESULT_ID=" . $RESULT_ID
                                        . "&formresult=" . urlencode($arResult["FORM_RESULT"])
                                        . (!empty($_POST['COMPONENT_MARKER']) ? "&marker=" . $_POST['COMPONENT_MARKER'] : "")
                                        . (!empty($_POST['OFFSET']) ? "&offset=" . $_POST['OFFSET'] : "")
                                    );
                                }
                            } elseif (strlen($_REQUEST["web_form_apply"]) > 0 && strlen($arParams["EDIT_URL"]) > 0) {
                                if ($arParams["SEF_MODE"] == "Y") {
                                    //LocalRedirect(str_replace("#RESULT_ID#", $RESULT_ID, $arParams["EDIT_URL"])."?strFormNote=".urlencode($arResult["FORM_NOTE"]));
                                    LocalRedirect(
                                        str_replace(
                                            array('#WEB_FORM_ID#', '#RESULT_ID#'),
                                            array($arParams['WEB_FORM_ID'], $RESULT_ID),
                                            $arParams["EDIT_URL"]
                                        )
                                        . (strpos($arParams["EDIT_URL"], "?") === false ? "?" : "&")
                                        . "formresult=" . urlencode($arResult["FORM_RESULT"])
                                        . (!empty($_POST['COMPONENT_MARKER']) ? "&marker=" . $_POST['COMPONENT_MARKER'] : "")
                                        . (!empty($_POST['OFFSET']) ? "&offset=" . $_POST['OFFSET'] : "")
                                    );
                                } else {
                                    LocalRedirect(
                                        $arParams["EDIT_URL"]
                                        . (strpos($arParams["EDIT_URL"], "?") === false ? "?" : "&")
                                        . "WEB_FORM_ID=" . $arParams["WEB_FORM_ID"]
                                        . "&RESULT_ID=" . $RESULT_ID
                                        . "&formresult=" . urlencode($arResult["FORM_RESULT"])
                                        . (!empty($_POST['COMPONENT_MARKER']) ? "&marker=" . $_POST['COMPONENT_MARKER'] : "")
                                        . (!empty($_POST['OFFSET']) ? "&offset=" . $_POST['OFFSET'] : "")
                                    );
                                }
                                die();
                            }

                            $arResult["return"] = true;
                        }

                        if (strlen($arParams["SUCCESS_URL"]) > 0) {
                            if ($arParams['SEF_MODE'] == 'Y') {
                                LocalRedirect(
                                    str_replace(
                                        array('#WEB_FORM_ID#', '#RESULT_ID#'),
                                        array($arParams['WEB_FORM_ID'], $RESULT_ID),
                                        $arParams["SUCCESS_URL"]
                                    )
                                    . (strpos($arParams["SUCCESS_URL"], "?") === false ? "?" : "&")
                                    . "formresult=" . urlencode($arResult["FORM_RESULT"])
                                    . (!empty($_POST['COMPONENT_MARKER']) ? "&marker=" . $_POST['COMPONENT_MARKER'] : "")
                                    . (!empty($_POST['OFFSET']) ? "&offset=" . $_POST['OFFSET'] : "")
                                );
                            } else {
                                LocalRedirect(
                                    $arParams["SUCCESS_URL"]
                                    . (strpos($arParams["SUCCESS_URL"], "?") === false ? "?" : "&")
                                    . "WEB_FORM_ID=" . $arParams["WEB_FORM_ID"]
                                    . "&RESULT_ID=" . $RESULT_ID
                                    . "&formresult=" . urlencode($arResult["FORM_RESULT"])
                                    . (!empty($_POST['COMPONENT_MARKER']) ? "&marker=" . $_POST['COMPONENT_MARKER'] : "")
                                    . (!empty($_POST['OFFSET']) ? "&offset=" . $_POST['OFFSET'] : "")
                                );
                            }

                            die();
                        } elseif ($arParams["SEF_MODE"] == "Y") {
                            LocalRedirect(
                                $APPLICATION->GetCurPageParam(
                                    "formresult=" . urlencode($arResult["FORM_RESULT"])
                                    . (!empty($_POST['COMPONENT_MARKER']) ? "&marker=" . $_POST['COMPONENT_MARKER'] : "")
                                    . (!empty($_POST['OFFSET']) ? "&offset=" . $_POST['OFFSET'] : ""),
                                    array('formresult', 'strFormNote', 'SEF_APPLICATION_CUR_PAGE_URL', 'unique-btn')
                                )
                            );

                            die();
                        } else {
                            LocalRedirect(
                                $APPLICATION->GetCurPageParam(
                                    "WEB_FORM_ID=" . $arParams["WEB_FORM_ID"]
                                    . "&RESULT_ID=" . $RESULT_ID
                                    . "&formresult=" . urlencode($arResult["FORM_RESULT"])
                                    . (!empty($_POST['COMPONENT_MARKER']) ? "&marker=" . $_POST['COMPONENT_MARKER'] : "")
                                    . (!empty($_POST['OFFSET']) ? "&offset=" . $_POST['OFFSET'] : ""),
                                    array('formresult', 'strFormNote', 'WEB_FORM_ID', 'RESULT_ID', 'marker', 'offset', 'unique-btn')
                                )
                            );

                            die();
                            //LocalRedirect($APPLICATION->GetCurPage()."?WEB_FORM_ID=".$arParams["WEB_FORM_ID"]."&strFormNote=".urlencode($arResult["FORM_NOTE"]));
                        }
                    } else {
                        if ($arParams['USE_EXTENDED_ERRORS'] == 'Y')
                            $arResult["FORM_ERRORS"] = array($GLOBALS["strError"]);
                        else
                            $arResult["FORM_ERRORS"] = $GLOBALS["strError"];

                        unset($_SESSION["ESPUTNIK_SUBSCRIBE"]);
                    }
                }
            }
        }

        /*
        if (is_array($arResult["FORM_ERRORS"]))
        {
            $arResult["FORM_ERRORS"] = implode("<br />", $arResult["FORM_ERRORS"]);
        }
        */

        //if (!empty($_REQUEST["strFormNote"])) $arResult["FORM_NOTE"] = $_REQUEST["strFormNote"];
        if (!empty($_REQUEST["formresult"]) && (empty($arParams['COMPONENT_MARKER']) || $_REQUEST['marker'] == $arParams['COMPONENT_MARKER']) && $_REQUEST['WEB_FORM_ID'] == $arParams['WEB_FORM_ID']) {
            $formResult = strtoupper($_REQUEST['formresult']);
            switch ($formResult) {
                case strtoupper($arParams["FORM_RESULT_ADDOK"]):
                    $arResult['FORM_NOTE'] = str_replace("#RESULT_ID#", intval($_REQUEST["RESULT_ID"]), GetMessage('FORM_NOTE_ADDOK'));
            }
        }

        $arResult["isFormErrors"] =
            (
                $arParams['USE_EXTENDED_ERRORS'] == 'Y' && is_array($arResult["FORM_ERRORS"]) && count($arResult["FORM_ERRORS"]) > 0
                ||
                $arParams['USE_EXTENDED_ERRORS'] != 'Y' && strlen($arResult["FORM_ERRORS"]) > 0
            )
                ? "Y" : "N";

        // ************************************************************* //
        //                                             output                                                                    //
        // ************************************************************* //

        //echo '<pre>'; print_r($arResult['arForm']); echo '</pre>';

        if ($arParams["IGNORE_CUSTOM_TEMPLATE"] == "N" && $arResult["arForm"]["USE_DEFAULT_TEMPLATE"] == "N" && strlen($arResult["arForm"]["FORM_TEMPLATE"]) > 0) {
            // use visual template
            if (!$bCache || $bCache && !$bVarsFromCache) {
                if ($bCache) {
                    $obFormCache->StartDataCache();
                }

                //if (is_array($arResult['FORM_ERRORS']))
                //$arResult['FORM_ERRORS'] = implode('<br />', $arResult['FORM_ERRORS']);

                $FORM = new CFormOutput();
                // initialize template


                $FORM->InitializeTemplate($arParams, $arResult);
                //echo '<pre>',htmlspecialcharsbx(print_r($arParams, true)),htmlspecialcharsbx(print_r($arResult, true)),htmlspecialcharsbx(print_r($FORM, true)),'</pre>';

                // cache image files paths
                $FORM->ShowFormImage();
                $FORM->getFormImagePath();

                if ($bCache) {
                    $obFormCache->EndDataCache(
                        array(
                            "arResult" => $arResult,
                            "FORM" => $FORM,
                        )
                    );
                }
            } else {
                $FORM->strFormNote = $arResult['FORM_NOTE'];
                $FORM->isFormNote = (bool)$arResult['FORM_NOTE'];
            }

            // if form uses CAPCHA initialize it
            if ($arResult["arForm"]["USE_CAPTCHA"] == "Y") {
                if($arParams['USE_RECATPCHA']) {
                    $FORM->CAPTCHACode = $arResult["CAPTCHACode"] = ReCaptcha::APPCaptchaGetCode();
                }
                else {
                    $FORM->CAPTCHACode = $arResult["CAPTCHACode"] = $APPLICATION->CaptchaGetCode();
                }
            }


            // get template
            if ($strReturn = $FORM->IncludeFormCustomTemplate()) {
                // add icons
                $back_url = $_SERVER['REQUEST_URI'];

                $editor = "/bitrix/admin/fileman_file_edit.php?full_src=Y&site=" . SITE_ID . "&";
                $href = "javascript:window.location='" . $editor . "path=" . urlencode($path) . "&lang=" . LANGUAGE_ID . "&back_url=" . urlencode($back_url) . "'";

                if ($arParams['USE_EXTENDED_ERRORS'] == 'Y')
                    $APPLICATION->SetAdditionalCSS($this->GetPath() . "/error.css");

                if ($APPLICATION->GetShowIncludeAreas() && $USER->IsAdmin()) {
                    // define additional icons for Site Edit mode
                    $arIcons = array(
                        // form template edit icon
                        array(
                            'URL' => "javascript:" . $APPLICATION->GetPopupLink(
                                    array(
                                        'URL' => "/bitrix/admin/form_edit.php?bxpublic=Y&from_module=form&lang=" . LANGUAGE_ID . "&ID=" . $FORM->WEB_FORM_ID . "&tabControl_active_tab=edit5&back_url=" . urlencode($_SERVER["REQUEST_URI"]),
                                        'PARAMS' => array(
                                            'width' => 700,
                                            'height' => 500,
                                            'resize' => false,
                                        )
                                    )
                                ),
                            'ICON' => 'bx-context-toolbar-edit-icon',
                            'TITLE' => Loc::getMessage("PB_MAIN_BEFSEND_FORM_PUBLIC_ICON_EDIT_TPL")
                        ),

                        array(
                            'URL' => "javascript:" . $APPLICATION->GetPopupLink(
                                    array(
                                        'URL' => "/bitrix/admin/form_edit.php?bxpublic=Y&from_module=form&lang=" . LANGUAGE_ID . "&ID=" . $FORM->WEB_FORM_ID . "&back_url=" . urlencode($_SERVER["REQUEST_URI"]),
                                        'PARAMS' => array(
                                            'width' => 700,
                                            'height' => 500,
                                            'resize' => false,
                                        )
                                    )
                                ),
                            'ICON' => 'bx-context-toolbar-edit-icon',
                            'TITLE' => Loc::getMessage("PB_MAIN_BEFSEND_FORM_PUBLIC_ICON_EDIT"),
                        ),
                    );

                    $this->AddIncludeAreaIcons($arIcons);
                }

                // output template
                echo $strReturn;

                return;
            }
        }

        if ($arResult["arForm"]["USE_CAPTCHA"] == "Y") {
            if($arParams['USE_RECATPCHA']) {
                $arResult["CAPTCHACode"] = htmlspecialchars(ReCaptcha::APPCaptchaGetCode());;
            }
            else {
                $arResult["CAPTCHACode"] = $APPLICATION->CaptchaGetCode();
            }
        }

        // include CSS with additional icons for Site Edit mode
        if ($APPLICATION->GetShowIncludeAreas() && $USER->IsAdmin()) {
            // define additional icons for Site Edit mode
            $arIcons = array(
                // form params edit icon
                array(
                    'URL' => "javascript:" . $APPLICATION->GetPopupLink(
                            array(
                                'URL' => "/bitrix/admin/form_edit.php?bxpublic=Y&from_module=form&lang=" . LANGUAGE_ID . "&ID=" . $arParams["WEB_FORM_ID"] . "&back_url=" . urlencode($_SERVER["REQUEST_URI"]),
                                'PARAMS' => array(
                                    'width' => 700,
                                    'height' => 500,
                                    'resize' => false,
                                )
                            )
                        ),
                    'ICON' => 'bx-context-toolbar-edit-icon',
                    'TITLE' => Loc::getMessage("PB_MAIN_BEFSEND_FORM_PUBLIC_ICON_EDIT"),

                ),
            );

            // append icons
            $this->AddIncludeAreaIcons($arIcons);
        }


        // define variables to assign
        $arResult = array_merge(
            $arResult,
            array(
                "isFormNote" => strlen($arResult["FORM_NOTE"]) ? "Y" : "N", // flag "is there a form note"
                "isAccessFormParams" => $arResult["F_RIGHT"] >= 25 ? "Y" : "N", // flag "does current user have access to form params"
                "isStatisticIncluded" => CModule::IncludeModule('statistic') ? "Y" : "N", // flag "is statistic module included"

                "FORM_HEADER" => sprintf( // form header (<form> tag and hidden inputs)
                        "<form name=\"%s\" action=\"%s\" method=\"%s\" enctype=\"multipart/form-data\">",
                        $arResult["arForm"]["SID"], POST_FORM_ACTION_URI, "POST"
                    ) . $res .= bitrix_sessid_post() . '<input type="hidden" name="WEB_FORM_ID" value="' . $arParams['WEB_FORM_ID'] . '" />',

                "FORM_TITLE" => trim(htmlspecialcharsbx($arResult["arForm"]["NAME"])), // form title

                "FORM_DESCRIPTION" => // form description
                    $arResult["arForm"]["DESCRIPTION_TYPE"] == "html" ?
                        trim($arResult["arForm"]["DESCRIPTION"]) :
                        nl2br(htmlspecialcharsbx(trim($arResult["arForm"]["DESCRIPTION"]))),

                "isFormTitle" => strlen($arResult["arForm"]["NAME"]) > 0 ? "Y" : "N", // flag "does form have title"
                "isFormDescription" => strlen($arResult["arForm"]["DESCRIPTION"]) > 0 ? "Y" : "N", // flag "does form have description"
                "isFormImage" => intval($arResult["arForm"]["IMAGE_ID"]) > 0 ? "Y" : "N", // flag "does form have image"
                "isUseCaptcha" => $arResult["arForm"]["USE_CAPTCHA"] == "Y", // flag "does form use captcha"
                "modeUseCaptcha" => ($arParams['USE_RECATPCHA']) ? 'recaptcha' : 'bitrix',
                "useAntispam" => ($arParams['USE_ANTISPAM']),
                "DATE_FORMAT" => CLang::GetDateFormat("SHORT"), // current site date format
                "REQUIRED_SIGN" => CForm::ShowRequired("Y"), // "required" sign
                "FORM_FOOTER" => "</form>", // form footer (close <form> tag)
            )
        );

        /*
        if ($arResult["isFormNote"] == "Y")
        {
            ob_start();
            ShowMessage($arResult["FORM_NOTE"]);
            $arResult["FORM_NOTE"] = ob_get_contents();
            ob_end_clean();
        }
        */

        // get template vars for form image
        if ($arResult["isFormImage"] == "Y") {
            $arResult["FORM_IMAGE"]["ID"] = $arResult["arForm"]["IMAGE_ID"];
            // assign form image url
            $arResult["FORM_IMAGE"]["URL"] = CFile::GetPath($arResult["arForm"]["IMAGE_ID"]);

            // check image file existance and assign image data
            if (
                file_exists($_SERVER["DOCUMENT_ROOT"] . $arResult["FORM_IMAGE"]["URL"])
                &&
                list(
                    $arResult["FORM_IMAGE"]["WIDTH"],
                    $arResult["FORM_IMAGE"]["HEIGHT"],
                    $arResult["FORM_IMAGE"]["TYPE"],
                    $arResult["FORM_IMAGE"]["ATTR"]
                    ) = @getimagesize($_SERVER["DOCUMENT_ROOT"] . $arResult["FORM_IMAGE"]["URL"])
            ) {
                $arResult["FORM_IMAGE"]["HTML_CODE"] = CFile::ShowImage($arResult["arForm"]["IMAGE_ID"]);
            }
        }

        $arResult["QUESTIONS"] = array();
        reset($arResult["arQuestions"]);

        foreach ($arResult["arQuestions"] as $key => $arQuestion) {
            $FIELD_SID = $arQuestion["SID"];
            if (in_array($FIELD_SID, $arParams['IGNORE_FIELDS'])) {
                continue;
            }
            $arResult["QUESTIONS"][$FIELD_SID] = array(
                "CAPTION" => // field caption
                    $arResult["arQuestions"][$FIELD_SID]["TITLE_TYPE"] == "html" ?
                        $arResult["arQuestions"][$FIELD_SID]["TITLE"] :
                        nl2br(htmlspecialcharsbx($arResult["arQuestions"][$FIELD_SID]["TITLE"])),

                "IS_HTML_CAPTION" => $arResult["arQuestions"][$FIELD_SID]["TITLE_TYPE"] == "html" ? "Y" : "N",
                "REQUIRED" => $arResult["arQuestions"][$FIELD_SID]["REQUIRED"] == "Y" ? "Y" : "N",
                "IS_INPUT_CAPTION_IMAGE" => intval($arResult["arQuestions"][$FIELD_SID]["IMAGE_ID"]) > 0 ? "Y" : "N",
            );

            // ******************************** customize answers ***************************** //

            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"] = array();

            if (is_array($arResult["arAnswers"][$FIELD_SID])) {
                $res = "";

                reset($arResult["arAnswers"][$FIELD_SID]);
                if (is_array($arResult["arDropDown"][$FIELD_SID])) reset($arResult["arDropDown"][$FIELD_SID]);
                if (is_array($arResult["arMutiselect"][$FIELD_SID])) reset($arResult["arMutiselect"][$FIELD_SID]);

                $show_dropdown = "N";
                $show_multiselect = "N";

                foreach ($arResult["arAnswers"][$FIELD_SID] as $key => $arAnswer) {

                    if ($arAnswer["FIELD_TYPE"] == "dropdown" && $show_dropdown == "Y") continue;
                    if ($arAnswer["FIELD_TYPE"] == "multiselect" && $show_multiselect == "Y") continue;

                    $res = "";

                    if(strtolower($arAnswer["FIELD_PARAM"]) == strtolower($FIELD_SID) && $arAnswer["FIELD_TYPE"] == 'hidden') {
                        $arAnswer["FIELD_PARAM"] = '';
                    }
                    $arAnswer["FIELD_PARAM"] .= " data-code=\"" . $FIELD_SID . "\"";

                    switch ($arAnswer["FIELD_TYPE"]) {
                        case "radio":
                            if (strpos($arAnswer["FIELD_PARAM"], "id=") === false) {
                                $ans_id = $arAnswer["ID"];
                                $arAnswer["FIELD_PARAM"] .= " id=\"" . $ans_id . "\"";
                            } else {
                                $ans_id = "";
                            }

                            $value = CForm::GetRadioValue($FIELD_SID, $arAnswer, $arResult["arrVALUES"]);

                            if ($arResult["isFormErrors"] == 'Y') {
                                if (
                                    strpos(strtolower($arAnswer["FIELD_PARAM"]), "selected") !== false
                                    ||
                                    strpos(strtolower($arAnswer["FIELD_PARAM"]), "checked") !== false
                                ) {
                                    $arAnswer["FIELD_PARAM"] = preg_replace("/checked|selected/i", "", $arAnswer["FIELD_PARAM"]);
                                }
                            }

                            $input = CForm::GetRadioField(
                                $FIELD_SID,
                                $arAnswer["ID"],
                                $value,
                                $arAnswer["FIELD_PARAM"]);


                            if (strlen($ans_id) > 0) {
                                $res .= $input;
                                $res .= "<label for=\"" . $ans_id . "\">" . $arAnswer["MESSAGE"] . "</label>";
                            } else {
                                $res .= "<label>" . $input . $arAnswer["MESSAGE"] . "</label>";
                            }

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "checkbox":
                            if (strpos($arAnswer["FIELD_PARAM"], "id=") === false) {
                                $ans_id = $arAnswer["ID"];
                                $arAnswer["FIELD_PARAM"] .= " id=\"" . $ans_id . "\"";
                            } else {
                                $ans_id = "";
                            }

                            $value = CForm::GetCheckBoxValue($FIELD_SID, $arAnswer, $arResult["arrVALUES"]);

                            if ($arResult['isFormErrors'] == 'Y') {
                                if (
                                    strpos(strtolower($arAnswer["FIELD_PARAM"]), "selected") !== false
                                    ||
                                    strpos(strtolower($arAnswer["FIELD_PARAM"]), "checked") !== false
                                ) {
                                    $arAnswer["FIELD_PARAM"] = preg_replace("/checked|selected/i", "", $arAnswer["FIELD_PARAM"]);
                                }
                            }

                            $input = CForm::GetCheckBoxField(
                                $FIELD_SID,
                                $arAnswer["ID"],
                                $value,
                                $arAnswer["FIELD_PARAM"]);


                            if (strlen($ans_id) > 0) {
                                $res .= $input . "<label for=\"" . $ans_id . "\">" . $arAnswer["MESSAGE"] . "</label>";
                            } else {
                                $res .= "<label>" . $input . $arAnswer["MESSAGE"] . "</label>";
                            }

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "dropdown":
                            if ($show_dropdown != "Y") {
                                $value = CForm::GetDropDownValue($FIELD_SID, $arResult["arDropDown"], $arResult["arrVALUES"]);

                                if (strlen($arResult["FORM_ERROR"]) > 0)
                                    for ($i = 0; $i <= count($arDropDown[$FIELD_SID]["param"]) - 1; $i++)
                                        $arDropDown[$FIELD_SID]["param"][$i] = preg_replace("/checked|selected/i", "", $arDropDown[$FIELD_SID]["param"][$i]);

                                $res .= CForm::GetDropDownField(
                                    $FIELD_SID,
                                    $arResult["arDropDown"][$FIELD_SID],
                                    $value,
                                    $arAnswer["FIELD_PARAM"]);
                                $show_dropdown = "Y";
                            }

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "multiselect":
                            if ($show_multiselect != "Y") {
                                $value = CForm::GetMultiSelectValue($FIELD_SID, $arResult["arMultiSelect"], $arResult["arrVALUES"]);

                                if (strlen($arResult["FORM_ERROR"]) > 0)
                                    for ($i = 0; $i <= count($arResult["arMultiSelect"][$FIELD_SID]["param"]) - 1; $i++)
                                        $arResult["arMultiSelect"][$FIELD_SID]["param"][$i] = preg_replace("/checked|selected/i", "", $arResult["arMultiSelect"][$FIELD_SID]["param"][$i]);
                                $res .= CForm::GetMultiSelectField(
                                    $FIELD_SID,
                                    $arResult["arMultiSelect"][$FIELD_SID],
                                    $value,
                                    $arAnswer["FIELD_HEIGHT"],
                                    $arAnswer["FIELD_PARAM"]
                                );

                                $show_multiselect = "Y";
                            }

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "text":
                            if (strlen(trim($arAnswer["MESSAGE"])) > 0) {
                                $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $arAnswer["MESSAGE"];
                            }

                            $value = CForm::GetTextValue($arAnswer["ID"], $arAnswer, $arResult["arrVALUES"]);
                            $res .= CForm::GetTextField(
                                $arAnswer["ID"],
                                $value,
                                $arAnswer["FIELD_WIDTH"],
                                $arAnswer["FIELD_PARAM"]);

                            if(stristr($res, 'data-code="PHONE"')){
                                $res = str_replace(" type=\"text\" ", " type=\"tel\" " ,$res);
                            }

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;

                        case "hidden":
                            $value = CForm::GetHiddenValue($arAnswer["ID"], $arAnswer, $arResult["arrVALUES"]);
                            $res .= CForm::GetHiddenField(
                                $arAnswer["ID"],
                                $value,
                                $arAnswer["FIELD_PARAM"]);
                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;

                        case "password":
                            if (strlen(trim($arAnswer["MESSAGE"])) > 0) {
                                $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $arAnswer["MESSAGE"];
                            }

                            $value = CForm::GetPasswordValue($arAnswer["ID"], $arAnswer, $arResult["arrVALUES"]);
                            $res .= CForm::GetPasswordField(
                                $arAnswer["ID"],
                                $value,
                                $arAnswer["FIELD_WIDTH"],
                                $arAnswer["FIELD_PARAM"]);

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "email":
                            if (strlen(trim($arAnswer["MESSAGE"])) > 0) {
                                $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $arAnswer["MESSAGE"];
                            }
                            $value = CForm::GetEmailValue($arAnswer["ID"], $arAnswer, $arResult["arrVALUES"]);
                            $res .= CForm::GetEmailField(
                                $arAnswer["ID"],
                                $value,
                                $arAnswer["FIELD_WIDTH"],
                                $arAnswer["FIELD_PARAM"]);

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "url":
                            if (strlen(trim($arAnswer["MESSAGE"])) > 0) {
                                $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $arAnswer["MESSAGE"];
                            }
                            $value = CForm::GetUrlValue($arAnswer["ID"], $arAnswer, $arResult["arrVALUES"]);
                            $res .= CForm::GetUrlField(
                                $arAnswer["ID"],
                                $value,
                                $arAnswer["FIELD_WIDTH"],
                                $arAnswer["FIELD_PARAM"]);

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "textarea":
                            if (strlen(trim($arAnswer["MESSAGE"])) > 0) {
                                $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $arAnswer["MESSAGE"];
                            }

                            if (intval($arAnswer["FIELD_WIDTH"]) <= 0) $arAnswer["FIELD_WIDTH"] = "40";
                            if (intval($arAnswer["FIELD_HEIGHT"]) <= 0) $arAnswer["FIELD_HEIGHT"] = "5";

                            $value = CForm::GetTextAreaValue($arAnswer["ID"], $arAnswer, $arResult["arrVALUES"]);
                            $res .= CForm::GetTextAreaField(
                                $arAnswer["ID"],
                                $arAnswer["FIELD_WIDTH"],
                                $arAnswer["FIELD_HEIGHT"],
                                $arAnswer["FIELD_PARAM"],
                                $value
                            );

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "date":
                            if (strlen(trim($arAnswer["MESSAGE"])) > 0) {
                                $res .= $arAnswer["MESSAGE"];
                            }
                            $value = CForm::GetDateValue($arAnswer["ID"], $arAnswer, $arResult["arrVALUES"]);
                            $res .= CForm::GetDateField(
                                $arAnswer["ID"],
                                $arResult["arForm"]["SID"],
                                $value,
                                $arAnswer["FIELD_WIDTH"],
                                $arAnswer["FIELD_PARAM"]);

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res . " (" . CSite::GetDateFormat("SHORT") . ")";

                            break;
                        case "image":
                            $res .= CForm::GetFileField(
                                $arAnswer["ID"],
                                $arAnswer["FIELD_WIDTH"],
                                "IMAGE",
                                0,
                                "",
                                $arAnswer["FIELD_PARAM"]);

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                        case "file":

                            $res .= CForm::GetFileField(
                                $arAnswer["ID"],
                                $arAnswer["FIELD_WIDTH"],
                                "FILE",
                                0,
                                "",
                                $arAnswer["FIELD_PARAM"]);

                            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                            break;
                    } //endswitch;
                } //endwhile;


            } //endif(is_array($arAnswers[$FIELD_SID]));
            elseif (is_array($arResult["arQuestions"][$FIELD_SID]) && $arResult["arQuestions"][$FIELD_SID]["ADDITIONAL"] == "Y") {

                $res = "";

                switch ($arResult["arQuestions"][$FIELD_SID]["FIELD_TYPE"]) {
                    case "text":
                        $value = CForm::GetTextAreaValue("ADDITIONAL_" . $arResult["arQuestions"][$FIELD_SID]["ID"], array(), $arResult["arrVALUES"]);
                        $res .= CForm::GetTextAreaField(
                            "ADDITIONAL_" . $arResult["arQuestions"][$FIELD_SID]["ID"],
                            "60",
                            "5",
                            "",
                            $value
                        );

                        $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                        break;
                    case "integer":
                        $value = CForm::GetTextValue("ADDITIONAL_" . $arResult["arQuestions"][$FIELD_SID]["ID"], array(), $arResult["arrVALUES"]);
                        $res .= CForm::GetTextField(
                            "ADDITIONAL_" . $arResult["arQuestions"][$FIELD_SID]["ID"],
                            $value);

                        $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res;

                        break;
                    case "date":
                        $value = CForm::GetDateValue("ADDITIONAL_" . $arResult["arQuestions"][$FIELD_SID]["ID"], array(), $arResult["arrVALUES"]);
                        $res .= CForm::GetDateField(
                            "ADDITIONAL_" . $arResult["arQuestions"][$FIELD_SID]["ID"],
                            $arResult["arForm"]["SID"],
                            $value);

                        $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"][] = $res . " (" . CSite::GetDateFormat("SHORT") . ")";

                        break;
                } //endswitch;
            }

            $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"] = implode("<br />", $arResult["QUESTIONS"][$FIELD_SID]["HTML_CODE"]);

            // ******************************************************************************* //

            if ($arResult["QUESTIONS"][$FIELD_SID]["IS_INPUT_CAPTION_IMAGE"] == "Y") {
                $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["ID"] = $arResult["arQuestions"][$FIELD_SID]["IMAGE_ID"];

                // assign field image path
                $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["URL"] = CFile::GetPath($arResult["arQuestions"][$FIELD_SID]["IMAGE_ID"]);

                // check image file existance and assign image data
                if (
                    file_exists($_SERVER["DOCUMENT_ROOT"] . $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["URL"])
                    &&
                    list(
                        $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["WIDTH"],
                        $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["HEIGHT"],
                        $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["TYPE"],
                        $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["ATTR"]
                        ) = @getimagesize($_SERVER["DOCUMENT_ROOT"] . $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["URL"])
                ) {
                    $arResult["QUESTIONS"][$FIELD_SID]["IMAGE"]["HTML_CODE"] = CFile::ShowImage($arResult["arQuestions"][$FIELD_SID]["IMAGE_ID"]);
                }
            }

            // get answers raw structure
            $arResult["QUESTIONS"][$FIELD_SID]["STRUCTURE"] = $arResult["arAnswers"][$FIELD_SID];

            // nullify value
            $arResult["QUESTIONS"][$FIELD_SID]["VALUE"] = "";
        }

        // compability:

        if ($arResult["isFormErrors"] == "Y") {
            ob_start();
            if ($arParams['USE_EXTENDED_ERRORS'] == 'N')
                ShowError($arResult["FORM_ERRORS"]);
            else
                ShowError(implode('<br />', $arResult["FORM_ERRORS"]));

            $arResult["FORM_ERRORS_TEXT"] = ob_get_contents();
            ob_end_clean();
        }

        $arResult["SUBMIT_BUTTON"] = "<input " . (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "") . " type=\"submit\" name=\"web_form_submit\" value=\"" . (strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? Loc::getMessage("PB_MAIN_BEFSEND_FORM_ADD") : $arResult["arForm"]["BUTTON"]) . "\" />";
        $arResult["APPLY_BUTTON"] = "<input type=\"hidden\" name=\"web_form_apply\" value=\"Y\" /><input type=\"submit\" name=\"web_form_apply\" value=\"" . Loc::getMessage("PB_MAIN_BEFSEND_FORM_APPLY") . "\" />";
        $arResult["RESET_BUTTON"] = "<input type=\"reset\" value=\"" . Loc::getMessage("PB_MAIN_BEFSEND_FORM_RESET") . "\" />";
        $arResult["REQUIRED_STAR"] = $arResult["REQUIRED_SIGN"];
        $arResult["CAPTCHA_IMAGE"] = "<input type=\"hidden\" name=\"captcha_sid\" value=\"" . htmlspecialcharsbx($arResult["CAPTCHACode"]) . "\" /><img src=\"/bitrix/tools/captcha.php?captcha_sid=" . htmlspecialcharsbx($arResult["CAPTCHACode"]) . "\" width=\"180\" height=\"40\" />";
        $arResult["CAPTCHA_FIELD"] = "<input type=\"text\" name=\"captcha_word\" size=\"30\" maxlength=\"50\" value=\"\" class=\"inputtext\" />";
        $arResult["CAPTCHA"] = $arResult["CAPTCHA_IMAGE"] . "<br />" . $arResult["CAPTCHA_FIELD"];

        if ($bCache) {
            $obFormCache->StartDataCache();
            $obFormCache->EndDataCache(
                array(
                    "arResult" => $arResult,
                )
            );
        }

        foreach (GetModuleEvents("pb.main", "OnBeforeIncCompTmplBezrFormResultNewBefsend", true) as $arEvent) {
            ExecuteModuleEventEx($arEvent, array(&$this));
        }

        $this->IncludeComponentTemplate();
    } else {
        ShowError(GetMessage($arResult["ERROR"]));
    }
} else {
    echo ShowError(Loc::getMessage("PB_MAIN_BEFSEND_FORM_MODULE_NOT_INSTALLED"));
}
?>

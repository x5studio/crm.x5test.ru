<?
$MESS ['PB_MAIN_BEFSEND_FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['PB_MAIN_BEFSEND_FORM_APPLY'] = "Применить";
$MESS ['PB_MAIN_BEFSEND_FORM_ADD'] = "Добавить";
$MESS ['PB_MAIN_BEFSEND_FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['PB_MAIN_BEFSEND_FORM_DATA_SAVED1'] = "Спасибо!<br /><br />Ваша заявка №";
$MESS ['PB_MAIN_BEFSEND_FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['PB_MAIN_BEFSEND_FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['PB_MAIN_BEFSEND_FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['PB_MAIN_BEFSEND_FORM_PUBLIC_ICON_EDIT_TPL'] = "Редактировать шаблон веб-формы";
$MESS ['PB_MAIN_BEFSEND_FORM_PUBLIC_ICON_EDIT'] = "Редактировать параметры веб-формы";
$MESS ['PB_MAIN_BEFSEND_FORM_NOTE_ADDOK'] = "Спасибо! Ваша заявка #RESULT_ID# принята!";
$MESS ['PB_MAIN_BEFSEND_THANK_YOU'] = "Спасибо за вашу заявку!";
$MESS ['PB_MAIN_BEFSEND_WAIT_CALL'] = 'Мы свяжемся с вами в ближайшее время.';
$MESS ['PB_MAIN_BEFSEND_TRACK_FORMS'] = 'Формы';
$MESS ['PB_MAIN_BEFSEND_TRACK_SUCC'] = 'Успешно отправлены';
$MESS ['PB_MAIN_BEFSEND_TRACK_FORM_SEND'] = 'Отправка формы';
$MESS ['PB_MAIN_BEFSEND_TRACK_OFFICE_FROM_QUEUE'] = 'Подстановка офиса';
$MESS ['PB_MAIN_BEFSEND_TRACK_ERROR'] = 'Ошибки во время заполнения';
$MESS ['PB_MAIN_BEFSEND_SELECT_OFFICE'] = 'Выберите офис:';
$MESS ['PB_MAIN_BEFSEND_SELECT_OFFICE_LINK'] = 'Вы можете <a href="#">выбрать офис</a>';
$MESS ['PB_MAIN_BEFSEND_TO_CAPTION'] = 'Офис';
$MESS ['PB_MAIN_BEFSEND_FANTOME_LABEL'] = '';
$MESS ['PB_MAIN_BEFSEND_FANTOME_ANSWER'] = 'Ваше мнение важно для нас. Спасибо за обращение';
$MESS['PB_MAIN_BEFSEND_INVALID_PHONE'] = 'Неправильно заполнен номер телефона';
$MESS['PB_MAIN_BEFSEND_INVALID_EMAIL'] = 'Неправильно заполнен E-mail';
?>
<?
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_GROUP_PARAMS'] = "Параметры компонента";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_WEB_FORM_ID'] = "ID веб-формы";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_RESULT_ID'] = "ID результата";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_SHOW_OFFICE'] = "Список городов на которых в формах мы показываем поле офис и показываем кнопкку с предложением выбрать офис";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_LIST_URL'] = "Страница со списком результатов";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_EDIT_URL'] = "Страница редактирования результата";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_SUCCESS_URL'] = "Страница с сообщением об успешной отправке";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_CHAIN_ITEM_TEXT'] = "Название дополнительного пункта в навигационной цепочке";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_CHAIN_ITEM_LINK'] = "Ссылка на дополнительном пункте в навигационной цепочке";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_IGNORE_CUSTOM_TEMPLATE'] = "Игнорировать свой шаблон";
$MESS ['PB_MAIN_BEFSEND_COMP_FORM_PARAMS_USE_EXTENDED_ERRORS'] = "Использовать расширенный вывод сообщений об ошибках";
?>
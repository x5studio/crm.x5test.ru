<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?=PB\Main\LazyLoad::getInstance()->begin("form-general-".$arResult["SOULT_FORM"]);?>
<?$PARENT = $this->__component->__parent;?>
<div id="general-form-<?=$arResult["SOULT_FORM"]?>"
	 class="popup onecol j-form-popup hidden<?if($arParams['NORMAL_WIDTH']=='Y'):?> form-normal-width<?endif;?>"
	 data-salt-form="<?= $arResult["SOULT_FORM"] ?>"
	 data-btn="<?=$arParams["~BTN_CALL_FORM"]?>">
	<div class="popup-title">
		<div class="title-text"><?=$arResult["FORM_TITLE"]?></div>

		<div class="close j-close"></div>
	</div>
	<div class="popup-content">
		<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
		<?if(!empty($arResult["FORM_NOTE"]) && $arParams['SHOW_FORM_NOTE'] != "N"):?>
			<?//=$arResult["FORM_NOTE"]?>
		<?endif?>
		<form name="<?=$arResult["arForm"]["SID"]?>" action="<?=$APPLICATION->GetCurPageParam(false)?>"
		      method="POST" enctype="multipart/form-data" class="j-form">
			<input type="hidden" name="sessid" id="sessid" value="" />
			<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams['WEB_FORM_ID']?>" />
			<?if(!empty($arResult["FORM_DESCRIPTION"]) && $arParams['SHOW_FORM_DESCRIPTION'] != "N"):?>
			<div class="h2"><?=$arResult["FORM_DESCRIPTION"]?></div>
			<?endif?>
			<?$arHiddenID = Array();?>
			<div class="clearfix">
				<?$k=0;foreach($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):$k++;?>
					<?
					// TEXT EMAIL ...
					if(in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
						<div <?if(in_array($FIELD_SID,$arParams['HIDE_QUESTIONS'])):?> style="display:none;"<?endif;?>><!--float: left/right-->
							<?if($arParams['PLACEHOLDER_MODE']!="Y"):?><label><?=$arQuestion["CAPTION"]?><?if($arQuestion["REQUIRED"]=="Y"):?><span class="required">*</span><?endif?></label><?endif;?>
							<div class="text">
								<input
									name="form_<?=$arQuestion['STRUCTURE'][0]['FIELD_TYPE']?>_<?=$arQuestion['STRUCTURE'][0]['ID']?>"
									type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel": $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"
									data-type="<?=$arQuestion['STRUCTURE'][0]['FIELD_PARAM']?>"
								    value="<?=$arResult['arrVALUES'][$FIELD_SID]?:null?>"
								    <?if( ($arParams['PLACEHOLDER_MODE']=="Y") || ($arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE") ){?> placeholder="<?=$arQuestion["CAPTION"]?>" <?}?>
									/>
							</div>
						</div>
					<?
					// DROPDOWN
					elseif($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='dropdown')://__($arQuestion)?>
						<div <?if(in_array($FIELD_SID,$arParams['HIDE_QUESTIONS'])):?> style="display:none;"<?endif;?>>
                            <?if($arParams['PLACEHOLDER_MODE']!="Y"):?><label><?=$arQuestion["CAPTION"]?><?if($arQuestion["REQUIRED"]=="Y"):?><span class="required">*</span><?endif?></label><?endif;?>
							<div class="text">
								<?=$arQuestion["HTML_CODE"]?>
							</div>
						</div>
					<?
					// TEXTAREA
					elseif($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='textarea'):?>
						</div>
						<label for="userMess"><?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?></label>
						<div class="textarea"><?=$arQuestion["HTML_CODE"]?></div>
						<div class="clearfix">
					<?
					// FILE
					elseif($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='file'):?>
						<div <?if(in_array($FIELD_SID,$arParams['HIDE_QUESTIONS'])):?> style="display:none;"<?endif;?>>
							<label><?=$arQuestion["CAPTION"]?></label>
							<?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
							<div class="text" style="height: 30px; line-height: 30px"><?=$arQuestion["HTML_CODE"]?></div>
						</div>
					<?
                    // checkbox
                    elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='checkbox' && $FIELD_SID!=$arParams["SUBSCRIBE_QUESTION_CODE"]):?>
						<div class="block-checkbox" <?if(in_array($FIELD_SID,$arParams['HIDE_QUESTIONS'])):?> style="display:none;"<?endif;?>>
							<label><?=$arQuestion["CAPTION"]?><?if($arQuestion["REQUIRED"]=="Y"):?><span class="required">*</span><?endif?></label>
							<div class="text">
								<?=$arQuestion["HTML_CODE"]?>
							</div>
						</div>
                    <?
                    // radio
                    elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='radio'):?>
                        <div class="block-radiobox" <?if(in_array($FIELD_SID,$arParams['HIDE_QUESTIONS'])):?> style="display:none;"<?endif;?>>
                            <?=$arQuestion["HTML_CODE"]?>
                        </div>
                    <?
                    // hidden
					elseif($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='hidden'):$k--;?>
						<?array_push($arHiddenID, $FIELD_SID);?>
					<?endif;?>
				<?endforeach;?>
			</div>

			<?if($arResult["isUseCaptcha"] == "Y"):
                switch ($arResult['modeUseCaptcha']) {
                    case 'recaptcha':
                        ?>
                        <div class="j-recaptcha">
                            <?include($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/pb.main/tools/re_captcha.php");?>
                            <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"/>
                        </div>
                        <?break;
                    default:
                        $frame = $this->createFrame()->begin('');
                        ?>
                        <div class="clearfix">
                            <div class="float-left">
                                <label><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><span class="required">*</span></label>
                                <div class="text">
                                    <input type="text" name="captcha_word" size="30" maxlength="50" value="" data-validate="CAPTCHA" />
                                </div>
                            </div>
                            <div class="float-right">
                                <input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" />
                                <img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" style="margin-top: 15px" />
                            </div>
                        </div>
                 <? $frame->end();
                    break;
                }?>
			<?endif;?>

			<?// SUBMIT ?>
			<? $showFormDescr=false; if(!empty($arResult["FORM_DESCRIPTION"]) && $arParams['SHOW_FORM_DESCRIPTION'] == "N" && $arParams['NORMAL_WIDTH']=='Y'){ $showFormDescr=true; }?>
			<div class="popup-button<?if($showFormDescr):?> left-center<?endif;?>">
				<input type="submit" class="btn btn-big btn-yellow" name="web_form_submit" value="<?=$arParams['MESS']['SUBMIT_BTN']?:(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("PB_MAIN_BEFSEND_FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>">
				<?if($showFormDescr):?>
					<div class="inline-block form-descr-footer"><?=$arResult["FORM_DESCRIPTION"]?></div>
					<div class="clearfix"></div>
				<?endif;?>
				<?if($arParams['SHOW_CANCEL_BTN']=="Y"):?>
					<div class="inline-block"><?=\PB\Main\Loc::getMessage('PB_MAIN_BEFSEND_CT_GENERAL_OR')?> <span class="j-close"><?=\PB\Main\Loc::getMessage('PB_MAIN_BEFSEND_CT_GENERAL_REJECT')?></span></div>
				<?endif?>
                <?if($arParams['HIDE_PRIVACY_POLICE']!='Y'):?>
                    <div class="policy-text inline-block"><?include($_SERVER["DOCUMENT_ROOT"].$this->__component->GetPath()."/include/privacy_police.php");?></div>
                <?endif;?>
                <?if($arParams['ESPUTNIK_SUBSCRIBE_ON'] == 'Y' && $arParams['OFF_ESPUTNIK_SUBSCRIBE']!='Y' && is_array($arResult['QUESTIONS']['EMAIL']) && !in_array('EMAIL',$arParams['HIDE_QUESTIONS'])):?>
                    <div class="esputnik-text inline-block"><?include($_SERVER["DOCUMENT_ROOT"].$this->__component->GetPath()."/include/esputnik_subscribe.php");?></div>
                <?endif;?>
            </div>
			<?
			// HIDDENS
			if(!empty($arHiddenID)) {
				$k--;
				foreach($arHiddenID AS $hiddenID){
					echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
				}
			}?>
			<?if(!empty($arParams['COMPONENT_MARKER'])):?>
				<input type="hidden" name="COMPONENT_MARKER" value="<?=$arParams['COMPONENT_MARKER']?>">
			<?endif;?>

			<?
			// SUBSCRIBE CHECKBOX
			if(!empty($arParams['SUBSCRIBE_QUESTION_CODE']) && !empty($arParams['EMAIL_QUESTION_CODE'])):
				$CHECKBOX = $arResult["QUESTIONS"][$arParams["SUBSCRIBE_QUESTION_CODE"]]["STRUCTURE"][0];
				if(is_array($CHECKBOX)):?>
					<div class="line-subscribe">
						<div class="hr-subscribe"></div>
						<div class="check-subscribe">
							<input class="checkbox" type="checkbox" name="form_<?=$CHECKBOX["FIELD_TYPE"]?>_<?=$CHECKBOX["ID"]?>" id="form_<?=$CHECKBOX["FIELD_TYPE"]?>_<?=$CHECKBOX["ID"]?>"/>
							<label class="checkbox" for="form_<?=$CHECKBOX["FIELD_TYPE"]?>_<?=$CHECKBOX["ID"]?>"></label>
						</div>
						<label for="form_<?=$CHECKBOX["FIELD_TYPE"]?>_<?=$CHECKBOX["ID"]?>">
                            <?=\PB\Main\Loc::getMessage('PB_MAIN_BEFSEND_CT_GENERAL_SUBSCRIBE1')?>
						</label>
						<div class="clearfix"><?=\PB\Main\Loc::getMessage('PB_MAIN_BEFSEND_CT_GENERAL_SUBSCRIBE2')?></div>
					</div>
				<?endif;?>
			<?endif;?>
		</form>
	</div>
</div>
<?=PB\Main\LazyLoad::getInstance()->end();?>

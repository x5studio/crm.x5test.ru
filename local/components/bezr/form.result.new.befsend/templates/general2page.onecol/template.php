<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$PARENT = $this->__component->__parent;?>
<div id="general2page-form-<?=$arResult["SOULT_FORM"]?>"
	 class="j-form-page fbox"
	 data-salt-form="<?= $arResult["SOULT_FORM"] ?>"
	>
<?
/**********************************************************************************
  form header
 **********************************************************************************/
	if ($arResult["isFormTitle"])
	{?>
		<?if($arParams["SHOW_TITLE"] != "N"):?>
		<div class="h2 <?if($arParams["SECTION_TITLE"] == "Y"):?>section-title margin-bottom<?endif?>"><?=$arResult["FORM_TITLE"]?></div>
	<?endif?>
	<?
	} //endif ;
	?>

	<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

	<?if($arResult["FORM_NOTE"] && $arParams['SHOW_FORM_NOTE'] != "N"):?><p><?=$arResult["FORM_NOTE"]?></p><?endif?>

	<?if($arResult["isFormDescription"] == "Y" && $arParams['SHOW_FORM_DESCRIPTION'] != "N"):?>
		<p><?=$arResult["FORM_DESCRIPTION"]?></p>
	<?endif?>


	<form name="<?=$arResult["arForm"]["SID"]?>" action="<?=POST_FORM_ACTION_URI?>"
	      method="POST" enctype="multipart/form-data">
		<input type="hidden" name="sessid" id="sessid" value="" />
		<input type="hidden" name="WEB_FORM_ID" value="<?=$arParams['WEB_FORM_ID']?>" />

		<?$arHiddenID = Array();?>
		<div class="clearfix">
			<?$k=0;foreach($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):$k++;?>
				<?
				// TEXT EMAIL ...
				if(in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
					<div class="text" <?if(in_array($FIELD_SID,$arParams['HIDE_QUESTIONS'])):?> style="display:none;"<?endif;?>>
						<input
							name="form_<?=$arQuestion['STRUCTURE'][0]['FIELD_TYPE']?>_<?=$arQuestion['STRUCTURE'][0]['ID']?>"
							type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel": $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"<?if(!empty($arQuestion['STRUCTURE'][0]['FIELD_PARAM'])) {?>
							data-type="<?=$arQuestion['STRUCTURE'][0]['FIELD_PARAM']?>"<?}?>
							value="<?=$arResult['arrVALUES'][$FIELD_SID]?:null?>"
						    placeholder="<?=$arQuestion["CAPTION"]?>"
							/>
					</div>
				<?
				// DROPDOWN
				elseif($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='dropdown')://__($arQuestion)?>
					<div class="text">
						<?=$arQuestion["HTML_CODE"]?>
					</div>
				<?
				// TEXTAREA
				elseif($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='textarea'):?>
					<div class="textarea"><?=$arQuestion["HTML_CODE"]?></div>
				<?
				// FILE
				elseif($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='file'):?>
                    <div class="inline-block j-file-input text">
                        <input <?if ($arQuestion["REQUIRED"] == "Y"){?>class="j-required"<?}?> type="file" data-code="FILE" name="form_<?=$arResult["QUESTIONS"]["FILE"]["STRUCTURE"][0]["FIELD_TYPE"]."_".$arResult["QUESTIONS"]["FILE"]["STRUCTURE"][0]["ID"]?>" size="1" value="" data-type="<?if ($arQuestion["REQUIRED"] == "Y"){?>REQUIRED<?}?>"/>
                        <span class="strong dotted"><?=$arQuestion["CAPTION"]?></span>
                    </div>
                    <div class="inline-block" style="display: none;" id="j-file-loaded"><?=\PB\Main\Loc::getMessage("PB_MAIN_BEFSEND_CT_G2POC_ATTACH")?><?=$arQuestion["CAPTION"]?></div>
				<?elseif($arQuestion['STRUCTURE'][0]['FIELD_TYPE']=='hidden'):$k--;?>
					<?array_push($arHiddenID, $FIELD_SID);?>
				<?endif;?>
			<?endforeach;?>
		</div>

		<?if($arResult["isUseCaptcha"] == "Y"):
            switch ($arResult['modeUseCaptcha']) {
                case 'recaptcha':
                ?>
                <div class="j-recaptcha">
                    <?include($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/pb.main/tools/re_captcha.php");?>
                    <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"/>
                </div>
                <?break;
                default:
                $frame = $this->createFrame()->begin('');
                ?>
                    <div class="clearfix">
                        <div class="float-left">
                            <label><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><span class="required">*</span></label>
                            <div class="text">
                                <input type="text" name="captcha_word" size="30" maxlength="50" value="" data-validate="CAPTCHA" />
                            </div>
                        </div>
                        <div class="float-right">
                            <input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" style="margin-top: 15px" />
                        </div>
                    </div>
                <? $frame->end();
                break;
            }?>
		<?endif;?>

		<?// SUBMIT ?>
		<div class="panel-button">
			<input type="submit" class="btn btn-big btn-yellow width100" name="web_form_submit" value="<?=$arParams['MESS']['SUBMIT_BTN']?:(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? \PB\Main\Loc::getMessage("PB_MAIN_BEFSEND_CT_G2POC_FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>">
			<?if($arParams['SHOW_CANCEL_BTN']=="Y"):?>
				<div class="inline-block"><?=\PB\Main\Loc::getMessage('PB_MAIN_BEFSEND_CT_G2POC_OR')?> <span class="j-close"><?=\PB\Main\Loc::getMessage('PB_MAIN_BEFSEND_CT_G2POC_REJECT')?></span></div>
			<?endif?>
            <?if($arParams['HIDE_PRIVACY_POLICE']!='Y'):?>
                <div class="policy-text inline-block"><?include($_SERVER["DOCUMENT_ROOT"].$this->__component->GetPath()."/include/privacy_police.php");?></div>
            <?endif;?>
            <?if($arParams['ESPUTNIK_SUBSCRIBE_ON'] == 'Y' && $arParams['OFF_ESPUTNIK_SUBSCRIBE']!='Y' && is_array($arResult['QUESTIONS']['EMAIL']) && !in_array('EMAIL',$arParams['HIDE_QUESTIONS'])):?>
                <div class="esputnik-text inline-block"><?include($_SERVER["DOCUMENT_ROOT"].$this->__component->GetPath()."/include/esputnik_subscribe.php");?></div>
            <?endif;?>
        </div>
		<?
		// HIDDENS
		if(!empty($arHiddenID)) {
			$k--;
			foreach($arHiddenID AS $hiddenID){
				echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
			}
		}?>
		<?if(!empty($arParams['COMPONENT_MARKER'])):?>
			<input type="hidden" name="COMPONENT_MARKER" value="<?=$arParams['COMPONENT_MARKER']?>">
		<?endif;?>

		<?
		// SUBSCRIBE CHECKBOX
		if(!empty($arParams['SUBSCRIBE_QUESTION_CODE']) && !empty($arParams['EMAIL_QUESTION_CODE'])):
			$CHECKBOX = $arResult["QUESTIONS"][$arParams["SUBSCRIBE_QUESTION_CODE"]]["STRUCTURE"][0];
			if(is_array($CHECKBOX)):?>
				<div class="line-subscribe">
					<div class="hr-subscribe"></div>
					<div class="check-subscribe">
						<input class="checkbox" type="checkbox" checked name="form_<?=$CHECKBOX["FIELD_TYPE"]?>_<?=$CHECKBOX["ID"]?>" id="form_<?=$CHECKBOX["FIELD_TYPE"]?>_<?=$CHECKBOX["ID"]?>"/>
						<label class="checkbox" for="form_<?=$CHECKBOX["FIELD_TYPE"]?>_<?=$CHECKBOX["ID"]?>"></label>
					</div>
					<label for="form_<?=$CHECKBOX["FIELD_TYPE"]?>_<?=$CHECKBOX["ID"]?>">
					<?=\PB\Main\Loc::getMessage('PB_MAIN_BEFSEND_CT_G2POC_SUBSCRIBE_1')?>
					</label>
					<div class="clearfix"><?=\PB\Main\Loc::getMessage('PB_MAIN_BEFSEND_CT_G2POC_SUBSCRIBE_2')?></div>
				</div>
			<?endif;?>
		<?endif;?>
	</form>
</div>
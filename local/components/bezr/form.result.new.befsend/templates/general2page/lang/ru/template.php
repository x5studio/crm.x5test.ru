<?
$MESS ['PB_MAIN_BEFSEND_CT_G2P_FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_FORM_APPLY'] = "Применить";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_FORM_ADD'] = "Добавить";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_FORM_DATA_SAVED1'] = "Спасибо!<br><br>Ваша заявка №";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_THANK_YOU'] = "Спасибо за вашу заявку!";
$MESS ['PB_MAIN_BEFSEND_CT_G2P_WAIT_CALL'] = 'Мы свяжемся с вами в ближайшее время.';
?>

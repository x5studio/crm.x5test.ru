<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \PB\Main\Loc;
/***********************************************************************************
 * form header
 ***********************************************************************************/
if ($arResult["isFormTitle"]) {
    ?>
    <? if ($arParams["SHOW_TITLE"] != "N"): ?>
        <div class="h2 <? if ($arParams["SECTION_TITLE"] == "Y"): ?>section-title margin-bottom<? endif ?>"><?= $arResult["FORM_TITLE"] ?></div>
    <? endif ?>
    <?
} //endif ;
?>

<? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>

<? if ($arResult["FORM_NOTE"]): ?><p><?= $arResult["FORM_NOTE"] ?></p><? endif ?>

<? if ($arParams["SHOW_FORM_ALL_STATUSES"] == "Y" || $arResult["isFormNote"] != "Y") {
    ?>
  <div class="j-form-page" data-salt-form="<?= $arResult["SOULT_FORM"] ?>">
    <form name="<?= $arResult["arForm"]["SID"] ?>" action="<?= POST_FORM_ACTION_URI ?>"
          class="j-form"
          method="POST" enctype="multipart/form-data">
        <input type="hidden" name="sessid" id="sessid" value=""/>
        <input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>"/>

        <table>
            <?
            if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y") {
                ?>
                <tr>
                    <td><?

                        if ($arResult["isFormImage"] == "Y") {
                            ?>
                            <a href="<?= $arResult["FORM_IMAGE"]["URL"] ?>" target="_blank"
                               alt="<?= GetMessage("FORM_ENLARGE") ?>"><img
                                    src="<?= $arResult["FORM_IMAGE"]["URL"] ?>"
                                    <? if ($arResult["FORM_IMAGE"]["WIDTH"] > 300): ?>width="300"
                                    <? elseif ($arResult["FORM_IMAGE"]["HEIGHT"] > 200): ?>height="200"<? else: ?><?= $arResult["FORM_IMAGE"]["ATTR"] ?><? endif;
                                ?> hspace="3" vscape="3" border="0"/></a>
                            <? //=$arResult["FORM_IMAGE"]["HTML_CODE"]
                            ?>
                            <?
                        } //endif
                        ?>

                        <? if ($arResult["FORM_DESCRIPTION"]): ?>
                            <p><?= $arResult["FORM_DESCRIPTION"] ?></p><? endif ?>
                    </td>
                </tr>
                <?
            } // endif
            ?>
        </table>
        <?
        /***********************************************************************************
         * form questions
         ***********************************************************************************/
        ?>
        <table id="general2page-form-<?= $arResult["SOULT_FORM"] ?>" class="ftbl">
            <tbody>
            <?
            $arHiddenID = Array();
            foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                ?>
                <? if ($arResult["QUESTIONS"][$FIELD_SID]['STRUCTURE'][0]['FIELD_PARAM'] == 'LABEL_ROW'): ?>
                    <tr>
                        <td colspan="2">
                            <?= $arQuestion["CAPTION"] ?>
                        </td>
                    </tr>
                    <? continue; ?>
                <? endif ?>

                <? if ($arQuestion["STRUCTURE"][0]["FIELD_TYPE"] == "dropdown") {
                    if (strpos($arQuestion["HTML_CODE"], "class") == false) {
                        $arQuestion["HTML_CODE"] = preg_replace("/(<select)/im", "$1 class=\"j-select\"", $arQuestion["HTML_CODE"]);
                    } elseif (strpos($arQuestion["HTML_CODE"], "j-select") == false) {
                        $arQuestion["HTML_CODE"] = preg_replace("/class=\"([^\"]*)\"/im", "class=\"j-select \${1}\"", $arQuestion["HTML_CODE"]);
                    }
                } ?>


                <? if ($arQuestion["STRUCTURE"][0]["FIELD_TYPE"] != "hidden"):
                    if (!empty ($arQuestion['STRUCTURE'][0]['FIELD_PARAM'])) {
                        $arQuestion["HTML_CODE"] = preg_replace("/\b({$arQuestion['STRUCTURE'][0]['FIELD_PARAM']})\b/i", "$1 data-type=\"$1\"", $arQuestion["HTML_CODE"], 1);
                    }
                    ?>
                    <tr>
                        <td <? if ($arParams["STYLE"] == "IN_LINE"): ?>colspan="2" <? else: ?>width="50%"<? endif;
                        ?>>
                            <? if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])): ?>
                                <span class="error-fld" title="<?= $arResult["FORM_ERRORS"][$FIELD_SID] ?>"></span>
                            <? endif;?>
                            <?= $arQuestion["CAPTION"] ?><? if ($arQuestion["REQUIRED"] == "Y"): ?><?= $arResult["REQUIRED_SIGN"]; ?><? endif;?>
                            <?= $arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />" . $arQuestion["IMAGE"]["HTML_CODE"] : "" ?>
                            <? if ($arParams["STYLE"] != "IN_LINE"): ?>
                        </td>
                        <td width="50%"
                            class="text<?= $arQuestion["STRUCTURE"][0]["FIELD_TYPE"] != 'text' ? ' ' . $arQuestion["STRUCTURE"][0]["FIELD_TYPE"] : '' ?>">
                            <? endif;
                            ?>
                            <div
                                class="text<?= $arQuestion["STRUCTURE"][0]["FIELD_TYPE"] != 'text' ? ' ' . $arQuestion["STRUCTURE"][0]["FIELD_TYPE"] : '' ?>">
                                <?= $arQuestion["HTML_CODE"] ?>
                            </div>
                        </td>
                    </tr>
                <? else: ?>
                    <? array_push($arHiddenID, $FIELD_SID); ?>
                <? endif ?>
                <?
            } //endwhile
            ?>
            <?
            if ($arResult["isUseCaptcha"] == "Y") {
                switch ($arResult['modeUseCaptcha']) {
                    case 'recaptcha':
                        ?>
                        <div class="j-recaptcha">
                            <?include($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/pb.main/tools/re_captcha.php");?>
                            <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"/>
                        </div>
                        <?break;
                    default:
                        $frame = $this->createFrame()->begin('');
                        ?>
                        <tr>
                            <th colspan="2"><h3 style="margin-bottom: 0"><?= GetMessage("FORM_CAPTCHA_TABLE_TITLE") ?></h3>
                            </th>
                        </tr>
                        <tr>
                            <td <? if ($arParams["STYLE"] == "IN_LINE"): ?>colspan="2"<? endif;
                            ?>>
                                <?= GetMessage("FORM_CAPTCHA_FIELD_TITLE") ?><?= $arResult["REQUIRED_SIGN"]; ?>
                                <? if ($arParams["STYLE"] != "IN_LINE"): ?>
                            </td>
                            <td class="text">
                                <? endif;
                                ?>
                                <input type="hidden" name="captcha_sid"
                                       value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"/>
                                <img
                                    src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"
                                    width="180" height="40"/>
                                <input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext"
                                       data-validate="CAPTCHA"/>
                            </td>
                        </tr>
                    <? $frame->end();
                    break;
                }
            } // isUseCaptcha
            ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="2">
                    <input <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?>
                        class="btn btn-big btn-yellow btn-big-wide" type="submit" name="web_form_submit"
                        value="<?= strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? Loc::getMessage("PB_MAIN_BEFSEND_CT_G2P_FORM_ADD") : $arResult["arForm"]["BUTTON"]; ?>"/>
                </th>
            </tr>
            </tfoot>
        </table>
        <?if($arParams['HIDE_PRIVACY_POLICE']!='Y'):?>
            <p class="policy-text"><?include($_SERVER["DOCUMENT_ROOT"].$this->__component->GetPath()."/include/privacy_police.php");?></p>
        <?endif;?>
        <?if($arParams['ESPUTNIK_SUBSCRIBE_ON'] == 'Y' && $arParams['OFF_ESPUTNIK_SUBSCRIBE']!='Y' && is_array($arResult['QUESTIONS']['EMAIL']) && !in_array('EMAIL',$arParams['HIDE_QUESTIONS'])):?>
            <div class="esputnik-text inline-block"><?include($_SERVER["DOCUMENT_ROOT"].$this->__component->GetPath()."/include/esputnik_subscribe.php");?></div>
        <?endif;?>
        <p>
            <?= $arResult["REQUIRED_SIGN"]; ?> - <?= Loc::getMessage("PB_MAIN_BEFSEND_CT_G2P_FORM_REQUIRED_FIELDS") ?>
        </p>
        <? if (!empty($arHiddenID)) {
            foreach ($arHiddenID AS $hiddenID) {
                echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
            }
        } ?>
        <? if (!empty($arParams['COMPONENT_MARKER'])): ?>
            <input type="hidden" name="COMPONENT_MARKER" value="<?= $arParams['COMPONENT_MARKER'] ?>">
        <? endif; ?>
    </form>
  </div>
    <?
} //endif (isFormNote)
?>

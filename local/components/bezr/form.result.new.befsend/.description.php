<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use PB\Main\Loc;

$arComponentDescription = array(
	"NAME" => Loc::getMessage("PB_MAIN_BEFSEND_FORM_RESULT_NEW_COMPONENT_NAME"),
	"DESCRIPTION" => Loc::getMessage("PB_MAIN_BEFSEND_FORM_RESULT_NEW_COMPONENT_DESCR"),
	"ICON" => "/images/comp_result_new.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "service",
		"CHILD" => array(
			"ID" => "form",
			"NAME" => Loc::getMessage("PB_MAIN_BEFSEND_FORM_SERVICE"),
			"CHILD" => array(
				"ID" => "form_cmpx",
			),
		)
	),

	/*"AREA_BUTTONS" => array(
		array(
			'URL' => "javascript:EditFormTpl()",
			'ICON' => 'form_edit_tpl',
			'TITLE' => GetMessage("FORM_PUBLIC_ICON_EDIT_TPL")
		),	
	),*/
);
?>
<?php

function validatePhone($phone)
{
    // сравнение со строчными символами
    $validCharacters = [
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '+',
        '(',
        ')',
        '-'
    ];

    $symbols = array_filter(str_split($phone), function ($value) {
        return !is_null($value) && $value !== ' ';
    });

    foreach ($symbols as $symbol) {
        if (!in_array($symbol, $validCharacters)) {
            return false;
        }
    }

    preg_match_all('/[\d]/', $phone, $digits);

    // самое минимальное количество цифр в телефоне
    if (count($digits[0]) < 10) {
        return false;
    }

    return true;
}

function validateEmail($email)
{

    if (!check_email($email)) {
        return false;
    }

    return true;
}
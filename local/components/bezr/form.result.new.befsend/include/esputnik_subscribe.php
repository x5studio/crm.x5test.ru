<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

echo <<<EOL
<div class="check-esputnik-subscribe">
    <label>
      <input class="checkbox" type="checkbox" name="esputnik-subscribe-checkbox" id="checkbox-esputnik-subscribe" checked/> {$arParams['ESPUTNIK_TEXT_SUBSCRIBE']}
    </label>
</div>
EOL
;

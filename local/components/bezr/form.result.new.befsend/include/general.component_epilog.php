<?

use PB\Main\Loc;

if ($arResult["isFormErrors"] == "Y" ||
    ($_COOKIE["WEBFORM_" . $arResult["SOULT_FORM"]] && (empty($arParams['COMPONENT_MARKER']) ||
            $_REQUEST['marker'] == $arParams['COMPONENT_MARKER']) && $_REQUEST['WEB_FORM_ID'] == $arParams['WEB_FORM_ID'] && $_REQUEST['formresult'] == $arParams['FORM_RESULT_ADDOK'])
): ?>
    <?
    if ($arResult["isFormErrors"] == "Y") {
        $title = $arParams["MESS"]["ERROR"] ?: GetMessage("ERROR");
        $msg = $arParams["MESS"]["FORM_ERRORS_TEXT"] ?: $arResult["FORM_ERRORS_TEXT"];
    }
    if ($_REQUEST['formresult'] == $arParams['FORM_RESULT_ADDOK'] && $arResult["isFormErrors"] != "Y") {
        $title = htmlspecialchars_decode($arParams["MESS"]["THANK_YOU"] ?: Loc::getMessage("PB_MAIN_BEFSEND_THANK_YOU"));
        $msg = $arParams["MESS"]["WAIT_CALL"] ?: Loc::getMessage("PB_MAIN_BEFSEND_WAIT_CALL");
        if ($_COOKIE['ESPUTNIK_SUBSCRIBED'] == 'Y') {
            unset($_COOKIE['ESPUTNIK_SUBSCRIBED']);
            setcookie('ESPUTNIK_SUBSCRIBED', null, -1, '/');
            $arEmail = CFormResult::GetDataByID($_REQUEST['RESULT_ID'], array("EMAIL"), $arResult2, $arAnswer2);
            $ehost = explode('@', $arEmail['EMAIL'][0]['USER_TEXT']);
            $etext = '<a href="http://' . $ehost[1] . '">' . $arEmail['EMAIL'][0]['USER_TEXT'] . '</a>';
            $msg2 = str_replace('#email_host#', $etext, ($arParams["POPUP_TEXT"] ?: ""));
            $msg2 = str_replace(PHP_EOL, "<br>", ($msg2 ?: ""));
            $msg = str_replace('#email_host#', $etext, ($msg ?: ""));
            $msg = str_replace(PHP_EOL, "<br>", ($msg ?: ""));
        } else {
            $arEmail = CFormResult::GetDataByID($_REQUEST['RESULT_ID'], array("EMAIL"), $arResult2, $arAnswer2);
            $ehost = explode('@', $arEmail['EMAIL'][0]['USER_TEXT']);
            $etext = '<a href="http://' . $ehost[1] . '">' . $arEmail['EMAIL'][0]['USER_TEXT'] . '</a>';
            $msg = str_replace('#email_host#', $etext, ($msg ?: ""));
            $msg = str_replace(PHP_EOL, "<br>", ($msg ?: ""));

        }
        //добавляем куку для блока "Не уходи"
        if ($arParams["BTN_CALL_FORM"] == "#before_exit_popup__data" && $arParams["DONT_RESHOW"]) {
            $host = parse_url($_SERVER['HTTP_HOST'], PHP_URL_HOST);
            $arHost = array_reverse(explode(".", $host));
            setcookie("ne_uhodi_used", true, time() + 360360000, "/", $arHost[1] . "." . $arHost[0]);
        }
        if ($arParams['SKIP_SPUTNIK_MSG'] == 'Y') $msg2 = '';
    }
    $time = 10000;
    ?>
    <div id="popup">
        <div class="h3"><?= $title ?></div>
        <p>
            <?= $msg ?>
        </p>
        <? if (strlen($msg2) > 0) { ?>
            <br/>
            <p>
                <?= $msg2 ?>
            </p>
        <? } ?>
        <? if ($arParams['ALERT_ADD_SHARE'] == "Y") {
            echo "<p>";
            $APPLICATION->IncludeComponent("bezr:empty", "social_share_ya", array("CACHE_TIME" => "N"));
            echo "</p>";
            $time = 10000;
        }
        foreach (GetModuleEvents("pb.main", "WriteComponentBannerPopup", true) as $arEvent) {
            ExecuteModuleEventEx($arEvent, array(&$this));
        }
        ?>
    </div>
    <script type="text/javascript">
        function popupCloseListener() {
            window.popupIsClosed = false;
            var iterator = <?=$time?>;
            var timer = setInterval(function (){
                if (iterator === 0 || window.popupIsClosed) {
                    clearInterval(timer);
                    $.fancybox.close();
                }
                iterator = iterator - 1000;
            }, 1000);

        };


        (function (f) {
            if (window.jQuery)
                $(document).ready(f);
            else
                document.addEventListener('DOMContentLoaded', f)
        })(
            function () {
                require(['main'], function () {
                    require(['Controls/Responsive', 'fancybox', 'cookie', 'tools/tools'], function (obResponsive) {
                        if ($.inArray(obResponsive.getCurType(), ['xsmall']) >= 0) {
                            var topRatio = 0;
                        } else {
                            var topRatio = 0.5;
                        }

                        var afterCloseFunction = function () {
                            window.popupIsClosed = true;
                        }

                          var afterLoadFunction = function () {
                            var offset = getUrlParam('offset', 0);
                            if (offset) {
                                $('html, body').animate({
                                    scrollTop: offset
                                }, 0);
                            }

                            $(document).trigger(
                                "form.result.popup.open",
                                [{
                                    'error': <?=$arResult["isFormErrors"] != "Y" ? 'false' : 'true';?>,
                                    'formId': '<?=$arResult['arForm']['ID']?>',
                                    'formSalt': '<?=$arResult["SOULT_FORM"]?>'
                                }]
                            );

                              popupCloseListener();

                                //setTimeout(function () {
                                //     $.fancybox.close();
                                //}, <?//=$time?>//);
                            deleteCookie("WEBFORM_<?=$arResult["SOULT_FORM"]?>", "/");
                            <?if($arResult["isFormErrors"] != "Y"):?>
                            // emulate path for analytics target
                            var path = getCookie("trackStep");
                            if (path) {
                                deleteCookie("trackStep");
                                if (typeof window.trackStep === 'function') {
                                    window.trackStep(path + "/virtual/add-result-ok/");
                                }
                            }
                            <?endif;?>
                        }

                        var fancyBoxVersion = $.fancybox.version.split('.')[0];

                        <? if ($arParams["FANCYBOX_FALSE"] != false && $arParams["FANCYBOX_HIDDEN"] != 'Y') { ?>

                        if (fancyBoxVersion === '3') {
                            $.fancybox.open({
                                src: '#popup',
                                type: 'inline',
                                opts: {
                                    closeExisting: false,
                                    baseTpl: '<div class="fancybox-container fancybox-wrap " tabindex="-1" role="dialog"  ><div class="fancybox-bg"></div><div class="fancybox-skin" ><div class="fancybox-outer"><div class="fancybox-inner" ><div class="fancybox-stage"></div></div></div></div></div>',
                                    afterShow: afterLoadFunction,
                                    afterClose: afterCloseFunction
                                }
                            });
                        } else {
                            $.fancybox.open('#popup', {
                                padding: 0,
                                margin: 0,
                                topRatio: topRatio,
                                wrapCSS: 'inner-close',
                                afterLoad: afterLoadFunction,
                                afterClose: afterCloseFunction
                            });
                        }

                        <? } ?>


                    });
                });
                if (typeof window.track === 'function') {
                    <?if($arResult["isFormErrors"] != "Y"):?>
                    window.track(['<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_FORMS')?>',
                        '<?=!empty($arParams['GA_SEND']['ACTION']) ? $arParams['GA_SEND']['ACTION'] : Loc::getMessage('PB_MAIN_BEFSEND_TRACK_SUCC')?>',
                        '<?=!empty($arParams['GA_SEND']['TAG']) ? $arParams['GA_SEND']['TAG'] : $arResult['arForm']['NAME'] . " - " . $arResult['arForm']['ID']?>', 1]);

                    <?if($arParams['GA_SEND']['ACTION2']):?>
                    window.track(['<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_FORMS')?>',
                        '<?=$arParams['GA_SEND']['ACTION2']?>',
                        '<?=!empty($arParams['GA_SEND']['TAG']) ? $arParams['GA_SEND']['TAG'] : $arResult['arForm']['NAME'] . " - " . $arResult['arForm']['ID']?>', 1]);
                    <?endif;?>

                    <?
                    if($arParams["EMAIL_ONLY_FROM_RESIPIENTS"] == 'N' && !empty($arParams["CITY_QUESTION_CODE"]) &&
                !empty($arParams["OFFICE_QUESTION_CODE"]) &&
                !empty($_REQUEST['RESULT_ID']) && empty($arParams['SERVICE_SECT_ID'])){
                    $arAnswer = CFormResult::GetDataByID($_REQUEST['RESULT_ID'], array("OFFICE"), $arResultAns, $arAnswer2);
                    if(!empty($arAnswer['OFFICE'][0]['USER_TEXT'])){?>
                    window.track(['<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_FORM_SEND')?>',
                        '<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_OFFICE_FROM_QUEUE')?>',
                        '<?=$arAnswer['OFFICE'][0]['USER_TEXT'] . " - " . $arResult['arForm']['ID']?>', 1]); <?
                    }
                    }
                    ?>
                    <?else:?>
                    window.track(['<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_FORMS')?>', '<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_ERROR')?>', '<?=$arResult['arForm']['NAME'] . " - " . $arResult['arForm']['ID']?>', 1]);
                    <?endif;?>
                }
            }
        )
    </script>
<? endif; ?>

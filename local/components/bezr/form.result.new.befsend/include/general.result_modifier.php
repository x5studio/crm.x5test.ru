<?php

use PB\Contacts\ContactManager;
use PB\Main\FilterHelper;
use \PB\Main\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var ContactManager $contMng */

global $obOfficeFilter;
$contMng = ContactManager::getInstance();

$obCache = \Bitrix\Main\Data\Cache::createInstance();
$cacheId = Array(
    $arResult["SOULT_FORM"],
    $_SESSION['REGION']['ID'],
    $arResult["arrVALUES"],
    $arParams['BASE_PRODUCT_NAME'],
    $arParams['SERVICE_SECT_ID'],
    $arParams['HIDE_OFFICE_TEXT_AFTER_SELECT']
);
if (empty($_GET)) {
    $cacheId[] = Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage();
} else {
	$region = !empty($_SESSION["REGION"]["ID"]) ? $_SESSION["REGION"]["ID"] : \Bitrix\Main\Config\Option::get("pb.geo", "MSK");
    $cacheId[] = $contMng->getOfficeIDs($obOfficeFilter, array("IBLOCK_SECTION_ID" => $region));
}
$cacheId = serialize($cacheId);

if ($obCache->initCache($arParams['CACHE_TIME'], $cacheId, "/form.result.new.befsend/{$arResult["SOULT_FORM"]}/")) {
    $vars = $obCache->getVars();
    $arResult["QUESTIONS"][$arParams["CITY_QUESTION_CODE"]] = $vars['CITY'];
    $arResult["QUESTIONS"][$arParams["OFFICE_QUESTION_CODE"]] = $vars['OFFICE'];
    $arResult["QUESTIONS"][$arParams["PRODUCT_QUESTION_CODE"]] = $vars['PRODUCT'];
    $arResult["QUESTIONS"][$arParams["TO_QUESTION_CODE"]] = $vars['TO'];
} elseif ($obCache->startDataCache()) {

    CModule::IncludeModule("iblock");

    if (!empty($arResult["arrVALUES"])) {
        $obCache->abortDataCache();
    }

    $CITY = &$arResult["QUESTIONS"][$arParams["CITY_QUESTION_CODE"]];
    $OFFICE = &$arResult["QUESTIONS"][$arParams["OFFICE_QUESTION_CODE"]];
    $PRODUCT = &$arResult["QUESTIONS"][$arParams["PRODUCT_QUESTION_CODE"]];
    $TO = &$arResult["QUESTIONS"][$arParams["TO_QUESTION_CODE"]];

    $arListRegionOfficeHidden = unserialize(\Bitrix\Main\Config\Option::get('pb.main', 'LIST_REGION_OFFICE_HIDDEN'));
    $arListRegionOfficeQuestion = unserialize(\Bitrix\Main\Config\Option::get('pb.main', 'LIST_REGION_OFFICE_QUESTION'));

// Город
    if (!empty($CITY)) {
        global $obCityFilter;
        $cityFilter = array();
        if (isset($arParams["CITY_FILTER"]) && !empty($arParams["CITY_FILTER"])) {
            foreach ($arParams["CITY_FILTER"] as $code => $value) {
                $cityFilter[$code] = $value;
            }
        }
        $FIELD_PARAM = $CITY["STRUCTURE"][0]["FIELD_PARAM"];
        $validateCSSClass = !empty($FIELD_PARAM) ? " validate-{$FIELD_PARAM}" : '';

        if (
            $arParams["STRONG_SHOW_CITY"] == 'Y'
            || (
                $CITY["STRUCTURE"][0]["FIELD_TYPE"] != "hidden" &&
                !(in_array($_SESSION['REGION']['ID'], (array)$arListRegionOfficeHidden) || in_array('all', (array)$arListRegionOfficeHidden)) &&
                !(in_array($_SESSION['REGION']['ID'], (array)$arListRegionOfficeQuestion) || in_array('all', (array)$arListRegionOfficeQuestion))
            )
        ) {
            $CITY_VALUE = &$arResult["arrVALUES"]["form_" . $CITY["STRUCTURE"][0]["FIELD_TYPE"] . "_" . $CITY["STRUCTURE"][0]["ID"]];

            $selectCITY = $contMng->getViewInstance()->getSelectCity(
                'form_' . $CITY["STRUCTURE"][0]["FIELD_TYPE"] . '_' . $CITY["STRUCTURE"][0]["ID"],
                Array(
                    'SELECTED' => $CITY_VALUE ?: ($arParams["INNER_CITY_ID"] ?: $_SESSION["REGION"]["ID"])
                ),
                Array(
                    'id' => 'userCity' . $arResult["SOULT_FORM"],
                    'class' => 'j-select j-related-selects rsg-location select-288' . $validateCSSClass,
                ),
                $obCityFilter,
                $cityFilter
            );
            $CITY["HTML_CODE"] = $selectCITY;
            $CITY["STRUCTURE"][0]["FIELD_TYPE"] = "dropdown";
            $CITY["CUSTOM_LIST"] = "Y";
        } else {
            $hiddenCity = $contMng->getViewInstance()->getHiddenCity(
                'form_' . $CITY["STRUCTURE"][0]["FIELD_TYPE"] . '_' . $CITY["STRUCTURE"][0]["ID"],
                Array(
                    'ID' => $_SESSION['REGION']['ID']
                ),
                Array(
                    'id' => 'userCity' . $arResult["SOULT_FORM"],
                    'class' => 'j-related-selects rsg-location' . $validateCSSClass,
                )
            );

            $CITY["HTML_CODE"] = $hiddenCity;
            $CITY["STRUCTURE"][0]["FIELD_TYPE"] = "hidden";
        }
    }

//Офис
    if (!empty($OFFICE)) {
    	$FIELD_PARAM = $OFFICE["STRUCTURE"][0]["FIELD_PARAM"];
        $isOfficeHidden = false;
        $validateCSSClass = !empty($FIELD_PARAM) ? " validate-{$FIELD_PARAM}" : '';
        $scrollOfficeClass = (!empty($arParams['NEED_SCROLL_OFFICE']) && $arParams['NEED_SCROLL_OFFICE']) ? " j-select" : " j-select-smart";
		$OFFICE["SELECT_OFFICE_BLOCK"] = "N";
		$hideOfficeTextAfterSelect = false;
		if($arParams['HIDE_OFFICE_TEXT_AFTER_SELECT'] == 'Y') {
			$hideOfficeTextAfterSelect = true;
		}
        // feature-23140576 дополнение функионала выбора офиса локальной настройкой из компонента
        if ($arParams["STRONG_HIDE_OFFICE"] != 'Y' && !(in_array($_SESSION['REGION']['ID'], (array)$arListRegionOfficeHidden) || in_array('all', (array)$arListRegionOfficeHidden))||$arParams["STRONG_SHOW_OFFICE"] == 'Y' || in_array($_SESSION['REGION']['ID'], (array)$arParams['CITY_SHOW_OFFICE'] ) ) {
        	$selectOfficeHtml ="";
            $arOffices = $contMng->getOfficeList($obOfficeFilter, array("IBLOCK_SECTION_ID" => $_SESSION["REGION"]["ID"]));
        // feature-23140576 дополнение функионала выбора офиса из очереди если офис не выбран
            if (
                !(
                    in_array($_SESSION['REGION']['ID'], (array)$arListRegionOfficeQuestion) ||
                    in_array('all', (array)$arListRegionOfficeQuestion)
                    || in_array($_SESSION['REGION']['ID'], (array)$arParams['CITY_SHOW_OFFICE'])
                )
            ) {
				$optionArr = Array(
                    'EMPTY' => Loc::getMessage('PB_MAIN_BEFSEND_SELECT_OFFICE'),
                );      	
            }     
            else {
            	$optionArr = Array(
            		'EMPTY' => Loc::getMessage('PB_MAIN_BEFSEND_SELECT_OFFICE'),
            		'QUEUE' => 'queue'
            	);
            	$selectOfficeHtml = '<div class="selectOfficeFormHidden" data-hide-office-text="'.$arParams['HIDE_OFFICE_TEXT_AFTER_SELECT'].'">'.Loc::getMessage('PB_MAIN_BEFSEND_SELECT_OFFICE_LINK').'</div>';
            	$OFFICE["SELECT_OFFICE_BLOCK"] = "Y";
                if (count($arOffices) <= 1) {
                    $isOfficeHidden = true;
                }
            }
            $selectOffice = $contMng->getViewInstance()->getSelectOffice(
                'form_' . $OFFICE["STRUCTURE"][0]["FIELD_TYPE"] . '_' . $OFFICE["STRUCTURE"][0]["ID"],
                $optionArr,
                Array(
                    'id' => 'userOffice' . $arResult["SOULT_FORM"],
                    'class' => 'select-288 soi-select rsg-location' . $scrollOfficeClass . $validateCSSClass,
                ),
                $obOfficeFilter
            );
			if(!$isOfficeHidden) {
	            $OFFICE["HTML_CODE"] = $selectOfficeHtml. $selectOffice;
	            $OFFICE["STRUCTURE"][0]["FIELD_TYPE"] = "dropdown";
			}
        } else {
            $isOfficeHidden = true;
        }
        if ($isOfficeHidden) {
            $hiddenOffice = $contMng->getViewInstance()->getHiddenOffice(
                'form_' . $OFFICE["STRUCTURE"][0]["FIELD_TYPE"] . '_' . $OFFICE["STRUCTURE"][0]["ID"],
                Array(
                    'ID' => 'queue'
                ),
                Array(
                    'id' => 'userOffice' . $arResult["SOULT_FORM"],
                    'class' => $validateCSSClass,
                )
            );

            $OFFICE["HTML_CODE"] = $hiddenOffice;
            $OFFICE["STRUCTURE"][0]["FIELD_TYPE"] = "hidden";
        }
    }

//Продукт
    if (!empty($PRODUCT)) {
        $PRODUCT["HTML_CODE"] = '<input type="hidden" data-code="PRODUCT" id="userProduct' . $arResult["SOULT_FORM"] . '" name="form_' . $PRODUCT["STRUCTURE"][0]["FIELD_TYPE"] . '_' . $PRODUCT["STRUCTURE"][0]["ID"] . '" value="' . $arParams["BASE_PRODUCT_NAME"] . '" />';
        $PRODUCT["STRUCTURE"][0]["FIELD_TYPE"] = "hidden";
    }

//Адресаты - Выбор адресата по описанию
    if ($arParams['EMAIL_ONLY_FROM_RESIPIENTS'] == 'Y' && $arParams['SERVICE_SECT_ID']) {
        $arRecipientFilter = Array(
            "ACTIVE" => "Y",
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ID" => IB_RECIPIENTS,
            "CNT_ACTIVE" => "Y",
            "PROPERTY_GROUP" => $arParams['SERVICE_SECT_ID']
        );
        $arRecipientFilter[] = (new FilterHelper())->makeCity();
        $rsRecipient = CIBlockElement::GetList(Array("SORT" => "ASC"), $arRecipientFilter, false, false);
        $arRecipientEmails = Array();
        $arRecipientEmailsComplex = Array();
        while ($obRecipient = $rsRecipient->GetNextElement()) {
            $arRecipient = $obRecipient->GetFields();
            $arRecipient['PROPERTIES'] = $obRecipient->GetProperties();
            foreach ($arRecipient['PROPERTIES']["EMAIL"]["VALUE"] AS $key => $curVal) {
                $curDescription = $arRecipient['PROPERTIES']["EMAIL"]["DESCRIPTION"][$key];
                $curVal = trim($curVal);
                $curDescription = trim($curDescription);
                $arRecipientEmails[] = $curVal;
                if (!empty($curDescription)) {
                    $arRecipientEmailsComplex[] = Array(
                        'VALUE' => $curVal,
                        'DESCRIPTION' => $curDescription,
                    );
                }
            }
        }
        $arRecipientEmails = array_unique($arRecipientEmails);

        if ($arParams['RESIPIENTS_SMART_SELECT'] == 'Y' || $TO["STRUCTURE"][0]["FIELD_TYPE"] != "hidden") {
            if (count($arRecipientEmailsComplex) > 1) {
                $FIELD_PARAM = $TO["STRUCTURE"][0]["FIELD_PARAM"];
                if (empty($FIELD_PARAM)) {
                    $FIELD_PARAM = 'REQUIRED';
                }
                $validateCSSClass = !empty($FIELD_PARAM) ? " validate-{$FIELD_PARAM}" : '';
                $selectTO = '<select class="select-288 j-select' . $validateCSSClass . '" name="form_' . $TO["STRUCTURE"][0]["FIELD_TYPE"] . '_' . $TO["STRUCTURE"][0]["ID"] . '">';
                $selectTO .= "<option value=\"\">".Loc::getMessage('PB_MAIN_BEFSEND_SELECT_OFFICE')."</option>";
                foreach ($arRecipientEmailsComplex AS $arEmail) {
                    $selectTO .= "<option value=\"{$arEmail['VALUE']}\">{$arEmail['DESCRIPTION']}</option>";
                }
                $selectTO .= '</select>';

                $TO["CAPTION"] = Loc::getMessage('PB_MAIN_BEFSEND_TO_CAPTION');
                $TO["HTML_CODE"] = $selectTO;
                $TO["STRUCTURE"][0]["FIELD_TYPE"] = "dropdown";
                $TO["CUSTOM_LIST"] = "Y";
                $TO["REQUIRED"] = "Y";
            } else {
                $TO["HTML_CODE"] = '<input type="hidden" name="form_' . $TO["STRUCTURE"][0]["FIELD_TYPE"] . '_' . $TO["STRUCTURE"][0]["ID"] . '" value="' . implode(', ', $arRecipientEmails) . '" />';
                $TO["STRUCTURE"][0]["FIELD_TYPE"] = "hidden";
            }
        } else {
            $TO["HTML_CODE"] = '<input type="hidden" name="form_' . $TO["STRUCTURE"][0]["FIELD_TYPE"] . '_' . $TO["STRUCTURE"][0]["ID"] . '" value="' . implode(', ', $arRecipientEmails) . '" />';
        }
    }

    $obCache->endDataCache(Array(
        'CITY' => $CITY,
        'OFFICE' => $OFFICE,
        'PRODUCT' => $PRODUCT,
        'TO' => $TO,
    ));

}
if (count($arResult['arDropDown']) > 0) {
    foreach ($arResult['arDropDown'] as $keySelect => $valSelect) {
        if (count($valSelect["reference_id"]) > 0) {
            $htmlSelect = '<select class="select-288 j-select" name="form_dropdown_' . $keySelect . '" id="form_dropdown_' . $keySelect . '" data-code="' . $keySelect . '">';
            foreach ($valSelect["reference"] as $keyRef => $valOption) {
                $htmlSelect .= '<option value="' . $valSelect["reference_id"][$keyRef] . '">' . $valOption . '</option>';
            }
            $htmlSelect .= '</select>';
            $arResult["QUESTIONS"][$keySelect]["HTML_CODE"] = $htmlSelect;
        }
    }
}

$arResult["FORM_TITLE"] = $arParams['MESS']['TITLE'] ? : $arResult["FORM_TITLE"];
$arResult["arForm"]["BUTTON"] = $arParams['MESS']['SUBMIT_BTN'] ? : $arResult["arForm"]["BUTTON"];
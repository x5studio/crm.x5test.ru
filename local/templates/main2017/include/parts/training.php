<?
$curPage = rtrim($APPLICATION->GetCurPage(), '/').'/';
$includeDirLocalPath = $curPage.'/include/';
$includeDirAbsPath = $_SERVER['DOCUMENT_ROOT'].$includeDirLocalPath;

$descriptionIncludeFileName = 'training__description.php';

if (!file_exists($includeDirAbsPath)) {
    mkdir($includeDirAbsPath);
}
if (!file_exists($includeDirAbsPath.$descriptionIncludeFileName)) {
    file_put_contents($includeDirAbsPath.$descriptionIncludeFileName, 'Обучение CRM — важный этап при масштабировании бизнеса, который позволит сделать работу сотрудников более эффективной. Первый Бит обучает online по всем пользовательским ролям CRM, курсы разработаны по программе, позволяющей полностью освоить функционал продукта без предварительной подготовки');
}
?>


<section class="crm-training">
    <div class="container crm-training__container">
        <div class="crm-training__svg-box">
            <svg width="32" height="31" aria-hidden="true">
                <use xlink:href="#icon-bulb"></use>
            </svg>
        </div>
        <p class="crm-training__text"><?$APPLICATION->IncludeFile($includeDirLocalPath.$descriptionIncludeFileName)?></p>
    </div>
</section>


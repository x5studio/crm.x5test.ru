<section class="quote">
    <h2 class="visually-hidden">Преимущества работы с нами</h2>
    <div class="container quote__container">
        <p class="quote__text">
          <span class="quote__svg-box">
            <svg width="73" height="55" aria-hidden="true">
              <use xlink:href="#icon-quotes-2"></use>
            </svg>
          </span>
            Первый Бит поможет настроить CRM под бизнес клиента, расширить
            функционал продукта, а также обучить пользователем компании.
            Обеспечиваем оперативный контроль работоспособности программы,
            а также быстрое реагирование на запросы пользователей.
        </p>
    </div>
</section>
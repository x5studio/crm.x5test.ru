<section class="statement section section--statement">
    <div class="container statement__container clearfix">
        <cite class="statement__author">
            <figure class="statement__img-box">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets_new/img/content/dolgov9.jpg" />
            </figure>
            <figcaption>
            <span class="statement__position">
              Генеральный директор компании
            </span>
                <span class="statement__name">
              Антон Долгов
            </span>
            </figcaption>
        </cite>
        <h2 class="statement__title">
            Интегратор эффективных IT-решений для вашего бизнеса
        </h2>
        <blockquote class="statement__quote">
          <span class="statement__svg-box">
            <svg width="52" height="39" aria-hidden="true">
              <use xlink:href="#icon-quotes-2"></use>
            </svg>
          </span>
            <p>
                Наша цель – решить все Ваши текущие задачи и создать новые возможности для Вашего успешного развития. Сделать это качественно и профессионально.
            </p>
        </blockquote>
    </div>
</section>
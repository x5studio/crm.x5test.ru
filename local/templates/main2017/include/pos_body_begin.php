<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3MCLG3";
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<script>
    window.dataLayer = window.dataLayer || [];
    window.track = function(p) {
        dataLayer.push({
            'event': p[1],
            'event_category': p[0],
        });
    }

    window.trackStep = function(path) {
        try { console.log(path);
            if(path.indexOf('/general-form-5feedback/virtual/add-result-ok/')>=0) {
                track(['Call','Send']);
            }else if(path.indexOf('/general-form-5feedback/virtual/open-form/')>=0) {
                track(['Call','Click']);
            }else if(path.indexOf('add-result-ok')>=0) {
                track(['form','send']);
            } else if(path.indexOf('open-form')>=0) {
                track(['form', 'click']);
            }else {
                dataLayer.push({
                    'event':'VirtualPageview',
                    'virtualPageURL':path
                });
            }
        } catch (e) {}
    }
</script>
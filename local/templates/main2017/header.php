<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Page\Asset;
CUtil::InitJSCore();

$GLOBALS['IS_APPEND_ACTION_PAGE'] = \Xfive\Tools::isAppendActionPage();

// Meta  -------
Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">');

// Styles ------
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/js/vendor/bootstrap/dist/css/bootstrap.min.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/js/vendor/fontawesome/css/font-awesome.min.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/base.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/layout.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/main_page.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/modal.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/product.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/product-bitrix.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/modules.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/contacts.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/cases.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/mobile.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/js/vendor/slick-carousel/slick/slick.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/js/vendor/slick-carousel/slick/slick-theme.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/js/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/js/calendar/datepicker.min.css');
?>
<?
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/calendar/calendar.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/contacts.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/template_scripts.js');

$GLOBALS['IS_NEW_PAGE'] = \Xfive\Tools::isNewPage();

if ($GLOBALS['IS_NEW_PAGE']) {
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets_new/css/style.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets_new/css/style_fixes.css');
}

global $obOfficeFilter, $obCityFilter;

$obCityFilter = new ContactManagerFilter();
$obCityFilter->setGeneral(Array(
	'UF_HIDE_IN_CITY_LIST' => false,
));

$obOfficeFilter = new ContactManagerFilter();
$obOfficeFilter->setGeneral(Array('PROPERTY_HIDE_IN_CITY_LIST' => false));

$contMng = ContactManager::getInstance();
$arOfficeList = $contMng->getOfficeList($obOfficeFilter, Array('IBLOCK_SECTION_ID' => $_SESSION['REGION']['ID']));

if (count($arOfficeList) == 0)
{
	$arOfficeList = $contMng->getOfficeList($obOfficeFilter, Array('CODE' => 'msk'));
}
$cities = array_shift($arOfficeList);
$emailCity = $cities["PROPERTIES"]["EMAIL"]["VALUE"];
if (defined("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE'])) && constant("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE'])) != '')
{
	$emailCity .= ", " . constant("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE']));
}
?>

<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID; ?>">
<head>
	<title><? $APPLICATION->ShowTitle(); ?></title>

	<script src="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH . "/js/vendor/jquery/dist/jquery.min.js", true) ?>"></script>
	<script src="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH . "/js/vendor/require/index.js", true) ?>"
			data-main="main"></script>
	<script ><?
		$config = file_get_contents(__DIR__ . '/js/config.js');
		$configMin = preg_replace("(\s+|\r|\n)", ' ', $config);
		echo $configMin;
		?></script>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
	<? $APPLICATION->IncludeFile('include/pos_head.php');?>
	<? $APPLICATION->ShowHead(); ?>

<!-- calltouch -->
<script type="text/javascript">
(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)},m=typeof Array.prototype.find === 'function',n=m?"init-min.js":"init.js";s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/"+n+"?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","cpa2utpt");
</script>
<script type="text/javascript">
	$(document).ready(function(){
jQuery('form').on('submit', function() {
    var m = jQuery(this);
	try {
    var ct_fio = m.find('input[placeholder*="Имя"]').val();
    var formTitle = m.find('input[data-code*="FORM_TITLE"]').val();
	var fromName = m.attr('name');
    var ct_mail = m.find('input[placeholder*="E-mail"]').val();
    var ct_phone = m.find('input[placeholder*="Телефон"]').val();
    var ct_comment = m.find('textarea').val();
    var ct_site_id = '47955';
    var ct_sub = 'Заявка с '+location.hostname;
	if(!!formTitle){ct_sub = formTitle}
	if(fromName=='SIMPLE_FORM_4'){ct_sub = 'Заявка на подбор и запуск CRM';}
    var ct_data = {
        fio: ct_fio,
        email: ct_mail,
        comment: ct_comment,
    phoneNumber: ct_phone,
    subject: ct_sub,
    requestUrl: location.href,
        sessionId: window.call_value
    };
    var ct_valid = (!!ct_phone||!!ct_mail) && window.ct_snd_flag != 1;
    if (ct_valid){
		window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 10000);
        console.log(ct_data);
        jQuery.ajax({
            url: 'https://api.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',  
            dataType: 'json', type: 'POST', data: ct_data, async: false
        });
    }
} catch (e) { console.log(e); } 
}); 

});

</script>
<script type='text/javascript'>
var _ctreq_jivo = function(sub) {
    var sid = '47955';
    var jc = jivo_api.getContactInfo(); var fio = ''; var phone = ''; var email = '';
    if (!!jc.client_name){fio = jc.client_name;} if (!!jc.phone){phone = jc.phone;} if (!!jc.email){email = jc.email;}
    var ct_data = { fio: fio, phoneNumber: phone, email: email, subject: sub, requestUrl: location.href, sessionId: window.call_value };
    var request = window.ActiveXObject?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest();
    var post_data = Object.keys(ct_data).reduce(function(a, k) {if(!!ct_data[k]){a.push(k + '=' + encodeURIComponent(ct_data[k]));}return a}, []).join('&');
    var url = 'https://api.calltouch.ru/calls-service/RestAPI/'+sid+'/requests/orders/register/';
    request.open("POST", url, true); request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); request.send(post_data);
}
window.jivo_onIntroduction = function() { _ctreq_jivo('JivoSite посетитель оставил контакты'); }
window.jivo_onCallStart = function() { _ctreq_jivo('JivoSite обратный звонок'); }
window.jivo_onMessageSent = function() { ct('goal','jivo_ms'); }
</script>
<!-- calltouch -->
    <!-- Marquiz script start -->
    <script>
        let id;
        switch(window.location.pathname){
            case '/' : id = '6188d258e00b3b003f24d0b6';
                break;
            case '/blog/crm-chto-eto-takoe/' : id = '6188d258e00b3b003f24d0b6';
                break;
            case '/vnedrenie-crm/': id = '619bb2c8f4202f003fdbc204';
                break;
            case '/products/bitriks-24/': id = '619bb0976e831f003fe2ad09';
                break;
            case '/blog/crm-megaplan-obzor/' : id = '619b8dbdac45f0003f8b035b';
                break;
            case '/products/megaplan/' : id = '619b8dbdac45f0003f8b035b';
                break;
            case '/products/amocrm/' : id = '619f67d1016311003f05d4d8';
                break;
            case '/blog/amocrm-chto-eto-takoe/' : id = '619f67d1016311003f05d4d8';
                break;
            case '/products/1s-crm/' : id = '61c9c14c16c42b004fb0b354';
                break;
            case '/products/bit-crm-3-0/' : id = '619f6ac92cf824003f54f21e';
        }
        (function(w, d, s, o){
            var j = d.createElement(s); j.async = true; j.src = '//script.marquiz.ru/v2.js';j.onload = function() {
                if (document.readyState !== 'loading') Marquiz.init(o);
                else document.addEventListener("DOMContentLoaded", function() {
                    Marquiz.init(o);
                });
            };
            d.head.insertBefore(j, d.head.firstElementChild);
        })(window, document, 'script', {
                host: '//quiz.marquiz.ru',
                region: 'eu',
                id: id,
                autoOpen: false,
                autoOpenFreq: 'once',
                openOnExit: false,
                disableOnMobile: false
            }
        );

    </script>
    <!-- Marquiz script end -->
</head>
<body class="<?=($GLOBALS['IS_NEW_PAGE'] ? 'wrapper body-sup' : '')?>">
<? $APPLICATION->IncludeFile('include/pos_body_begin.php') ?>

<?if ($GLOBALS['IS_NEW_PAGE']):?>
    <?include (__DIR__.'/include/resource.php');?>
<?endif;?>


<?if($GLOBALS["USER"]->IsAuthorized()):?>
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <script>
        BX.ready(function () {
            var MyPanel = BX("header"),
                BxPanel = BX.admin.panel,
                FxPanel = function () {
                    if (window.pageYOffset >= BxPanel.DIV.clientHeight && BxPanel.isFixed() === false) {
                        MyPanel.style.top = 0;
                    } else if (BxPanel.isFixed() === true) {
                        MyPanel.style.top = BxPanel.DIV.clientHeight + "px";
                    } else {
                        MyPanel.style.top = BxPanel.DIV.clientHeight - window.pageYOffset + "px";
                    }
                };
            if (!!MyPanel) {
                FxPanel();
                window.onscroll = FxPanel;
                BX.addCustomEvent('onTopPanelCollapse', BX.delegate(FxPanel, this));
                BX.addCustomEvent('onTopPanelFix', BX.delegate(FxPanel, this));
            }
        });
    </script>
<?endif;?>



<div class="wrap_mobile_menu visible-xs">

	<div class="btn-group city-choice_mobile">
		<div class="xsmall-header-city small-header-city"></div>
	</div>

	<div class="mobile_menu">
		<div class="main-menu_mobile">
			<a href="/about-crm/">О CRM</a>
			<a href="/products/" class="mobile_menu_products_trigger">Продукты</a>
			<div class="mobile_menu_products">
                <a href="/products/bitriks-24/">Битрикс24</a>
                <a href="/products/bit-crm-3-0/">БИТ.CRM 3</a>
				<a href="/products/storverk-crm/">STORVERK CRM</a>
                <a href="/products/1s-crm/">1C:CRM</a>
                <a href="/products/amocrm/">amoCRM</a>
                <a href="/products/megaplan/">Мегаплан</a>
			</div>
			<a href="javascript:return false;" class="mobile_menu_services_trigger">Услуги</a>
			<div class="mobile_menu_services">
                <a href="/vnedrenie-crm/">Внедрение CRM</a>
                <a href="/obuchenie-rabote-v-crm/">Обучение работе в CRM</a>
                <a href="/tekhnicheskaya-podderzhka/">Техническая поддержка CRM</a>
			</div>
			<a href="/clients/">Кейсы</a>
			<a href="/blog/">Блог</a>
			<a href="/events/">Семинары</a>
			<a href="/about/">О компании</a>
			<a href="/contacts/">Контакты</a>
		</div>
	</div>

	<div class="contacts_mobile">
		<?
		global $arContFilter, $arContMESS;
		$arContFilter['IBLOCK_SECTION_ID'] = $_SESSION['REGION']['ID'];
		$APPLICATION->IncludeComponent("bezr:contact.box", "phone.top", Array(
			"OFFICE_IBLOCK_ID" => IB_CONTACTS,
			"DIVISION_IBLOCK_ID" => IB_DIVISION,
			"PAGE_OFFICE_IBLOCK_ID" => IB_PAGE_OFFICE,
			"FILTER_NAME" => "arContFilter",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "86400",
			"CACHE_FILTER" => "Y",
			"CACHE_GROUPS" => "N",
			"DETAIL_URL" => "/contacts/#SECTION_CODE#/#ELEMENT_CODE#/",
			"LIMIT_VIEW_ITEMS" => "2",
			"COMPONENT_MARKER" => $_SESSION["REGION"]["ID"],
			"MESS" => $arContMESS
		),
			false,
			array(
				"HIDE_ICONS" => "Y"
			)
		);
		?>
	</div>

</div>

<div id="wrap">
	<header id="header">

		<section class="phones-wrap visible-xs clearfix">
			<div class="col-xs-6 phone-number">
				<?$APPLICATION->ShowViewContent('office_top_block');?>
			</div>
			<div class="col-xs-6 order-call">
                <a href="javascript:void(0)" class="btn_modal_feedback">Заказать звонок</a>
			</div>
		</section>


		<section class="top_level hidden-xs">
			<div class="container clearfix">
				<div class="row">
					<div class="pull-left">
                        <div class="btn-group pull-left">
                            <div class="j-leaping" data-target="header-city">
                                <?
                                global $cityFilter;
                                $cityFilter = Array(
                                    "CITY_CODE" => $_SESSION["REGION"]["CODE"],
                                );
                                $HTML_TITLE = '<a href="##HTML_LIST_ID#" class="btn btn-default btn-lg city-choice header-city j-city-popup">Ваш город: #NAME#<span class="glyphicon glyphicon-chevron-down"></span></a>';
                                $APPLICATION->IncludeComponent("vitams:city.dropdown", "city_popup",
                                    Array(
                                        "IBLOCK_TYPE" => IBT_CONTACTS,
                                        "IBLOCK_ID" => IB_CONTACTS,
                                        "DROPDOWN" => $_REQUEST['dropdown'], // for cache only
                                        "SECTION_CODE" => "",
                                        "SECTION_URL" => "",
                                        "COUNT_ELEMENTS" => "Y",
                                        "TOP_DEPTH" => "1",
                                        "SECTION_FIELDS" => "",
                                        "FILTER_NAME" => "cityFilter",
                                        "SECTION_USER_FIELDS" => "",
                                        "ADD_SECTIONS_CHAIN" => "Y",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_NOTES" => "",
                                        "CACHE_GROUPS" => "N",
                                        "CACHE_FILTER" => "Y",
                                        "LINK_IN_BASEURL" => "Y",
                                        "LINK_WITH_SUBDOMAIN" => "Y",
                                        "SEF_MODE" => "N",
                                        "SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
                                        "SEF_URL_TEMPLATES" => array(),
                                        "HTML_LIST_ID" => "header-city-list",
                                        "HTML_TITLE" => $HTML_TITLE,
                                        "SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
                                        "COUNTRY_CODE" => $_SESSION['GEOIP']['country_code'],
                                        "PAGER_TEMPLATE" => "",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        'UI_CITY_STRONG' => Array(),
                                        'UI_CITY_PRIORITET' => Array(173,153),
                                        'LINK_LOCAL_CHOOSE' => 'Y',
                                    ), false,
                                    Array("HIDE_ICONS" => "Y")
                                );
                                ?>
                            </div>
						</div>
						<div class="send-mention pull-left btn_modal_mention">
							<span>Написать директору</span>
							<i class="fa fa-smile-o" aria-hidden="true"></i>
							<i class="fa fa-frown-o" aria-hidden="true"></i>
						</div>
					</div>

					<div class="pull-right"></div>
				</div>


			</div>
		</section>

		<section class="middle_level image-row-active">
			<div class="container clearfix">
				<div class="row">
					<div class="menu-trigger visible-xs">
						<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/menu-trigger.png" alt="">
					</div>

					<a href="/" class="pull-left logo-links hidden-xs">
						<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/layout/logo.svg" width="180" alt="">
					</a>

					<div class="logo_shap_mobile visible-xs">
						<a href="/" class="logo-links">
							<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/layout/logo.svg" width="147" alt="">
						</a>
					</div>

					<div class="pull-right contacts-block hidden-xs">
						<?$APPLICATION->ShowViewContent('office_top_block');?>

                        <button class="btn_modal_feedback" data-form-name="Заказать звонок" tabindex="0">
                            <i class="glyphicon glyphicon-earphone"></i>
                            <span>Заказать звонок</span>
                        </button>

					</div>

				</div>

			</div>
		</section>

		<section class="bottom_level hidden-xs">
			<div class="container">
				<div class="super-row wrap_main-menu">

                    <div class="main-menu topmenu">
                        <a href="/about-crm/">О CRM</a>
                        <a href="javascript:return false;" class="products-link">Продукты</a>
                        <a href="javascript:return false;" class="services-link">Услуги</a>
                        <?/*><a href="/services/">Услуги</a><?*/?>
                        <a href="/clients/">Кейсы</a>
                        <a href="/blog/">Блог</a>
                        <a href="/events/">Семинары</a>
                        <a href="/about/">О компании</a>
                        <a href="/contacts/">Контакты</a>
                    </div>
                    <div class="dropdown-products-list">
                        <a href="/products/bitriks-24/">Битрикс24</a>
                        <a href="/products/bit-crm-3-0/">БИТ.CRM 3</a>
						<a href="/products/storverk-crm/">STORVERK CRM</a>
                        <a href="/products/1s-crm/">1C:CRM</a>
                        <a href="/products/amocrm/">amoCRM</a>
                        <a href="/products/megaplan/">Мегаплан</a>
                    </div>

                    <div class="dropdown-services-list">
                        <a href="/vnedrenie-crm/">Внедрение CRM</a>
                        <a href="/obuchenie-rabote-v-crm/">Обучение работе в CRM</a>
						<a href="/tekhnicheskaya-podderzhka/">Техническая поддержка CRM</a>
                    </div>
				</div>
			</div>
		</section>

		<section class="banner_level">
			<div class="container slider-top">
			<? if (!defined("NO_BANNERS_TOP")): ?>
				<?
				global $bannerFilter;
				$bannerFilter = array(
					GetTagsArrFilter(Array(), "banner")
				);

				$APPLICATION->IncludeComponent("bitrix:news.list", "banner", array(
						"IBLOCK_TYPE" => IBT_BANNERS,
						"IBLOCK_ID" => IB_BANNERS,
						"PROPERTY_CODE" => array(
							'LINK'
						),
						"FILTER_NAME" => 'bannerFilter',
						"SET_TITLE" => "N",
						"CACHE_FILTER" => "Y",
						"CACHE_GROUPS" => "Y",
						"SORT_BY1" => "SORT",
						"SORT_ORDER1" => "ASC",
						"ACTIVE" => "Y",
						"SORT_BY2" => "ACTIVE_FROM",
						"SORT_ORDER2" => "DESC",
						"ADD_SECTIONS_CHAIN" => "N",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"PAGER_TEMPLATE" => "",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"PAGER_TITLE" => "",
						"PAGER_SHOW_ALWAYS" => "N",
						"COMPONENT_MARKER" => "banner"
					)
				); ?>
			<? endif ?>
			</div>
		</section>
	</header>
	<? if (
        !CSite::InDir('/index.php')
        && !$GLOBALS['IS_NEW_PAGE']
    ): ?>
		<div class="container">
			<div class="row">
				<? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "",
					array(
						"START_FROM" => "0",
						"PATH" => "",
						"SITE_ID" => "s1"
					)
				); ?>
			</div>
		</div>
	<? endif ?>

<?if ($GLOBALS['IS_NEW_PAGE']):?>
    <main class="page-main">
<?endif;?>
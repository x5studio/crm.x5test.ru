window.onload = function(){
    if ($('[data-scroll="request-form"]').length){
        let requestPos = $('[data-scroll="request-form"]').offset().top;
        $(".scroll-to-request-form").click(function () {
            $('html, body').animate({
                scrollTop: requestPos - 30
            }, 500);
        });
    }

    if ($('[data-scroll="feedback-form"]').length){
        let feedbackPos = $('[data-scroll="feedback-form"]').offset().top;
        $(".scroll-to-feedback-form").click(function () {
            $('html, body').animate({
                scrollTop: feedbackPos - 200
            }, 500);
        });
    }
}

$(document).ready(function () {

    $('.mobile_menu_services_trigger').click(function (e) {
        e.preventDefault();
        $('.mobile_menu_services').toggleClass('active');
    });

    $('body').on('mouseenter', '.services-link', function () {
        $('.dropdown-services-list').addClass('active');
        $(this).addClass('active');
    });
    $('body').on('mouseleave', '.services-link', function (e) {
        if (!e.toElement.closest('.dropdown-services-list')) {
            $('.dropdown-services-list').removeClass('active');
            $('.services-link').removeClass('active');
        }
    });
    $('body').on('mouseleave', '.dropdown-services-list', function (e) {
        if (!e.toElement.closest('.services-link')) {
            $('.dropdown-services-list').removeClass('active');
            $('.services-link').removeClass('active');
        }
    });


    /*collapse header by scroll*/
    let lastScrollTop = 0;
    window.addEventListener("scroll", function(){
        let st = window.pageYOffset || document.documentElement.scrollTop;
        if (st > 108){  //108 - it's sum of collapsing blocks .top_level & .bottom_level
            if (st > lastScrollTop){
                // downscroll code
                $('header .top_level').slideUp(200);
                $('header .bottom_level').slideUp(200);
            } else {
                // upscroll code
                $('header .top_level').slideDown(200);
                $('header .bottom_level').slideDown(200);
            }
        }
        lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
    }, false);

}); // document ready
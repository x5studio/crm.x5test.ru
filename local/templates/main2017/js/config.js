requirejs.config({
    baseUrl: "/local/templates/main2017/js",
    urlArgs: 'v=0.0.1',
    waitSeconds: 0,
    config: {
        'Controls/Responsive': {
            borderTypes: {
                xsmall: [0, 479],
                small: [480, 767],
                medium: [768, 979],
                large: [980, 99999]
            }
        }
    },
    deps: ['jquery', 'modernizr', 'bootstrap','jqueryCookie'],
    paths: {
        main: 'main',
        Controls: 'Controllers',
        fancybox: 'vendor/fancybox/source/jquery.fancybox.pack',
        flipclock: 'vendor/flipclock/compiled/flipclock.min',
        maskedinput: 'vendor/jquery.maskedinput/dist/jquery.maskedinput.min',
        jqueryRouter: 'vendor/jquery.router/index',
        validate: 'vendor/jquery-validation/dist/jquery.validate.min',
        ouibounce: 'tools/ouibounce',
        flexslider: 'vendor/flexslider/jquery.flexslider-min',
        uniform: 'vendor/jquery.uniform/jquery.uniform.min',
        actual: 'vendor/jquery.actual/jquery.actual.min',
        modernizr: 'tools/modernizr.min',
        cookie: 'tools/cookie',
        generalTools: 'tools/tools',
        router: 'tools/router',
        bootstrap: 'vendor/bootstrap/dist/js/bootstrap',
        cusel: 'vendor/cusel.custom/index',
        slick: 'vendor/slick-carousel/slick/slick.min',
        owlcarousel2:'vendor/OwlCarousel2/dist/owl.carousel.min',
        jqueryCookie:'vendor/jquery.cookie/jquery.cookie',
        tablesawJquery: 'vendor/tablesaw/dist/tablesaw.jquery',
        mCustomScrollbar: 'vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar',
        mousewheel: 'vendor/jquery-mousewheel/jquery.mousewheel.min'
    },
    shim: {
        'fancybox': {
            deps: ['jquery'],
            exports: 'jQuery.fancybox'
        },
        'ouibounce': {
            deps: [],
            exports: 'ouibounce'
        },
        'jqueryRouter': {
            deps: ['tools/trash'],
            exports: 'jQuery.router'
        },
        'modernizr': {
            exports: 'Modernizr'
        },
        'mCustomScrollbar': {
            deps: ['mousewheel']
        }
    }
});
define('jquery', [], function () {
    return window.jQuery
})
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Ready'], function () {
		$(document).ready(function () {
			require(['slick'], function () {
				runSliders();
				$(window).resize(function () {
					runSliders();
				});
			});


		require(['Controllers/SeoTrackHelper'], function(SeoTrackHelper) {
			window.SeoTrackHelper = new SeoTrackHelper();
			window.SeoTrackHelper.counters = {
				yandex: window.yaCounter29316340
			}

			window.SeoTrackHelper.goalsData = {
				'open-5feedback':{
					'yandex': {
						code: 'call-click'
					},
				},
				'send-5feedback':{
					'yandex': {
						code: 'call-send'
					},
				},
				'send-4request':{
					'yandex': {
						code: 'podbor-send'
					},
				},
				'open-default':{
					'yandex': {
						code: 'feedback-click'
					},
				},
				'send-default':{
					'yandex': {
						code: 'feedback-send'
					},
				}
			}
		});
		// subscribe to result popup event
		$(document).on("form.result.popup.open", function (event, resultArr) {
			if(!resultArr.error) {
				window.SeoTrackHelper.sendGoal('send',resultArr.formSalt);
			}
		});
		// subscribe to open popup event
		$(document).on("afterFormLoad", function (event) {
			window.SeoTrackHelper.sendGoal('open',$(event.target).data('saltForm'));

		});


		var fBox = $("#general-form-3request2");
		fBox.on("jFormPopup.init", function () {

			if (typeof window.formGeneral3request2 != "object") return;

			window.formGeneral3request2.afterLoad = function (obj) {
				var btn = $(obj.targetBtn);
				var _this = this;
				if(btn.data('form-name').length > 0) {
					_this.Form.parents('#general-form-3request2').find('.title-text').text(btn.data('form-name'));
				}
			};

		});

		var eventBox = $("#general-form-6enroll");
		eventBox.on("jFormPopup.init", function () {
			if (typeof window.formGeneral6enroll != "object") return;
			window.formGeneral6enroll.afterLoad = function (obj) {
				var btn = $(obj.targetBtn);
				var _this = this;
				_this.root.find('input[data-code="SEMINAR_NAME"]').val(btn.parents(".description-seminar-data").find(".description-seminar-title").text());
				_this.root.find('input[data-code="EMAIL_EVENT"]').val(btn.data('email'));
			};

		});

		$('.mobile_menu_products_trigger').click(function (e) {
			e.preventDefault();
			$('.mobile_menu_products').toggleClass('active');
		});

		require(['Controls/Form/FormPopupOpen'], function (FormPopupOpen) {
			new FormPopupOpen();
		});
		require(['Controls/ReCaptchaProvider'], function (ReCaptchaProvider) {
			new ReCaptchaProvider();
		});
		
		// $('.mask-phone').mask("+7(999) 999-9999");

		require(['maskedinput'], function () {
			$('.mask-phone').mask("+7(999) 999-9999");
		});


		$('body').on('click', '.menu-trigger, .black-bg.open', function () {
			$('.menu-trigger').toggleClass('open');
			$('.wrap_mobile_menu').toggleClass('open');
			$('.black-bg').toggleClass('open');
		});

		$('body').on('mouseenter', '.products-link', function () {
			$('.dropdown-products-list').addClass('active');
			$(this).addClass('active');
		});
		$('body').on('mouseleave', '.products-link', function (e) {
			if (!e.toElement.closest('.dropdown-products-list')) {
				$('.dropdown-products-list').removeClass('active');
				$('.products-link').removeClass('active');
			}
		});
		$('body').on('mouseleave', '.dropdown-products-list', function (e) {
			if (!e.toElement.closest('.products-link')) {
				$('.dropdown-products-list').removeClass('active');
				$('.products-link').removeClass('active');
			}
		});

		$(document).on("submit", 'form', function (e) {
			var error = false;
			var form = $(this);
			form.find('input.error').removeClass('error');

			form.find('input.required').each(function (input) {
				if ($(this).val() == "") {
					$(this).addClass('error');
					error = true;
				}
			});

			if (error) {
				return false;
			}
		});

		//Заполняем скрытый инпут форм
		var $sessid = $('form #sessid');
		if ($sessid.length > 0) {
			$sessid.val(BX.bitrix_sessid());
		}

		function FormPageMngMain(){
        var $formPage = $('.j-form-page');
		if ($formPage.length > 0) {
            $formPage.each(function () {
			var $item = $(this);
                require(['Controls/Form/FormPageMng'], function (FormPageMng) {
                    var obFormPage = new FormPageMng({
                        root: $item
                    });
                    var globalName = obFormPage.getGlobalName();
                    window[globalName] = obFormPage;
                    obFormPage.init();
                    $item.trigger("jFormPage.init");
                });
            });
        }
    }

    if ($('.j-deffered').size() > 0) {
        require(["Controls/DeferredDataMng"],
            function (DeferredDataMng) {
                $('.j-deffered').each(function () {
			new DeferredDataMng(this);
                });
			FormPageMngMain();
            });
    }
    else {
        FormPageMngMain();
		}

		if ($('.j-leaping').size() > 0) {
			require(["Controls/Leaping"], function (Leaping) {
				$('.j-leaping').each(function () {
					new Leaping(this);
				});
			});
		}

		var $formPopup = $('.j-form-popup');
		if ($formPopup.length > 0) {
			$formPopup.each(function () {
				var $item = $(this);
				require(['Controls/Form/FormPopupMng'], function (FormPopupMng) {
					var obFormPopup = new FormPopupMng({
						root: $item
					});
					var globalName = obFormPopup.getGlobalName();
					window[globalName] = obFormPopup;
					obFormPopup.init();
					$item.trigger("jFormPopup.init");
				});

				});
			}

		var fancyboxGroup = $(".fancybox").add('[rel = prettyPhoto]').add('[rel = group]');
		if (fancyboxGroup.length > 0) {
			require(['fancybox'], function () {
				fancyboxGroup.fancybox();
			});
		}

		var $fancybox = $('.j-fancybox');
		var $popupLink = $('.j-popup-link');
		if ($fancybox.length > 0 || $popupLink.length > 0) {
			require(['jquery', 'fancybox', 'cusel'], function ($) {
				if ($fancybox.length > 0) {
					$fancybox.fancybox({
						padding: 40
					});
				}
				if ($popupLink.length > 0) {
					$popupLink.fancybox({
						fitToView: false,
						padding: 0,
						arrows: false,
						closeBtn: false,
						afterLoad: function () {
							$(".j-close").live("click", function () {
								$.fancybox.close(true);
							});
						},
						afterShow: function () {
							var id = '';
							$('.fancybox-wrap .cusel input').each(function (i) {
								if (i != 0) id += ',';
								id += '#' + $(this).attr('id');
							});
							var params = {
								refreshEl: id,
								visRows: 6,
								scrollArrows: true
							};
							cuSelRefresh(params);
						}
					});
				}
			});
		}

		var $textScroll = $('.j-text-scroll');
		if ($textScroll.length > 0) {
			$textScroll.each(function () {
				var $item = $(this);
				require(['Controls/TextScroll'], function (TextScroll) {
					new TextScroll($item);
				});
			});
		}

		if ($('.j-select').length > 0) {
			require(['cusel'], function () {
				var params = {
					changedEl: ".j-select",
					visRows: 6,
					scrollArrows: true
				};
				cuSel(params);
				$(document).data('jSelectInit', true);
				$(document).trigger("jSelect.init");
			});
		} else {
			$(document).data('jSelectInit', true);
			$(document).trigger("jSelect.init");
		}

		var $relatedSelects = $('.j-related-selects');
		if ($relatedSelects.length > 0) {
			$(document).on("jSelect.init", function () {
				$('.j-related-selects').each(function () {
					var $item = $(this);
					require(['Controls/RelatedSelects'], function (RelatedSelects) {
						new RelatedSelects({
							select1: $item
						});
					});
				});
			});
		}

		$('.j-owl-carousel').each(function () {
			var item = $(this);
			require(['Controls/OwlCarousel2'], function (OwlCarousel2) {
				new OwlCarousel2(item);
			});
		});


		var $slider1 = $('.j-slider1');
		if ($slider1.length > 0) {
			$slider1.each(function () {
				var $item = $(this);
				require(['Controls/Slider1'], function (Slider1) {
					new Slider1($item);
				});
			});
		}

	});



	function runSliders() {

		if ($('.slider:not(.new-pages)').is('.slick-initialized')) {
			$('.slider:not(.new-pages)').slick('unslick');
		}

		if (window.innerWidth <= 767) {
			if (!$('.effective_launching_wrap').is('.slick-initialized')) {

				$('.effective_launching_wrap').slick({
					dots: true,
					arrows: false
				});

			}

			$('.slider:not(.new-pages)').slick({
				dots: true,
				infinite: true,
				speed: 500,
				fade: true,
				cssEase: 'linear',
				prevArrow: $('.wrap-slider-nav .prev-slider-arrow'),
				nextArrow: $('.wrap-slider-nav .next-slider-arrow')
			});

		} else {
			if ($('.effective_launching_wrap').is('.slick-initialized')) {
				$('.effective_launching_wrap').slick('unslick');
			}

			$('.slider:not(.new-pages)').slick({
				dots: true,
				infinite: true,
				speed: 500,
				fade: true,
				cssEase: 'linear',
				appendDots: $('.wrap-slider-nav .wrap-dots'),
				prevArrow: $('.wrap-slider-nav .prev-slider-arrow'),
				nextArrow: $('.wrap-slider-nav .next-slider-arrow')
			});
		}
	}
});
}
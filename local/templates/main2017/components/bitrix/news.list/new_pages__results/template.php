<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <?$arSection = end($arResult['SECTION']['PATH']);?>
    <section class="results">
        <div class="container">
            <h2 class="results__title"><?=($arSection['DESCRIPTION'] ?: $arSection['NAME'])?></h2>
            <ul class="results__list">
                <?foreach ($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="results__item">
                        <div class="results__item-title">
                            <?=$arItem['NAME']?>
                            <span class="results__svg-box">
                                <svg width="15" height="3" aria-hidden="true">
                                  <use xlink:href="#icon-points"></use>
                                </svg>
                            </span>
                        </div>
                        <p class="results__item-description"><?=$arItem['PREVIEW_TEXT']?></p>
                    </li>
                <?endforeach;?>
            </ul>
        </div>
    </section>
<?endif;?>


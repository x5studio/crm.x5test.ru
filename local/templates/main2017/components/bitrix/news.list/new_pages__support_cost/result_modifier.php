<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$includePath = '/include/';

$curPageClean = $APPLICATION->GetCurPage();
$curPageClean = trim($curPageClean, '/');
$curPageClean = str_replace('/', '__', $curPageClean);

$buttonCaptionFileName = $curPageClean.'__button_caption.php';
$priceFileName = $curPageClean.'__price.php';

$arResult['INCLUDE_PATH'] = $includePath;
$arResult['BUTTON_CAPTION_FILE_PATH'] = $includePath.$buttonCaptionFileName;
$arResult['PRICE_FILE_PATH'] = $includePath.$priceFileName;

$this->__component->SetResultCacheKeys([
    'INCLUDE_PATH',
    'BUTTON_CAPTION_FILE_PATH',
    'PRICE_FILE_PATH',
]);


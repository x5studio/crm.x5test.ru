<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


if (!file_exists(__DIR__.$arResult['INCLUDE_PATH'])) {
    mkdir(__DIR__.$arResult['INCLUDE_PATH']);
}

if (!file_exists(__DIR__.$arResult['BUTTON_CAPTION_FILE_PATH'])) {
    file_put_contents(__DIR__.$arResult['BUTTON_CAPTION_FILE_PATH'], 'Свяжитесь со мной');
}

if (!file_exists(__DIR__.$arResult['PRICE_FILE_PATH'])) {
    file_put_contents(__DIR__.$arResult['PRICE_FILE_PATH'], '2800');
}


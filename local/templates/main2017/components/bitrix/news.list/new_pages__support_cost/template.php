<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <?$arSection = end($arResult['SECTION']['PATH']);?>
    <section class="sup-cost section">
        <div class="container">
            <h2 class="sup-cost__title title"><?=$arSection['NAME']?></h2>
            <div class="sup-cost__description section__description"><?=$arSection['DESCRIPTION']?></div>
            <div class="sup-cost__price-block">
                <p class="sup-cost__price">
                    <?$APPLICATION->IncludeFile($templateFolder.$arResult['PRICE_FILE_PATH'], [], ['NAME'=>'Цена',])?>
                    <svg aria-hidden="true" width="14" height="16">
                        <use xlink:href="#icon-rouble"></use>
                    </svg>/час
                </p>
                <a class="button button--sup-cost btn_modal_feedback_support" href=""><?$APPLICATION->IncludeFile($templateFolder.$arResult['BUTTON_CAPTION_FILE_PATH'], [], ['NAME'=>'Подпись кнопки',])?></a>
            </div>
            <div class="sup-cost__services">
                <?foreach ($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="sup-cost__services-row clearfix">
                        <div class="sup-cost__servires-title"><?=$arItem['NAME']?></div>
                        <div class="sup-cost__servires-description"><?=$arItem['PREVIEW_TEXT']?></div>
                        <div class="sup-cost__servires-available">
                            <svg width="16" height="16" aria-hidden="true">
                                <use xlink:href="#icon-available"></use>
                            </svg>
                            Есть
                        </div>
                    </div>
                <?endforeach;?>
            </div>
        </div>
    </section>
<?endif;?>


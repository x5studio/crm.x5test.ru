<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(empty($arResult["ITEMS"])) return;?>
<div class="slider-main flexslider j-slider1 box-thin">
	<ul class="slides">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<?
					$linkContent = false;
					if(!empty($arItem['PROPERTIES']['LINK']['VALUE'])) {
						$linkContent = $arItem['PROPERTIES']['LINK']['VALUE'];
					}
				?>
				<?if($linkContent):?><a href="<?=$linkContent?>" <?=$arItem['PROPERTIES']['TARGET']['VALUE']=='Y'?'target="_blank"':'';?>><?endif;?>
					<img alt="<?=$arItem["NAME"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" />
				<?if($linkContent):?></a><?endif;?>
			</li>
		<?endforeach;?>
	</ul>
</div>
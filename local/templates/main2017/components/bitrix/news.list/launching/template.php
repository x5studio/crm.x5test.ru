<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="launching">
    <div class="container">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			array(
				"AREA_FILE_SHOW"      => "file",
				"PATH"                => SITE_DIR."include/launching.php",
				"AREA_FILE_RECURSIVE" => "N",
				"EDIT_MODE"           => "html",
			),
			false,
			array('HIDE_ICONS' => 'N')
		);?>
        <div class="row clearfix effective_launching_wrap">
			<?foreach($arResult["ITEMS"] as $arItem):?>

                <?
                    $bgi = '';
                    if(!empty($arItem['PREVIEW_PICTURE']['SRC'])){
                        $bgi = 'style="background-image: url(' . $arItem['PREVIEW_PICTURE']['SRC'] . ')"';
                    }

                    $linkHref = 'javascript:void(0)';
                    $linkClass = 'scroll-to-feedback-form';
                    if(!empty($arItem['PROPERTIES']['LINK']['VALUE'])){
                        $linkHref = $arItem['PROPERTIES']['LINK']['VALUE'];
                        $linkClass = '';
                    }
                ?>

                <a href="<?=$linkHref?>" class="col-xs-12 col-md-4 col-sm-6 <?=$linkClass?>">
                    <div class="single_type_work" <?=$bgi?> >
                        <span class="title"><?=$arItem['NAME'];?></span>
                        <?=$arItem['PREVIEW_TEXT'];?>
                        <?/*<span class="more">подробнее</span>*/?>
                    </div>
                </a>

			<?endforeach;?>
        </div>
    </div>
</section>
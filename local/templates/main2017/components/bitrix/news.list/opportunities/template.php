<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="wrap-opportunities">
    <div class="container clearfix">
        <div class="row">
            <h2 class="text-center">Возможности Битрикс24</h2>
            <?$i = 1;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="col-sm-6"<?if($i > 4):?> style="display: none;"<?endif;?>>
        <div class="single_opportunity">
            <span class="title"><?=$arItem['NAME'];?></span>
			<?=$arItem['PREVIEW_TEXT'];?>
        </div>
    </div>
			<?$i++;?>
<?endforeach;?>
        </div>
        <?if (count($arResult["ITEMS"]) > 4):?>
        <button class="transparent-btn center-block">показать все возможности</button>
        <?endif;?>
    </div>
</section>

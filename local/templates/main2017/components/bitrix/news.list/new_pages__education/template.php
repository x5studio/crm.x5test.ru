<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <?$arSection = end($arResult['SECTION']['PATH']);?>
    <section class="what-included">
        <div class="container what-included__container">
            <h2 class="what-included__title"><?=$arSection['NAME']?></h2>
            <ul class="what-included__list">
                <?foreach ($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="what-included__item">
                        <?=$arItem['DISPLAY_PROPERTIES']['SVG']['DISPLAY_VALUE']?>
                        <p><?=$arItem['NAME']?></p>
                    </li>
                <?endforeach;?>
            </ul>
            <p class="what-included__quote">
                <span class="what-included__svg-box">
                    <svg width="42" height="32" aria-hidden="true">
                      <use xlink:href="#icon-quotes-2"></use>
                    </svg>
                </span>
                <?=$arSection['DESCRIPTION']?>
            </p>
        </div>
    </section>
<?endif;?>


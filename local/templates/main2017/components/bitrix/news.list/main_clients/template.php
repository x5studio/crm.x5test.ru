<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <section class="clients">
        <div class="container">
            <h2 class="text-center"><span class="blue-font">200+ внедрений</span> для наших клиентов</h2>
            <div class="row clearfix wrap-clients">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <div class="col-sm-6 col-md-3 col-xs-12 single-client">
<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <div class="hover-layer">
                        <p><?=$arItem['NAME'];?></p>
                    </div>
    <?
    $file = CFile::ResizeImageGet($arItem["DISPLAY_PROPERTIES"]["MAIN_PICTURE"]['VALUE'], array('width'=>126, 'height'=>74), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    ?>
                    <img src="<?=$file['src']?>" alt="">
</a>
                </div>

			<?endforeach;?>
            </div>

            <button onclick="location.href='/clients/';" class="transparent-btn center-block">Ещё кейсы <?=$arResult['CNT'];?></button>
        </div>
    </section>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="many_projects">
    <div class="container">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			array(
				"AREA_FILE_SHOW"      => "file",
				"PATH"                => SITE_DIR."include/many_projects.php",
				"AREA_FILE_RECURSIVE" => "N",
				"EDIT_MODE"           => "html",
			),
			false,
			array('HIDE_ICONS' => 'N')
		);?>
        <div class="row clearfix">
			<?foreach($arResult["ITEMS"] as $arItem):?>
            <div class="col-sm-2 col-xs-6 single-project-sphere">
                <div class="img-project-wrap">
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="">
                </div>
                <span><?=$arItem['NAME'];?></span>
            </div>
			<?endforeach;?>
        </div>
    </div>
</section>
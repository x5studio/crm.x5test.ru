<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="launching product_launching">
    <div class="container">
        <div class="text-center">
            <span class="h2">Наши услуги по запуску CRM</span>
        </div>
        <div class="row clearfix effective_launching_wrap">

            <?foreach($arResult["ITEMS"] as $arItem):?>

                <?
                $bgi = '';
                if(!empty($arItem['PREVIEW_PICTURE']['SRC'])){
                    $bgi = 'style="background-image: url(' . $arItem['PREVIEW_PICTURE']['SRC'] . ')"';
                }

                $linkHref = 'javascript:void(0)';
                $linkClass = 'scroll-to-feedback-form';
                if(!empty($arItem['PROPERTIES']['LINK']['VALUE'])){
                    $linkHref = $arItem['PROPERTIES']['LINK']['VALUE'];
                    $linkClass = '';
                }
                ?>

                <a href="<?=$linkHref?>" class="col-xs-12 col-md-4 col-sm-6 <?=$linkClass?>">
                    <div class="single_type_work" <?=$bgi?> >
                        <span class="title"><?=$arItem['NAME'];?></span>
                        <?/*<span class="more">подробнее</span>*/?>
                    </div>
                </a>

            <?endforeach;?>

        </div>
    </div>
</section>
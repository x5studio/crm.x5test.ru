<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):
    $i++;
    if ($i == 4) {?>
        <div id="WIDGET_FORM_INLINE"></div>
  <?}?>
	<?if($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 1"):?>
    <?
    $style = "";
        if ($arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC'] != ""){
			$style = "style=\"background-image: url(".$arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC'].")\"";
		}
		?>
    <section class="wrap-task task-socnet"<?=$style?>>
        <div class="wrap-task-img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt=""></div>
        <div class="container clearfix">
            <div class="col-sm-6 task-desc pull-right">
                <h2><?=$arItem['NAME'];?></h2>
                <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
				<?=$arItem['DETAIL_TEXT'];?>
            </div>
        </div>
    </section>
		<?elseif($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 2"):?>
    <section class="wrap-task blue-grey-bg task-full-managment">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <img src="<?=$arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC'];?>" alt="" class="ico_for_task hidden-xs">
                    <h2><?=$arItem['NAME'];?></h2>
                    <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
                </div>
                <div class="col-sm-6 wrap-task-img">
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 ">
					<?=$arItem['DETAIL_TEXT'];?>
                </div>

                <div class="col-sm-6">
                    <?=$arItem['DISPLAY_PROPERTIES']['TEXT']['~VALUE']['TEXT'];?>
                </div>
            </div>

        </div>
    </section>
	<?elseif($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 3"):?>
    <section class="wrap-task">
        <div class="container">
            <div class="row text-center task-desc bg_ico_management">
                <h2><?=$arItem['NAME'];?></h2>
                <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
            </div>
            <div class="row task-desc">

                <div class="col-xs-12 col-sm-6 padding_r_20">
					<?=$arItem['DETAIL_TEXT'];?>
                </div>

                <div class="col-xs-12 col-sm-6 padding_r_20">
					<?=$arItem['DISPLAY_PROPERTIES']['TEXT']['~VALUE']['TEXT'];?>
                </div>

            </div>

        </div>
    </section>
	<?elseif($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 4"):?>
    <section class="wrap-task blue-grey-bg">
        <div class="container">
            <div class="super-row">
                <div class="col-sm-3 col-md-2 wrap-task-img text-left hidden-xs"><img src="<?=$arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC'];?>" alt=""></div>
                <div class="col-sm-9 col-md-10 task-desc">
                    <h2><?=$arItem['NAME'];?></h2>
                    <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
    				<?=$arItem['DETAIL_TEXT'];?>
                </div>
            </div>
        </div>
    </section>
	<?elseif($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 5"):?>
    <section class="wrap-task task_with_screen bg_right_top">
        <div class="wrap-task-img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt=""></div>
        <div class="container">
           <div class="super-row">
                <div class="col-sm-6 col-md-7 task-desc pull-right">
                    <h2><?=$arItem['NAME'];?></h2>
                    <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
    				<?=$arItem['DETAIL_TEXT'];?>
                </div>
            </div>
        </div>
    </section>

	<?elseif($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 6"):?>
    <section class="wrap-task">
        <div class="container">
            <div class="super-row">
                <div class="col-sm-9 col-md-10 task-desc">
                    <h2><?=$arItem['NAME'];?></h2>
                    <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
    				<?=$arItem['DETAIL_TEXT'];?>
                </div>
                <div class="col-sm-3 col-md-2 wrap-task-img text-right hidden-xs"><img src="<?=$arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC'];?>" alt=""></div>
            </div>
        </div>
    </section>
	<?elseif($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 7"):?>
    <section class="wrap-task">
        <div class="container">
            <div class="col-sm-3 col-md-2 wrap-task-img text-left hidden-xs"><img src="<?=$arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC'];?>" alt=""></div>
            <div class="text-right col-sm-9 col-md-10 task-desc text-left-xs">
                <h2><?=$arItem['NAME'];?></h2>
                <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
				<?=$arItem['DETAIL_TEXT'];?>
            </div>
        </div>
    </section>
	<?else:?>

    <?endif;?>
<?endforeach;?>

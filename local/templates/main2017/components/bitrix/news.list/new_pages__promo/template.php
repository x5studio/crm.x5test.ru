<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult["ITEMS"])):?>
    <?$arItem = reset($arResult["ITEMS"]);?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <section id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="promo <?=(count($arItem['DISPLAY_PROPERTIES']['SERVICES']['DISPLAY_VALUE']) ? '' : 'promo--tech-sup promo--education')?>">
        <div class="container">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "new_pages",
                array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <div class="promo__wrapper">
                <h1 class="promo__title promo__title--bold"><?=$arItem["NAME"]?></h1>
                <div class="promo__description"><?=$arItem["PREVIEW_TEXT"];?></div>
                <?if (count($arItem['DISPLAY_PROPERTIES']['SERVICES']['DISPLAY_VALUE'])):?>
                    <ul class="promo__services">
                        <?foreach ($arItem['DISPLAY_PROPERTIES']['SERVICES']['DISPLAY_VALUE'] as $service):?>
                            <li class="promo__service">
                                <svg class="promo__icon-check" width="16" height="16" aria-hidden="true">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                                <?=$service?>
                            </li>
                        <?endforeach;?>
                    </ul>
                <?endif;?>
                <a class="button promo__button btn_modal_request_crm" href="javascript: void(0);"
                   data-form-name="Оставить заявку"><?=($arItem['DISPLAY_PROPERTIES']['BUTTON_CAPTION']['DISPLAY_VALUE'] ?: 'Заказать консультацию')?></a>
            </div>
        </div>
    </section>
<?endif;?>


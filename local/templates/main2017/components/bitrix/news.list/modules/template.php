<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="adding-modules">
    <div class="container packages-info">
        <div class="row">
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				array(
					"AREA_FILE_SHOW"      => "file",
					"PATH"                => $arParams["INCLUDE_FILE_PATH"],
					"AREA_FILE_RECURSIVE" => "N",
					"EDIT_MODE"           => "html",
				),
				false,
				array('HIDE_ICONS' => 'N')
			);?>
        </div>
    </div>
    <?/*
    <div class="container accordeon-adding-modules-wrap visible-xs">
        <div class="col-xs-12 mobile-modules-list">
            <div class="single-module">Скрипты разговора</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">Монитор достижений</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">KPI + план-фактный анализ</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">Управление поручениями</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">СМС и почтовые рассылки</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">Интеграция с мессенджерами</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">Монитор достижений</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">KPI + план-фактный анализ</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">Управление поручениями</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">СМС и почтовые рассылки</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">Интеграция с мессенджерами</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">Монитор достижений</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">KPI + план-фактный анализ</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">Управление поручениями</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module">СМС и почтовые рассылки</div>
            <div class="module-desc">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
            <div class="single-module border-radius-bottom">Интеграция с мессенджерами</div>
            <div class="module-desc border-radius-bottom">
                <h3>Скрипты разговора</h3>
                <p>Авто скрипт разговора позволяет даже новичку профессионально общаться с клиентом. В зависимости от типа клиента и стадии сделки программа выведет на экран все необходимые вопросы и предложит ход развития разговора.</p>
                <span class="module-price">15 000 руб.</span>
            </div>
        </div>
    </div>
    */?>
    <div class="container adding-modules-wrap hidden-xs">
        <div class="modules-list">
            <div class="title-modules">Дополнительные модули (<?=count($arResult["ITEMS"])?>)</div>
			<?foreach($arResult["ITEMS"] as $key =>$arItem):?>
            <div class="single-module<?if($key == 0):?> active<?endif;?>" data-module="<?=$key?>"><?=$arItem['NAME']?></div>
            <?endforeach;?>
        </div>
        <div class="module-content">
			<?foreach($arResult["ITEMS"] as $key =>$arItem):?>
            <div class="module-desc<?if($key > 0):?> hidden<?endif;?>" data-module="<?=$key?>">
                <h3><?=$arItem['NAME']?></h3>
				<?=$arItem['PREVIEW_TEXT']?>
               <?if(!empty($arItem['DISPLAY_PROPERTIES']['PRICE']['VALUE'])){?> 
					<span class="module-price"><?=$arItem['DISPLAY_PROPERTIES']['PRICE']['VALUE'];?> руб.</span>
				<?}?>
            </div>
			<?endforeach;?>
        </div>
    </div>

</section>
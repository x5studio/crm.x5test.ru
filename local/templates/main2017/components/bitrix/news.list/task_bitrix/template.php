<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?if($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 1"):?>
        <section class="wrap-task blue-grey-bg task-full-managment">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/products/bitrix/ico_full_managment.png" alt="" class="ico_for_task hidden-xs">
                        <h2><?=$arItem['NAME'];?></h2>
                        <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
                    </div>
                    <div class="col-sm-6 wrap-task-img">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="">
                    </div>
                </div>
                <div class="row task-desc">
                    <div class="col-sm-6">
                        <p>Объемный функционал по управлению процессом продаж позволяет настраивать бизнес-процессы под задачи вшей компании, выделять этапы продаж, которые соответствуют вашей специфике. Будьте всегда в курсе текущего состояния продаж с помощью широкого выбора отчетов.</p>
                    </div>

                    <div class="col-sm-6">
                        <p>Значительно ускорьте работу сотрудников с помощью встроенной IP-телефонии и генератора коммерческих предложений. Пусть они уделают больше времени на общение с клиентами и меньше на решение административных вопросов.</p>
                    </div>
                </div>

            </div>
        </section>
	<?elseif($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 2"):?>
        <section class="wrap-task task-management-projects">
            <div class="wrap-task-img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt=""></div>
            <div class="container">

                <div class="col-sm-6 task-desc pull-right">
                    <h2><?=$arItem['NAME'];?></h2>
                    <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
					<?=$arItem['DETAIL_TEXT'];?>
                </div>
            </div>
        </section>
	<?else:?>
        <section class="wrap-task task-socnet">
            <div class="wrap-task-img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt=""></div>
            <div class="container clearfix">
                <div class="col-sm-6 task-desc pull-right">
                    <h2><?=$arItem['NAME'];?></h2>
                    <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
					<?=$arItem['DETAIL_TEXT'];?>
                </div>
            </div>
        </section>
	<?endif;?>
<?endforeach;?>

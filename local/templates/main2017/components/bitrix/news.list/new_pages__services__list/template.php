<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <?$arSection = end($arResult['SECTION']['PATH']);?>
    <section class="services <?=($arParams['SERVICES_OVERLAP']=='Y' ? 'section-overlap-top' : '')?>">
        <div class="container">
            <div class="services__wrapper">
                <h2 class="services__title title title--white"><?=$arSection['NAME']?></h2>
                <ul class="services__list">
                    <?foreach ($arResult["ITEMS"] as $arItem):?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="services__item">
                            <?=$arItem['DISPLAY_PROPERTIES']['SVG']['DISPLAY_VALUE']?>
                            <p class="services__item-title"><?=$arItem['NAME']?></p>
                        </li>
                    <?endforeach;?>
                </ul>
                <div class="services__bg">
                    <img class="services__bg-image" src="<?=SITE_TEMPLATE_PATH?>/assets_new/img/bg/bg-services-sm.svg" width="210" height="182" />
                </div>
            </div>
        </div>
    </section>
<?endif;?>


<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <?$arSection = end($arResult['SECTION']['PATH']);?>
    <section class="tasks">
        <div class="container">
            <div class="tasks__wrapper">
                <h2 class="tasks__title title"><?=($arSection['DESCRIPTION'] ?: $arSection['NAME'])?></h2>
                <ul class="tasks__list">
                    <?foreach ($arResult["ITEMS"] as $arItem):?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="tasks__card">
                            <div class="tasks__logo-block">
                                <picture>
                                    <img class="tasks__logo tasks__logo--default" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
                                </picture>
                            </div>
                            <p class="tasks__card-title"><?=$arItem['NAME']?></p>
                            <p class="tasks__description"><?=$arItem['PREVIEW_TEXT']?></p>
                            <a class="tasks__link <?=($arItem['DISPLAY_PROPERTIES']['BUTTON_LINK']['VALUE'] ? '' : 'scroll-to-request-form')?>"
                               href="<?=($arItem['DISPLAY_PROPERTIES']['BUTTON_LINK']['VALUE'] ?: 'javascript:return false;')?>"
                            ><?=($arItem['DISPLAY_PROPERTIES']['BUTTON_CAPTION']['DISPLAY_VALUE'] ?: 'Подробнее')?></a>
                        </li>
                    <?endforeach;?>
                </ul>
            </div>
        </div>
    </section>
<?endif;?>


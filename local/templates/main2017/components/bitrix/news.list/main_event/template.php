<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <h4>Ближайший семинар</h4>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $date_format = FormatDate('j F', strtotime($arItem['PROPERTIES']['DATE']['VALUE']),strtotime($arItem['PROPERTIES']['DATE']['VALUE']));
    $time_format = FormatDate('h:i', strtotime($arItem['PROPERTIES']['DATE']['VALUE']),strtotime($arItem['PROPERTIES']['DATE']['VALUE']));
    $srcImg = ( !empty($arItem["DISPLAY_PROPERTIES"]['IMG_RECOMMENDED']['FILE_VALUE']['SRC']) ) ?
        $arItem["DISPLAY_PROPERTIES"]['IMG_RECOMMENDED']['FILE_VALUE']['SRC'] :
        "/local/templates/main2017/assets/img/event.png";
    ?>
    <div class="col-xs-12 single-event" style="background-image: url(<?=$arItem["DISPLAY_PROPERTIES"]['IMG_RECOMMENDED']['FILE_VALUE']['SRC']?>);">
        <div class="wrap-article-desc description-seminar-data">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title description-seminar-title"><?=$arItem['NAME'];?></a>
            <p><img src="/local/templates/main2017/assets/img/icons/ico-date-grey.png" class="ico-date" alt="Дата"> <span><? echo $date_format; ?> <? echo $time_format; ?></span> </p>
            <p><img src="/local/templates/main2017/assets/img/icons/geo-marker-grey.png" class="ico-maker" alt="Место"> <span><?=$arItem['PROPERTIES']['PLACE']['VALUE']?></span> </p>
            <button class="btn_modal_enroll_event" data-email="<?=$arItem['PROPERTIES']['EMAIL']['VALUE']?>">Записаться на семинар</button>
        </div>
    </div>
<?endforeach;?>
    <button onclick="location.href='/events/';" class="pull-right transparent-btn">Ещё семинары <?=$arResult['CNT'];?></button>
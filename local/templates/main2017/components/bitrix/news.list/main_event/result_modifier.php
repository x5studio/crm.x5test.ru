<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$arResult['CNT'] = CIBlockElement::GetList(
	array(),
	array('IBLOCK_ID' => $arParams['IBLOCK_ID']),
    array(),
    false,
    array('ID', 'NAME')
);
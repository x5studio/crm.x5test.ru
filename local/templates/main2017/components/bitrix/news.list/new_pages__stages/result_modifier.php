<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$numberTextRepresentation = [
    'Первый',
    'Второй',
    'Третий',
    'Четвертый',
    'Пятый',
    'Шестой',
    'Седьмой',
    'Восьмой',
    'Девятый',
    'Десятый',
];

$i = 0;
foreach($arResult["ITEMS"] as &$arItem) {
    $arItem['NUMBER_TEXT_REPRESENTATION'] = $numberTextRepresentation[$i];

    $arItem["PREVIEW_PICTURE"] = \CFile::ResizeImageGet(
        $arItem["PREVIEW_PICTURE"],
        [
            'width' => 155,
            'height' => 139,
        ],
        BX_RESIZE_IMAGE_EXACT
    );

    $i++;
}


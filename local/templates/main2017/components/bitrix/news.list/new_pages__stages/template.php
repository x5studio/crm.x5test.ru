<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <?$arSection = end($arResult['SECTION']['PATH']);?>
    <section class="stages">
        <div class="stages__wrapper">
            <img class="stages__bg-image" src="<?=SITE_TEMPLATE_PATH?>/assets_new/img/bg/stages-bg.jpg" />
            <div class="container">
                <h2 class="stages__title title title--white"><?=$arSection['NAME']?></h2>
                <div class="stages__slider slider new-pages">
                    <div class="slider__thumb-container swiper-container">
                        <ul class="swiper-wrapper slider__thumbs">
                            <?foreach ($arResult["ITEMS"] as $arItem):?>
                                <li class="slider__thumb swiper-slide"><?=$arItem['NUMBER_TEXT_REPRESENTATION']?> этап</li>
                            <?endforeach;?>
                        </ul>
                    </div>
                    <div class="slider__container swiper-container">
                        <ul class="slider__wrapper swiper-wrapper">
                            <?foreach ($arResult["ITEMS"] as $arItem):?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="slider__item swiper-slide">
                                    <p class="slider__slide-title"><?=$arItem['NAME']?></p>
                                    <p class="slider__slide-description"><?=$arItem['PREVIEW_TEXT']?></p>
                                    <img class="slider__slide-image"
                                         src="<?=($arItem['DISPLAY_PROPERTIES']['SVG']['FILE_VALUE']['SRC'] ?: $arItem['PREVIEW_PICTURE']['src'])?>"/>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                    <div class="slider__controls">
                        <button class="new-pages slider__button-prev swiper-button-prev"></button>
                        <div class="slider__pagination swiper-pagination"></div>
                        <button class="new-pages slider__button-next swiper-button-next"></button>
                    </div>
                </div>
                <div class="stages__description">
                    <svg class="stages__icon" aria-hidden="true">
                        <use xlink:href="#icon-quotes"></use>
                    </svg>
                    <p><?=$arSection['DESCRIPTION']?></p>
                </div>
            </div>
        </div>
    </section>
<?endif;?>


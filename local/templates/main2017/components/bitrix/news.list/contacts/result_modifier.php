<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$arFilter = Array(
	'IBLOCK_ID'=> $arParams['IBLOCK_ID'],
	'ACTIVE'=>'Y',
	//'DEPTH_LEVEL' => 2,
);

$rsSection = CIBlockSection::GetList(array("SORT"=>"ASC"), $arFilter, false, array('ID', 'NAME', 'UF_NAME_DATIVE', 'IBLOCK_SECTION_ID'));

while($arSection = $rsSection->Fetch()) {
	$arResult['SECTIONS'][$arSection['ID']] = $arSection;
}

$arResult['POSITION']['yandex_lat'] = $arTmp['0'];
$arResult['POSITION']['yandex_lon'] = $arTmp['1'];

foreach ($arResult['ITEMS'] as $arItem) {
	$arResult['CONTACTS'][$arItem['IBLOCK_SECTION_ID']][] = $arItem;
	$arResult['REGIONS'][$arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]= array(
		"ID" => $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID'],
		"NAME" => $arResult['SECTIONS'][$arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['NAME'],
	);
	$arResult['REGION'][$arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']][$arItem['IBLOCK_SECTION_ID']] = array(
		"ID" => $arItem['IBLOCK_SECTION_ID'],
		"NAME" => $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['NAME'],
		"NAME_DATIVE" => $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['UF_NAME_DATIVE'],
	);
	$arResult['CITY'][$arItem['IBLOCK_SECTION_ID']] = array(
		"ID" => $arItem['IBLOCK_SECTION_ID'],
		"NAME" => $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['NAME'],
		"NAME_DATIVE" => $arResult['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['UF_NAME_DATIVE'],
	);
	if ($arItem['DISPLAY_PROPERTIES']['MAP']['VALUE'] != "") {
		$arTmp = explode(",", $arItem['DISPLAY_PROPERTIES']['MAP']['VALUE']);
		if (!isset($arResult['MAP_CENTERS'][$arItem['IBLOCK_SECTION_ID']])) {
			$arResult['MAP_CENTERS'][$arItem['IBLOCK_SECTION_ID']] = array($arTmp['0'], $arTmp['1']);
		}
		$arResult['MAP'][$arItem['IBLOCK_SECTION_ID']][] = array(
			'lon' => $arTmp['1'],
			'lat' => $arTmp['0'],
			'name' => $arItem['NAME'],
			'address' => $arItem['NAME'] . " - " . $arItem['DISPLAY_PROPERTIES']['ADDRESS']['VALUE'],
			'mail' => $arItem['DISPLAY_PROPERTIES']['email']['VALUE'],
			'phone' => $arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'],
		);
	}

//	if ($arItem['DISPLAY_PROPERTIES']['MAP']['VALUE'] != "") {
//		$arTmp = explode(",", $arItem['DISPLAY_PROPERTIES']['MAP']['VALUE']);
//		$arResult['POSITION']['PLACEMARKS'][] = array(
//			'LON' => $arTmp['1'],
//			'LAT' => $arTmp['0'],
//			'TEXT' =>$arItem['NAME'],
//		);
//	}
}
//echo "<pre>".print_r($arResult['CITY'], true)."</pre>";
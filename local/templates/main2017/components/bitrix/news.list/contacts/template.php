<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$PARENT = $this->__component->__parent;?>
<section class="contacts_wrap clearfix">
    <div class="container" itemscope itemtype="https://schema.org/Organization">
		<meta itemprop="name" content="Первый Бит">
        <div class="row section_title_wrap clearfix">
            <h1 class="title"><?=$APPLICATION->ShowTitle(false);?></h1>
                <div class="contacts_trigger">
                    <div class="btn-group pull-left contacts-city-choice">
                        <?
                        global $cityFilter;
                        $cityFilter = Array(
                            "CITY_CODE" => $_SESSION["REGION"]["CODE"],
                        );
                        $HTML_TITLE = '<a href="##HTML_LIST_ID#" class="btn btn-default btn-lg contacts_trigger_btn contact-city-choice header-city j-city-popup"><img src="'.SITE_TEMPLATE_PATH.'/assets/img/icons/change-city.png" />Выбрать город</a>';
                        $APPLICATION->IncludeComponent("vitams:city.dropdown", "city_popup",
                            Array(
                                "IBLOCK_TYPE" => IBT_CONTACTS,
                                "IBLOCK_ID" => IB_CONTACTS,
                                "DROPDOWN" => $_REQUEST['dropdown'], // for cache only
                                "SECTION_CODE" => "",
                                "SECTION_URL" => "",
                                "COUNT_ELEMENTS" => "Y",
                                "TOP_DEPTH" => "1",
                                "SECTION_FIELDS" => "",
                                "FILTER_NAME" => "cityFilter",
                                "SECTION_USER_FIELDS" => "",
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_NOTES" => "",
                                "CACHE_GROUPS" => "N",
                                "CACHE_FILTER" => "Y",
                                "LINK_IN_BASEURL" => "Y",
                                "LINK_WITH_SUBDOMAIN" => "Y",
                                "SEF_MODE" => "N",
                                "SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
                                "SEF_URL_TEMPLATES" => array(),
                                "HTML_LIST_ID" => "contacts-city-list",
                                "HTML_TITLE" => $HTML_TITLE,
                                "SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
                                "COUNTRY_CODE" => $_SESSION['GEOIP']['country_code'],
                                "PAGER_TEMPLATE" => "",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "PAGER_TITLE" => "",
                                "PAGER_SHOW_ALWAYS" => "N",
                                'UI_CITY_STRONG' => Array(),
                                'UI_CITY_PRIORITET' => Array(173,153),
                                'LINK_LOCAL_CHOOSE' => 'Y',
                            ), $PARENT,
                            Array("HIDE_ICONS" => "Y")
                        );
                        ?>
                    </div>
                    <div class="contacts_trigger_btn show_on_map_btn" data-current-region="<?=$arParams['CACHE_BY_REGION']?>">
                        <img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/geo_marker.png" alt="" />
                        <span>Показать на карте</span>
                    </div>
                </div>
        </div>
        <div class="row">
            <div id="contacts_map" class="hidden"></div>
			<?$i=1;?>
			<?foreach ($arResult['CITY'] as $arCity):?>
                <div class="city_contacts_list"<?if($_SESSION['REGION']['ID'] != $arCity['ID']):?> style="display: none;"<?endif;?> data-city="<?=$arCity['ID'];?>">
                    <div class="row contacts_region clearfix">
                        <div class="col-sm-3 contacts_region_title">
                            <span>Офисы в <?=$arCity['NAME_DATIVE'];?></span>
                        </div>
                        <div class="col-sm-9 contacts_table">

                            <div class="row contacts_row hidden-xs">
                                <div class="col-sm-6">
                                    <span class="contacts_field_title">Адрес</span>
                                </div>
                                <div class="col-sm-4">
                                    <span class="contacts_field_title">Телефон</span>
                                </div>
                                <div class="col-sm-2">
                                    <span class="contacts_field_title">E-mail</span>
                                </div>
                            </div>

							<?foreach($arResult['CONTACTS'][$arCity['ID']] as $arItem):?>
                                <div class="row contacts_row">
                                    <div class="col-sm-6" itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                                        <span itemprop="streetAddress"><?=$arItem['NAME']?>, <?=$arItem['DISPLAY_PROPERTIES']['ADDRESS']['VALUE'];?></span>
										<meta  itemprop="addressLocality" content="<?= $arCity['NAME'] ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <span itemprop="telephone"><?=$arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'];?></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <span itemprop="email"><?=$arItem['DISPLAY_PROPERTIES']['email']['VALUE'];?></span>
                                    </div>
                                </div>
							<? endforeach;?>

                        </div>
                    </div>
                </div>
				<?$i++;?>
			<?endforeach;?>


        </div>
    </div>

</section>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" ></script>
<script >
    var city_markers = <?=json_encode($arResult['MAP'])?>;
    var city_centers = <?=json_encode($arResult['MAP_CENTERS'])?>;
</script>
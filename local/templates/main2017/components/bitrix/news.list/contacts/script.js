function getOfficeInfoMarker(office){
    return '<table class="markers_info"><tr><td><i class="fa fa-map-marker" aria-hidden="true"></i></td><td><span>' + office.address + '</span></td></tr><tr><td><i class="fa fa-phone" aria-hidden="true"></i></td><td><span>' + office.phone + '</span></td></tr><tr><td><i class="fa fa-envelope" aria-hidden="true"></i></td><td><span>' + office.mail + '</span></td></tr></table>';
}

function initMap( city_id ) {
    var myMap, myPlacemark, i;
    var select_city_markers = city_markers[ city_id ];
    var select_city_center = city_centers[ city_id ];

    if (myMap){
        myMap.setCenter(select_city_center);
        myMap.geoObjects.removeAll();
    } else {
        myMap = new ymaps.Map("contacts_map", {
            center: select_city_center,
            controls: ['zoomControl'],
            zoom: 10
        });
    }

    for (i = 0; i < select_city_markers.length; i++) {
        myPlacemark = new ymaps.Placemark([select_city_markers[i].lat, select_city_markers[i].lon], {
            hintContent: select_city_markers[i].name,
            balloonContent: getOfficeInfoMarker(select_city_markers[i])
        });
        myMap.geoObjects.add(myPlacemark)
    }
}

$(document).ready( function() {

    $('button.contact-city-choice').click(function (e) {
        $('#modal_contact_change_city').modal('show');
    });

    $('body').on('click', '.show_on_map_btn', function(){
        $(this).toggleClass('active');

        if ( $(this).is('.active') ){
            $('#contacts_map').removeClass('hidden');
            $('#contacts_map ymaps').remove();
            initMap( $(this).data('current-region') );
        } else {
            $('#contacts_map').addClass('hidden');
        }

    });
});
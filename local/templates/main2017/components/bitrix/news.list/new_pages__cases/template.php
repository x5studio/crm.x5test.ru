<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult["ITEMS"])):?>
    <section class="works">
        <div class="container">
            <h2 class="works__title title">Наши кейсы</h2>
            <ul class="works__list">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="works__item">
                        <picture>
                            <img class="works__image" src="<?=$arItem["DISPLAY_PROPERTIES"]["NEW_PAGES_PICTURE"]["FILE_VALUE"]["src"]?>" />
                        </picture>
                        <div class="works__header">
                            <a class="works__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                            <div class="works__description"><?=$arItem["PREVIEW_TEXT"];?></div>
                        </div>
                        <picture>
                            <img class="works__logo works__logo--default" src="<?=$arItem["DISPLAY_PROPERTIES"]["NEW_PAGES_ICON"]["FILE_VALUE"]["SRC"]?>" />
                        </picture>
                        <a class="works__button works__button--item" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Посмотреть кейс</a>
                    </li>
                <?endforeach;?>
            </ul>
            <a class="works__button works__button--bottom" href="/clients/">Больше кейсов</a>
        </div>
    </section>
<?endif;?>


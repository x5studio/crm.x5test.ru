<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


foreach($arResult["ITEMS"] as &$arItem) {
    $arItem["DISPLAY_PROPERTIES"]["NEW_PAGES_PICTURE"]["FILE_VALUE"] = \CFile::ResizeImageGet(
        $arItem["DISPLAY_PROPERTIES"]["NEW_PAGES_PICTURE"]["FILE_VALUE"],
        [
            'width' => 585,
            'height' => 350,
        ]
    );
}


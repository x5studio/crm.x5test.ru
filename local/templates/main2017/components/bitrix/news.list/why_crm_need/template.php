<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="why_crm_need">
	<div class="container">
		<div class="row clearfix">
			<div class="col-sm-6">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					array(
						"AREA_FILE_SHOW"      => "file",
						"PATH"                => SITE_DIR."include/why_crm_need.php",
						"AREA_FILE_RECURSIVE" => "N",
						"EDIT_MODE"           => "html",
					),
					false,
					array('HIDE_ICONS' => 'N')
				);?>
			</div>
				<?$i=1;?>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<div class="col-xs-12 col-sm-3 single-reason">
						<div class="wrap_img">
							<img src="<?=$arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC']?>" alt="">
						</div>
						<span><?=$arItem['NAME'];?></span>
					</div>
					<?$i++;?>
					<?if ($i == 3):?>
			</div>
			<div class="row clearfix">
					<?endif;?>
				<?endforeach;?>

		</div>
	</div>
</section>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="mainpage-slider-wrap">
    <div class="container wrap-mobile-slider">
        <div class="clearfix">
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				array(
					"AREA_FILE_SHOW"      => "file",
					"PATH"                => SITE_DIR."include/mainpage-slider.php",
					"AREA_FILE_RECURSIVE" => "N",
					"EDIT_MODE"           => "html",
				),
				false,
				array('HIDE_ICONS' => 'N')
			);?>
            <div class="wrap-slider-nav pull-right">
                <div class="prev-slider-arrow slider-arrow"></div>
                <div class="wrap-dots"></div>
                <div class="next-slider-arrow slider-arrow"></div>
            </div>
        </div>

        <div class="slider">
			<?foreach($arResult["ITEMS"] as $arItem):?>
            <div class="single-slide">
                <div class="clearfix">
                    <div class="col-sm-6 wrap-slider-desc">
                        <a href="<?=$arItem['DISPLAY_PROPERTIES']['LINK']['VALUE'];?>" class="title"><?=$arItem['NAME'];?></a>
						<?=$arItem['PREVIEW_TEXT'];?>
                        <a href="<?=$arItem['DISPLAY_PROPERTIES']['LINK']['VALUE'];?>" class="more">Подробнее</a>
                        <button class="btn_modal_request_crm" data-form-name="Подобрать CRM">Подобрать CRM</button>
                    </div>

                    <div class="col-sm-6 wrap-slider-img hidden-xs">
                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt="" class="pull-right">
                    </div>
                </div>

            </div>

			<?endforeach;?>

        </div>

        <div class="wrap-dots_mobile"></div>

    </div>
</section>
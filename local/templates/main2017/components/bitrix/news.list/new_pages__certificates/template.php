<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <?$arSection = end($arResult['SECTION']['PATH']);?>
    <section class="certificates">
        <div class="container certificates__container clearfix">
            <h2 class="certificates__title title title--light"><?=$arSection['NAME']?></h2>
            <div class="certificates__block">
                <div class="certificates__list swiper-container">
                    <div class="certificates__wrapper swiper-wrapper">
                        <?foreach ($arResult["ITEMS"] as $arItem):?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                            <div class="certificates__item swiper-slide">
                                <a id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="certificates__link" data-fslightbox="gallery" href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
                                    <picture>
                                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
                                    </picture>
                                </a>
                            </div>
                        <?endforeach;?>
                    </div>
                    <div class="certificates__scrollbar swiper-scrollbar"></div>
                </div>
            </div>
            <div class="certificates__controllers">
                <div class="certificates__pagination swiper-pagination"></div>
                <button class="new-pages certificates__btn-prev swiper-button-prev"></button>
                <button class="new-pages certificates__btn-next swiper-button-next"></button>
            </div>
        </div>
    </section>
<?endif;?>


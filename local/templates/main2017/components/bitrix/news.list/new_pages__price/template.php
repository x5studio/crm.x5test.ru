<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult["ITEMS"])):?>
    <?$arItem = reset($arResult["ITEMS"]);?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="container">
        <section class="price section">
            <div class="section__header">
                <h2 class="price__title section__title section__title--white"><?=$arItem["NAME"]?></h2>
                <div class="price__description section__description"><?=$arItem["PREVIEW_TEXT"];?></div>
            </div>
            <p class="price__cost">
                <span><?=$arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE']?></span>
                <svg class="price__icon" aria-hidden="true">
                    <use xlink:href="#icon-rouble"></use>
                </svg>
            </p>
            <a class="button button--price btn_modal_request_crm" href="javascript: void(0);"
               data-form-name="Оставить заявку"><?=($arItem['DISPLAY_PROPERTIES']['BUTTON_CAPTION']['DISPLAY_VALUE'] ?: 'Оставить заявку')?></a>
            <picture>
                <img class="price__image"
                     src="<?=($arItem['PREVIEW_PICTURE']['SRC'] ?: SITE_TEMPLATE_PATH.'/assets_new/img/content/price-bg.png')?>"/>
            </picture>
        </section>
    </div>
<?endif;?>


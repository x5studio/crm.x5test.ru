<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult["ITEMS"])):?>
    <?$arItem = reset($arResult["ITEMS"]);?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <section id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="merit section">
        <div class="container">
            <h2 class="merit__title title"><?=$arItem["NAME"]?></h2>
            <div class="merit__block">
                <ul class="merit__list">
                    <?foreach ($arItem['PROPERTIES'] as $prop):?>
                        <?if (in_array($prop['CODE'], ['PICTURES'])) continue;?>
                        <li class="merit__item">
                            <span class="merit__item-number">
                                <?=$prop['VALUE']?>
                            </span>
                            <span class="merit__item-title"><?=$prop['NAME']?></span>
                        </li>
                    <?endforeach;?>
                </ul>
                <ul class="merit__partners">
                    <?foreach ($arItem['DISPLAY_PROPERTIES']['PICTURES']['FILE_VALUE'] as $picture):?>
                        <li class="merit__partner">
                            <div class="merit__img-box">
                                <picture>
                                    <img src="<?=$picture['SRC']?>" />
                                </picture>
                            </div>
                            <span class="merit__description"><?=$picture['DESCRIPTION']?></span>
                        </li>
                    <?endforeach;?>
                </ul>
            </div>
        </div>
    </section>
<?endif;?>


<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


foreach ($arResult['ITEMS'] as &$arItem) {
    foreach ($arItem['PROPERTIES'] as &$prop) {
        if (is_string($prop['VALUE'])) {
            $prop['VALUE'] = str_replace('&gt;', '<span class="merit__symbol">&gt;</span>', $prop['VALUE']);
            $prop['VALUE'] = str_replace('&lt;', '<span class="merit__symbol">&lt;</span>', $prop['VALUE']);
        }
    }
}


<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <?if($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 1"):?>
        <section class="wrap-task">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-md-10 task-desc">
                        <h2><?=$arItem['NAME'];?></h2>
                        <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
    					<?=$arItem['DETAIL_TEXT'];?>
                    </div>
                    <div class="col-sm-3 col-md-2 wrap-task-img text-right hidden-xs"><img src="<?=$arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC'];?>" alt=""></div>
                </div>
            </div>
        </section>
    <?elseif($arItem['DISPLAY_PROPERTIES']['BLOCK_TYPE']['VALUE'] == "Вид 2"):?>
        <section class="wrap-task blue-grey-bg">
            <div class="container">
                <div class="col-sm-3 col-md-2 wrap-task-img text-left hidden-xs"><img src="<?=$arItem['DISPLAY_PROPERTIES']['ICON']['FILE_VALUE']['SRC'];?>" alt=""></div>
                <div class="col-sm-9 col-md-10 task-desc">
                    <h2><?=$arItem['NAME'];?></h2>
                    <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
					<?=$arItem['DETAIL_TEXT'];?>
                </div>
            </div>
        </section>
	<?else:?>

    <section class="wrap-task task_with_screen">
        <div class="wrap-task-img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt=""></div>
        <div class="container">

            <div class="col-sm-6 col-md-7 task-desc pull-right">
                <h2><?=$arItem['NAME'];?></h2>
                <span class="task-subtitle"><?=$arItem['PREVIEW_TEXT'];?></span>
				<?=$arItem['DETAIL_TEXT'];?>
            </div>
        </div>
    </section>
    <?endif;?>
<?endforeach;?>
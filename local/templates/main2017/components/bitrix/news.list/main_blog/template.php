<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h4>Статьи</h4>

<div class="row">

<?foreach($arResult["ITEMS"] as $arItem):?>
    <article class="col-sm-12 col-md-6 col-xs-12">
        <div class="single-article">
            <div class="wrap-article-img">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" alt=""></a>
                <div class="tags_wrap">
                    <?/*
                    <span>интеграции</span>
                    <span>битрикс</span>
                    */?>
                </div>
            </div>
            <div class="wrap-article-desc">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title"><?=$arItem['NAME'];?></a>
                <?=$arItem['PREVIEW_TEXT'];?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="more">Читать</a>
            </div>
        </div>
    </article>
<?endforeach;?>


</div>

<button  onclick="location.href='/blog/';" class="pull-right transparent-btn">Ещё статьи <?=$arResult['CNT'];?></button>
<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult)) {
	return '';
}

$strReturn = '';

$strReturn .= '<ul class="breadcrumbs promo__breadcrumbs" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++) {
	$title = htmlspecialcharsex($arResult[$index]['TITLE']);
	$classMain = ($arResult[$index]['LINK']=='/' ? 'breadcrumbs__item--main' : '');

	if ($arResult[$index]['LINK'] <> '' && $index != $itemSize - 1) {
		$strReturn .= '
			<li class="breadcrumbs__item ' . $classMain . '" id="bx_breadcrumb_' . $index . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a class="breadcrumbs__link" itemprop="item" href="' . $arResult[$index]['LINK'] . '" title="' . $title . '" itemprop="url">
					' . $title . '
				</a>
				<meta itemprop="position" content="' . ($index + 1) . '" />
			</li>';
	} else {
		$strReturn .= '
			<li class="breadcrumbs__item ' . $classMain . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<span class="breadcrumbs__link breadcrumbs__link--active" itemprop="name">' . $title . '</span>
				<meta itemprop="position" content="' . ($index + 1) . '" />
			</li>';
	}
}

$strReturn .= '</ul>';

return $strReturn;

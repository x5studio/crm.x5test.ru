<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult)) {
	return '';
}

$strReturn = '';

$strReturn .= '<div class="breadcrumbs row hidden-xs" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++) {
	$title = htmlspecialcharsex($arResult[$index]['TITLE']);
	$slash = ($index > 0 ? ' / ' : '');

	if ($arResult[$index]['LINK'] <> '' && $index != $itemSize - 1) {
		$strReturn .= '
			<div class="bx-breadcrumb-item" id="bx_breadcrumb_' . $index . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				' . $slash .
				' <a class="bx-breadcrumb-item-link" itemprop="item" href="' . $arResult[$index]['LINK'] . '" title="' . $title . '" itemprop="url">
					<span class="bx-breadcrumb-item-text" itemprop="name">' . $title . '</span>
				</a>
				<meta itemprop="position" content="' . ($index + 1) . '" />
			</div>';
	} else {
		$strReturn .= '
			<div class="bx-breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				' . $slash .
				' <span class="bx-breadcrumb-item-text" itemprop="name">' . $title . '</span>
				<meta itemprop="position" content="' . ($index + 1) . '" />
			</div>';
	}
}

$strReturn .= '</div>';

return $strReturn;

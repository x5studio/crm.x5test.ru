<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="bg_3_figure">

    <div class="case_header" style="">
        <div class="container clearfix">
            <h1><?=$arResult["NAME"]?></h1>
            <? /* <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="" class="round-logo visible-lg pull-right"> */ ?>
        </div>
    </div>

    <div class="container">
        <div class="super-row margin-0-xs">
            <div class="about-case  clearfix">
                <div class="clearfix">
                    <div class="col-sm-8 col-xs-12">
						<?=$arResult["PREVIEW_TEXT"]?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 clearfix">
                    <div class="info_about_project clearfix">
                        <div class="col-sm-7">
                            <h2>Информация о проекте</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?if ($arResult['DISPLAY_PROPERTIES']['PROJECT_EXECUTOR']['VALUE'] != ""):?>
                                    <div class="single_item">
                                        <span class="grey-title">Исполнитель проекта</span>
                                        <span><?=$arResult['DISPLAY_PROPERTIES']['PROJECT_EXECUTOR']['VALUE']?></span>
                                    </div>
                                    <?endif;?>
									<?if ($arResult['DISPLAY_PROPERTIES']['INDUSTRY']['VALUE'] != ""):?>
                                    <div class="single_item">
                                        <span class="grey-title">Отрасль</span>
                                        <span><?=$arResult['DISPLAY_PROPERTIES']['INDUSTRY']['VALUE']?></span>
                                    </div>
									<?endif;?>
									<?if ($arResult['DISPLAY_PROPERTIES']['STATE']['VALUE'] != ""):?>
                                    <div class="single_item">
                                        <span class="grey-title">Штат</span>
                                        <span><?=$arResult['DISPLAY_PROPERTIES']['STATE']['VALUE']?></span>
                                    </div>
                                    <?endif;?>
                                </div>
                                <div class="col-sm-6">
                                    <?if ($arResult['DISPLAY_PROPERTIES']['PROJECT_CUSTOMER']['VALUE'] != ""):?>
                                    <div class="single_item">
                                        <span class="grey-title">Заказчик проекта</span>
                                        <span><?=$arResult['DISPLAY_PROPERTIES']['PROJECT_CUSTOMER']['VALUE']?></span>
                                    </div>
									<?endif;?>
									<?if ($arResult['DISPLAY_PROPERTIES']['SPECIFICITY']['VALUE'] != ""):?>
                                    <div class="single_item">
                                        <span class="grey-title">Специфика</span>
                                        <span><?=$arResult['DISPLAY_PROPERTIES']['SPECIFICITY']['VALUE']?></span>
                                    </div>
									<?endif;?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 main-thesis">
							<?if ($arResult['DISPLAY_PROPERTIES']['BUSINESS_TASKS']['VALUE'] != ""):?>
                            <div class="single_item">
                                <span class="grey-title">Бизнес-задачи</span>
                                <span class="blue-big-font"><?=$arResult['DISPLAY_PROPERTIES']['BUSINESS_TASKS']['VALUE']?></span>
                            </div>
							<?endif;?>
							<?if ($arResult['DISPLAY_PROPERTIES']['AUTOMATED']['VALUE'] != ""):?>
                            <div class="single_item">
                                <span class="grey-title">Автоматизировано</span>
                                <span class="blue-big-font"><?=$arResult['DISPLAY_PROPERTIES']['AUTOMATED']['VALUE']?></span>
                            </div>
							<?endif;?>
							<?if ($arResult['DISPLAY_PROPERTIES']['DURATION']['VALUE'] != ""):?>
                            <div class="single_item">
                                <span class="grey-title">Продолжительность проекта</span>
                                <span class="blue-big-font"><?=$arResult['DISPLAY_PROPERTIES']['DURATION']['VALUE']?></span>
                            </div>
							<?endif;?>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-sm-8 col-xs-12">
						<?=$arResult["DETAIL_TEXT"]?>
                    </div>
                </div>
              <?/*  <div class="results_block">
                    
                    <h2>Результаты</h2>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="single-result">С нуля создана надежная ИТ-инфраструктура для мобильного сервиса под iOS для управления всей цепочкой продаж</div>
                            <div class="single-result">Обеспечена бесперебойная работа мобильного сервиса и его постоянное развитие, заключен договор SLA (соглашение об уровне сервиса)</div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="single-result">В 14 раз сократилось время обработки информации о продажах (с двух недель до одного дня)</div>
                            <div class="single-result">Расчет KPI работников Хейнекен и дистрибьюторов теперь осуществляется в онлайн режиме</div>
                        </div>
                    </div>
                    
                </div>*/?>
            </div>

        </div>
    </div>
</section>
<?/*
<section class="client_mention">
    <div class="container">
        <div class="super-row margin-0-xs">
            <div class="col-sm-4 col-xs-12">
                <img src="img/cases/client.png" alt="">
            </div>
            <div class="col-sm-8 col-xs-12">
                <blockquote>Уже через полгода мы получили первые результаты. Процессы, которые занимали несколько недель, на данный момент решаются за несколько часов и даже несколько минут. Я могу с уверенностью заявить, что проект оправдывает ожидания компании HEINEKEN и инвестиции были правильными»</blockquote>
                <span class="client_name">Александр Кондрашов</span>
                <span class="client_position">Руководитель проектов компании HEINEKEN Russia</span>
            </div>
        </div>

    </div>
</section>
*/?>
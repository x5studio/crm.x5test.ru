<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="cases_header">
    <div class="container">
        <h1><?=$APPLICATION->ShowTitle('h1',false);?></h1>
    </div>
</div>


<div class="container">
    <div class="super-row margin-0-xs">
        <div class="cases_wrap clearfix">

            <div class="first_column_cases col-sm-6 col-xs-12">

				<?foreach($arResult["ITEMS"] as $key => $arItem):?>

                <? if (($key+1)%2 !=0):?>

                <div class="single_case <?if($key > 5):?>hidden<?endif;?>">
                    <div class="wrap_img clearfix"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></div>
                    <span class="title"><?echo $arItem["NAME"]?></span>
					<?echo $arItem["PREVIEW_TEXT"];?>
                    <a target="_blank" class="cases_button" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Посмотреть кейс</a>
					<?//echo $arItem["DETAIL_PAGE_URL"]?>
                </div>

                <?endif;?>

				<?endforeach;?>

            </div>

            <div class="second_column_cases col-sm-6 col-xs-12">

				<?foreach($arResult["ITEMS"] as $key => $arItem):?>
				<? if (($key+1)%2 ==0):?>
                <div class="single_case <?if($key > 5):?>hidden<?endif;?>">
                    <div class="wrap_img clearfix"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></div>
                    <span class="title"><?echo $arItem["NAME"]?></span>
					<?echo $arItem["PREVIEW_TEXT"];?>
                    <a target="_blank" class="cases_button" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Посмотреть кейс</a>
					<?//echo $arItem["DETAIL_PAGE_URL"]?>
                </div>
					<?endif;?>
                <?endforeach;?>

            </div>


        </div>
		<?if($key > 5):?>
        <button class="transparent-btn center-block cases-show">Загрузить ещё кейсы</button>
		<?endif;?>
    </div>
</div>
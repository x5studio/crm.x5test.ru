<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
                <article class="col-sm-12 col-md-4 col-xs-12 long-article">

                    <div class="single-article description-seminar-data">
                        <div class="wrap-article-img">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
                            <div class="tags_wrap">
                                <span>семинары</span>
                            </div>
                        </div>

                        <div class="wrap-article-desc">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title description-seminar-title"><?echo $arItem["NAME"]?></a>
							<?echo $arItem["PREVIEW_TEXT"];?>
                            <div class="event_date_price_wrap">
                                <div class="date_view_count_info clearfix">
												<span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-date.png" class="ico-date" /> <?=$arItem["DISPLAY_PROPERTIES"]['DATE']['VALUE'];?>
												</span>
                                </div>
                                <div class="date_view_count_info clearfix">
												<span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/geo_marker.png" class="ico-maker" /> <?=$arItem["DISPLAY_PROPERTIES"]['PLACE']['VALUE'];?>
												</span>
                                </div>
                                <div class="date_view_count_info clearfix">
												<span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-price.png" class="ico-date" /> <?=$arItem["DISPLAY_PROPERTIES"]['PRICE']['VALUE'];?>
												</span>
                                </div>
                            </div>


                            <a href="javascript:void(0);" class="more btn_modal_enroll_event" data-email="<?=$arItem['PROPERTIES']['EMAIL']['VALUE']?>">Купить билет</a>
                        </div>
                    </div>

                </article>
<?endforeach;?>

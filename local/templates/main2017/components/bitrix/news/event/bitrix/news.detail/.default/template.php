<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <div class="container">
        <div class="super-row margin-0-xs">
            <div class="row clearfix">
                <div class="col-sm-12 col-xs-12">

                    <div class="">

                        <article class="col-sm-12 col-xs-12 big_article dark_article main_event description-seminar-data">
                            <div class="single-event" style="background-image: url(<?=$arResult["DETAIL_PICTURE"]["SRC"]?>);">
                                <div class="tags_wrap">
                                    <span>семинар</span>
                                </div>
                                <div class="wrap-article-desc">
                                    <span class="title description-seminar-title"><?=$arResult['NAME']?></span>
									<?=$arResult['PREVIEW_TEXT']?>
                                    <div class="date_view_count_info clearfix">
												<span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-date-grey.png" class="ico-date" /> <?=$arResult["DISPLAY_PROPERTIES"]['DATE']['VALUE'];?>
												</span>
                                        <span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/geo-marker-grey.png" class="ico-maker" /> <?=$arResult["DISPLAY_PROPERTIES"]['PLACE']['VALUE'];?></span>
                                        <span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-price-grey.png" class="ico-date" /> <?=$arResult["DISPLAY_PROPERTIES"]['PRICE']['VALUE'];?></span>
                                    </div>
                                    <button class="btn_modal_enroll_event" data-email="<?=$arResult['PROPERTIES']['EMAIL']['VALUE']?>">Записаться на семинар</button>
                                </div>
                            </div>
                        </article>

                    </div>


                </div>

            </div>

            <div class="col-sm-8 event_content col-xs-12">

				<?=$arResult['DETAIL_TEXT']?>

            </div>


            <? if (!empty($arResult['PROPERTIES']['EXPERTS']['VALUE'])): ?>
                <?/*<div class="experts-wrap">
                    <h3>Эксперты</h3>

                    <div class="clearfix">
                        <div class="col-sm-6 col-xs-12 single-expert clearfix">
                            <div class="expert-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/events/expert-1.png" alt=""></div>
                            <div class="expert-desc">
                                <span class="title">Степанов Дмитрий</span>
                                <p>Бизнес-консультант по автоматизации строительных компаний различного типа и масштаба</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 single-expert clearfix">
                            <div class="expert-img"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/events/expert-2.png" alt=""></div>
                            <div class="expert-desc">
                                <span class="title">Дарина Сергеева</span>
                                <p>Представитель разработчика BIM-решений компании Renga Software</p>
                            </div>
                        </div>
                    </div>

                </div>*/?>
            <? endif; ?>

        </div>
    </div>


    <section class="event-map">
        <div class="container event-place-desc-wrap">
            <div class="event-place-desc">
                <span class="title">Место проведения</span>
                <div class="desc-ico clearfix">
                    <img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/geo_marker_white.png" alt=""><span><?=$arResult["DISPLAY_PROPERTIES"]['PLACE']['VALUE'];?></span>
                </div>
                <div class="desc-ico clearfix">
                    <? if (!empty($arResult['PROPERTIES']['PHONE']['VALUE'])): ?>
                        <img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-call-white.png" alt=""><span><?=$arResult['PROPERTIES']['PHONE']['VALUE']?></span>
                    <? endif; ?>
                </div>
                <span class="italic">При себе необходимо иметь паспорт или права</span>
            </div>
        </div>
        <?



			$arTmp = explode(",", $arResult['DISPLAY_PROPERTIES']['MAP']['VALUE']);
			$arResult['POSITION']['yandex_lat'] = $arTmp['0'];
			$arResult['POSITION']['yandex_lon'] = $arTmp['1'];
			$arResult['POSITION']['yandex_scale']='13';
			$arResult['POSITION']['PLACEMARKS'][] = array(
				'LON' => $arTmp['1'],
				'LAT' => $arTmp['0'],
				'TEXT' =>$arItem['NAME'],
			);

            $APPLICATION->IncludeComponent(
				"bitrix:map.yandex.view",
				"",
				Array(
					"INIT_MAP_TYPE" => "MAP",
					"MAP_DATA" => serialize($arResult['POSITION']),
					"MAP_WIDTH" => "100%",
					"MAP_HEIGHT" => "450",
					"CONTROLS" => array("ZOOM", "SCALELINE"),
					"OPTIONS" => array("ENABLE_DRAGGING"),
					"MAP_ID" => "yandex-map"
				)
			);?>
    </section>

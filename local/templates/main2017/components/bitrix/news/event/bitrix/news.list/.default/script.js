$(document).ready( function(){

    $('.events_tags_wrap .date_view_count_info').datepicker({
        prevHtml: '<i class="fa fa-angle-left"></i>',
        nextHtml: '<i class="fa fa-angle-right"></i>',
        eventDates: eventDates,
        onRenderCell: function (date, cellType) {
            var currentMonth = date.getMonth();
            var currentDay = date.getDate();
            var currentDate = date.format();
            if (cellType == 'day' && eventDates.indexOf(currentDate) != -1) {
                return {
                    classes: 'event-date'
                }
            }
        },
        onSelect: function(formattedDate, date){
            window.location.href = '/events/?date='+formattedDate;
            console.log(formattedDate);
        }

    });

    $('body').on('click', '.datepicker-btn', function(){
        $('.date_view_count_info .datepicker-inline').fadeToggle();
    })

    $(document).on('click', 'body', function(event) {
        if ($(event.target).closest(".datepicker, .datepicker-btn").length) return;
        if ( $(".datepicker-inline").is(':visible') ){
            $(".datepicker-inline").fadeOut();
        }
        event.stopPropagation();
    });

    $(document).on('click', '.js-event-type:not(.active)', function(event) {
        $('.js-event-type').removeClass('active');
        $(this).toggleClass('active');
        filterEvents();
    });

    $(document).on('click', '.js-event-free', function(event){
        $(this).toggleClass('active');
        filterEvents();
    });

});

function filterEvents(){
    var event_type = $('.js-event-type.active').attr('data-type');
    var evet_free = $('.js-event-free').is('.active');
    $('.js-event-article').addClass('hidden');

    if ( event_type == 'all' ) {

        if (evet_free){
            $('.js-event-article[data-price="free"]').removeClass('hidden');
        } else {
            $('.js-event-article').removeClass('hidden');
        }

    } else {

        if (evet_free){
            $('.js-event-article[data-type="' + event_type + '"][data-price="free"]').removeClass('hidden');
        } else {
            $('.js-event-article[data-type="' + event_type + '"]').removeClass('hidden');
        }

    }
}
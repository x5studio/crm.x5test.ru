<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <div class="">
        <div class="events_tags_wrap clearfix">
            <div class="pull-left js-event-tags_wrap">
                <span class="event_tag js-event-tag js-event-type active" data-type="all">все мероприятия</span>
                <span class="event_tag js-event-tag js-event-type" data-type="seminars">семинары</span>
                <span class="event_tag js-event-tag js-event-type" data-type="webinars">вебинары</span>
                <span class="event_tag js-event-tag js-event-type" data-type="business-practic">бизнес-практикумы</span>
                <span class="event_tag js-event-tag js-event-free">бесплатные</span>
            </div>
            <div class="date_view_count_info pull-right">
                <span class="event_tag active datepicker-btn"><img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-date.png" class="ico-date" /> <?if (isset($_REQUEST['date']) && $_REQUEST['date'] != ""):?><?=$_REQUEST['date'];?><?else:?>не выбрано<?endif;?></span>
            </div>
        </div>
<?/*
                        <article class="col-sm-12 col-xs-12 big_article dark_article main_event">
                            <div class="single-event" style="background-image: url(<?=SITE_TEMPLATE_PATH;?>/assets/img/blog/medium-preview/dark-2.png);">
                                <div class="tags_wrap">
                                    <span>бизнес-практикум</span>
                                </div>
                                <div class="wrap-article-desc">
                                    <span class="title">Инструментарий продаж: CRM, скрипты, телефония</span>
                                    <p>Если Вас интеруют возможности внедрения в Вашей компании: телефонии, мобильных приложений, скриптов, интеграция с сайтом, тогда семинар «Первого БИТа» – для вас! Участие в мероприятии будет интересно собственникам и руководителям компаний, а также руководителям отделов продаж.</p>
                                    <div class="date_view_count_info clearfix">
												<span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-date-grey.png" class="ico-date" /> 28 ноября 10:00
												</span>
                                        <span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/geo-marker-grey.png" class="ico-maker" class="ico-maker" />Казань, ул.Халитова, 2
												</span>
                                        <span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-price-grey.png" class="ico-date" />Бесплатно
												</span>
                                    </div>
                                    <!-- <a href="#" class="more">Читать</a> -->
                                    <button>Записаться на семинар</button>
                                </div>
                            </div>
                        </article>

                    </div>


                </div>


*/?>
    </div>
            <div class="row blog_events_articles events_articles">
                <?$arDate = array();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
	$datetime = explode(" ", $arItem["DISPLAY_PROPERTIES"]['DATE']['VALUE']);
    if (!in_array($datetime[0], $arDate)) {
		$arDate[] =  $datetime[0];
    }
    ?>
                <article class="col-sm-12 col-md-4 col-xs-12 long-article js-event-article"<?if($arItem["DISPLAY_PROPERTIES"]['PRICE']['VALUE'] == "бесплатно"):?> data-price="free"<?endif;?> data-type="<?=$arItem["DISPLAY_PROPERTIES"]['TYPE']['VALUE_XML_ID']?>">

                    <div class="single-article description-seminar-data">
                        <div class="wrap-article-img">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
                            <div class="tags_wrap">
                                <span><?=$arItem["DISPLAY_PROPERTIES"]['TYPE']['VALUE']?></span>
                            </div>
                        </div>

                        <div class="wrap-article-desc">
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title description-seminar-title"><?echo $arItem["NAME"]?></a>
							<?echo $arItem["PREVIEW_TEXT"];?>
                            <div class="event_date_price_wrap">
                                <div class="date_view_count_info clearfix">
												<span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-date.png" class="ico-date" /> <?=$arItem["DISPLAY_PROPERTIES"]['DATE']['VALUE'];?>
												</span>
                                </div>
                                <div class="date_view_count_info clearfix">
												<span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/geo_marker.png" class="ico-maker" /> <?=$arItem["DISPLAY_PROPERTIES"]['PLACE']['VALUE'];?>
												</span>
                                </div>
                                <div class="date_view_count_info clearfix">
												<span>
													<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-price.png" class="ico-date" /> <?=$arItem["DISPLAY_PROPERTIES"]['PRICE']['VALUE'];?>
												</span>
                                </div>
                            </div>


                            <a href="javascript:void(0);" class="more btn_modal_enroll_event" data-email="<?=$arItem['PROPERTIES']['EMAIL']['VALUE']?>">Зарегистрироваться</a>
                        </div>
                    </div>

                </article>
<?endforeach;?>
			<div class="col-sm-12 col-md-4 col-xs-12">
                <div class="subscribe-form">
                    <div class="title">Подпишитесь<br /> на еженедельную<br /> рассылку о новых<br /> мероприятиях</div>
                    <p>Еженедельные уведомления о новых мероприятий компании «Первый БИТ»<br /> по CRM</p>
                    <div class="footer">
                        <input type="text" placeholder="Email" />
                        <button>подписаться</button>
                        <p class="accept-confirm">Нажатием кнопки я принимаю условия Оферты по использованию сайта и согласен с Политикой конфиденциальности</p>
                    </div>
                </div>
            </div>

            </div>

<script>
    var eventDates = <?=json_encode($arDate)?>;
</script>
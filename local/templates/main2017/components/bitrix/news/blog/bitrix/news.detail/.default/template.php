<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h1><?echo $arResult["NAME"];?></h1>

<div class="article_body">
    <div class="row">
        <div class="col-md-8">
            <? if ($arResult['DISPLAY_PROPERTIES']['CONTENTS_HTML']['DISPLAY_VALUE']): ?>
                <div class="article-contents">
                    <div class="article-contents__title"><?=GetMessage("CONTENTS")?></div>
                    <nav class="article-contents__nav" id="js-article-navigation">
                            <? echo $arResult['DISPLAY_PROPERTIES']['CONTENTS_HTML']['DISPLAY_VALUE']; ?>
                    </nav>
                </div>
            <? endif; ?>
        </div>
        <div class="col-md-1">

        </div>
        <div class="col-md-3">
            <?if($arResult['DISPLAY_PROPERTIES']['AUTHOR']['VALUE']):?>
                <div class="article-author">
                    <div>
                        <?if($arResult['DISPLAY_PROPERTIES']['AUTHOR_IMG']['FILE_VALUE']['SRC']):?>
                            <div class="article-author__img" style="background-image: url('<?=$arResult['DISPLAY_PROPERTIES']['AUTHOR_IMG']['FILE_VALUE']['SRC'];?>');"></div>
                        <?else:?>
                            <div class="article-author__img" style="background-image: url('<?=$templateFolder?>/img/author-default.svg');"></div>
                        <?endif;?>
                    </div>
                    <div>
                        <div class="article-author__name"><?=$arResult['DISPLAY_PROPERTIES']['AUTHOR']['VALUE']?></div>

                        <?if($arResult['DISPLAY_PROPERTIES']['AUTHOR_POSITION']['VALUE']):?>
                            <div class="article-author__position"><?=$arResult['DISPLAY_PROPERTIES']['AUTHOR_POSITION']['VALUE']?></div>
                        <?else:?>
                            <div class="article-author__position"><?=GetMessage("COMPANY_MEMBER")?></div>
                        <?endif;?>
                    </div>
                </div>
            <?endif;?>
        </div>
    </div>
    <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="">
	<?echo $arResult["DETAIL_TEXT"];?>
</div>
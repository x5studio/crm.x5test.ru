<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

// === SLIDER ON THE FIRST PAGE ===
if ($arResult['NAV_RESULT']->PAGEN == "1") {
    $arTopItems = array();
    $arFilter = Array("IBLOCK_ID"=>$arResult['ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_TOP_VALUE" => "Да");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), Array("*"));
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arFields['DETAIL_PICTURE_SRC'] = CFile::GetPath($arFields["DETAIL_PICTURE"]);
        $arTopItems[] = $arFields;
    }


}

$this->setFrameMode(true);
?>
<div class="article_wrap clearfix">

    <?if (!empty($arTopItems)):?>
        <div class="owl-carousel owl-theme j-owl-carousel" data-responsive="0=1" data-dots="Y" data-autoplay="Y" data-loop="Y" data-margin="10" data-nav="N">
            <? foreach($arTopItems as $topItem): ?>
                <div class="col-sm-12 col-xs-12 big_article item dark_article item-arcslide">
                    <div class="single-event" style="background-image: url(<?=$topItem["DETAIL_PICTURE_SRC"]?>);">
                        <div class="tags_wrap">
                            <?php
                            if (!empty($topItem['TAGS'])):
                                foreach ($topItem['TAGS'] as $tagName):
                                    ?>
                                    <a href="<?php echo $arResult['NAV_RESULT']->strListUrl; ?>tags/<?php echo $tagName; ?>/"><span><?php echo $tagName; ?></span></a>
                                <?php
                                endforeach;
                            endif;
                            ?>
                        </div>
                        <a href="<?=$topItem["DETAIL_PAGE_URL"]?>" >
                            <div class="wrap-article-desc">
                                <div class="date_view_count_info clearfix">
                                    <span>
                                        <img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-date-grey.png" class="ico-date" /> <?=$topItem["DISPLAY_ACTIVE_FROM"]?>
                                    </span>
                                </div>
                                <span class="title"><?=$topItem["NAME"]?></span>
                                <p><?=$topItem["PREVIEW_TEXT"];?></p>
                                <span class="more">Читать</span>
                            </div>
                        </a>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    <? endif; ?>

	<?foreach($arResult["ITEMS"] as $arItem):?>

        <article class="col-sm-12 col-md-6 col-xs-12">

            <div class="single-article">
                <div class="wrap-article-img">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""></a>
                    <div class="tags_wrap">
                        <?php
                        if (!empty($arItem['TAGS'])):
                            foreach ($arItem['TAGS'] as $tagName):
                                ?>
                                <a href="<?php echo $arResult['NAV_RESULT']->strListUrl; ?>tags/<?php echo $tagName; ?>/"><span><?php echo $tagName; ?></span></a>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </div>
                </div>

                <div class="wrap-article-desc">
                    <div class="date_view_count_info clearfix">
                        <span>
                            <img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/icons/ico-date.png" class="ico-date" /> <?=$arItem["DISPLAY_ACTIVE_FROM"]?>
                        </span>
                    </div>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="title"><?=$arItem["NAME"]?></a>
                    <?=$arItem["PREVIEW_TEXT"];?>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="more">Читать</a>
                </div>
            </div>

        </article>

	<?endforeach;?>
    <?/*
    <article class="col-sm-12 col-md-6 col-xs-12 dark_article">
        <div class="single-event">
            <div class="tags_wrap">
                <span>интеграции</span>
            </div>

            <div class="wrap-article-desc">
                <div class="date_view_count_info clearfix">
												<span>
													<img src="img/icons/ico-date-grey.png" class="ico-date" /> 13 ноября 2017
												</span>
                </div>
                <span class="title">Лёгкое внедрение сложной СRM</span>
                <p>Настройка отчетности в разных разрезах для оперативного контроля.</p>
                <a href="#" class="more">Читать</a>
            </div>
        </div>
    </article>
    */?>

</div>

<div class="text-center">

    <?if($arResult["NAV_RESULT"]):?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
    <?endif;?>

</div>

<script>
/*
    require(['owlcarousel2'], function() {
        $('#topArticleSlide').owlCarousel({
            loop: true,
            nav: false,
            dots: false,
            autoplay: false,
            navSpeed: 200,
            dotsSpeed: 200,
            responsive: {
                0: {
                    items: 1
                }
            },
            slideBy: 3,
            navText: '',
            margin: 0
        });
    });
*/
</script>
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$component = $this->__component;

$bTopExists = false;
if (!empty($arResult['ITEMS']))
{
    foreach($arResult['ITEMS'] as $key => $arItem)
    {
        if ($arItem['DISPLAY_PROPERTIES']['TOP']['VALUE'] == 'Да')
        {
            if (!$bTopExists)
            {
                unset($arResult['ITEMS'][$key]);
                array_unshift($arResult['ITEMS'], $arItem);
                $bTopExists = true;
            }
            else
            {
                $arResult['ITEMS'][$key]['DISPLAY_PROPERTIES']['TOP']['VALUE'] = '';
            }
        }

        if (!empty($arItem['TAGS'])) {
            $arTags = explode(',', $arItem['TAGS']);

            foreach ($arTags as $i => $tagName) {
                $arTags[$i] = trim($tagName);
            }

            $arResult['ITEMS'][$key]['TAGS'] = $arTags;
        }
    }
}

if (!$bTopExists)
{
    $arItem = array();
    $arFilter = array(
        'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'PROPERTY_TOP_VALUE' => 'Да',
    );

    if (!empty($arParams['FILTER_NAME']))
    {
        $arFilter = array_merge($arFilter, $GLOBALS[$arParams['FILTER_NAME']]);
    }

    $obElement = \CIBlockElement::GetList(
        array(),
        $arFilter,
        false,
        array(
            'nTopCount' => 1,
        ),
        array('*', 'PROPERTY_*')
    )->GetNextElement();

    if ($obElement)
    {
        $arItem = $obElement->GetFields();
        $arItem["DISPLAY_PROPERTIES"] = $obElement->GetProperties();
        $arItem["DETAIL_PICTURE"] = array(
            'ID' => $arItem["DETAIL_PICTURE"],
            'SRC' => CFile::GetPath($arItem["DETAIL_PICTURE"]),
        );
    }

    if ($arItem)
    {
        array_pop($arResult['ITEMS']);
        array_unshift($arResult['ITEMS'], $arItem);
    }
}

<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$component = $this->__component;

if (!empty($arResult['TAGS'])) {
    $arTags = explode(',', $arResult['TAGS']);

    foreach ($arTags as $i => $tagName) {
        $arTags[$i] = trim($tagName);
    }

    $arResult['TAGS'] = $arTags;
}

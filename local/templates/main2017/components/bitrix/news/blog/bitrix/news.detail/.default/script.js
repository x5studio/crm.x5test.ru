function scrollToTarget(e){
    e.preventDefault();
    var href = e.target.getAttribute("href");
    if (!href){
        return false;
    }
    var target = document.querySelector(href);

    if (target !== null){
        var offset = 150;

        if (document.body.clientWidth < 768){
            offset = 100;
        }
        if(BX.admin){
            var BXPanelHeight = parseInt(BX.admin.panel.DIV.clientHeight);
            offset += BXPanelHeight;
        }
        var offsetTop = target.offsetTop + offset;
        scroll({
            top: offsetTop,
            behavior: "smooth"
        });
    }
}

BX.ready(function() {
    var nav = document.getElementById('js-article-navigation');
    if (typeof nav !== 'undefined'){
        var links = nav.getElementsByTagName('a');
        if (!!links && links.length > 0)
        {
            for (var i = 0; links.length > i; i++)
            {
                BX.bind(links[i], 'click', BX.delegate(function(e){ scrollToTarget(e);}, this));
            }
        }
    }
});
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


\Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/js/vendor/OwlCarousel2/dist/assets/owl.carousel.min.css');

foreach (array_keys($_GET) as $key) {
    if (strpos($key, 'PAGEN_') === 0) {
        $APPLICATION->SetPageProperty('canonical', 'https://'.$_SERVER['SERVER_NAME'].'/blog/');

        break;
    }
}


<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

include($_SERVER["DOCUMENT_ROOT"].$this->GetPath()."/include/general.component_epilog.php");

// BEGIN: Это код не получится перенести в result_modifier. Подробности в файле "result_modifier.php"
$includePath = '/include/';

$curPageClean = $APPLICATION->GetCurPage();
$curPageClean = trim($curPageClean, '/');
$curPageClean = str_replace('/', '__', $curPageClean);

$title1FileName = $curPageClean.'__title_1.php';
$title2FileName = $curPageClean.'__title_2.php';
$descriptionFileName = $curPageClean.'__description.php';

$arResult['INCLUDE_PATH'] = $includePath;
$arResult['TITLE_1_FILE_PATH'] = $includePath.$title1FileName;
$arResult['TITLE_2_FILE_PATH'] = $includePath.$title2FileName;
$arResult['DESCRIPTION_FILE_PATH'] = $includePath.$descriptionFileName;
// END: Это код не получится перенести в result_modifier. Подробности в файле "result_modifier.php"

if (!file_exists(__DIR__.$arResult['INCLUDE_PATH'])) {
    mkdir(__DIR__.$arResult['INCLUDE_PATH']);
}

if (!file_exists(__DIR__.$arResult['TITLE_1_FILE_PATH'])) {
    file_put_contents(__DIR__.$arResult['TITLE_1_FILE_PATH'], 'Заявка на подбор и запуск CRM');
}

if (!file_exists(__DIR__.$arResult['TITLE_2_FILE_PATH'])) {
    file_put_contents(__DIR__.$arResult['TITLE_2_FILE_PATH'], 'После внедрения CRM не потеряется ни одна заявка!');
}

if (!file_exists(__DIR__.$arResult['DESCRIPTION_FILE_PATH'])) {
    file_put_contents(__DIR__.$arResult['DESCRIPTION_FILE_PATH'], 'Удобный и простой сервис для быстрых продаж или компаний с небольшим штатом сотрудников');
}


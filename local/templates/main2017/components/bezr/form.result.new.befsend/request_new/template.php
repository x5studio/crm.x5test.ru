<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$PARENT = $this->__component->__parent;

// BEGIN: Это код не получится перенести в result_modifier. Подробности в файле "result_modifier.php"
$arResult["HIDDEN_INPUTS"] = [];
foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
    if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
        array_push($arResult["HIDDEN_INPUTS"], $FIELD_SID);
    }
}
$includePath = '/include/';

$curPageClean = $APPLICATION->GetCurPage();
$curPageClean = trim($curPageClean, '/');
$curPageClean = str_replace('/', '__', $curPageClean);

$title1FileName = $curPageClean.'__title_1.php';
$title2FileName = $curPageClean.'__title_2.php';
$descriptionFileName = $curPageClean.'__description.php';

$arResult['INCLUDE_PATH'] = $includePath;
$arResult['TITLE_1_FILE_PATH'] = $includePath.$title1FileName;
$arResult['TITLE_2_FILE_PATH'] = $includePath.$title2FileName;
$arResult['DESCRIPTION_FILE_PATH'] = $includePath.$descriptionFileName;
// END: Это код не получится перенести в result_modifier. Подробности в файле "result_modifier.php"
?>


<div class="request" data-scroll="request-form">
    <p class="request__title"><?$APPLICATION->IncludeFile($templateFolder.$arResult['TITLE_1_FILE_PATH'], [], ['NAME'=>'Заголовок 1',])?></p>
    <div class="request__wrapper">
        <div class="request__explanation">
            <b class="request__lead title"><?$APPLICATION->IncludeFile($templateFolder.$arResult['TITLE_2_FILE_PATH'], [], ['NAME'=>'Заголовок 2',])?></b>
            <p class="request__description"><?$APPLICATION->IncludeFile($templateFolder.$arResult['DESCRIPTION_FILE_PATH'], [], ['NAME'=>'Описание',])?></p>
        </div>
        <form class="request__form j-form-page j-form" name="<?= $arResult["arForm"]["SID"] ?>" action="<?= POST_FORM_ACTION_URI ?>"
              method="POST" enctype="multipart/form-data" data-salt-form="<?= $arResult["SOULT_FORM"] ?>">
            <input type="hidden" name="sessid" id="sessid" value=""/>
            <input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>"/>
            <div class="request__input-wrapper">
                <? foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion): ?>
                    <?if (in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
                        <div class="text">
                            <input name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
                                   type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel" : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"
                                    <? if (!empty($arQuestion['STRUCTURE'][0]['FIELD_PARAM'])): ?>
                                        data-type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] ?>"
                                    <? endif; ?>
                                   value="<?= $arResult['arrVALUES'][$FIELD_SID] ?: null ?>"
                                   placeholder="<?= $arQuestion["CAPTION"] ?>"
                                   class="request__input <?if($arQuestion['REQUIRED'] == "Y"):?>required<?endif;?> <?if($arQuestion['CAPTION'] == "Телефон"):?>mask-phone<?endif;?>"/>
                        </div>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
            <p class="request__agreement">
                Нажимая кнопку, я принимаю условия
                <a class="request__link" href="/oferta/" target="_blank" rel="nofollow noopener">Оферты</a>
                по использованию сайта и согласен с
                <a class="request__link" href="/politika-konfidencialnosti/" target="_blank" rel="nofollow noopener">
                    <span class="request__link-politic">Политикой</span>
                    <span class="request__link--confidentiality">конфиденциальности</span>
                </a>
            </p>

            <? if($arResult["isUseCaptcha"] == "Y"): ?>
                <?if ($arResult['modeUseCaptcha'] == 'recaptcha'):?>
                    <div class="j-recaptcha">
                        <?include($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/pb.main/tools/re_captcha.php");?>
                        <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"/>
                    </div>
                <?else:?>
                    <? $frame = $this->createFrame()->begin(''); ?>
                    <div class="clearfix">
                        <div class="float-left">
                            <label><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><span class="required">*</span></label>
                            <div class="text">
                                <input type="text" name="captcha_word" size="30" maxlength="50" value="" />
                            </div>
                        </div>
                        <div class="float-right">
                            <input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" style="margin-top: 15px" />
                        </div>
                    </div>
                    <? $frame->end(); ?>
                <? endif; ?>
            <? endif; ?>

            <button class="button button--request" type="submit">
                Отправить
            </button>

            <? $arHiddenID = $arResult["HIDDEN_INPUTS"]; ?>
            <?if (!empty($arHiddenID)) {
                foreach ($arHiddenID AS $hiddenID)
                {
                    echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
                }
            } ?>
            <? if (!empty($arParams['COMPONENT_MARKER'])): ?>
                <input type="hidden" name="COMPONENT_MARKER" value="<?= $arParams['COMPONENT_MARKER'] ?>">
            <? endif; ?>
        </form>
    </div>
</div>


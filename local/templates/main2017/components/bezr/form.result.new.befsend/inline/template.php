<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$PARENT = $this->__component->__parent;
?>
<section class="form_inline">
    <div class="container">

				<form name="<?= $arResult["arForm"]["SID"] ?>" action="<?=  POST_FORM_ACTION_URI ?>"
							method="POST" enctype="multipart/form-data"  class="main-feedback-form j-form-page j-form" data-salt-form="<?= $arResult["SOULT_FORM"] ?>">
                    <?php
                    if ($arParams["FORM_TITLE"]):?>
                        <h2><?= $arParams['FORM_TITLE']; ?></h2>
                        <h4><?= $arParams['FORM_SUBTITLE']; ?> </h4>
                    <? else:?>
                        <h2>Расскажем подробнее про <?= $arParams["PRODUCT_NAME"] ?></h2>
                        <h4>Наш специалист свяжется с Вами через час или раньше</h4>
                    <?endif?>
					<input type="hidden" name="sessid" id="sessid" value=""/>
					<?/*<input type="hidden" name="sessid" id="sessid" value=""/>*/?>
					<input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>"/>
                    <div class="row">
					<? $arHiddenID = Array(); ?>
                        <div class="col-md-12 col-sm-12 col-xs-12">
						<? $k = 0;
						foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):$k++; ?>
							<?
							// TEXT EMAIL ...
							if (in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
									<div class="text col-md-3">
									<input name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
											type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel" : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"<? if (!empty($arQuestion['STRUCTURE'][0]['FIELD_PARAM']))
									{ ?>	data-type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] ?>"<?
									} ?>	value="<?= $arResult['arrVALUES'][$FIELD_SID] ?: null ?>"
											placeholder="<?= $arQuestion["CAPTION"] ?>"
										    class="form-control<?if($arQuestion['REQUIRED'] == "Y"):?> required<?endif;?><?if($arQuestion['CAPTION'] == "Телефон"):?> mask-phone<?endif;?>"/>
									</div>
								<?
							// DROPDOWN
							elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'dropdown')://__($arQuestion)
								?>
								<div class="text"></div><?= $arQuestion["HTML_CODE"] ?></div>
								<?
							// FILE
							elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'file'):

							elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'):
								array_push($arHiddenID, $FIELD_SID);
							?>
							<? endif; ?>
						<? endforeach; ?>
						
						<?// recaptcha ?>
						<? if($arResult["isUseCaptcha"] == "Y"):
							switch ($arResult['modeUseCaptcha']):
								case 'recaptcha': ?>
									<div class="j-recaptcha">
										<?include($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/pb.main/tools/re_captcha.php");?>
										<input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"/>
									</div>
									<? break;
								default:
									$frame = $this->createFrame()->begin(''); ?>
									<div class="clearfix">
										<div class="float-left">
											<label><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><span class="required">*</span></label>
											<div class="text">
												<input type="text" name="captcha_word" size="30" maxlength="50" value="" />
											</div>
										</div>
										<div class="float-right">
											<input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" />
											<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" style="margin-top: 15px" />
										</div>
									</div>
									<? $frame->end();
									break;
							endswitch; ?>
						<? endif; ?>
						
                        <div class="col-md-3"><button type="submit" class="pull-right">отправить</button></div>
                                </div>
                    </div>
                                    <div class="row clearfix">
                                        <div class="col-md-12 convention_text_inline text-center">
                                            <p>Нажимая кнопку, я принимаю условия <a href="/oferta/">Оферты по использованию сайта</a> и согласен с <a href="/politika-konfidencialnosti/">Политикой конфиденциальности</a></p>
                                        </div>

                                    </div>

					<? // SUBMIT ?>
					<?
					// HIDDENS
					if (!empty($arHiddenID))
					{
						$k--;
						foreach ($arHiddenID AS $hiddenID)
						{
							echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
						}
					} ?>
					<? if (!empty($arParams['COMPONENT_MARKER'])): ?>
						<input type="hidden" name="COMPONENT_MARKER" value="<?= $arParams['COMPONENT_MARKER'] ?>">
					<? endif; ?>
				</form>
	</div>
</section>
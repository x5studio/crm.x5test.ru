<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$PARENT = $this->__component->__parent; ?>
<section class="leavecomment">
	<div class="container">
		<div class="row">
			<div id="general2page-form-<?= $arResult["SOULT_FORM"] ?>"
					 class="j-form-page fbox"
					 data-salt-form="<?= $arResult["SOULT_FORM"] ?>"
			>
				<?
				/**********************************************************************************
				 * form header
				 **********************************************************************************/
				?>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="dontknow">
						<h2> Не знаете какая лицензия Битрикс 24 нужна вам?</h2>
						<p>Заполните форму, мы изучим потребности вашего бизнеса и порекомендуем наиболее оптимальную и выгодную
							для вас лицензию</p>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="dontknow">
						<? if ($arResult["isFormErrors"] == "Y"): ?><?//= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>

						<form name="<?= $arResult["arForm"]["SID"] ?>" action="<?= POST_FORM_ACTION_URI ?>"
									method="POST" enctype="multipart/form-data">
							<input type="hidden" name="sessid" id="sessid" value=""/>
							<input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>"/>

							<? $arHiddenID = Array(); ?>
							<? $k = 0;
							foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):$k++; ?>
								<?
								// TEXT EMAIL ...
								if (in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
									<div class="form-group has-feedback<?if(strlen($arResult["FORM_ERRORS"][$FIELD_SID]) > 0):?> has-error<?endif;
									?>">
										<input
												name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
												type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel" : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"<? if (!empty($arQuestion['STRUCTURE'][0]['FIELD_PARAM']))
										{ ?>
											data-type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] ?>"<?
										} ?>
												value="<?= $arResult['arrVALUES'][$FIELD_SID] ?: null ?>"
												placeholder="<?= $arQuestion["CAPTION"] ?>"
												class="form-control"
										/>
										<span class="glyphicon glyphicon-remove form-control-feedback"></span>
									</div>
									<?
								// DROPDOWN
								elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'dropdown')://__($arQuestion)
									?>
									<?= $arQuestion["HTML_CODE"] ?>
									<?
								// TEXTAREA
								elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea'):?>
									<?= $arQuestion["HTML_CODE"] ?>
									<?
								// FILE
								elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'file'):?>
									<label><?= $arQuestion["CAPTION"] ?></label>
									<? if ($arQuestion["REQUIRED"] == "Y"):?><?= $arResult["REQUIRED_SIGN"]; ?><? endif; ?>
									<div class="text" style="height: 30px; line-height: 30px"><?= $arQuestion["HTML_CODE"] ?></div>
								<? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'):$k--; ?>
									<? array_push($arHiddenID, $FIELD_SID); ?>
								<? endif; ?>
							<? endforeach; ?>
							
							<?// recaptcha ?>
							<? if($arResult["isUseCaptcha"] == "Y"):
								switch ($arResult['modeUseCaptcha']):
									case 'recaptcha': ?>
										<div class="j-recaptcha">
											<?include($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/pb.main/tools/re_captcha.php");?>
											<input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"/>
										</div>
										<? break;
									default:
										$frame = $this->createFrame()->begin(''); ?>
										<div class="clearfix">
											<div class="float-left">
												<label><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><span class="required">*</span></label>
												<div class="text">
													<input type="text" name="captcha_word" size="30" maxlength="50" value="" />
												</div>
											</div>
											<div class="float-right">
												<input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" />
												<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" style="margin-top: 15px" />
											</div>
										</div>
										<? $frame->end();
										break;
								endswitch; ?>
							<? endif; ?>
							
							<? // SUBMIT ?>
							<div class="row submit">
								<input type="submit" class="btn btn-default submit" name="web_form_submit"
											 value="<?= $arParams['MESS']['SUBMIT_BTN'] ?: (strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>">
							</div>
							<?
							// HIDDENS
							if (!empty($arHiddenID))
							{
								$k--;
								foreach ($arHiddenID AS $hiddenID)
								{
									echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
								}
							} ?>
							<? if (!empty($arParams['COMPONENT_MARKER'])): ?>
								<input type="hidden" name="COMPONENT_MARKER" value="<?= $arParams['COMPONENT_MARKER'] ?>">
							<? endif; ?>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$PARENT = $this->__component->__parent; ?>

<div id="general-form-<?=$arResult["SOULT_FORM"]?>"
     class="popup onecol j-form-popup popup-boss"
     data-salt-form="<?= $arResult["SOULT_FORM"] ?>"
     data-btn="<?=$arParams["~BTN_CALL_FORM"]?>"
     style="display: none;">
    <div class="popup-title">
        <div class="close j-close"></div>
        <div class="title-text"><?=(!empty($arParams['MESS']['FORM_TITLE']) ? $arParams['MESS']['FORM_TITLE'] : $arResult["FORM_TITLE"]) ?></div>
    </div>
    <div class="popup-content">
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="wrap_img">
                        <img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/head_first_bit.png" alt="">
                    </div>
                    <div class="wrap_desc">
                        <blockquote>Наша цель – решить все Ваши текущие задачи и создать новые возможности для Вашего успешного развития. Сделать это качественно и профессионально. Ваши предложения и пожелания – ключ к этому. Буду рад обратной связи по работе как подразделений, так и отдельных сотрудников»</blockquote>
                        <span class="head_name">Антон Долгов</span>
                        <span class="head_position">Руководитель Компании «Первый БИТ»</span>
                    </div>
                </div>
				<?//= $arResult["SOULT_FORM"] ?>

				<? if ($arResult["isFormErrors"] == "Y"): ?><? //= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>

                <form name="<?= $arResult["arForm"]["SID"] ?>" action="<?= POST_FORM_ACTION_URI ?>"
                      method="POST" enctype="multipart/form-data"  class="main-feedback-form">
                    <input type="hidden" name="sessid" id="sessid" value=""/>
                    <input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>"/>
                    <div class="super-row">
						<? $arHiddenID = Array(); ?>
                        <div class="col-md-6 col-sm-12 col-xs-12">
							<? $k = 0;
							foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):$k++; ?>
							<?
							// TEXT EMAIL ...
							if (in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
                                <div class="text">
                                    <input name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
                                            type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel" : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"<? if (!empty($arQuestion['STRUCTURE'][0]['FIELD_PARAM']))
                                    { ?> data-type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] ?>"<?
                                    } ?> value="<?= $arResult['arrVALUES'][$FIELD_SID] ?: null ?>" placeholder="<?= $arQuestion["CAPTION"] ?>" class="form-control<?if($arQuestion['REQUIRED'] == "Y"):?> required<?endif;?><?if($arQuestion['CAPTION'] == "Телефон"):?> mask-phone<?endif;?>"
                                    />
                                </div>
							<?
							// DROPDOWN
                            elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'dropdown')://__($arQuestion)
								?>
								<div class="text"><?= $arQuestion["HTML_CODE"] ?></div>
							<?
							// TEXTAREA
                            elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea'):?>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 row-textarea">
                            <div class="text"><textarea class="form-control" cols="30" rows="10" id="description" placeholder="<?= $arQuestion["CAPTION"] ?>" name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"><?= $arResult['arrVALUES'][$FIELD_SID] ?: null ?></textarea></div>
							<?
							// FILE
                            elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'file'):

                            elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'):
                                array_push($arHiddenID, $FIELD_SID);?>

							<? endif; ?>
							<? endforeach; ?>
                        </div>
                    </div>
					
					<?// recaptcha ?>
					<? if($arResult["isUseCaptcha"] == "Y"):
						switch ($arResult['modeUseCaptcha']):
							case 'recaptcha': ?>
								<div class="j-recaptcha">
									<?include($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/pb.main/tools/re_captcha.php");?>
									<input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($arResult["CAPTCHACode"]); ?>"/>
								</div>
								<? break;
							default:
								$frame = $this->createFrame()->begin(''); ?>
								<div class="clearfix">
									<div class="float-left">
										<label><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><span class="required">*</span></label>
										<div class="text">
											<input type="text" name="captcha_word" size="30" maxlength="50" value="" />
										</div>
									</div>
									<div class="float-right">
										<input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" />
										<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" style="margin-top: 15px" />
									</div>
								</div>
								<? $frame->end();
								break;
						endswitch; ?>
					<? endif; ?>
					
                    <div class="super-row clearfix">
                        <div class="col-sm-12 col-md-7 col-xs-12 convention_text">
                            <p>Нажимая кнопку, я принимаю условия <a href="/oferta/" class="blue-font">Оферты</a> по использованию сайта и согласен с <a href="/politika-konfidencialnosti/" class="blue-font">Политикой конфиденциальности</a></p>
                        </div>
						<?/*
                                    <input type="submit" class="btn btn-default submit" name="web_form_submit"
                                           value="<?= $arParams['MESS']['SUBMIT_BTN'] ?: (strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>">
*/?>
                        <button type="submit" class="pull-right">отправить отзыв</button>
                    </div>

					<? // SUBMIT ?>
					<?
					// HIDDENS
					if (!empty($arHiddenID))
					{
						$k--;
						foreach ($arHiddenID AS $hiddenID)
						{
							echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
						}
					} ?>
					<? if (!empty($arParams['COMPONENT_MARKER'])): ?>
                        <input type="hidden" name="COMPONENT_MARKER" value="<?= $arParams['COMPONENT_MARKER'] ?>">
					<? endif; ?>
                </form>
            </div>
        </div>
</div>

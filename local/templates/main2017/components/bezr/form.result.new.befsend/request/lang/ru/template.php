<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['FORM_APPLY'] = "Применить";
$MESS ['FORM_ADD'] = "Добавить";
$MESS ['FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['FORM_DATA_SAVED1'] = "Спасибо!<br><br>Ваша заявка №";
$MESS ['FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['LEAVE_COMPLAINT'] = "Оставить отзыв";
$MESS ['LEAVE_COMPLAINT_DESC'] = "Оставьте отзыв о нашей<br>работе и услугах";
$MESS ['TO_PRAISE'] = "Похвалить";
$MESS ['TO_COMPLAIN'] = "Пожаловаться";
$MESS ['ERROR'] = "Ошибка!";
$MESS ['THANK_YOU'] = "Спасибо за вашу заявку!";
$MESS ['WAIT_CALL'] = 'Мы свяжемся с вами в ближайшее время.';
$MESS ['ORDER_CALL'] = "Заказать звонок";
?>

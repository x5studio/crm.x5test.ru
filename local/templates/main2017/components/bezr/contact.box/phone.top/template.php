<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Grid\Declension;

$officeDeclension = new Declension('филиал', 'филиала', 'филиалов');
$officeCount = count($arResult["ITEMS"]);
?>
<a class="tel" href="tel:<?=preg_replace("/[^0-9]/", '', $arResult["ITEMS"][0]['PROPERTIES']['PHONE']['VALUE'])?>"><?= $arResult["ITEMS"][0]['PROPERTIES']['PHONE']['VALUE']; ?></a>
<a href="/contacts/" class="more-filials">ещё <?=$officeCount; ?> <?=$officeDeclension->get($officeCount);?></a>
<?$this->SetViewTarget('office_top_block');?>
<a class="tel" href="tel:<?=preg_replace("/[^0-9]/", '', $arResult["ITEMS"][0]['PROPERTIES']['PHONE']['VALUE'])?>"><?= $arResult["ITEMS"][0]['PROPERTIES']['PHONE']['VALUE']; ?></a>
<a href="/contacts/" class="more-filials">ещё <?=$officeCount; ?> <?=$officeDeclension->get($officeCount);?></a>
<?$this->EndViewTarget();?>
<?$this->SetViewTarget('office_phone');?>
<a href="tel:<?=preg_replace("/[^0-9]/", '', $arResult["ITEMS"][0]['PROPERTIES']['PHONE']['VALUE'])?>" class="tel"><?= $arResult["ITEMS"][0]['PROPERTIES']['PHONE']['VALUE']; ?></a>
<?$this->EndViewTarget();?>

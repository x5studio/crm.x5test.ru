<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<?if ($GLOBALS['IS_NEW_PAGE']):?>
    </main>
<?endif;?>

<?if (!$GLOBALS['IS_NEW_PAGE']):?>
    <?
    $page = $APPLICATION->GetCurPage();
    $caption = ($page == "/products/bit-crm/") ? true : false;

    $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "request", Array(
        "WEB_FORM_ID" => WEB_FORM_REQUEST,  // ID веб-формы
        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
        "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
        "EMAIL_QUESTION_CODE" => "EMAIL",
        "CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
        "TO_QUESTION_RESIPIENTS" => "",
        "EMAIL_ONLY_FROM_RESIPIENTS" => "",
        "CAPTION_SWITCH" => $caption,
        "PRODUCT_QUESTION_CODE" => "PRODUCT",
        "TO_QUESTION_CODE" => "TO",
        "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
        "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
        "CACHE_TYPE" => "A",  // Тип кеширования
        "CACHE_TIME" => "3600",  // Время кеширования (сек.)
        "LIST_URL" => "",  // Страница со списком результатов
        "EDIT_URL" => "",  // Страница редактирования результата
        "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
        "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
        "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        ),
        "ALERT_ADD_SHARE" => "N",
        "HIDE_PRIVACY_POLICE" => "Y",
        "BTN_CALL_FORM" => ".btn_order",
        "STRONG_SHOW_CITY" => "N",
        "STRONG_SHOW_OFFICE" => "N",
        "GA_SEND" => array("ACTION" => "podbor_send"),
        "HIDE_FIELDS" => array(
            0 => "CITY",
            1 => "OFFICE",
        ),
        "COMPONENT_MARKER" => "request",
        "SHOW_FORM_DESCRIPTION" => "N",
        "MESS" => array(
            "THANK_YOU" => "Спасибо, ваша заявка принята!",
            "WAIT_CALL" => "Мы свяжемся с вами в течении 2-х часов.",
            "TITLE_FORM" => "Оставить заявку",
        ),
        "SHOW_TITLE" => "N",
    ),
        false,
        array(
            "HIDE_ICONS" => "Y"
        )
    );
    ?>
<?endif;?>
<footer>
	<div class="container">
		<div class="row">

			<div class="col-sm-3 hidden-xs">
				<a href="">
					<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/layout/firstbit.svg" width="180" alt="" />
				</a>
				<span class="copyright">&copy;1997-<?=date('Y');?> – «Первый БИТ» </span>

			</div>

			<div class="col-sm-6 hidden-xs">
				<ul class="col-sm-4">
					<li><span class="margin_bottom_10 a_text">Продукты</span></li>
                    <li><a href="/products/bitriks-24/" class="sub_menu">Битрикс24</a></li>
					<li><a href="/products/bit-crm-3-0/" class="sub_menu">БИТ.CRM 3</a></li>
					<li><a href="/products/storverk-crm/" class="sub_menu">STORVERK CRM</a></li>
					<li><a href="/products/1s-crm/" class="sub_menu">1С:CRM</a></li>
					<li><a href="/products/amocrm/" class="sub_menu">amoCRM</a></li>
					<li><a href="/products/megaplan/" class="sub_menu">Мегаплан</a></li>
				</ul>
				<ul class="col-sm-4">
					<li><span class="margin_bottom_10 a_text">Услуги</span></li>
                    <li><a href="/vnedrenie-crm/" class="sub_menu">Внедрение CRM</a></li>
					<li><a href="/obuchenie-rabote-v-crm/" class="sub_menu">Обучение работе в CRM</a></li>
					<li><a href="/tekhnicheskaya-podderzhka/" class="sub_menu">Техническая поддержка CRM</a></li>
				</ul>
				<ul class="col-sm-4">
					<?/* <li><a href="#">Услуги</a></li> */?>
					<li><a href="/events/">Мероприятия</a></li>
					<li><a href="/clients/">Кейсы</a></li>
					<?/*<li><a href="/instruction/">База знаний</a></li>*/?>
				</ul>
				<ul class="col-sm-4">
					<li><a href="/blog/">Блог</a></li>
					<li><a href="/contacts/">Контакты</a></li>
					<li><a href="/about/">О компании</a></li>
				</ul>
			</div>

			<div class="col-xs-12 visible-xs">
				<ul class="col-xs-6">
                    <li><span class="margin_bottom_10 a_text">Продукты</span></li>
					<?/* <li><a href="#">Услуги</a></li> */?>
                    <li><a href="/events/">Мероприятия</a></li>
                    <li><a href="/clients/">Кейсы</a></li>

				</ul>
				<ul class="col-xs-6">
                    <?/*<li><a href="/instruction/">База знаний</a></li>*/?>
                    <li><a href="/blog/">Блог</a></li>
                    <li><a href="/contacts/">Контакты</a></li>
                    <li><a href="/about/">О компании</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-xs-12 ">
				<button class="btn_modal_request_crm" data-form-name="Заявка на консультацию" data-scroll-bg-lock="Y">ЗАЯВКА НА КОНСУЛЬТАЦИЮ</button>
				<div class="tel-socnets-wrap hidden-xs">
					<?$APPLICATION->ShowViewContent('office_phone');?>
					<div class="soc-nets-wrap pull-right">
						<a href="https://vk.com/studiobitru"><i class="fa fa-vk" aria-hidden="true"></i></a>
						<a href="https://www.facebook.com/studiobit/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					</div>
				</div>

			</div>

			<div class="col-sm-3 col-xs-12 visible-xs clearfix tel-socnets-wrap">
				<?$APPLICATION->ShowViewContent('office_phone');?>
				<div class="soc-nets-wrap pull-right">
					<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				</div>
			</div>

			<div class="col-xs-12 visible-xs clearfix logo-copyright-wrap">
				<a href="" class="pull-left mobile-footer-logo">
					<img src="<?=SITE_TEMPLATE_PATH;?>/assets/img/layout/logo-white.svg" width="110" alt="" />
				</a>
				<span class="copyright pull-right">&copy;1997-<?=date('Y');?> – «Первый БИТ» </span>

			</div>

		</div>
	</div>

</footer>
<!--wrap end-->
</div>
<div class="black-bg"></div>
<?
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "mention", Array(
	"WEB_FORM_ID" => WEB_FORM_MENTION,  // ID веб-формы
	"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
	"COMPANY_IBLOCK_ID" => IB_CONTACTS,
	"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
	"EMAIL_QUESTION_CODE" => "EMAIL",
	"CITY_QUESTION_CODE" => "CITY",
	"OFFICE_QUESTION_CODE" => "OFFICE",
	"TO_QUESTION_RESIPIENTS" => "",
	"EMAIL_ONLY_FROM_RESIPIENTS" => "",
	"PRODUCT_QUESTION_CODE" => "PRODUCT",
	"TO_QUESTION_CODE" => "TO",
	"IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
	"USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
	"SEF_MODE" => "N",  // Включить поддержку ЧПУ
	"CACHE_TYPE" => "A",  // Тип кеширования
	"CACHE_TIME" => "3600",  // Время кеширования (сек.)
	"LIST_URL" => "",  // Страница со списком результатов
	"EDIT_URL" => "",  // Страница редактирования результата
	"SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
	"CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
	"CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
	"VARIABLE_ALIASES" => array(
		"WEB_FORM_ID" => "WEB_FORM_ID",
		"RESULT_ID" => "RESULT_ID",
	),
	"ALERT_ADD_SHARE" => "N",
	"HIDE_PRIVACY_POLICE" => "Y",
	"BTN_CALL_FORM" => ".btn_modal_mention",
	"STRONG_SHOW_CITY" => "N",
	"STRONG_SHOW_OFFICE" => "N",
	"HIDE_FIELDS" => array(
		0 => "CITY",
		1 => "OFFICE",
	),
	"COMPONENT_MARKER" => "request1",
	"SHOW_FORM_DESCRIPTION" => "N",
	"MESS" => array(
		"THANK_YOU" => "Спасибо, ваш отзыв был успешно отправлен!",
		"WAIT_CALL" => "Мы свяжемся с вами в течении 2-х часов.",
		"TITLE_FORM" => "Оставить заявку",
		"FORM_TITLE" => "Жалобы, отзывы и предложения"
	),
	"SHOW_TITLE" => "N",
),
	false,
	array(
		"HIDE_ICONS" => "Y"
	)
);
?>

<?
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
	"WEB_FORM_ID" => WEB_FORM_FEEDBACK,  // ID веб-формы
	"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
	"COMPANY_IBLOCK_ID" => IB_CONTACTS,
	"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
	"EMAIL_QUESTION_CODE" => "EMAIL",
	"CITY_QUESTION_CODE" => "CITY",
	"OFFICE_QUESTION_CODE" => "OFFICE",
	"TO_QUESTION_RESIPIENTS" => "",
	"EMAIL_ONLY_FROM_RESIPIENTS" => "",
	"PRODUCT_QUESTION_CODE" => "PRODUCT",
	"TO_QUESTION_CODE" => "TO",
	"IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
	"USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
	"SEF_MODE" => "N",  // Включить поддержку ЧПУ
	"CACHE_TYPE" => "A",  // Тип кеширования
	"CACHE_TIME" => "3600",  // Время кеширования (сек.)
	"LIST_URL" => "",  // Страница со списком результатов
	"EDIT_URL" => "",  // Страница редактирования результата
	"SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
	"CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
	"CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
	"VARIABLE_ALIASES" => array(
		"WEB_FORM_ID" => "WEB_FORM_ID",
		"RESULT_ID" => "RESULT_ID",
	),
	"ALERT_ADD_SHARE" => "N",
	"HIDE_PRIVACY_POLICE" => "Y",
	"BTN_CALL_FORM" => ".btn_modal_feedback",
	"STRONG_SHOW_CITY" => "N",
	"STRONG_SHOW_OFFICE" => "N",
	"HIDE_FIELDS" => array(
		0 => "CITY",
		1 => "OFFICE",
	),
	"COMPONENT_MARKER" => "feedback",
	"SHOW_FORM_DESCRIPTION" => "N",
	"MESS" => array(
		"THANK_YOU" => "Спасибо, Ваша заявка принята!",
		"WAIT_CALL" => "Мы свяжемся с вами в течении 2-х часов.",
		"TITLE_FORM" => "Оставить заявку",
	),
	"SHOW_TITLE" => "N",
),
	false,
	array(
		"HIDE_ICONS" => "Y"
	)
);
?>

<?
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
	"WEB_FORM_ID" => \Xfive\Conf::WEB_FORM_FEEDBACK_SUPPORT,  // ID веб-формы
	"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
	"COMPANY_IBLOCK_ID" => IB_CONTACTS,
	"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
	"EMAIL_QUESTION_CODE" => "EMAIL",
	"CITY_QUESTION_CODE" => "CITY",
	"OFFICE_QUESTION_CODE" => "OFFICE",
	"TO_QUESTION_RESIPIENTS" => "",
	"EMAIL_ONLY_FROM_RESIPIENTS" => "",
	"PRODUCT_QUESTION_CODE" => "PRODUCT",
	"TO_QUESTION_CODE" => "TO",
	"IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
	"USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
	"SEF_MODE" => "N",  // Включить поддержку ЧПУ
	"CACHE_TYPE" => "A",  // Тип кеширования
	"CACHE_TIME" => "3600",  // Время кеширования (сек.)
	"LIST_URL" => "",  // Страница со списком результатов
	"EDIT_URL" => "",  // Страница редактирования результата
	"SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
	"CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
	"CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
	"VARIABLE_ALIASES" => array(
		"WEB_FORM_ID" => "WEB_FORM_ID",
		"RESULT_ID" => "RESULT_ID",
	),
	"ALERT_ADD_SHARE" => "N",
	"HIDE_PRIVACY_POLICE" => "Y",
	"BTN_CALL_FORM" => ".btn_modal_feedback_support",
	"STRONG_SHOW_CITY" => "N",
	"STRONG_SHOW_OFFICE" => "N",
	"HIDE_FIELDS" => array(
		0 => "CITY",
		1 => "OFFICE",
	),
	"COMPONENT_MARKER" => "feedback",
	"SHOW_FORM_DESCRIPTION" => "N",
	"MESS" => array(
		"THANK_YOU" => "Спасибо, Ваша заявка принята!",
		"WAIT_CALL" => "Мы свяжемся с вами в течении 2-х часов.",
		"TITLE_FORM" => "Оставить заявку",
	),
	"SHOW_TITLE" => "N",
),
	false,
	array(
		"HIDE_ICONS" => "Y"
	)
);
?>

<?
$Recipients = DEFAULT_EMAIL_EVENTS;

if(!empty($_REQUEST['EMAIL_EVENT'])) {
	$Recipients = $_REQUEST['EMAIL_EVENT'];
}

$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "enroll_event", Array(
	"WEB_FORM_ID" => WEB_FORM_EVENT,  // ID веб-формы
	"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
	"COMPANY_IBLOCK_ID" => IB_CONTACTS,
	"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
	"EMAIL_QUESTION_CODE" => "EMAIL",
	"CITY_QUESTION_CODE" => "CITY",
	"OFFICE_QUESTION_CODE" => "OFFICE",
	"TO_QUESTION_RESIPIENTS" => $Recipients,
	"EMAIL_ONLY_FROM_RESIPIENTS" => "Y",
	"PRODUCT_QUESTION_CODE" => "PRODUCT",
	"TO_QUESTION_CODE" => "TO",
	"IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
	"USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
	"SEF_MODE" => "N",  // Включить поддержку ЧПУ
	"CACHE_TYPE" => "A",  // Тип кеширования
	"CACHE_TIME" => "3600",  // Время кеширования (сек.)
	"LIST_URL" => "",  // Страница со списком результатов
	"EDIT_URL" => "",  // Страница редактирования результата
	"SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
	"CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
	"CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
	"VARIABLE_ALIASES" => array(
		"WEB_FORM_ID" => "WEB_FORM_ID",
		"RESULT_ID" => "RESULT_ID",
	),
	"ALERT_ADD_SHARE" => "N",
	"HIDE_PRIVACY_POLICE" => "Y",
	"BTN_CALL_FORM" => ".btn_modal_enroll_event",
	"STRONG_SHOW_CITY" => "N",
	"STRONG_SHOW_OFFICE" => "N",
	"HIDE_FIELDS" => array(
		0 => "CITY",
		1 => "OFFICE",
	),
	"COMPONENT_MARKER" => "enroll",
	"SHOW_FORM_DESCRIPTION" => "N",
	"MESS" => array(
		"THANK_YOU" => "Спасибо, Ваша заявка принята!",
		"WAIT_CALL" => "Мы свяжемся с вами в течении 2-х часов.",
		"TITLE_FORM" => "Оставить заявку",
		"FORM_TITLE" => "Для записи укажите Ваши контактные данные"
	),
	"SHOW_TITLE" => "N",
),
	false,
	array(
		"HIDE_ICONS" => "Y"
	)
);
?>

<?
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "request_crm", Array(
	"WEB_FORM_ID" => WEB_FORM_REQUEST_CRM,  // ID веб-формы
	"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
	"COMPANY_IBLOCK_ID" => IB_CONTACTS,
	"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
	"EMAIL_QUESTION_CODE" => "EMAIL",
	"CITY_QUESTION_CODE" => "CITY",
	"OFFICE_QUESTION_CODE" => "OFFICE",
	"TO_QUESTION_RESIPIENTS" => "",
	"EMAIL_ONLY_FROM_RESIPIENTS" => "",
	"PRODUCT_QUESTION_CODE" => "PRODUCT",
	"TO_QUESTION_CODE" => "TO",
	"IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
	"USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
	"SEF_MODE" => "N",  // Включить поддержку ЧПУ
	"CACHE_TYPE" => "A",  // Тип кеширования
	"CACHE_TIME" => "3600",  // Время кеширования (сек.)
	"LIST_URL" => "",  // Страница со списком результатов
	"EDIT_URL" => "",  // Страница редактирования результата
	"SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
	"CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
	"CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
	"VARIABLE_ALIASES" => array(
		"WEB_FORM_ID" => "WEB_FORM_ID",
		"RESULT_ID" => "RESULT_ID",
	),
	"ALERT_ADD_SHARE" => "N",
	"HIDE_PRIVACY_POLICE" => "Y",
	"BTN_CALL_FORM" => ".btn_modal_request_crm",
	"STRONG_SHOW_CITY" => "N",
	"STRONG_SHOW_OFFICE" => "N",
	"HIDE_FIELDS" => array(
		0 => "CITY",
		1 => "OFFICE",
	),
	"COMPONENT_MARKER" => "request2",
	"SHOW_FORM_DESCRIPTION" => "N",
	"MESS" => array(
		"THANK_YOU" => "Спасибо, Ваша заявка принята!",
		"WAIT_CALL" => "Мы свяжемся с вами в течении 2-х часов.",
		"TITLE_FORM" => "Оставить заявку",
	),
	"SHOW_TITLE" => "N",
	"IS_NEW_PAGE" => $GLOBALS['IS_NEW_PAGE'], // Добавил для формирования отдельного кеша для новых страниц
),
	false,
	array(
		"HIDE_ICONS" => "Y"
	)
);
?>
<div class="modal fade service-modal2" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <h4 class="modal-title">Заявка на консультацию успешно отправлена.</h4>
                <br>
                <p>Наш специалист свяжется с вами по указанному номеру телефона.</p>
            </div>
        </div>
    </div>
</div>

<?if ($GLOBALS['IS_NEW_PAGE']):?>
    <script src="<?=SITE_TEMPLATE_PATH . '/assets_new/js/vendor.min.js'?>"></script>
    <script src="<?=SITE_TEMPLATE_PATH . '/assets_new/js/main.min.js'?>"></script>
<?endif;?>
<script src="//code-ya.jivosite.com/widget/IaQYRYs6ee" async></script>
<script>
    (function(t, p) {window.Marquiz ? Marquiz.add([t, p]) : document.addEventListener('marquizLoaded', function() {Marquiz.add([t, p])})})('Widget', {id: id, position: 'left', delay: 1});
</script>
</body>
</html>

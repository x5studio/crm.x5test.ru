<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

//composer
if (!defined('DISABLE_INCLUDE_COMPOSER') || DISABLE_INCLUDE_COMPOSER != 'Y') {
	$vendorAutoloadFile = getLocalPath('vendor/autoload.php');
	if (!empty($vendorAutoloadFile) && file_exists($_SERVER['DOCUMENT_ROOT'] . $vendorAutoloadFile)) {
		require_once($_SERVER['DOCUMENT_ROOT'] . $vendorAutoloadFile);
	}
}

// new composer
require_once (__DIR__.'/composer/vendor/autoload.php');

\Xfive\Iblock\IblockIdCache::init();

\Bitrix\Main\Loader::includeModule('pb.main');
\Bitrix\Main\Loader::includeModule('iblock');

/*
AddEventHandler("main", "OnEpilog", "On404PageInSiteStyle");
function On404PageInSiteStyle(){
    if(defined('ERROR_404') && ERROR_404 == 'Y'){
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'].'/404.php';
    }
}
*/

/**
 * Фильтр для баннера по тегам и др.
 *
 * @param $arTags
 * @param null $type
 *
 * @return array
 * @deprecated
 */
function GetTagsArrFilter($arTags = null, $type = null, $arParamKill = array())
{
    $arParams = Array();
    $arFilter = Array();
    $arParams['IS_GROUP'] = true;
    if (!empty($arTags["GROUP"])) {
    	$arParams['GROUP'] = $arTags["GROUP"];
    }
    
    $arParams['IS_CITY'] = true;
    if (!empty($arTags["CITY"])) {
        $arParams['CITY'] = $arTags["CITY"];
    }

    if (isset($arTags["FORM"])) {
    	$arParams['FORM'] = $arTags["FORM"];
    }
    
    if (isset($arTags["OTRASL"])) {
        $arParams['OTRASL'] = $arTags["OTRASL"];
    }
    if (isset($arTags["TASK"])) {
        $arParams['TASK'] = $arTags["TASK"];
    }

    switch ($type) {
        case "banner":
            $arParams['IS_PAGES'] = true;
            $arFilter["!PREVIEW_PICTURE"] = false;
            break;
        case "multi_banner":
            $arParams['IS_PAGES'] = true;
            break;
    }

    $obFilter = new FilterHelper($arParams);

    switch ($type) {
        case "city":
            $retFilter = $obFilter->makeCity();
            break;
        case "tags":
            $retFilter = $obFilter->makeTags();
            break;
        case "banner":
        case "multi_banner":
        default:
            $retFilter = $obFilter->make($arFilter, array(), $arParamKill);
    }

    return $retFilter;
}


function p($data, $ignore = false){
    global $USER;

    if ($USER->IsAdmin() || $ignore) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}


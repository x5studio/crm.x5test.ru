<?php

namespace Xfive;

class Tools
{
    public static function getNewPages()
    {
        return [
            '/vnedrenie-crm/',
            '/obuchenie-rabote-v-crm/',
            '/tekhnicheskaya-podderzhka/',
        ];
    }

    public static function isNewPage()
    {
        global $APPLICATION;

        $newPages = self::getNewPages();

        foreach ($newPages as $newPage) {
            if (\CSite::InDir($newPage)) {
                return true;
            }
        }

        return false;
    }

    public static function isAppendActionPage($url='')
    {
        global $APPLICATION;

        if (!$url) {
            $url = $APPLICATION->GetCurPage();
        }

        $appendActionPages = [
            '/blog/novogodnie-skidki-na-bitriks24/'
        ];

        if (in_array($url, $appendActionPages)) {
            return true;
        }

        return false;
    }

}
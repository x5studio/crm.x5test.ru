<?php
/**
 * Created by PhpStorm.
 * User: formi
 * Date: 13.11.2020
 * Time: 13:11
 */

namespace Xfive\Iblock;


class IblockIdCache
{
    private static $iblockIdCache = [];

    public static function init()
    {
        \CModule::IncludeModule('iblock');

        $dbRes = \CIBlock::GetList();
        while ($arRes = $dbRes->Fetch()) {
            if ($arRes['CODE']) {
                self::$iblockIdCache[strtolower($arRes['CODE'])] = $arRes['ID'];
            }
        }
    }

    public static function getIblockIdByCode($code)
    {
        return self::$iblockIdCache[strtolower($code)];
    }
}
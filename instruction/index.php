<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Wiki");

?>
	<section>
        <div class="container">
            <div class="row margin-0-xs">
                <div class="col-sm-4 col-xs-12">
					<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.section.list",
						"old",
						array(
							"IBLOCK_ID" => "31",
							"COUNT" => "10",
							"UF_CODE" => "UF_FOR_MAIN",
							"UF_VALUE" => "",
							"UF_VALUE_NOT" => "Y",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "3600",
							"MAX_WIDTH" => "",
							"MAX_HEIGHT" => "",
							"COMPONENT_TEMPLATE" => "old",
							"IBLOCK_TYPE" => "info",
							"SECTION_ID" => $_REQUEST["SECTION_ID"],
							"SECTION_CODE" => "",
							"COUNT_ELEMENTS" => "N",
							"TOP_DEPTH" => "2",
							"SECTION_FIELDS" => array(
								0 => "",
								1 => "",
							),
							"SECTION_USER_FIELDS" => array(
								0 => "",
								1 => "",
							),
							"VIEW_MODE" => "LINE",
							"SHOW_PARENT_NAME" => "Y",
							"SECTION_URL" => "/instruction/#SECTION_CODE_PATH#/",
							"CACHE_GROUPS" => "N",
							"ADD_SECTIONS_CHAIN" => "N"
						),
						false
					);?>
                </div>
                <div class="col-sm-8 col-xs-12">
					<?
					global $USER;
					if ($USER->IsAuthorized()) {
						?>
                        <div class="redact">
							<?if($_GET['bitrix_include_areas'] != "Y"){?>
                                <a href="?bitrix_include_areas=Y">Включить режим добавления</a>
							<?}else{?>
                                <a class="act" href="?bitrix_include_areas=N">Выключить режим добавления</a>
							<?}?>
                            <div class="clear"></div>


                        </div>

						<?
					}else{
						?><div class="redact">
                            <b>Для включения возможности добавления материалов, авторизуйтесь.</b> <br/>
                            <a href="/auth/">Перейти к авторизации</a><br/>
                        </div>
						<?
					}
					?>

                    <?$APPLICATION->IncludeComponent(
						"bitrix:catalog",
						"old123",
						array(
							"COMPONENT_TEMPLATE" => "old123",
							"IBLOCK_TYPE" => "info",
							"IBLOCK_ID" => "31",
							"TEMPLATE_THEME" => "blue",
							"MESS_BTN_BUY" => "Купить",
							"MESS_BTN_ADD_TO_BASKET" => "В корзину",
							"MESS_BTN_COMPARE" => "Сравнение",
							"MESS_BTN_DETAIL" => "Подробнее",
							"MESS_NOT_AVAILABLE" => "Нет в наличии",
							"DETAIL_USE_VOTE_RATING" => "N",
							"DETAIL_USE_COMMENTS" => "N",
							"DETAIL_BRAND_USE" => "N",
							"SIDEBAR_SECTION_SHOW" => "N",
							"SIDEBAR_DETAIL_SHOW" => "N",
							"SIDEBAR_PATH" => "",
							"SEF_MODE" => "Y",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "N",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "N",
							"USE_MAIN_ELEMENT_SECTION" => "N",
							"SET_LAST_MODIFIED" => "N",
							"SET_TITLE" => "Y",
							"ADD_SECTIONS_CHAIN" => "Y",
							"ADD_ELEMENT_CHAIN" => "Y",
							"USE_ELEMENT_COUNTER" => "N",
							"USE_FILTER" => "N",
							"FILTER_VIEW_MODE" => "VERTICAL",
							"ACTION_VARIABLE" => "action",
							"PRODUCT_ID_VARIABLE" => "id",
							"USE_COMPARE" => "N",
							"PRICE_CODE" => array(
							),
							"USE_PRICE_COUNT" => "N",
							"SHOW_PRICE_COUNT" => "1",
							"PRICE_VAT_INCLUDE" => "Y",
							"PRICE_VAT_SHOW_VALUE" => "N",
							"BASKET_URL" => "/personal/basket.php",
							"USE_PRODUCT_QUANTITY" => "N",
							"PRODUCT_QUANTITY_VARIABLE" => "",
							"ADD_PROPERTIES_TO_BASKET" => "N",
							"PRODUCT_PROPS_VARIABLE" => "prop",
							"PARTIAL_PRODUCT_PROPERTIES" => "N",
							"PRODUCT_PROPERTIES" => array(
							),
							"SHOW_TOP_ELEMENTS" => "Y",
							"TOP_ELEMENT_COUNT" => "9",
							"TOP_LINE_ELEMENT_COUNT" => "3",
							"TOP_ELEMENT_SORT_FIELD" => "sort",
							"TOP_ELEMENT_SORT_ORDER" => "asc",
							"TOP_ELEMENT_SORT_FIELD2" => "id",
							"TOP_ELEMENT_SORT_ORDER2" => "desc",
							"TOP_PROPERTY_CODE" => array(
								0 => "",
								1 => "",
							),
							"SECTION_COUNT_ELEMENTS" => "Y",
							"SECTION_TOP_DEPTH" => "2",
							"SECTIONS_VIEW_MODE" => "LIST",
							"SECTIONS_SHOW_PARENT_NAME" => "Y",
							"PAGE_ELEMENT_COUNT" => "30",
							"LINE_ELEMENT_COUNT" => "3",
							"ELEMENT_SORT_FIELD" => "sort",
							"ELEMENT_SORT_ORDER" => "asc",
							"ELEMENT_SORT_FIELD2" => "id",
							"ELEMENT_SORT_ORDER2" => "desc",
							"LIST_PROPERTY_CODE" => array(
								0 => "",
								1 => "",
							),
							"INCLUDE_SUBSECTIONS" => "Y",
							"LIST_META_KEYWORDS" => "-",
							"LIST_META_DESCRIPTION" => "-",
							"LIST_BROWSER_TITLE" => "-",
							"DETAIL_PROPERTY_CODE" => array(
								0 => "",
								1 => "",
							),
							"DETAIL_META_KEYWORDS" => "-",
							"DETAIL_META_DESCRIPTION" => "-",
							"DETAIL_BROWSER_TITLE" => "-",
							"DETAIL_SET_CANONICAL_URL" => "N",
							"SECTION_ID_VARIABLE" => "SECTION_ID",
							"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
							"SHOW_DEACTIVATED" => "N",
							"DETAIL_DISPLAY_NAME" => "Y",
							"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
							"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
							"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
							"LINK_IBLOCK_TYPE" => "",
							"LINK_IBLOCK_ID" => "",
							"LINK_PROPERTY_SID" => "",
							"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
							"USE_STORE" => "N",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "Y",
							"PAGER_TITLE" => "Товары",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"SET_STATUS_404" => "Y",
							"SHOW_404" => "Y",
							"MESSAGE_404" => "",
							"ADD_PICT_PROP" => "-",
							"LABEL_PROP" => "-",
							"USE_SEARCH" => "Y",
							"SEARCH_CHECK_DATES" => "Y",
							"SEARCH_NO_WORD_LOGIC" => "Y",
							"SEARCH_PAGE_RESULT_COUNT" => "30",
							"SEARCH_RESTART" => "N",
							"SEARCH_USE_LANGUAGE_GUESS" => "N",
							"TOP_VIEW_MODE" => "SECTION",
							"DETAIL_STRICT_SECTION_CHECK" => "Y",
							"SEF_FOLDER" => "/instruction/",
							"SEF_URL_TEMPLATES" => array(
								"sections" => "",
								"section" => "#SECTION_CODE_PATH#/",
								"element" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
								"compare" => "",
								"smart_filter" => "",
							),
							"VARIABLE_ALIASES" => array(
								"compare" => array(
									"ACTION_CODE" => "action",
								),
							)
						),
						false
					);?>
                </div>
            </div>
        </div>
    </section>



<?
/*
<div class="center clients prod"> 

  <div class="wiki-left"> 		  	</div>
 	
  <div class="wiki-right"> 





global $USER;
if ($USER->IsAuthorized()) {
?>
<div class="redact">
<?if($_GET['bitrix_include_areas'] != "Y"){?>
<a href="?bitrix_include_areas=Y">Включить режим добавления</a>
<?}else{?>
<a class="act" href="?bitrix_include_areas=N">Выключить режим добавления</a>
<?}?>
<div class="clear"></div>


</div>

<?
}else{
?><div style="text-align:right;">
<b>Для включения возможности добавления материалов, авторизуйтесь.</b> <br/>
<a href="/auth/">Перейти к авторизации</a><br/>
</div>
<?
}




	







 	</div>
 	

 <div class="clear"> 	</div>

 </div>
    */
    ?>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
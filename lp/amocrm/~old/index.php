<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Внедрение amoCRM</title>
		
	<meta property="og:url" content="http://crm.ru/page4143770.html" />
	<meta property="og:title" content="Внедрение amoCRM" />
	<meta property="og:description" content="Запуск CRM системы с максимальной эффективностью" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="images/tild3163-3964-4966-b738-346632316233__partner.png" />
	<meta property="fb:app_id" content="257953674358265" />
	
	<meta name="format-detection" content="telephone=no" />
	<meta http-equiv="x-dns-prefetch-control" content="on">
	
	
	<meta name="description" content="Запуск CRM системы с максимальной эффективностью" />
	
		
	<link rel="canonical" href="http://crm.ru/page4143770.html">
	<link rel="shortcut icon" href="images/tild3464-3061-4531-b530-313832643130__favicon.ico" type="image/x-icon" />
	
			<link rel="stylesheet" href="css/tilda-grid-3.0.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/tilda-blocks-2.12.css?t=1543827867" type="text/css" media="all" />
	<link rel="stylesheet" href="css/tilda-animation-1.0.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/tilda-slds-1.4.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/tilda-popup-1.1.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" type="text/css" />
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/tilda-scripts-2.8.min.js"></script>
	<script src="js/tilda-blocks-2.7.js?t=1543827867"></script>
	<script src="js/tilda-animation-1.0.min.js" charset="utf-8"></script>
	<script src="js/tilda-slds-1.4.min.js" charset="utf-8"></script>
	<script src="js/hammer.min.js" charset="utf-8"></script>
	<script src="js/tilda-forms-1.0.min.js" charset="utf-8"></script>
	
	
<script type="text/javascript">window.dataLayer = window.dataLayer || [];</script>

</head>
<body class="t-body" style="margin:0;">
	<!--allrecords-->
<div id="allrecords" class="t-records" data-hook="blocks-collection-content-node" data-tilda-project-id="151620" data-tilda-page-id="4143770"  data-tilda-formskey="e5ed39ed731e9412e892712502a41413"   >

<div id="rec76264221" class="r t-rec" style=" " data-animationappear="off" data-record-type="257"   >

<!-- T228 -->
<div id="nav76264221marker"></div>
  <div class="t228__mobile">
    <div class="t228__mobile_container">
      <div class="t228__mobile_text t-name t-name_md" field="text">&nbsp;</div> 
      <div class="t228__burger">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>  
    </div>
  </div>
<div id="nav76264221" class="t228 t228__hidden t228__positionfixed t228__beforeready" style="background-color: rgba(255,255,255,0.0); height:100px; " data-bgcolor-hex="#ffffff" data-bgcolor-rgba="rgba(255,255,255,0.0)" data-navmarker="nav76264221marker" data-appearoffset="500" data-bgopacity-two="90" data-menushadow="" data-bgopacity="0.0"  data-bgcolor-rgba-afterscroll="rgba(255,255,255,0.90)" data-menu-items-align="right" data-menu="yes">
    <div class="t228__maincontainer t228__c12collumns" style="height:100px;">
          <div class="t228__padding40px"></div>
          
          <div class="t228__leftside">
                        <div class="t228__leftcontainer">    
                <a href="/" target="_blank" style="color:#ffffff;font-size:120px;"><img src="images/tild3163-3964-4966-b738-346632316233__partner.png" class="t228__imglogo t228__imglogomobile" imgfield="img" style="max-width: 220px;width: 220px; height: auto; display: block;" alt="Company"></a>            </div>
                      </div>
        
          <div class="t228__centerside t228__menualign_right">
                        <div class="t228__centercontainer">
            <ul class="t228__list ">
                        <li class="t228__list_item"><a class="t-menu__link-item" href="#about"  style="color:#000000;font-weight:700;font-family:&apos;Montserrat&apos;;" data-menu-item-number="1">О amoCRM</a></li>
                        <li class="t228__list_item"><a class="t-menu__link-item" href="#price"  style="color:#000000;font-weight:700;font-family:&apos;Montserrat&apos;;" data-menu-item-number="2">Тарифы</a></li>
                        <li class="t228__list_item"><a class="t-menu__link-item" href="#zapusk"  style="color:#000000;font-weight:700;font-family:&apos;Montserrat&apos;;" data-menu-item-number="3">Запуск amoCRM</a></li>
                        <li class="t228__list_item"><a class="t-menu__link-item" href="#zayavka"  style="color:#000000;font-weight:700;font-family:&apos;Montserrat&apos;;" data-menu-item-number="4">Заявка</a></li>
                        <li class="t228__list_item"><a class="t-menu__link-item" href="#onas"  style="color:#000000;font-weight:700;font-family:&apos;Montserrat&apos;;" data-menu-item-number="5">О нас</a></li>
                        <li class="t228__list_item"><a class="t-menu__link-item" href=""  style="color:#000000;font-weight:700;font-family:&apos;Montserrat&apos;;" data-menu-item-number="6">+7 (495) 748-33-02</a></li>
                    
                        </ul>
            </div>
                      </div>
          
          <div class="t228__rightside">
                       </div>
          
          <div class="t228__padding40px"></div>    
  </div>
</div>
<style>
@media screen and (max-width: 980px) {
  #rec76264221 .t228__leftcontainer{
    padding: 20px;
  }
}
@media screen and (max-width: 980px) {
  #rec76264221 .t228__imglogo{
    padding: 20px 0;
  }
}
</style>

<script type="text/javascript">
    
      $(document).ready(function() {
        t228_highlight();
        
                t228_checkAnchorLinks('76264221');
                        
      }); 
    
 

$(window).resize(function() {
    t228_setBg('76264221');
});
$(document).ready(function() {
    t228_setBg('76264221');
});     
    
        $(document).ready(function() {
            $('.t228').removeClass('t228__beforeready');      
            t228_appearMenu('76264221');
            $(window).bind('scroll', t_throttle(function(){t228_appearMenu('76264221')}, 200));
        });
    
      
        $(document).ready(function() {
            t228_changebgopacitymenu('76264221');
            $(window).bind('scroll', t_throttle(function(){t228_changebgopacitymenu('76264221')}, 200));
        }); 
    
</script>
<script type="text/javascript">
      
        $(document).ready(function() {
            t228_createMobileMenu('76264221');
        }); 
    
</script>
      
  
<!--[if IE 8]>
<style>
#rec76264221 .t228 {
  filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#D9ffffff', endColorstr='#D9ffffff');
}
</style>
<![endif]-->

</div>


<div id="rec76324870" class="r t-rec" style=" " data-animationappear="off" data-record-type="204"   >
<!-- cover -->
	




<div class="t-cover" id="recorddiv76324870" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); " >

	<div class="t-cover__carrier" id="coverCarry76324870" data-content-cover-id="76324870"  data-content-cover-bg="images/tild3736-3037-4334-b863-353562353039__d946dbce69a24e0288d5.jpg" data-content-cover-height="100vh" data-content-cover-parallax="fixed"   data-content-video-url-youtube="eAqyaLWHtQ0"     style="height:100vh; "></div>

    <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));background-image: -o-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));background-image: -ms-linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));background-image: linear-gradient(top, rgba(0,0,0,0.70), rgba(0,0,0,0.70));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#4c000000', endColorstr='#4c000000');"></div>

	<div class="t-container">
        <div class="t-col t-col_8">
			<div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                <div class="t181">
                  <div data-hook-content="covercontent">
                  <div class="t181__wrapper">
                    <div class="t181__title t-title t-title_md" style="" field="title">Эффективное внедрение amoCRM <br /></div>                    <div class="t181__descr t-descr t-descr_lg t-opacity_70" style="" field="descr">Оставьте заявку на внедрение amoCRM<br /></div>                                        <div style="margin-top:60px;">
                          <a href="#zayavka" target="" class="t-btn  "    style="color:#ffffff;background-color:#4998c6;border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;"><table style="width:100%; height:100%;"><tr><td>Заказать внедрение</td></tr></table></a>                          <a href="#price" target="" class="t-btn  "    style="color:#4998c6;background-color:#ffd255;border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;"><table style="width:100%; height:100%;"><tr><td>Купить amoCRM</td></tr></table></a>                    </div>
                                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- arrow -->
  <div class="t-cover__arrow"><div class="t-cover__arrow-wrapper t-cover__arrow-wrapper_animated"><div class="t-cover__arrow_mobile"><svg class="t-cover__arrow-svg" style="fill:#4998c6;" x="0px" y="0px" width="38.417px" height="18.592px" viewBox="0 0 38.417 18.592" style="enable-background:new 0 0 38.417 18.592;"><g><path d="M19.208,18.592c-0.241,0-0.483-0.087-0.673-0.261L0.327,1.74c-0.408-0.372-0.438-1.004-0.066-1.413c0.372-0.409,1.004-0.439,1.413-0.066L19.208,16.24L36.743,0.261c0.411-0.372,1.042-0.342,1.413,0.066c0.372,0.408,0.343,1.041-0.065,1.413L19.881,18.332C19.691,18.505,19.449,18.592,19.208,18.592z"/></g></svg></div></div></div>
  <!-- arrow -->
  

</div>
    
<style>

#rec76324870 .t-btn[data-btneffects-first],
#rec76324870 .t-btn[data-btneffects-second],
#rec76324870 .t-submit[data-btneffects-first],
#rec76324870 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec76942948" class="r t-rec" style=" "  data-record-type="215"   >
<a name="about" style="font-size:0;"></a>
</div>


<div id="rec76681549" class="r t-rec t-rec_pt_60 t-rec_pb_30" style="padding-top:60px;padding-bottom:30px; "  data-record-type="30"   >
<!-- T015 -->
<div class="t015">
  <div class="t-container t-align_center">
    <div class="t-col t-col_10 t-prefix_1">
            <div class="t015__title t-title t-title_lg" field="title" style="">Возможности <span style="color: rgb(73, 152, 198);">amoCRM</span></div>          </div>
  </div>
</div>
</div>


<div id="rec76668899" class="r t-rec t-rec_pt_30 t-rec_pb_60" style="padding-top:30px;padding-bottom:60px; " data-animationappear="off" data-record-type="670"   >

<div class="t670">
  <div class="t-slds" style="visibility: hidden;">
    <div class="t-container t-slds__main">
      <div class="t-slds__container t-width t-width_9 t-margin_auto">
        
        <div class="t-slds__items-wrapper t-slds_animated-none t-slds__witharrows" data-slider-transition="300" data-slider-with-cycle="true" data-slider-correct-height="true" data-auto-correct-mobile-width="false" data-slider-arrows-nearpic="yes"  >
                      <div class="t-slds__item  t-slds__item_active" data-slide-index="1">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild6639-3634-4037-b665-346261666661__nastrojka-amocrm.jpg"><meta itemprop="caption" content="Просто и понятный дизайн">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__0">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild6639-3634-4037-b665-346261666661__nastrojka-amocrm.jpg" style="background-image: url('images/tild6639-3634-4037-b665-346261666661__nastrojka-amocrm.jpg');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="2">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild6330-3639-4536-b464-316236373935__1.png"><meta itemprop="caption" content="Каждая сделка под контролем">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__1">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild6330-3639-4536-b464-316236373935__1.png" style="background-image: url('images/tild6330-3639-4536-b464-316236373935__1.png');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="3">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild3730-6561-4536-a439-303962653865__2.png"><meta itemprop="caption" content="Наглядная воронка продаж">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__2">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild3730-6561-4536-a439-303962653865__2.png" style="background-image: url('images/tild3730-6561-4536-a439-303962653865__2.png');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="4">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild3137-6233-4331-a136-393737316434__3.jpg"><meta itemprop="caption" content="Нет пропущенным лидам">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__3">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild3137-6233-4331-a136-393737316434__3.jpg" style="background-image: url('images/tild3137-6233-4331-a136-393737316434__3.jpg');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="5">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild3134-6630-4633-b633-393230313337__4.png"><meta itemprop="caption" content="Утепление клиентов на автомате">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__4">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild3134-6630-4633-b633-393230313337__4.png" style="background-image: url('images/tild3134-6630-4633-b633-393230313337__4.png');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="6">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild6537-3333-4234-b862-303237323331__5.png"><meta itemprop="caption" content="Вся почта в едином интерфейсе">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__5">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild6537-3333-4234-b862-303237323331__5.png" style="background-image: url('images/tild6537-3333-4234-b862-303237323331__5.png');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="7">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild3433-6130-4733-b433-633134636430__6.png"><meta itemprop="caption" content="Интеграция с сайтом">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__6">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild3433-6130-4733-b433-633134636430__6.png" style="background-image: url('images/tild3433-6130-4733-b433-633134636430__6.png');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="8">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild3039-3037-4565-b532-303134656661__7.png"><meta itemprop="caption" content="AmoCRM внедрение в телефонию">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__7">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild3039-3037-4565-b532-303134656661__7.png" style="background-image: url('images/tild3039-3037-4565-b532-303134656661__7.png');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="9">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild3037-6162-4039-b433-306639663234__8.png"><meta itemprop="caption" content="Аналитика продаж">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__8">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild3037-6162-4039-b433-306639663234__8.png" style="background-image: url('images/tild3037-6162-4039-b433-306639663234__8.png');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                      <div class="t-slds__item  " data-slide-index="10">
              <div class="t-width t-width_9 t-margin_auto" itemscope itemtype="http://schema.org/ImageObject">
                <div class="t-slds__wrapper t-align_center">
                                                            <meta itemprop="image" content="images/tild3334-6235-4265-b331-636130666433__9.png"><meta itemprop="caption" content="Управляйте продажами с телефона">                      <div class="t670__imgwrapper"  bgimgfield="gi_img__9">
                        <div class="t-slds__bgimg t-slds__bgimg-contain t-bgimg" data-original="images/tild3334-6235-4265-b331-636130666433__9.png" style="background-image: url('images/tild3334-6235-4265-b331-636130666433__9.png');"></div>
                        <div class="t670__separator" data-slider-image-width="860" data-slider-image-height="550"></div>                      </div>
                                                                          </div>
              </div>
            </div>
          
                  </div>
                      </div>
              <div class="t-slds__arrow_container t-slds__arrow_container-outside">
                  


<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-left" data-slide-direction="left">
  <div class="t-slds__arrow t-slds__arrow-left t-slds__arrow-withbg" style="width: 40px; height: 40px;background-color: rgba(73,152,198,1);">
    <div class="t-slds__arrow_body t-slds__arrow_body-left" style="width: 9px;">
      <svg style="display: block" viewBox="0 0 9.3 17" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <desc>Left</desc>
        <polyline
        fill="none" 
        stroke="#ffffff"
        stroke-linejoin="butt" 
        stroke-linecap="butt"
        stroke-width="1" 
        points="0.5,0.5 8.5,8.5 0.5,16.5" 
        />
      </svg>
    </div>
  </div>
</div>
<div class="t-slds__arrow_wrapper t-slds__arrow_wrapper-right" data-slide-direction="right">
  <div class="t-slds__arrow t-slds__arrow-right t-slds__arrow-withbg" style="width: 40px; height: 40px;background-color: rgba(73,152,198,1);">
    <div class="t-slds__arrow_body t-slds__arrow_body-right" style="width: 9px;">
      <svg style="display: block" viewBox="0 0 9.3 17" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <desc>Right</desc>
        <polyline
        fill="none" 
        stroke="#ffffff"
        stroke-linejoin="butt" 
        stroke-linecap="butt"
        stroke-width="1" 
        points="0.5,0.5 8.5,8.5 0.5,16.5" 
        />
      </svg>
    </div>
  </div>
</div>        </div>
                    <div class="t-slds__bullet_wrapper">
                               
            <div class="t-slds__bullet t-slds__bullet_active" data-slide-bullet-for="1">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="2">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="3">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="4">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="5">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="6">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="7">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="8">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="9">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
           
            <div class="t-slds__bullet " data-slide-bullet-for="10">
              <div class="t-slds__bullet_body" style="background-color: #4998c6;"></div>
            </div>
                  </div>
            <div class="t-slds__caption__container">
                                                      <div class="t-slds__caption t-col t-col_8 t-prefix_2  t-slds__caption-active" data-slide-caption="1">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__0">Просто и понятный дизайн</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__0">Ваши коллеги и новые сотрудники быстро адаптируются к работе в новой системе. Обучение не требуется</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="2">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__1">Каждая сделка под контролем</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__1">Удобный интерфейс карточки клиента и сделки в amoCRM позволяет получить всю необходимую информацию о событиях по сделке и её статусе в одном окне.</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="3">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__2">Наглядная воронка продаж</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__2">Ключевая особенность amoCRM - это воронка продаж, которая реализована в виде этапов. Каждая сделка проходит последовательно каждый этап от входящей заявки до успешной реализации. Вы можете самостоятельно настроить этапы воронки и внедрить amoCRM под свои бизнес-процессы, а удобные фильтры и наглядная статистика дадут полное понимание текущих продаж.</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="4">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__3">Нет пропущенным лидам</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__3">При грамотной настройке и внедрение амо срм все обращения в вашу компанию от потенциальных клиентов, будь то звонок, заявка с сайта или online чат, будут созданы автоматически в статусе “неразобранное”. Система сама заполнит все возможные данные.</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="5">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__4">Утепление клиентов на автомате</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__4">Digital - воронка - это уникальная особенность этой CRM-системы. Этот функционал позволяет на автопилоте выполнять список задач по взаимодействию с клиентом, исходя из того на каком этапе воронки находится сделка. АmoCRM сама напишет письмо клиенту, поставит задачу, подпишет или отпишет клиента от рассылки и более того покажет рекламу в соцсетях в соответствии с тет на каком этапе воронки находится клиент.</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="6">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__5">Вся почта в едином интерфейсе</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__5">Во время настройки amoCRM, есть возможность подключить неограниченное количество ящиков и вести переписку прямо в CRM. Более того, есть функционал позволяющий создавать шаблоны и на их основе готовить письма и клиентскую рассылку.</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="7">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__6">Интеграция с сайтом</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__6">Настроить amoCRM с сайтом, при помощи специализированных виджетов избавит Вас от рутинных операций при ручном переносе заявок в систему. После заполнения формы на сайте клиентом данные автоматически подгрузятся в систему.</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="8">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__7">AmoCRM внедрение в телефонию</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__7">Теперь Вам не придется мучительно долго вспоминать историю общения с клиентом, после интеграции с телефонией в CRM будет хранится вся статистика Ваших переговоров.</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="9">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__8">Аналитика продаж</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__8">Держите руку на пульсе продаж благодаря богатому набору отчётов. Помимо того, что благодаря этому функционалу у вас появляется возможность смотреть на текущие состояние продаж в любом разрезе, система умета прогнозировать продажи на основе статистики за предыдущие месяцы.</div>            </div>
          </div>
                                                                <div class="t-slds__caption t-col t-col_8 t-prefix_2  " data-slide-caption="10">
            <div class="t-slds__caption_wrapper t-align_center" style="border-color: #4a4a4a;">
              <div class="t-slds__title t-name t-name_xs" data-redactor-toolbar="no" style="color:#030303;font-size:25px;font-weight:600;font-family:&apos;Montserrat&apos;;" field="gi_title__9">Управляйте продажами с телефона</div>              <div class="t-slds__descr t-descr t-descr_xxs" data-redactor-toolbar="no" style="color:#000000;font-size:15px;font-weight:400;font-family:&apos;Arial&apos;;" field="gi_descr__9">Мобильное приложение amoCRM позволит вам, не находясь на рабочем месте, ставить задачи, создавать сделки и общаться с коллегами.</div>            </div>
          </div>
                      </div>
    </div>
    
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    t670_init('76668899');
    t_sldsInit('76668899');
    
  }); 
  $('.t670').bind('displayChanged',function(){
    t_slds_updateSlider('76668899');
    t_slds_positionArrows('76668899');
  });  
</script>
<style type="text/css">
  #rec76668899 .t-slds__bullet_active .t-slds__bullet_body {
    background-color: #ffd255 !important;
  }
  
  #rec76668899 .t-slds__bullet:hover .t-slds__bullet_body {
    background-color: #ffd255 !important;
  }
</style>
</div>


<div id="rec76942963" class="r t-rec" style=" "  data-record-type="215"   >
<a name="price" style="font-size:0;"></a>
</div>


<div id="rec76668756" class="r t-rec t-rec_pt_60 t-rec_pb_60" style="padding-top:60px;padding-bottom:60px; " data-animationappear="off" data-record-type="615"   >
<!-- T615 -->

			
			
			
	
	<style>
	#rec76668756 .t-btn:not(.t-animate_no-hover):hover{
	  
	  
	  
	  box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.4) !important;
	}	
	#rec76668756 .t-btn:not(.t-animate_no-hover){
	  -webkit-transition: background-color 0s ease-in-out, color 0s ease-in-out, border-color 0s ease-in-out, box-shadow 0s ease-in-out; transition: background-color 0s ease-in-out, color 0s ease-in-out, border-color 0s ease-in-out, box-shadow 0s ease-in-out;
	}
	</style>
	
	
  

<div class="t615">
  		<div class="t-section__container t-container">
		<div class="t-col t-col_12">
			<div class="t-section__topwrapper t-align_center">
				<div class="t-section__title t-title t-title_xs" field="btitle">Цены на тарифы</div>							</div>
		</div>
	</div>
		  <div class="t-container t615__withfeatured">
                                                                                                                          
            <div class="t615__col t-col t-col_4 t-align_center ">
      <div class="t615__content" style="border: 1px solid #e0e6ed;  border-radius: 9px;box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.40);">
                <div class="t615__title t-name t-name_xl" field="title" style=" ">БАЗОВЫЙ</div>        <div class="t615__descr t-descr t-descr_xs" field="descr" style=" "><ul><li>Настройки этапов воронки</li><li>Интеграция аккаунта с почтой и CRM</li><li>Автоматический поиск и объединение дублей</li></ul></div>                <div class="t615__price-wrap">
                    <div class="t615__price t-name t-name_md" field="price" style=" ">499 / месяц<br /><span style="font-size: 16px;">за пользователя</span><br /></div>        </div>
                          <a href="#popup:myform" target="_blank" class="t615__btn t-btn " style="color:#ffffff;background-color:#4998c6;border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;"><table style="width:100%; height:100%;"><tr><td>Выбрать</td></tr></table></a>              </div>
    </div>
     
                                                                                                                      
            <div class="t615__col t-col t-col_4 t-align_center t615__featured">
      <div class="t615__content" style="border: 1px solid #e0e6ed;  border-radius: 9px;box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.40);">
                <div class="t615__title t-name t-name_xl" field="title2" style=" "><div style="color:#ffffff;" data-customstyle="yes">РАСШИРЕННЫЙ</div></div>        <div class="t615__descr t-descr t-descr_xs" field="descr2" style=" "><div style="color:#ffffff;" data-customstyle="yes"><ul><li>Автоматическая воронка продаж</li><li>Обязательность заполнения полей сделок</li><li>Встроенный индекс потребительской лояльности</li></ul></div></div>                <div class="t615__price-wrap">
                    <div class="t615__price t-name t-name_md" field="price2" style=" "><div style="color:#ffffff;" data-customstyle="yes">999 / месяц<br /><span style="font-size: 16px;">за пользователя</span><br /></div></div>        </div>
                          <a href="#popup:myform" target="" class="t615__btn t-btn " style="color:#ffffff;border:2px solid #ffffff;border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;"><table style="width:100%; height:100%;"><tr><td>Выбрать</td></tr></table></a>              </div>
    </div>
     
                                                                                                                      
            <div class="t615__col t-col t-col_4 t-align_center ">
      <div class="t615__content" style="border: 1px solid #e0e6ed;  border-radius: 9px;box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.40);">
                <div class="t615__title t-name t-name_xl" field="title3" style=" ">ПРОФ</div>        <div class="t615__descr t-descr t-descr_xs" field="descr3" style=" "><ul><li>Digital-воронка для розничной торговли</li><li>Backup данных с отправкой архива на почту</li><li>Мониторинг активности сотрудников</li></ul></div>                <div class="t615__price-wrap">
                    <div class="t615__price t-name t-name_md" field="price3" style=" ">1499 / месяц<br /><span style="font-size: 16px;">за пользователя</span></div>        </div>
                          <a href="#popup:myform" target="" class="t615__btn t-btn " style="color:#ffffff;background-color:#4998c6;border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;"><table style="width:100%; height:100%;"><tr><td>Выбрать</td></tr></table></a>              </div>
    </div>
     
                                                                                                                      
     
      </div>
</div>

<style type="text/css">
  #rec76668756 .t615__featured .t615__content {
    
    border-color: #ffffff !important;
    background-color: #4998c6 !important;
    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.10) !important;
  }
</style>

<script type="text/javascript">
    $(document).ready(function() {
      t615_init('76668756');
      
      $(window).bind('resize', t_throttle(function(){t615_init('76668756')}, 250));
      $('.t615').bind('displayChanged',function(){
        t615_init('76668756');
      });
    });
</script>
   
</div>


<div id="rec76771996" class="r t-rec t-rec_pt_60 t-rec_pb_60" style="padding-top:60px;padding-bottom:60px;background-color:#4998c6; "  data-record-type="209"   data-bg-color="#4998c6">
<!-- T185 -->
<div class="t185">
  	<div class="t-container t-container_flex">
			<div class="t-col t-col_flex t-col_9 t-prefix_1">
				<div class="t-text t-text_lg" style="font-weight:600;" field="text"><div style="color:#ffffff;" data-customstyle="yes">Если у вас вопрос по тарифам, настройке или этапам запуска amoCRM - напишите нам. Мы поможем решить любую вашу проблему, даже если вы скажете "что-то не работает, но не знаю что"</div></div>
			</div>
			<div class="t185__butwrapper t-col t-col_1 ">
                <a href="#popup:subscribe" target="" class="t-btn "  style="color:#ffffff;border:2px solid #ffffff;" data-btneffects-first="btneffects-ripple"><table style="width:100%; height:100%;"><tr><td>Спросить</td></tr></table></a>			</div>
	</div>
</div>

<style>

#rec76771996 .t-btn[data-btneffects-first],
#rec76771996 .t-btn[data-btneffects-second],
#rec76771996 .t-submit[data-btneffects-first],
#rec76771996 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}



#rec76771996 .t-btn[data-btneffects-first="btneffects-ripple"] .t-btn_effects,
#rec76771996 .t-submit[data-btneffects-first="btneffects-ripple"] .t-btn_effects {
	position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
}

#rec76771996 .t-btn[data-btneffects-first="btneffects-ripple"] .t-btn_effects::after,
#rec76771996 .t-submit[data-btneffects-first="btneffects-ripple"] .t-btn_effects::after {
    content: '';
	position: absolute;
	height: 350%;
	width: 200%;
	top: 0;
	left: -100%;
	background: rgba(255, 255, 255, 0.8);
	border-radius: 100%;
	-webkit-transform: scale(0);
	-ms-transform: scale(0);
	transform: scale(0);
	z-index: 20;
	-webkit-animation-name: ripple;
	animation-name: ripple;
	-webkit-animation-duration: 6s;
	animation-duration: 6s;
	-webkit-animation-timing-function: linear;
	animation-timing-function: linear;
	-webkit-animation-iteration-count: infinite;
	animation-iteration-count: infinite;
}


#rec76771996 .t-btn[data-btneffects-first="btneffects-ripple"] .t-btn_effects_md::after,
#rec76771996 .t-submit[data-btneffects-first="btneffects-ripple"] .t-btn_effects_md::after {
	left: -130%;
}

#rec76771996 .t-btn[data-btneffects-first="btneffects-ripple"] .t-btn_effects_lg::after,
#rec76771996 .t-submit[data-btneffects-first="btneffects-ripple"] .t-btn_effects_lg::after {
	left: -150%;
}


@-webkit-keyframes ripple {
  20% {
    opacity: 0;
		-webkit-transform: scale(2.5);
		transform: scale(2.5);
  }
  100% {
    opacity: 0;
		-webkit-transform: scale(2.5);
		transform: scale(2.5);
  }
}

@keyframes ripple {
  20% {
    opacity: 0;
		-webkit-transform: scale(2.5);
		transform: scale(2.5);
  }
  100% {
    opacity: 0;
		-webkit-transform: scale(2.5);
		transform: scale(2.5);
  }
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	
    $('#rec76771996 .t-btn[data-btneffects-first], #rec76771996 .t-submit[data-btneffects-first]').append('<div class="t-btn_effects"></div>');

		if ($('#rec76771996 .t-btn[data-btneffects-first], #rec76771996 .t-submit').outerWidth() > 260) {
			$('#rec76771996 .t-btn[data-btneffects-first] .t-btn_effects, #rec76771996 .t-submit[data-btneffects-first] .t-btn_effects').addClass('t-btn_effects_md');
    }
		if ($('#rec76771996 .t-btn[data-btneffects-first], #rec76771996 .t-submit').outerWidth() > 360) {
			$('#rec76771996 .t-btn[data-btneffects-first] .t-btn_effects, #rec76771996 .t-submit[data-btneffects-first] .t-btn_effects').removeClass('t-btn_effects_md');
			$('#rec76771996 .t-btn[data-btneffects-first] .t-btn_effects, #rec76771996 .t-submit[data-btneffects-first] .t-btn_effects').addClass('t-btn_effects_lg');
		}
	

	

});

</script>
</div>


<div id="rec76773631" class="r t-rec t-rec_pt_0 t-rec_pb_0" style="padding-top:0px;padding-bottom:0px; " data-animationappear="off" data-record-type="316"   >

<style>
#rec76773631 input::-webkit-input-placeholder {color:#000000; opacity: 0.5;}
#rec76773631 input::-moz-placeholder          {color:#000000; opacity: 0.5;}
#rec76773631 input:-moz-placeholder           {color:#000000; opacity: 0.5;}
#rec76773631 input:-ms-input-placeholder      {color:#000000; opacity: 0.5;}
</style>

<div class="t281">
  <div class="t-popup" data-tooltip-hook="#popup:subscribe"  style="background-color: rgba(0,0,0,0.60);">
    <div class="t-popup__close">
      <div class="t-popup__close-wrapper">
      <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
          <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
          <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
        </g>
      </svg>
      </div>
    </div>
    <div class="t-popup__container t-width t-width_6" style="background-color:#ffffff;">
                <div class="t281__wrapper t-align_left" style="background-color:#ffffff;">
          <div class="t281__title t-title t-title_xxs" style="">Задать вопрос по amoCRM</div>          <div class="t281__descr t-descr t-descr_xs" style="">Напишите ваш номер, мы позвоним в течении 30 минут.</div>          <form id="form76773631" name='form76773631' role="form" action='https://forms.tildacdn.com/procces/' method='POST' data-formactiontype="2"  data-inputbox=".t281__blockinput"   class="js-form-proccess " data-tilda-captchakey="">                                                            <input type="hidden" name="formservices[]" value="a34ca78b9dfddc03e88b8ad591e94859" class="js-formaction-services">
                                                        <input type="hidden" name="tildaspec-formname" tabindex="-1" value="vopros">
            
                                                      <div class="t281__input-container">
                <div class="t281__input-wrapper">
                  <div class="t281__blockinput">
                    <input type="text" name="email" class="t281__input t-input js-tilda-rule " value="" placeholder="+7--- --- -- --" data-tilda-req="1" data-tilda-rule="email" style="color:#000000; border:0px solid #000000; background-color:#e6e6e6; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;">
                  </div>
                  <div class="t281__blockbutton">
                    <button type="submit" class="t281__submit t-submit" style="color:#ffffff;border:1px solid #ff5e00;background-color:#ff5e00;border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;" >Отправить</button>                  </div>
                </div>
                <div class="t281__allert-wrapper">
                  <div class="js-errorbox-all t281__blockinput-errorbox" style="display:none;">
                      <div class="t281__blockinput-errors-text t-descr t-descr_xxs" >
                          <p class="t281__blockinput-errors-item js-rule-error js-rule-error-all"></p>
                        	<p class="t281__blockinput-errors-item js-rule-error js-rule-error-req">Обязательное поле</p>
                        	<p class="t281__blockinput-errors-item js-rule-error js-rule-error-email">Пожалуйста, введите правильный email</p>
                        	<p class="t281__blockinput-errors-item js-rule-error js-rule-error-name">Name Wrong. Correct please</p>
                        	<p class="t281__blockinput-errors-item js-rule-error js-rule-error-phone">Пожалуйста, введите правильный номер</p>
                        	<p class="t281__blockinput-errors-item js-rule-error js-rule-error-string">Please enter letter, number or punctuation symbols.</p>
                      </div>
                  </div>
                  <div class="js-successbox t281__blockinput__success t-text t-text_md" style="display:none;">
                      <div class="t281__success-icon">
                        <svg width="50px" height="50px" viewBox="0 0 50 50">
                          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g fill="#222">
                              <path d="M25.0982353,49.2829412 C11.5294118,49.2829412 0.490588235,38.2435294 0.490588235,24.6752941 C0.490588235,11.1064706 11.53,0.0670588235 25.0982353,0.0670588235 C38.6664706,0.0670588235 49.7058824,11.1064706 49.7058824,24.6752941 C49.7058824,38.2441176 38.6664706,49.2829412 25.0982353,49.2829412 L25.0982353,49.2829412 Z M25.0982353,1.83176471 C12.5023529,1.83176471 2.25529412,12.0794118 2.25529412,24.6752941 C2.25529412,37.2705882 12.5023529,47.5182353 25.0982353,47.5182353 C37.6941176,47.5182353 47.9411765,37.2705882 47.9411765,24.6752941 C47.9411765,12.0794118 37.6941176,1.83176471 25.0982353,1.83176471 L25.0982353,1.83176471 Z"></path>
                              <path d="M22.8435294,30.5305882 L18.3958824,26.0829412 C18.0511765,25.7382353 18.0511765,25.18 18.3958824,24.8352941 C18.7405882,24.4905882 19.2988235,24.4905882 19.6435294,24.8352941 L22.8429412,28.0347059 L31.7282353,19.1488235 C32.0729412,18.8041176 32.6311765,18.8041176 32.9758824,19.1488235 C33.3205882,19.4935294 33.3205882,20.0517647 32.9758824,20.3964706 L22.8435294,30.5305882 L22.8435294,30.5305882 Z"></path>
                            </g>
                          </g>
                        </svg>
                      </div>
                                          <div class="t281__success-message t-descr t-descr_md" >Данные успешно отправлены. Спасибо!</div>
                                      </div>
                </div>
              </div>
        </form>		      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  setTimeout(function(){
    t281_initPopup('76773631');
  }, 500);
});
</script>

<style>

#rec76773631 .t-btn[data-btneffects-first],
#rec76773631 .t-btn[data-btneffects-second],
#rec76773631 .t-submit[data-btneffects-first],
#rec76773631 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec76942998" class="r t-rec" style=" "  data-record-type="215"   >
<a name="zapusk" style="font-size:0;"></a>
</div>


<div id="rec76264227" class="r t-rec t-rec_pt_75 t-rec_pb_30" style="padding-top:75px;padding-bottom:30px; "  data-record-type="43"   >
<!-- T030 -->
<div class="t030">
  <div class="t-container t-align_left">
    <div class="t-col t-col_11 ">      
      <div class="t030__title t-title t-title_xxs" field="title" style=""><div style="font-size:52px;" data-customstyle="yes">Эффективное внедрение <span style="color: rgb(73, 152, 198);">amoCRM</span> решает следующие задачи:</div></div>      <div class="t030__descr t-descr t-descr_md" field="descr" style=""><div style="color:#1e1e1e;" data-customstyle="yes"></div></div>    </div>
  </div>
</div>
</div>


<div id="rec76598038" class="r t-rec t-rec_pt_45 t-rec_pb_60" style="padding-top:45px;padding-bottom:60px; " data-animationappear="off" data-record-type="820"   >
<!-- t820 -->

<div class="t820">
		
<div class="t820__container t-container">
<div class="t-col t-col_6  t820__col_first t-item">
  <div class="t-cell t-valign_middle">
                <svg class="t820__checkmark" style="width:30px; height:30px;" fill="#4998c6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/></svg>
          </div>
  <div class="t820__textwrapper t-cell t-valign_middle" style="">
    <div class="t-name t-name_md " style="" field="li_title__1518168977220">Вся информация о клиентах в одном месте<br /></div>      </div>
</div>
<div class="t-col t-col_6   t-item">
  <div class="t-cell t-valign_middle">
                <svg class="t820__checkmark" style="width:30px; height:30px;" fill="#4998c6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/></svg>
          </div>
  <div class="t820__textwrapper t-cell t-valign_middle" style="">
    <div class="t-name t-name_md " style="" field="li_title__1518170299525">Автоматизация бизнес-процессов<br /></div>      </div>
</div>
<div class="t-clear t820__separator" style=""></div>

<div class="t-col t-col_6   t-item">
  <div class="t-cell t-valign_middle">
                <svg class="t820__checkmark" style="width:30px; height:30px;" fill="#4998c6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/></svg>
          </div>
  <div class="t820__textwrapper t-cell t-valign_middle" style="">
    <div class="t-name t-name_md " style="" field="li_title__1518172035131">Все сделки и история работы сохранены<br /></div>      </div>
</div>
<div class="t-col t-col_6   t-item">
  <div class="t-cell t-valign_middle">
                <svg class="t820__checkmark" style="width:30px; height:30px;" fill="#4998c6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/></svg>
          </div>
  <div class="t820__textwrapper t-cell t-valign_middle" style="">
    <div class="t-name t-name_md " style="" field="li_title__1518172059965"> Интеграция с вашей телефонией <br /></div>      </div>
</div>
<div class="t-clear t820__separator" style=""></div>

<div class="t-col t-col_6   t-item">
  <div class="t-cell t-valign_middle">
                <svg class="t820__checkmark" style="width:30px; height:30px;" fill="#4998c6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/></svg>
          </div>
  <div class="t820__textwrapper t-cell t-valign_middle" style="">
    <div class="t-name t-name_md " style="" field="li_title__1518174560104">Напоминания о задачах и контактах<br /></div>      </div>
</div>
<div class="t-col t-col_6   t-item">
  <div class="t-cell t-valign_middle">
                <svg class="t820__checkmark" style="width:30px; height:30px;" fill="#4998c6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/><path d="M50.2 1.9C23.5 1.9 1.7 23.7 1.7 50.4s21.8 48.5 48.5 48.5 48.4-21.8 48.5-48.5c0-26.7-21.8-48.5-48.5-48.5zm-7.3 71.4L22.7 53.2l4.2-4.3 15.8 15.7 32.1-35.4 4.4 4-36.3 40.1z"/></svg>
          </div>
  <div class="t820__textwrapper t-cell t-valign_middle" style="">
    <div class="t-name t-name_md " style="" field="li_title__1518174591090">Контроль работы сотрудников<br /></div>      </div>
</div>
</div>
	


</div>
</div>


<div id="rec76658370" class="r t-rec t-rec_pt_45 t-rec_pb_60" style="padding-top:45px;padding-bottom:60px; "  data-record-type="547"   >
<!-- T547 -->
<div class="t547">
  		<div class="t-section__container t-container">
		<div class="t-col t-col_12">
			<div class="t-section__topwrapper t-align_center">
				<div class="t-section__title t-title t-title_xs" field="btitle">Как проходит <span style="color: rgb(73, 152, 198);">внедрение</span></div>							</div>
		</div>
	</div>
		  <div class="t-container t547__container">
                          <div class="t547__item" >
      <div class="t-width t-width_8 t547__mainblock">
        <div class="t547__col">
          <div class="t547__block">
                        <div class="t547__title t-name t-name_lg" field="li_title__1479137044697" style="">Сбор требований и постановка задач</div>            <div class="t547__descr t-text t-text_xs" field="li_descr__1479137044697" style="">Выслушиваем пожелания и проводим предварительный аудит вашего отдела продаж</div>                      </div>
        </div>
                <div class="t547__line" style="background-color:#e0e0e0;"></div>
        <div class="t547__circle" style="background-color:#4998c6;"></div>
              </div>
      </div>
                          <div class="t547__item" >
      <div class="t-width t-width_8 t547__mainblock">
        <div class="t547__col t547__flipped">
          <div class="t547__block">
                        <div class="t547__title t-name t-name_lg" field="li_title__1479137356907" style="">Предлагаем лучшее решение на основе Ваших требований</div>            <div class="t547__descr t-text t-text_xs" field="li_descr__1479137356907" style="">На основе собранных данных составляем документ содержащий функциональные требования и наши рекомендации по внедрению</div>                      </div>
        </div>
                <div class="t547__line" style="background-color:#e0e0e0;"></div>
        <div class="t547__circle" style="background-color:#4998c6;"></div>
              </div>
      </div>
                          <div class="t547__item" >
      <div class="t-width t-width_8 t547__mainblock">
        <div class="t547__col">
          <div class="t547__block">
                        <div class="t547__title t-name t-name_lg" field="li_title__1479137790652" style="">Дорабатываем и интегрируем</div>            <div class="t547__descr t-text t-text_xs" field="li_descr__1479137790652" style="">Делаем сложные интеграции со сторонними системами и разрабатываем нетиповой функционал</div>                      </div>
        </div>
                <div class="t547__line" style="background-color:#e0e0e0;"></div>
        <div class="t547__circle" style="background-color:#4998c6;"></div>
              </div>
      </div>
                          <div class="t547__item" >
      <div class="t-width t-width_8 t547__mainblock">
        <div class="t547__col t547__flipped">
          <div class="t547__block">
                        <div class="t547__title t-name t-name_lg" field="li_title__1479137793455" style="">Производим обучение сотрудников работе с CRM</div>            <div class="t547__descr t-text t-text_xs" field="li_descr__1479137793455" style="">Мы расскажем вашим сотрудникам что такое СRM, для чего он нужен, научим выполнять базовые действия в системе, ответим на возникающие вопросы. <br /></div>                      </div>
        </div>
                <div class="t547__line" style="background-color:#e0e0e0;"></div>
        <div class="t547__circle" style="background-color:#4998c6;"></div>
              </div>
      </div>
                          <div class="t547__item" >
      <div class="t-width t-width_8 t547__mainblock">
        <div class="t547__col">
          <div class="t547__block">
                        <div class="t547__title t-name t-name_lg" field="li_title__1543238915051" style="">Поддержка и сопровождение</div>            <div class="t547__descr t-text t-text_xs" field="li_descr__1543238915051" style="">В течение 60 дней после внедрения ведём вас и вносим по необходимости корректировки</div>                      </div>
        </div>
                <div class="t547__line" style="background-color:#e0e0e0;"></div>
        <div class="t547__circle" style="background-color:#4998c6;"></div>
              </div>
      </div>
          </div>
  	

</div>
</div>


<div id="rec76641350" class="r t-rec" style=" "  data-record-type="215"   >
<a name="method" style="font-size:0;"></a>
</div>


<div id="rec76662917" class="r t-rec t-rec_pt_60 t-rec_pb_60" style="padding-top:60px;padding-bottom:60px; "  data-record-type="509"   >
<!-- t509 -->
<div class="t509">
		<div class="t-section__container t-container">
		<div class="t-col t-col_12">
			<div class="t-section__topwrapper t-align_center">
				<div class="t-section__title t-title t-title_xs" field="btitle">Результаты <span style="color: rgb(73, 152, 198);">внедрения</span></div>							</div>
		</div>
	</div>
		
<div class="t-container">
<div class="t509__colwrapper t-col t-col_10 t-prefix_1">
  
    					<div class="t509__col t-col t-col_5 t509__mobileimg"><div class="t509__blockimg t-bgimg" bgimgfield="li_img__1476897493817" data-original="images/tild3564-3461-4533-b236-393362356636__111.svg" style="background-image:url('images/tild3564-3461-4533-b236-393362356636__111.svg');"  data-image-width="350" data-image-height="340"></div></div>
		<div class="t509__col t-col t-col_5 t509__leftcol">
		<div class="t509__textwrapper t-align_left" style="max-width:400px;">
			<div class="t509__content t-valign_middle">
				<div class="t509__box">
					<div class="t509__title t-heading t-heading_xs t-margin_auto" field="li_title__1476897493817" style="">Систематизация бизнес-процессов</div>					<div class="t509__descr t-descr t-descr_sm t-margin_auto" field="li_descr__1476897493817" style="">Вы будете точно измерять эффективность работы отдела продаж и каждого менеджера<br /><br />Вся информация по клиенту от первого контакта до последней продажи теперь в единой карточке. Любой новый менеджер сможет легко продолжить работу по клиентам своего предшественника<br /></div>									</div>
			</div>
		</div>
	</div>
		<div class="t509__col t-col t-col_5 t509__rightcol t509__desktopimg">
		<div class="t509__imgwrapper" style="max-width:350px;">
			<div class="t509__blockimg t-bgimg" bgimgfield="li_img__1476897493817" data-original="images/tild3564-3461-4533-b236-393362356636__111.svg" style="background-image:url('images/tild3564-3461-4533-b236-393362356636__111.svg');"  data-image-width="350" data-image-height="340"></div>
		</div>
	</div>
		  
    	<div class="t509__separator t-clear" style=""></div>
  					<div class="t509__col t-col t-col_5 t509__mobileimg"><div class="t509__blockimg t-bgimg" bgimgfield="li_img__1476897695209" data-original="images/tild6365-3763-4037-b238-636564386238__2222.svg" style="background-image:url('images/tild6365-3763-4037-b238-636564386238__2222.svg');"  data-image-width="350" data-image-height="340"></div></div>
		<div class="t509__col t-col t-col_5 t509__leftcol t509__desktopimg">
		<div class="t509__imgwrapper" style="max-width:350px;">
			<div class="t509__blockimg t-bgimg" bgimgfield="li_img__1476897695209" data-original="images/tild6365-3763-4037-b238-636564386238__2222.svg" style="background-image:url('images/tild6365-3763-4037-b238-636564386238__2222.svg');"  data-image-width="350" data-image-height="340"></div>
		</div>
	</div>
		<div class="t509__col t-col t-col_5 t509__rightcol">
		<div class="t509__textwrapper t-align_left" style="max-width:400px;">
			<div class="t509__content t-valign_middle">
				<div class="t509__box">
					<div class="t509__title t-heading t-heading_xs t-margin_auto" field="li_title__1476897695209" style="">Повышение качества сервиса</div>					<div class="t509__descr t-descr t-descr_sm t-margin_auto" field="li_descr__1476897695209" style="">Повысите лояльность клиентов. Менеджеры больше не забудут связаться с клиентом в назначенное время, отправить КП, провести встречу<br /><br />Стоимость новых заявок снижается. Вы повысите конверсию из заявки в продажу и перестанете терять клиентов, за которых заплатили<br /></div>									</div>
			</div>
		</div>
	</div>
		</div>
</div>
	


</div>
<script type="text/javascript">
    $(window).resize(function() {
        t509_setHeight('76662917');
    });
    $(document).ready(function() {
        t509_setHeight('76662917');
        
    });
</script>

</div>


<div id="rec76641400" class="r t-rec t-rec_pb_0" style="padding-bottom:0px; "  data-record-type="215"   >
<a name="zayavka" style="font-size:0;"></a>
</div>


<div id="rec76609267" class="r t-rec t-rec_pt_0 t-rec_pb_0" style="padding-top:0px;padding-bottom:0px;background-color:#efefef; " data-animationappear="off" data-record-type="823"   data-bg-color="#efefef">
<!-- t823 -->
<div class="t823">
  <div class="t823__container ">
    <table class="t823__wrapper t823__wrapper_mobile" style="height:100vh;" data-height-correct-vh="yes">
      <tr>
                <td class="t823__col t823__col_img t823__col_img_mobile" style="width:50%;">
          <div class="t823__imgblock t-align_center">
            <div class="t823__imgwrapper" bgimgfield="img">
              <div class="t823__bgimg t-bgimg"  bgimgfield="img" data-original="images/tild3762-3231-4233-b232-346564323461__dawid-sobolewski-359.jpg" style="background-image: url('images/tild3762-3231-4233-b232-346564323461__dawid-sobolewski-359.jpg');"></div>
            </div>
          </div>
        </td>
        
        <td class="t823__col t823__col_form t823__col_form_mobile" style="vertical-align:middle;width:50%;">
          <div class="t823__main-wrapper">
            <div class="t823__title t-title t-title_xs "  style="" field="title">Заказать оценку внедрения<br /></div>                                    <div>
                                      	  		                  	  		                  	  		                  	  		
    <form id="form76609267" name='form76609267' role="form" action='' method='POST' data-formactiontype="2" data-inputbox=".t-input-group"   class="t-form js-form-proccess t-form_inputs-total_4 t-form_bbonly "  data-success-callback="t823_onSuccess"  >
	
	  			        	            	                <input type="hidden" name="formservices[]" value="a34ca78b9dfddc03e88b8ad591e94859" class="js-formaction-services">
	            	        
                            <input type="hidden" name="tildaspec-formname" tabindex="-1" value="forma v podvale">
            
            <div class="js-successbox t-form__successbox t-text t-text_md" style="display:none;"></div>

            <div class="t-form__inputsbox">

				                                                                                                
                  	                  	  <div class="t-input-group t-input-group_nm" data-input-lid="1518534410690">
                                                                              <div class="t-input-block">
                                                          <input type="text" name="Name" class="t-input js-tilda-rule t-input_bbonly" value="" placeholder="Ваше имя*" data-tilda-req="1" data-tilda-rule="name" style="color:#000000; border:1px solid #b3b3b3;  ">
                          						  <div class="t-input-error"></div>
                          </div>
          			  </div>

                                                                
                  	                  	  <div class="t-input-group t-input-group_ph" data-input-lid="1518534421664">
                                                                              <div class="t-input-block">
                                                          <input type="tel" name="Phone" class="t-input js-tilda-rule  t-input_bbonly" value="" placeholder="Ваш телефон*" data-tilda-req="1" data-tilda-rule="phone" pattern="[0-9]*+"  style="color:#000000; border:1px solid #b3b3b3;  ">                                                
                          						  <div class="t-input-error"></div>
                          </div>
          			  </div>

                                                                
                  	                  	  <div class="t-input-group t-input-group_em" data-input-lid="1543309466940">
                                                                              <div class="t-input-block">
                                                          <input type="text" name="Email" class="t-input js-tilda-rule t-input_bbonly" value="" placeholder="E-mail"  data-tilda-rule="email" style="color:#000000; border:1px solid #b3b3b3;  ">
                          						  <div class="t-input-error"></div>
                          </div>
          			  </div>

                                                                
                  	                  	  <div class="t-input-group t-input-group_rd" data-input-lid="1543244012613">
                          <div class="t-input-title t-descr t-descr_md" data-redactor-toolbar="no" field="li_title__1543244012613" style="">Выберите необходимые работы для расчета стоимости:</div>                                                    <div class="t-input-block">
                                                    		                                                            			                            <input type="hidden" class="t-checkboxes__hiddeninput js-tilda-rule" name="Выберите необходимые работы для расчета стоимости:" tabindex="-1" value="" >
									<div class="t-checkboxes__wrapper">
                                                                              <label class="t-checkbox__control t-text t-text_xs" style=""><input type="checkbox" value="Аналитика и настройка amoCRM" class="t-checkbox"  ><div class="t-checkbox__indicator" ></div>Аналитика и настройка amoCRM</label>
                                                                              <label class="t-checkbox__control t-text t-text_xs" style=""><input type="checkbox" value="Интеграция источников заявок" class="t-checkbox"  ><div class="t-checkbox__indicator" ></div>Интеграция источников заявок</label>
                                                                              <label class="t-checkbox__control t-text t-text_xs" style=""><input type="checkbox" value="Интеграция IP телефонии" class="t-checkbox"  ><div class="t-checkbox__indicator" ></div>Интеграция IP телефонии</label>
                                                                              <label class="t-checkbox__control t-text t-text_xs" style=""><input type="checkbox" value="Внедрение документооборота" class="t-checkbox"  ><div class="t-checkbox__indicator" ></div>Внедрение документооборота</label>
                                                                              <label class="t-checkbox__control t-text t-text_xs" style=""><input type="checkbox" value="Автоматизация задач" class="t-checkbox"  ><div class="t-checkbox__indicator" ></div>Автоматизация задач</label>
                                                                              <label class="t-checkbox__control t-text t-text_xs" style=""><input type="checkbox" value="Интеграция с 1С Предприятие" class="t-checkbox"  ><div class="t-checkbox__indicator" ></div>Интеграция с 1С Предприятие</label>
                                                                              <label class="t-checkbox__control t-text t-text_xs" style=""><input type="checkbox" value="Обучение и техподдержка" class="t-checkbox"  ><div class="t-checkbox__indicator" ></div>Обучение и техподдержка</label>
                                                                          </div>
                                    <script>
                                    
                                    $(document).ready(function(){
                                      function t_input_checkboxes_updateval(recid,lid){
                                      	   var qel=$('#rec'+recid).find('[data-input-lid="'+lid+'"]');
                                           var val="";
                                           qel.find('.t-checkbox:checked').each(function(){
                                              if(val!=""){val+="; ";}
                                              val += $(this).val();
                                           });
                                           qel.find('.t-checkboxes__hiddeninput').val(val);
                                      }
                                      function t_input_checkboxes_init(recid,lid){
                                          var qel=$('#rec'+recid).find('[data-input-lid="'+lid+'"]');
                                          qel.find('.t-checkbox__control').click(function() {
                                            t_input_checkboxes_updateval(recid,lid);
                                          });
	                                      t_input_checkboxes_updateval(recid,lid);
                                      }
                                      t_input_checkboxes_init('76609267','1543244012613');
                                    });
                                    
                                    </script>
	                                                            	                          						  <div class="t-input-error"></div>
                          </div>
          			  </div>

                                
                
								<!--[if IE 8]>
				<style>
				
				.t-checkbox__control .t-checkbox, .t-radio__control .t-radio {
				  left: 0px;
				  z-index: 1;
				  opacity: 1;
				}
				.t-checkbox__indicator, .t-radio__indicator {
				  display: none;
				}
				.t-img-select__control .t-img-select {
				  position: static;
				}
				
				</style>
				<![endif]-->
				
				
				<div class="t-form__errorbox-middle">
					                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                    <div class="t-form__errorbox-text t-text t-text_md">
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req">Обязательное поле</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email">Пожалуйста, введите правильный email</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone">Пожалуйста, введите правильный номер</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                    </div>
                </div>				</div>

                <div class="t-form__submit">
                    <button type="submit" class="t-submit" style="color:#ffffff;background-color:#4998c6;border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;" >Оставить заявку</button>
                </div>

            </div>

            <div class="t-form__errorbox-bottom">
            	                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                    <div class="t-form__errorbox-text t-text t-text_md">
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req">Обязательное поле</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email">Пожалуйста, введите правильный email</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone">Пожалуйста, введите правильный номер</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                    </div>
                </div>            </div>


    </form>


		
	<style>
	#rec76609267 input::-webkit-input-placeholder {color:#000000; opacity: 0.5;}
	#rec76609267 input::-moz-placeholder          {color:#000000; opacity: 0.5;}
	#rec76609267 input:-moz-placeholder           {color:#000000; opacity: 0.5;}
	#rec76609267 input:-ms-input-placeholder      {color:#000000; opacity: 0.5;}
	#rec76609267 textarea::-webkit-input-placeholder {color:#000000; opacity: 0.5;}
	#rec76609267 textarea::-moz-placeholder          {color:#000000; opacity: 0.5;}
	#rec76609267 textarea:-moz-placeholder           {color:#000000; opacity: 0.5;}
	#rec76609267 textarea:-ms-input-placeholder      {color:#000000; opacity: 0.5;}
	</style>
	
	            </div>
                      </div>
        </td>
              </tr>
    </table>
  </div>
</div>


<style>

#rec76609267 .t-btn[data-btneffects-first],
#rec76609267 .t-btn[data-btneffects-second],
#rec76609267 .t-submit[data-btneffects-first],
#rec76609267 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec76784221" class="r t-rec t-rec_pt_0" style="padding-top:0px; "  data-record-type="215"   >
<a name="onas" style="font-size:0;"></a>
</div>


<div id="rec76284701" class="r t-rec t-rec_pt_60 t-rec_pb_60" style="padding-top:60px;padding-bottom:60px;background-color:#ffffff; "  data-record-type="489"   data-bg-color="#ffffff">


<!-- T489 -->
<div class="t489">
  <div class="t-container">
    <div class="t489__top t-col t-col_5 t-prefix_1">
              <div class="t489__title t-title t-title_md" field="title" style=""><div style="font-size:46px;" data-customstyle="yes"><span style="font-size: 52px;">Первый БИТ</span> - <span style="font-size: 32px;">крупнейший федеральный IT-интегратор</span></div></div>        <div class="t489__subtitle t-name t-name_xs" field="subtitle" style="">web-решения для бизнеса</div>            </div>
      <div class="t-col t-col_5 ">
              <div class="t489__descr t-descr t-descr_md" field="descr" style="">90 офисов в 60 городах России, СНГ, ОАЭ и Канады. 220 000 успешных внедрений автоматизированных систем. Более 5000 сотрудников.<br /><br />Мы поддерживаем ваш бизнес 24 часа 7 дней в неделю!</div>          </div>
  </div>
</div>
</div>


<div id="rec76774428" class="r t-rec" style=" " data-animationappear="off" data-record-type="702"   >
<!-- T702 -->
<div class="t702">
  <div class="t-popup" data-tooltip-hook="#popup:myform"  >
    <div class="t-popup__close">
      <div class="t-popup__close-wrapper">
        <svg class="t-popup__close-icon" width="23px" height="23px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g stroke="none" stroke-width="1" fill="#fff" fill-rule="evenodd">
            <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
            <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect>
          </g>
        </svg>
      </div>  
    </div>
    <div class="t-popup__container t-width t-width_6" >
                <div class="t702__wrapper">
          <div class="t702__text-wrapper t-align_center">
            <div class="t702__title t-title t-title_xxs" style="">Купить тариф</div>            <div class="t702__descr t-descr t-descr_xs" style="">Просто оставьте ваши контакты и мы позвоним</div>          </div>  
                                    	  		                      	  		
    <form id="form76774428" name='form76774428' role="form" action='' method='POST' data-formactiontype="2" data-inputbox=".t-input-group"   class="t-form js-form-proccess t-form_inputs-total_2  "  data-success-callback="t702_onSuccess"  >
	
	  			        	            	                <input type="hidden" name="formservices[]" value="a34ca78b9dfddc03e88b8ad591e94859" class="js-formaction-services">
	            	        
                            <input type="hidden" name="tildaspec-formname" tabindex="-1" value="tarif">
            
            <div class="js-successbox t-form__successbox t-text t-text_md" style="display:none;"></div>

            <div class="t-form__inputsbox">

				                                                                                                
                  	                  	  <div class="t-input-group t-input-group_nm" data-input-lid="1495810359387">
                                                                              <div class="t-input-block">
                                                          <input type="text" name="Name" class="t-input js-tilda-rule " value="" placeholder="Ваше имя" data-tilda-req="1" data-tilda-rule="name" style="color:#000000; border:1px solid #c9c9c9;  border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;">
                          						  <div class="t-input-error"></div>
                          </div>
          			  </div>

                                                                                
                  	                  	  <div class="t-input-group t-input-group_ph" data-input-lid="1495810410810">
                                                                              <div class="t-input-block">
                                                          <input type="tel" name="Phone" class="t-input js-tilda-rule  " value="" placeholder="Ваш телефон" data-tilda-req="1" data-tilda-rule="phone" pattern="[0-9]*+"  style="color:#000000; border:1px solid #c9c9c9;  border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;">                                                
                          						  <div class="t-input-error"></div>
                          </div>
          			  </div>

                                
                
				
				
				<div class="t-form__errorbox-middle">
					                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                    <div class="t-form__errorbox-text t-text t-text_md">
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req">Обязательное поле</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email">Пожалуйста, введите правильный email</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone">Пожалуйста, введите правильный номер</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                    </div>
                </div>				</div>

                <div class="t-form__submit">
                    <button type="submit" class="t-submit" style="color:#ffffff;border:2px solid #ffffff;background-color:#4998c6;border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;" >Отправить</button>
                </div>

            </div>

            <div class="t-form__errorbox-bottom">
            	                <div class="js-errorbox-all t-form__errorbox-wrapper" style="display:none;">
                    <div class="t-form__errorbox-text t-text t-text_md">
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-all"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-req">Обязательное поле</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-email">Пожалуйста, введите правильный email</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-name"></p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-phone">Пожалуйста, введите правильный номер</p>
                        <p class="t-form__errorbox-item js-rule-error js-rule-error-string"></p>
                    </div>
                </div>            </div>


    </form>


		
	<style>
	#rec76774428 input::-webkit-input-placeholder {color:#000000; opacity: 0.5;}
	#rec76774428 input::-moz-placeholder          {color:#000000; opacity: 0.5;}
	#rec76774428 input:-moz-placeholder           {color:#000000; opacity: 0.5;}
	#rec76774428 input:-ms-input-placeholder      {color:#000000; opacity: 0.5;}
	#rec76774428 textarea::-webkit-input-placeholder {color:#000000; opacity: 0.5;}
	#rec76774428 textarea::-moz-placeholder          {color:#000000; opacity: 0.5;}
	#rec76774428 textarea:-moz-placeholder           {color:#000000; opacity: 0.5;}
	#rec76774428 textarea:-ms-input-placeholder      {color:#000000; opacity: 0.5;}
	</style>
	
	                  </div>
      </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  setTimeout(function(){
    t702_initPopup('76774428');
  }, 500);
});
</script>

<style>

#rec76774428 .t-btn[data-btneffects-first],
#rec76774428 .t-btn[data-btneffects-second],
#rec76774428 .t-submit[data-btneffects-first],
#rec76774428 .t-submit[data-btneffects-second] {
	position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}





</style>



<script type="text/javascript">

$(document).ready(function() {
	

	

});

</script>
</div>


<div id="rec76675923" class="r t-rec" style=" "  data-record-type="270"   >
<div class="t270" ></div>
<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      var $root = $('html, body');
      $('a[href*=#]:not([href=#],.carousel-control,.t-carousel__control,[href^=#price],[href^=#popup],[href^=#prodpopup],[href^=#order])').click(function() {        
        var target = $(this.hash);  
        if (target.length == 0){ target = $('a[name="' + this.hash.substr(1) + '"]'); }
        if (target.length == 0){
          return true;
        } 
        $root.animate({ scrollTop: target.offset().top + 3 }, 500);  
        return false;  
      });
    }, 500);
  });
     
</script>

</div>

</div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Техническая поддержка CRM-системы включает круглосуточный мониторинг работоспособности продукта, настройки и доработки функционала программы, а также консультации по работе. Обращайтесь к нам ☎ +7 (495) 748-33-02");
$APPLICATION->SetTitle("Техподдержка CRM-систем — стоимость услуг");
?>

<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/promo.php');?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/support_cost.php');?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/merit.php');?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/statement.php');?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/certificates.php');?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/quote.php');?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
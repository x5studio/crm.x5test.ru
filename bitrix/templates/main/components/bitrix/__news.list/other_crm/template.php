<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="center clients prod">
	<div class='big_title'>Обзор других CRM-систем:</div>

<ul class="client_list prod">
    <?foreach($arResult["ITEMS"] as $arItem){
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
 	    $img=CFile::ResizeImageGet($arItem[DETAIL_PICTURE][ID],array("width" => 220, "height" => 180),BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ?>
    	<li id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="cursor:default; padding:45px 0;">
            <div class="client_list_img fl" style="background:url(<?=$img[src]?>) no-repeat center center;" ></div>
            <div class="client_list_description fr">
               <?/*?> <div class="client_list_title"><?=$arItem[NAME]?></div><?*/?>
                <div class="client_list_text"><?=$arItem[DETAIL_TEXT]?></div>
            </div>
			<div class="pr-right">
				<div class="pr-right-wr" >
					<button class="btn" onclick="form2()">консультация</button>
				<div class="clear"></div>
				</div>
			</div>
			
			
			
            <div class="cl"></div>
    	</li>
    <?}?>
</ul>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>
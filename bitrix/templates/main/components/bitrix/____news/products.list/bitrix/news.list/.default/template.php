<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="center clients prod">
	<div class='big_title'>Первый бит рекомендует:</div>

<ul class="client_list prod">
    <?foreach($arResult["ITEMS"] as $arItem){
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
 	    $img=CFile::ResizeImageGet($arItem[DETAIL_PICTURE][ID],array("width" => 300, "height" => 180),BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ?>
    	<li id="<?=$this->GetEditAreaId($arItem['ID']);?>" title="перейти" style="cursor:default;">
          <div style="width:725px; float:left;  cursor:pointer;"  onclick="location.href='<?=$arItem['DETAIL_PAGE_URL']?>';">
		  <div class="client_list_img fl" style="background:url(<?=$img[src]?>) no-repeat center center;"></div>
            <div class="client_list_description fr" onclick="location.href='<?=$arItem['DETAIL_PAGE_URL']?>';" style="cursor:pointer;">
               <?/*?> <div class="client_list_title"><?=$arItem[NAME]?></div><?*/?>
                <div class="client_list_text"><?=$arItem[PREVIEW_TEXT]?></div>
            </div>
		</div>

			<div class="pr-right" style="cursor:default;">
				<div class="pr-right-wr" >
				<?if($arItem['PROPERTIES']['PR_BUY']['VALUE']!=''){?>
					<div class="pr-buy">
						<img style="padding-left:5px" src="/img/box2.png"/> <span>Покупка от: <?=$arItem['PROPERTIES']['PR_BUY']['VALUE']?></span>
						<div class="clear"></div>
					</div>
				<?}?>
				<?if($arItem['PROPERTIES']['PR_RENT']['VALUE']!=''){?>
				<div class="pr-rent">
					<img style="width:40px" src="/img/cloud3.png"/> <span>Аренда от: <?=$arItem['PROPERTIES']['PR_RENT']['VALUE']?></span>
				</div>
				<?}?>
				<button class="btn" onclick="form3('<?=$arItem[NAME]?>')">попробовать бесплатно</button>
				<div class="clear"></div>
				</div>
			</div>
			
			
			
            <div class="clear"></div>
    	</li>
    <?}?>
</ul>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

</div>
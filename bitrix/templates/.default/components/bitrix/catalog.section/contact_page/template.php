<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->AddChainItem($arResult[NAME],$arResult[SECTION_PAGE_URL] );?>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=ru-RU" type="text/javascript"></script>
<?$this->setFrameMode(true);?>
<div class="center">
    <div class='big_title contacts_select_city'><h1>Офисы в <span><?=$arResult[UF_NAME_DATIVE]?></span></h1></div>
    <div class='geo_contact'></div>
    <div class='contactbox'>
    <?//pr($arResult);?>
        <?if($arResult[ID]==14){//особый вывод для москвы?>
            
            <div class="contacts_control">
                <div class="box fl">
                    <span class="list switch-active">Списком</span>
                    <span class="switch">
                        <span class="switch_control left"></span>
                    </span>
                    <span class='to_map'>На карте</span>
                </div>
                <span class="timework fr">Время работы: <span class="timework_time"><?=strip_tags($arResult["~UF_OFFICES_HOURS"]);?></span></span>
                <div class="cl"></div>
            </div>
            <div class='contact-map'>
            <!-- Москва отображение на карте -->
                <div id="map"></div>
                
            <!-- москва конец -->
            </div>
            <div class='contact-list'>
            <!-- Москва списком --> 
            <?$APPLICATION->IncludeFile("/.inc/contact_map_header.php");?>
                        <?foreach($arResult[ITEMS] as $item){?>
                   		   //*****
                       	    curGeoObject = new ymaps.GeoObject({
                                                        geometry: {type: "Point", coordinates: [<?=$item[PROPERTIES][COORDINATES][VALUE]?>]},
                                                		properties: {
                                                			clusterCaption: '<?=$item[NAME]?>',
                                                			balloonContentHeader: "<a class='map_office_name' href='/contacts/<?=$item[CODE]?>/' style='font-size: 18px;'><?=$item[NAME]?></a>"+
                                                                                  "<div class='map_office_phone'><?=$item[PROPERTIES][phone][VALUE]?></div>"+
                                                                                  "<div class='map_office_worktime'><?foreach($item[PROPERTIES][WORK_TIME][VALUE] as $k=>$v){?><div><?=$item[PROPERTIES][WORK_TIME][DESCRIPTION][$k]?> <?=$v?> </div><?}?></div>"+
                                                                                  "<div class='map_office_adres'><?=$item[PROPERTIES][address][VALUE]?></div>"+
                                                                                  "<div class='map_office_email'><a href='mailto:<?=$item[PROPERTIES][email][VALUE]?>'><?=$item[PROPERTIES][email][VALUE]?></a></div>"
                                                                                  <?if($item[PROPERTIES][hotline][VALUE]){?>+"<div class='map_office_consult'>Линия консультаций:</div><div class='map_office_phone'><?=$item[PROPERTIES][hotline][VALUE]?></div>"<?}?>
                                                            ,
                                                            hintContent: "<?=$item[NAME]?>"
                                                		}
                            	               });
                        	myGeoObjects90.push(curGeoObject);
                        	myCollection.add(curGeoObject);
                        	//*****		
                    	<?}?>		
            <?$APPLICATION->IncludeFile("/.inc/contact_map_footer.php");?>      	
            
            
            <div class="city"><?=$arResult[UF_TITLE]?></div>
            <table>
                <?foreach($arResult[ITEMS] as $item){?>
                    <tr>
                        <td><a href="/contacts/<?=$item[CODE]?>/" class='office'><?=$item[NAME]?></a></td>
                        <td width="250"><div class='phone'><?=$item[PROPERTIES][phone][VALUE]?></div></td>
                        <td>
                            <div class='adres'><?=$item[PROPERTIES][address][VALUE]?></div>
                            <div class='email'><a href="mailto:<?=$item[PROPERTIES][email][VALUE]?>"><?=$item[PROPERTIES][email][VALUE]?></a></div>
                        </td>
                    </tr>
                <?}?>
            </table>
            
            <!-- Москва списком конец -->
            </div>
        <?}else{//обычный вывод?>
    
            <div class='standart_contact'>
                <div class="box fl">
                    <?foreach($arResult[ITEMS] as $item){?>
                        <div class='office_item'>
                            <a class="office_name" href='/contacts/<?=$item[CODE]?>/'><?=$item[NAME]?></a>
                            <div class='office_phone'><?=$item[PROPERTIES][phone][VALUE]?></div>
                            <div class='office_adres'><?=$item[PROPERTIES][address][VALUE]?></div>
                            <div class='office_email'><a href="mailto:<?=$item[PROPERTIES][email][VALUE]?>"><?=$item[PROPERTIES][email][VALUE]?></a></div>
                            <?if($item[PROPERTIES][hotline][VALUE]){?>
                                <div class='office_consult'>Линия консультаций:</div>
                                <div class="office_phone">
                                    <?=$item[PROPERTIES][hotline][VALUE]?>
                                </div>
                            <?}?>
                        </div>
                    <?}?>
                </div>
                <div class="box fr">
                    <div id="map"></div>
                    <?$APPLICATION->IncludeFile("/.inc/contact_map_header.php");?>
                        <?foreach($arResult[ITEMS] as $item){?>
                   		   //*****
                       	    curGeoObject = new ymaps.GeoObject({
                                                        geometry: {type: "Point", coordinates: [<?=$item[PROPERTIES][COORDINATES][VALUE]?>]},
                                                		properties: {
                                                			clusterCaption: '<?=$item[NAME]?>',
                                                			balloonContentHeader: "<a class='map_office_name' href='/contacts/<?=$item[CODE]?>/' style='font-size: 18px;'><?=$item[NAME]?></a>"+
                                                                                  "<div class='map_office_phone'><?=$item[PROPERTIES][phone][VALUE]?></div>"+
                                                                                  "<div class='map_office_worktime'><?foreach($item[PROPERTIES][WORK_TIME][VALUE] as $k=>$v){?><div><?=$item[PROPERTIES][WORK_TIME][DESCRIPTION][$k]?> <?=$v?> </div><?}?></div>"+
                                                                                  "<div class='map_office_adres'><?=$item[PROPERTIES][address][VALUE]?></div>"+
                                                                                  "<div class='map_office_email'><a href='mailto:<?=$item[PROPERTIES][email][VALUE]?>'><?=$item[PROPERTIES][email][VALUE]?></a></div>"
                                                                                  <?if($item[PROPERTIES][hotline][VALUE]){?>+"<div class='map_office_consult'>Линия консультаций:</div><div class='map_office_phone'><?=$item[PROPERTIES][hotline][VALUE]?></div>"<?}?>
                                                            ,
                                                            hintContent: "<?=$item[NAME]?>"
                                                		}
                            	               });
                        	myGeoObjects90.push(curGeoObject);
                        	myCollection.add(curGeoObject);
                        	//*****		
                    	<?}?>		
                    <?$APPLICATION->IncludeFile("/.inc/contact_map_footer.php");?>  
                    <script>
                        $(document).ready(function(){
                            setTimeout(init,500);
                        });
                    </script>
                </div>
                <div class="cl"></div>
            </div>
        
        <?}?>
    </div>
</div>
<script>
    $(document).ready(function(){
        var ini=false;
        $('.contacts_control span').click(function(){
            var q=$(this);
            
            if(!q.hasClass("switch-active") && !q.hasClass('switch_control')){
               if($('.contacts_control .list').hasClass("switch-active")){
                   $('.contacts_control .list').removeClass("switch-active");
                   $('.contacts_control .to_map').addClass("switch-active");
                   $('.contacts_control .switch_control').removeClass("left").addClass("right");
                   $('.contact-map').show();
                   setTimeout(function(){
                    if(!ini){  $("#map").html(''); init(); ini=true;}
                   
                    },500);
                   //window.location.hash="/contact-map";
		   $.cookie('view_contact', 'map', { expires: 7, path: '/' });
               }else{
                   $('.contacts_control .list').addClass("switch-active");
                   $('.contacts_control .to_map').removeClass("switch-active");
                   $('.contacts_control .switch_control').removeClass("right").addClass("left");
                   $('.contact-map').hide();
                   //window.location.hash="/contact-list";
		   $.cookie('view_contact', 'list', { expires: 7, path: '/' });
               }
            } 
        });
         
        //проверка после перезагрузки страницы
        //if(window.location.hash=="#/contact-map") $('.contacts_control .switch').click();
	if($.cookie('view_contact')=="map") $('.contacts_control .switch').click();
        
        
        
        
        
        //-----------------------------
            $(".contacts_select_city span").click(function(){
                var q=$(this);
                $('.geo_contact').html($('.geo_resource').html());
                $('.geo_contact .modal_geo').show();
                $('.geo_contact .overlay_transparent').show();
                
                //вешаем обработчики закрытия
                $('.geo_contact .modal_geo_close,.geo_contact .overlay_transparent').click(function(){
                    
                    $('.geo_contact .modal_geo').remove();
                    $('.geo_contact .overlay_transparent').remove();
                });
            
                //установка кода вместо id
                $.each($('.modal_geo a'),function(){
                   $(this).attr("href","/contacts/"+$(this).attr("code")+"/");
                });
                
            });
        
        //-----------------------------
    });
</script>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
CModule::IncludeModule("iblock");

?>
<div class="center">
    <div class="product_detail">
        <div class="product_img fl">
            <?$img=CFile::ResizeImageGet($arResult[DETAIL_PICTURE],array("width" => 315, "height" => 440),BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
            <img src="<?=$img[src]?>"/>
        </div>
        <div class="product_description fr">
            <div class="product_title"><h1><?=$arResult[NAME]?></h1></div>
            <div class="product_text"><?=$arResult[DETAIL_TEXT]?></div>
            <?if(file_exists($_SERVER[DOCUMENT_ROOT]."/.inc/".$arResult[CODE].".php")){?>
            <?$APPLICATION->IncludeComponent(
            	"bitrix:main.include", 
            	".default", 
            	array(
            		"AREA_FILE_SHOW" => "file",
            		"AREA_FILE_SUFFIX" => "inc",
            		"EDIT_TEMPLATE" => "",
            		"PATH" => "/.inc/".$arResult[CODE].".php"
            	),
            	false
            );?>
            <?}?>
            <button class="btn" onclick="form6('<?=$arResult[NAME]?>');"><span>попробовать</span></button>
            <button class="btn" onclick="form7('<?=$arResult[NAME]?>');"><span>демонстрация</span></button>
            <button class="btn" onclick="form8('<?=$arResult[NAME]?>');"><span>консультация</span></button>
        </div>
        <div class="cl"></div>
    </div>
</div>
<div id="tabs">
    <div class="tab_control">
        <div class="center">
            <ul>
                <li><a href="#tab1">о продукте</a></li>
                <li><a href="#tab2">цены</a></li>
                <li><a href="#tab3">клиенты</a></li>
            </ul>
        </div>
    </div>
    <div class="center tab_content">
        <div id="tab1">
            <!-- о продукте -->
                <?foreach($arResult[PROPERTIES][ABOUT_PRODUCT][VALUE] as $k=>$about){
                     $obj_about=CIBlockElement::GetByID($about)->GetNextElement();  
                     $fields=$obj_about->GetFields();
                     $props=$obj_about->GetProperties();
                     $img=CFile::ResizeImageGet($fields[PREVIEW_PICTURE],array("width" => 355, "height" => 239),BX_RESIZE_IMAGE_EXACT , true);
                     ?>
                     <div class="spoiler_box">
                         <?if($k%2){ //изображение слево?>
                            <div class="box580 fl">
                                        <div class="spoiler_title"><?=$fields[NAME]?></div>
                                        <?=$fields[PREVIEW_TEXT]?>
					<?if($props[SHOW_BUTTON][VALUE]){?>
                                        	<div class="test_drive"><button class="btn" onclick="form6('<?=$arResult[NAME]?>');">тест-драйв</button></div>
					<?}?>
                            </div>
                            <div class="box356 fr">
                                <img src="<?=$img[src]?>"/>
                            </div> 
                            
                            
                         <?}else{ //изображение справо?>
                            <div class="box580 fr">
                                        <div class="spoiler_title"><?=$fields[NAME]?></div>
                                        <?=$fields[PREVIEW_TEXT]?>
                                        <?if($props[SHOW_BUTTON][VALUE]){?>
                                        	<div class="test_drive"><button class="btn" onclick="form6('<?=$arResult[NAME]?>');">тест-драйв</button></div>
					<?}?>
                            </div>
                            <div class="box356 fl">
                                <img src="<?=$img[src]?>"/>
                            </div>
                         <?}?>
                         <div class="cl"></div>
                         
                         <div class="spoiler_hide">
                            <div class="spoiler_hide_text"><?=$fields[DETAIL_TEXT]?></div>
                            <ul class="screenshots">
                                <?foreach($props[SCREENS][VALUE] as $screen){
                                    $original=CFile::GetPath($screen);
                                    $img=CFile::ResizeImageGet($screen,array("width" => 234, "height" => 185),BX_RESIZE_IMAGE_EXACT , true);
                                ?>
                                    <li><a href='<?=$original?>' class="fancybox" rel="group_<?=$fields[ID]?>"><img src="<?=$img[src]?>"/></a></li>
                                <?}?>
                            </ul>
                            <div class="cl"></div>
                         </div>
                         <a class="spoler_control <?=($k%2)?"fl":"fr";?>">Подробнее &raquo;</a>
                         <div class="cl"></div>
                     </div>
                     
                     
                  <?}?>
            <!-- /о продукте -->
        </div>
        <div id="tab2">
            <div class="product_cost">
                <!-- цены -->
                    <?=$arResult[PROPERTIES][COST]["~VALUE"][TEXT]?>
                <!-- /цены -->
            </div>
        </div>
        <div id="tab3">
            <?//выборка клиентов
                $res=CIBlockElement::GetList(
                    Array(),
                    Array(
                        "IBLOCK_ID"=>7,
                        "PROPERTY_PRODUCT"=>$arResult[ID],
                        "ACTIVE"=>"Y"),
                    false,
                    false,
                    Array()
                );
                
                while($tmp=$res->GetNext()){
                    $arClient[]=array(
                                    "ID"=>$tmp[ID],
                                    "PREVIEW_PICTURE"=>$tmp[PREVIEW_PICTURE],
                                    "PREVIEW_TEXT"=>$tmp[PREVIEW_TEXT],
                                    "NAME"=>$tmp[NAME]
                    );
                }?>
                <div class="show_previw_client">
                    <?foreach($arClient as $client){
                            $img=CFile::ResizeImageGet($client[PREVIEW_PICTURE],array("width" => 300, "height" => 200),BX_RESIZE_IMAGE_PROPORTIONAL , true);
                        ?>
                        <div class="preview_client">
                            <img class="preview_img" src="<?=$img[src]?>"/>
                            <div class="spoiler_title"><?=$client[NAME]?></div>
                            <?=$client[PREVIEW_TEXT]?>
                            <div class="cl"></div>
                        </div>
                    <?}?>
                    
                </div>
                <ul class="preview_control">
                    <?foreach($arClient as $client){
                        $img=CFile::ResizeImageGet($client[PREVIEW_PICTURE],array("width" => 233, "height" => 130),BX_RESIZE_IMAGE_PROPORTIONAL , true);
                        ?>
                        <li style="background: url(<?=$img[src]?>) no-repeat center center;"></li>
                    <?}?>
                </ul>
                <div class="cl"></div>
        </div>
    </div>            
</div> 
<?$APPLICATION->IncludeComponent(
	"bit:form", 
	".default", 
	array(
		"FORM_NAME" => "webform2",
		"TITLE" => "Бесплатный расчет стоимости проекта",
		"DESCRIPTION" => "В течение 2 часов в рабочее время",
		"BUTTON_NAME" => "отправить заявку",
		"TYPE_CEVENT" => "----",
		"MESSAGE_TRUE" => "----",
		"MESSAGE_FALSE" => "---",
		"SHOW_LINE" => "N"
	),
	false
);?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$arProps=array(
    //"PRODUCT", //Продукт+
    "BRANCH", //Отрасль+
    "EXECUTIVE", //Исполнитель проекта+
    "SPECIFICS", //Специфика+
    "SCALE", //Масштаб деятельности+
    "STAFF", //Штат+
    "CENTRAL_OFFICE", //Центральный офис+
    "BUSINESS_OBJECTIVES", //Бизнес-задачи+
    "AUTO_WORKPLACES", //Автоматизировано рабочих мест+
    "AUTO_STRUCT_UNITS", //Автоматизированные структурные единицы+
    "DURATION", //Продолжительность проекта+
    "CUSTOMER" //  заказчик проекта+
)
?>
<div class="clients_detail_head">
    <div class="clients_detail_head_img fl">
        <?$img=CFile::ResizeImageGet($arResult[PROPERTIES][DETAIL_PICTURE][VALUE],array("width" => 580, "height" => 360),BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
        <img src="<?=$img[src]?>"/>
    </div>
    <div class="clients_detail_head_text fr">
        <div class="clients_detail_head_title"><h1><?=$arResult[NAME]?></h1></div>
        <?=$arResult[PROPERTIES][PREVIEW_TEXT]["~VALUE"][TEXT]?>
    </div>
    <div class="cl"></div>
</div>
<div class="clients_detail_body">
    <div class="clients_detail_body_description fl">
        <div class="clients_detail_body_description_text"><?=$arResult[DETAIL_TEXT]?></div>
        <?if($arResult[PROPERTIES][GOALS_AND_TASKS][VALUE][TEXT]){?>
            <div class="clients_detail_infobox goal_task">
                <div class="clients_detail_infobox_title">ЦЕЛИ И ЗАДАЧИ ПРОЕКТА</div>
                <?=$arResult[PROPERTIES][GOALS_AND_TASKS]["~VALUE"][TEXT]?>
            </div>
        <?}?>
        <?if($arResult[PROPERTIES][DESCRIPTION][VALUE][TEXT]){?>
            <div class="clients_detail_infobox description_project">
                <div class="clients_detail_infobox_title">ОПИСАНИЕ ПРОЕКТА</div>
                <?=$arResult[PROPERTIES][DESCRIPTION]["~VALUE"][TEXT]?>
            </div>
        <?}?>
        <?if($arResult[PROPERTIES][RESULT][VALUE][TEXT]){?>
            <div class="clients_detail_infobox result_project">
                <div class="clients_detail_infobox_title">РЕЗУЛЬТАТЫ ПРОЕКТА</div>
                <?=$arResult[PROPERTIES][RESULT]["~VALUE"][TEXT]?>
            </div>
        <?}?>
    </div>
    <div class="clients_detail_body_detail fr">
        <div class="props_box">  
            <?foreach($arResult[PROPERTIES] as $prop){
                if(in_array($prop[CODE],$arProps) && !empty($prop[VALUE])){?>
                
                <div class="clients_props">
                    <div class="clients_props_title"><?=$prop[NAME]?></div>
                    <?if($prop[PROPERTY_TYPE]=="S") print $prop[VALUE];
		      if($prop[PROPERTY_TYPE]=="L"){
				foreach($prop[VALUE] as $v){
					print $v."<br>";
				}
			}	
                      if($prop[PROPERTY_TYPE]=="E"){
                        $res=CIBlockElement::GetList(
                             Array(),
                             Array("ACTIVE"=>"Y","ID"=>$prop[VALUE]),
                             false,
                             false,
                             Array("NAME","DETAIL_PAGE_URL")
                            );
                        while($tmp=$res->GetNext()){
                            print "<a href='$tmp[DETAIL_PAGE_URL]'>$tmp[NAME]</a> <br>";
                        }
                      }
                      ?>
                </div>
                <?}?>
            <?}?>
        </div>
        <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"client_logos", 
	array(
		"IBLOCK_TYPE" => "IB_INFO",
		"IBLOCK_ID" => "7",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
    </div>
    <div class="cl"></div>
</div>
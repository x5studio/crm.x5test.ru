<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=ru-RU" type="text/javascript"></script>
<?$this->setFrameMode(true);?>

<?//достаем название города в котором находимся
    $section=CIBlockSection::GetList(Array(),Array("IBLOCK_ID"=>$arParams[IBLOCK_ID],"ID"=>$arResult[IBLOCK_SECTION_ID]),false,Array("UF_NAME_DATIVE"),false)->GetNext();
?>

<?$APPLICATION->AddChainItem($section[NAME], $section[SECTION_PAGE_URL]);?>
<?$APPLICATION->AddChainItem($arResult[NAME], $arResult[DETAIl_PAGE_URL]);?>


<div class="center">
    <div class='big_title contacts_select_city'><h1>Офисы в <span><?=$section[UF_NAME_DATIVE]?></span></h1></div>
    <div class='geo_contact'></div>
    <div class='contactbox'>
            <div class='standart_contact'>
                <div class="box fl">
                        <div class='office_item'>
                            <span class="office_name" href='#'><?=$arResult[NAME]?></span>
                            <div class='office_phone'><?=$arResult[PROPERTIES][phone][VALUE]?></div>
                            <div class='office_worktime'>
                            
                                <?foreach($arResult[PROPERTIES][WORK_TIME][VALUE] as $k=>$v){?>
                                    <div><?=$arResult[PROPERTIES][WORK_TIME][DESCRIPTION][$k]?> <?=$v?> </div>
                                    
                                <?}?>
                            
                            
                            
                            </div>
                            <div class='office_adres'><?=$arResult[PROPERTIES][address][VALUE]?></div>
                            <div class='office_email'><a href="mailto:<?=$arResult[PROPERTIES][email][VALUE]?>"><?=$arResult[PROPERTIES][email][VALUE]?></a></div>
                            <br />
                            <?if($arResult[PROPERTIES][hotline][VALUE]){?>
                                <div class='office_consult'>Линия консультаций:</div>
                                <div class="office_phone">
                                    <?=$arResult[PROPERTIES][hotline][VALUE]?>
                                </div>
                            <?}?>
                        </div>
                </div>
                <div class="box fr">
                    <div id="map"></div>
                    <?$APPLICATION->IncludeFile("/.inc/contact_map_header.php");?>
                       
                   		   //*****
                       	    curGeoObject = new ymaps.GeoObject({
                                                        geometry: {type: "Point", coordinates: [<?=$arResult[PROPERTIES][COORDINATES][VALUE]?>]},
                                                		properties: {
                                                			clusterCaption: '<?=$arResult[NAME]?>',
                                                			balloonContentHeader: "<a class='map_office_name' href='/contacts/<?=$arResult[CODE]?>/' style='font-size: 18px;'><?=$arResult[NAME]?></a>"+
                                                                                  "<div class='map_office_phone'><?=$arResult[PROPERTIES][phone][VALUE]?></div>"+
                                                                                  "<div class='map_office_worktime'><?foreach($arResult[PROPERTIES][WORK_TIME][VALUE] as $k=>$v){?><div><?=$arResult[PROPERTIES][WORK_TIME][DESCRIPTION][$k]?> <?=$v?> </div><?}?></div>"+
                                                                                  "<div class='map_office_adres'><?=$arResult[PROPERTIES][address][VALUE]?></div>"+
                                                                                  "<div class='map_office_email'><a href='mailto:<?=$arResult[PROPERTIES][email][VALUE]?>'><?=$arResult[PROPERTIES][email][VALUE]?></a></div>"
                                                                                  <?if($arResult[PROPERTIES][hotline][VALUE]){?>+"<div class='map_office_consult'>Линия консультаций:</div><div class='map_office_phone'><?=$arResult[PROPERTIES][hotline][VALUE]?></div>"<?}?>
                                                            ,
                                                            hintContent: "<?=$arResult[NAME]?>"
                                                		}
                            	               });
                        	myGeoObjects90.push(curGeoObject);
                        	myCollection.add(curGeoObject);
                        	//*****		
                	
                    <?$APPLICATION->IncludeFile("/.inc/contact_map_footer.php");?>  
                    <script>
                        $(document).ready(function(){
                            setTimeout(init,500);
                        });
                    </script>
                </div>
                <div class="cl"></div>
            </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var ini=false;
        $('.contacts_control span').click(function(){
            var q=$(this);
            
            if(!q.hasClass("switch-active") && !q.hasClass('switch_control')){
               if($('.contacts_control .list').hasClass("switch-active")){
                   $('.contacts_control .list').removeClass("switch-active");
                   $('.contacts_control .to_map').addClass("switch-active");
                   $('.contacts_control .switch_control').removeClass("left").addClass("right");
                   $('.contact-map').show();
                   setTimeout(function(){
                    if(!ini){  $("#map").html(''); init(); ini=true;}
                   
                    },500);
                   //window.location.hash="/contact-map";
		   $.cookie('view_contact', 'map', { expires: 7, path: '/' });
               }else{
                   $('.contacts_control .list').addClass("switch-active");
                   $('.contacts_control .to_map').removeClass("switch-active");
                   $('.contacts_control .switch_control').removeClass("right").addClass("left");
                   $('.contact-map').hide();
                   //window.location.hash="/contact-list";
		   $.cookie('view_contact', 'list', { expires: 7, path: '/' });
               }
            } 
        });
         
        //проверка после перезагрузки страницы
        //if(window.location.hash=="#/contact-map") $('.contacts_control .switch').click();
	if($.cookie('view_contact')=="map") $('.contacts_control .switch').click();
        
        
        
        
        
        //-----------------------------
            $(".contacts_select_city span").click(function(){
                var q=$(this);
                $('.geo_contact').html($('.geo_resource').html());
                $('.geo_contact .modal_geo').show();
                $('.geo_contact .overlay_transparent').show();
                
                //вешаем обработчики закрытия
                $('.geo_contact .modal_geo_close,.geo_contact .overlay_transparent').click(function(){
                    
                    $('.geo_contact .modal_geo').remove();
                    $('.geo_contact .overlay_transparent').remove();
                });
            
                //установка кода вместо id
                $.each($('.modal_geo a'),function(){
                   $(this).attr("href","/contacts/"+$(this).attr("code")+"/");
                });
                
            });
        
        //-----------------------------
    });
</script>
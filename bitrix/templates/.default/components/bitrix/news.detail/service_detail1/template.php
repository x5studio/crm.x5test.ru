<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
CModule::IncludeModule("iblock");
?>
<div class="center">
    <div class="product_detail service_detail">
        <div class="product_img fl">
            <?$img=CFile::ResizeImageGet($arResult[DETAIL_PICTURE],array("width" => 315, "height" => 440),BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
            <img src="<?=$img[src]?>"/>
        </div>
        <div class="product_description fr">
            <div class="product_title"><h1><?=$arResult[NAME]?></h1></div>
            <div class="product_text"><?=$arResult[DETAIL_TEXT]?></div>
            <button class="btn" onclick="form8('<?=$arResult[NAME]?>');"><span>консультация</span></button>
        </div>
        <div class="cl"></div>
    </div>
</div>
    <div class="center tab_content service_content">
        <div id="tab1">
            <!-- о продукте -->
                <?foreach($arResult[PROPERTIES][ABOUT_PRODUCT][VALUE] as $k=>$about){
                     $obj_about=CIBlockElement::GetByID($about)->GetNextElement();  
                     $fields=$obj_about->GetFields();
                     $props=$obj_about->GetProperties();
                     $img=CFile::ResizeImageGet($fields[PREVIEW_PICTURE],array("width" => 355, "height" => 239),BX_RESIZE_IMAGE_EXACT , true);
                     ?>
                     <div class="spoiler_box">
                         
                            <div class="">
                                
                                        <?=$fields[PREVIEW_TEXT]?>
                            </div>

                            
                            
                         
                         <div class="cl"></div>
                         
                         <div class="spoiler_hide">
                            <div class="spoiler_hide_text"><?=$fields[DETAIL_TEXT]?></div>
                            <ul class="screenshots">
                                <?foreach($props[SCREENS][VALUE] as $screen){
                                    $original=CFile::GetPath($screen);
                                    $img=CFile::ResizeImageGet($screen,array("width" => 234, "height" => 185),BX_RESIZE_IMAGE_EXACT , true);
                                ?>
                                    <li><a href='<?=$original?>' class="fancybox" rel="group_<?=$fields[ID]?>"><img src="<?=$img[src]?>"/></a></li>
                                <?}?>
                            </ul>
                            <div class="cl"></div>
			    <?if($props[SHOW_BUTTON][VALUE]){?>
                            	<div class="test_drive" style=" display:block; margin-top: 40px;height: 60px;text-align: center;"><button class="btn" onclick="form2('','');">тест-драйв</button></div>
                            <?}?>
			 </div>
                         <a class="spoler_control <?=(!$k%2)?"fl":"fr";?>">Подробнее &raquo;</a>
                         <div class="cl"></div>
                     </div>
                     
                     
                  <?}?>
            <!-- /о продукте -->
        </div>
    </div>            
<?$APPLICATION->IncludeComponent(
	"bit:form", 
	".default", 
	array(
		"FORM_NAME" => "webform2",
		"TITLE" => "Бесплатный расчет стоимости проекта",
		"DESCRIPTION" => "В течение 2 часов в рабочее время",
		"BUTTON_NAME" => "отправить заявку",
		"TYPE_CEVENT" => "----",
		"MESSAGE_TRUE" => "----",
		"MESSAGE_FALSE" => "---",
		"SHOW_LINE" => "N"
	),
	false
);?>
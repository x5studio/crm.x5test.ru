$(document).ready(function(){
    $("#tabs").tabs();
    $(".spoler_control").click(function(){
        var q=$(this);
        var hide_box=q.prev();
        var btn=q.parent().find(".test_drive");
        if(hide_box.css("display")=="none"){
      
            hide_box.slideDown(500,function(){
                q.html("Свернуть &raquo;");
            });
        }else {
  
            hide_box.slideUp(500,function(){
                q.html("Подробнее &raquo;");
            });
        }
    });
    
    
    //продукт вкладка клиенты
    $(".preview_client").first().show().addClass("active");
    $(".preview_control li").first().addClass("active");
    $(".preview_control li").click(function(){
        //убираем превью
        $(".preview_control li").removeClass("active");
        $(".preview_client.active").slideUp(1000).removeClass("active");
        $(this).addClass("active");
        $(".preview_client").eq($(this).index()).slideDown(800).addClass("active");
        
    });
    
    
});
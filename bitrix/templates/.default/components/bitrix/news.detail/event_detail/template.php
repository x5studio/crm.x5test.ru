<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
    <ul class="event_list">
   	    <?$img=CFile::ResizeImageGet($arResult[PROPERTIES][LEADING][VALUE],array("width" => 200, "height" => 150),BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
        	<li class="event_item" id="<?=$this->GetEditAreaId($arResult['ID']);?>">
        		<div class="event_info fl">
                    <div class="event_tip"><?=$arResult[PROPERTIES][EVENT][VALUE]?></div>
		    	<table class="event_place">
				<tr>
					<td>Место:</td>
					<td><div><?=$arResult[PROPERTIES][EVENT_PLACE][VALUE]?></div></td>
				</tr>
			</table>
			<table class="event_dat">
				<tr>
					<td>Дата:</td>
					<td><div><?=date("d.m.Y",strtotime($arResult[PROPERTIES][DATETIME_START][VALUE]))?></div></td>
				</tr>
			</table>
			<table class="event_time">
				<tr>
					<td>Время:</td>
					<td><div><?=date("H:i",strtotime($arResult[PROPERTIES][DATETIME_START][VALUE]))?></div></td>
				</tr>
			</table>
                    
                    
                    
                    <div class="event_liading">
                        <?if($img[src]){?>
                            <img src="<?=$img[src]?>"/>
                            <div><?=$arResult[PROPERTIES][LEADING_NAME][VALUE]?></div>
                            <?=$arResult[PROPERTIES][LEADING_GRADE][VALUE]?>
                        <?}?>
                    </div>
                </div>
                <div class="event_description fr">
                    <div class="event_cost"><?=$arResult[PROPERTIES][TYPE][VALUE]?></div>
                    <div class="event_title"><h1><?=$arResult[NAME]?></h1></div>
                    <div class="event_preview"><?=$arResult[PREVIEW_TEXT]?></div>
                    <button class="btn" onclick="form10('<?=$arResult[NAME]?>')">записаться</button><br />
                </div>
                <div class="cl"></div>
                <div class="event_detail">
                    <div class="event_detail_title">Программа</div>
                    <?=$arResult[DETAIL_TEXT]?></div>
        	</li>
    </ul>
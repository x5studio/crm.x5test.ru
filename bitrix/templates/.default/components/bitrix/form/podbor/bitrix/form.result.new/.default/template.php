<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?/*if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;*/?>
<?=$arResult["FORM_NOTE"]?>
<?if ($arResult["isFormNote"] != "Y"){ //pr($arResult);?>



<script>
    $(document).ready(function(){
        $('input[name=form_text_2]').mask('+7 (999) 999-99-99');
    });
</script>

<div class='podbor_crm'>
    <div class="podbor_crm_headtext"><?=$arResult[arForm][DESCRIPTION]?></div>
    <?=$arResult["FORM_HEADER"]?>
    <input type='hidden' name="web_form_apply" value="Y"/>
        <div class='first_inputs'>
            <ul>
                <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){//pr($arQuestion);
                    if(
                       $FIELD_SID!='SIMPLE_QUESTION_243' &&
                       $FIELD_SID!='SIMPLE_QUESTION_950' &&
                       $FIELD_SID!='SIMPLE_QUESTION_705' &&
                       $FIELD_SID!='SIMPLE_QUESTION_297' &&
                       $FIELD_SID!='SIMPLE_QUESTION_101' &&
                       $FIELD_SID!='SIMPLE_QUESTION_246'
                    ) continue;
                   
                    ?>
                    <li>
                        <?if($arQuestion[STRUCTURE][0][FIELD_TYPE]=='text' || $arQuestion[STRUCTURE][0][FIELD_TYPE]=='email'){?>
                            <?if($arQuestion[REQUIRED]=="Y"){?>
                                <div class="inputs_box">
                                    <div class="error"><?=$arResult[arQuestions][$FIELD_SID][COMMENTS]?></div>
                                    <input placeholder="<?=$arQuestion[CAPTION]?> <?=($arQuestion[REQUIRED]=="Y")?"*":"";?>" type="text" class="inputtext <?=$FIELD_SID?>" name="form_text_<?=$arQuestion[STRUCTURE][0][ID]?>" value="<?=$arResult[arrVALUES]["form_text_{$arQuestion[STRUCTURE][0][ID]}"]?>">
                                </div>
                            <?}else{?>
                                <input placeholder="<?=$arQuestion[CAPTION]?> <?=($arQuestion[REQUIRED]=="Y")?"*":"";?>" type="text" class="inputtext" name="form_text_<?=$arQuestion[STRUCTURE][0][ID]?>" value="<?=$arResult[arrVALUES]["form_text_{$arQuestion[STRUCTURE][0][ID]}"]?>">
                            <?}?>
                        <?}else{?>
                            <select placeholder='<?=$arQuestion[CAPTION]?>' class="inputselect" name="form_dropdown_<?=$FIELD_SID?>" id="form_dropdown_<?=$FIELD_SID?>">
                                <option value="">Выберите отрасль</option>
                                <?foreach($arQuestion[STRUCTURE] as $inp){?>
                                    <option value="<?=$inp[ID]?>"><?=$inp[MESSAGE]?></option>
                                <?}?>
                            </select>
                        <?}?>
                   </li>
            	<?}?>
            </ul>
            <div class="cl"></div> 
        </div>
                <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
                        if($FIELD_SID!='SIMPLE_QUESTION_309') continue;
                        ?>
                        <div class="second_inputs">
                            <div class="second_inputs_title"><?=$arQuestion[CAPTION]?></div>
                            <ul>
                                <?foreach($arQuestion[STRUCTURE] as $inp){
                                   
                                    if(in_array($inp[ID],$arResult[arrVALUES]["form_checkbox_$FIELD_SID"])) $check='checked'; else $check='';                                    
                                    ?>
                                    <li>
                                        <input <?=$check?> type="checkbox" id="<?=$inp[ID]?>" name="form_checkbox_<?=$FIELD_SID?>[]" value="<?=$inp[ID]?>" />
                                        <label for="<?=$inp[ID]?>"><?=$inp[MESSAGE]?></label>
                                    </li>
                                <?}?>
                            </ul>
                            <div class="cl"></div>
                        </div>
                <?}?>
            
                <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
                        if($FIELD_SID!='SIMPLE_QUESTION_259') continue;
                        ?>
                        <div class="third_inputs">
                            <div class="third_inputs_title"><?=$arQuestion[CAPTION]?></div>
                            <ul>
                                <?foreach($arQuestion[STRUCTURE] as $inp){
                                    if(in_array($inp[ID],$arResult[arrVALUES]["form_checkbox_$FIELD_SID"])) $check='checked'; else $check='';
                                    ?>
                                    <li>
                                        <input <?=$check?> type="checkbox" id="<?=$inp[ID]?>" name="form_checkbox_<?=$FIELD_SID?>[]" value="<?=$inp[ID]?>" />
                                        <label for="<?=$inp[ID]?>"><?=$inp[MESSAGE]?></label>
                                    </li>
                                <?}?>
                            </ul>
                            <div class="cl"></div>
                        </div>
                <?}?>
                <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
                        if($FIELD_SID!='SIMPLE_QUESTION_488') continue;
                        ?>
                        <div class="fourth_inputs">
                            <div class="fourth_inputs_title"><?=$arQuestion[CAPTION]?></div>
                            <ul>
                                <?foreach($arQuestion[STRUCTURE] as $inp){
                                    if(in_array($inp[ID],$arResult[arrVALUES]["form_checkbox_$FIELD_SID"])) $check='checked'; else $check='';
                                    ?>
                                    <li>
                                        <input <?=$check?> type="checkbox" id="<?=$inp[ID]?>" name="form_checkbox_<?=$FIELD_SID?>[]" value="<?=$inp[ID]?>" />
                                        <label for="<?=$inp[ID]?>"><?=$inp[MESSAGE]?></label>
                                    </li>
                                <?}?>
                            </ul>
                            <div class="cl"></div>
                        </div>
                <?}?>
                <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
                        if($FIELD_SID!='SIMPLE_QUESTION_518') continue;
                        ?>
                        <div class="fifth_inputs">
                            <table>
                                <tr>
                                    <td><div class="fifth_inputs_title"><?=$arQuestion[CAPTION]?></div></td>
                                    <td>
                                        <input placeholder="" type="text" class="inputtext" name="form_text_<?=$arQuestion[STRUCTURE][0][ID]?>" value="<?=$arResult[arrVALUES]["form_text_{$arQuestion[STRUCTURE][0][ID]}"]?>">
                                    </td>
                                </tr>
                            </table>
                            <div class="cl"></div>
                        </div>
                <?}?>
                <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
                        if($FIELD_SID!='SIMPLE_QUESTION_786') continue;
                        ?>
                        <div class="sixth_inputs">
                            <table>
                                <tr>
                                    <td><div class="sixth_inputs_title"><?=$arQuestion[CAPTION]?></div></td>
                                    <td>
                                        <input placeholder="" type="text" class="inputtext" name="form_text_<?=$arQuestion[STRUCTURE][0][ID]?>" value="<?=$arResult[arrVALUES]["form_text_{$arQuestion[STRUCTURE][0][ID]}"]?>">
                                        <div>руб.</div>
                                    </td>
                                </tr>
                            </table>
                            <div class="cl"></div>
                        </div>
                <?}?>
                <?foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
                        if($FIELD_SID!='SIMPLE_QUESTION_568') continue;
                        ?>
                        <div class="seventh_inputs">
                            <textarea placeholder='<?=$arQuestion[CAPTION]?>' name="form_textarea_<?=$arQuestion[STRUCTURE][0][ID]?>"  class="inputtextarea"><?=$arResult[arrVALUES]["form_textarea_{$arQuestion[STRUCTURE][0][ID]}"]?></textarea>
                            <div class="cl"></div>
                        </div>
                <?}?>
    
    
    <input class="btn" type="submit" name="web_form_submit" value="<?=$arResult["arForm"]["BUTTON"];?>" /><br>
	<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/privacy_policy.php"),Array(),Array("MODE"=>"html"));?>
    <?=$arResult["FORM_FOOTER"]?>
</div>    


<script>
    $(document).ready(function(){
        $.each($('.podbor_crm .error'),function(){
                //навесим обработчик на клик
                $(this).next().click(function(){
                    $(this).removeClass("error_input").prev().hide();
                });
            });
        $('.podbor_crm form').submit(function(){
            var nam=$('.SIMPLE_QUESTION_243');
            var tel=$('.SIMPLE_QUESTION_950');
            var email=$('.SIMPLE_QUESTION_705');
            
            //пробежимся поошибкам и поправим вылет
            $.each($('.podbor_crm .error'),function(){
                var w=$(this).width();
                w=w+38;
                $(this).css("margin-left","-"+w+"px");
                
                //сразу навесим обработчик на клик
                $(this).next().click(function(){
                    
                });
            });
            
            var send=true;
            
            if(nam.val()==''){
                nam.addClass('error_input');
                nam.parent().find('.error').show();
                send=false;
            }
            if(tel.val()==''){
                tel.addClass('error_input');
                tel.parent().find('.error').show();
                send=false;
            }
            if(email.val()=='' || !validateEmail(email.val())){
                email.addClass('error_input');
                email.parent().find('.error').show();
                send=false;
            }
            
            
            
            if(send){
                $.post($(this).attr('action')+"?action=edit",$(this).serialize(),function(dd){
                   endform('Спасибо! Ваша заявка принята! Специалист свяжется с Вами в течении 15 минут.');
                });
            }else{
                $('body').animate({
                    scrollTop:"200px"
                },500);
            }
            return false;
        }); 
    });
</script>




      
<?}?>
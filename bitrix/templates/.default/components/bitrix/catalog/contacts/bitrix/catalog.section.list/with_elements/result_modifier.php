<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult["ELEMENTS"] = Array();
$arSelect = Array(
	"ID", "NAME", "SORT", "CODE"
);
foreach($arParams["LIST_PROPERTY_CODE"] AS $CODE){
	array_push($arSelect, "PROPERTY_".$CODE);
}
foreach(array_merge(Array($arResult["SECTION"]), $arResult["SECTIONS"]) AS $keySection => $arSection){
	if($arSection["ELEMENT_CNT"] == 1){
		$arFilter = Array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arSection["ID"],
			"ACTIVE" => "Y"
		);
		if($arParams["USE_FILTER"] == "Y"){
			global $$arParams["FILTER_NAME"];
			$arFilter = array_merge($arFilter, $$arParams["FILTER_NAME"] ?: Array());
		}
		$rsElements = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
		while($arElements = $rsElements->GetNext()){
			$arCoordinates = explode(",", $arElements["PROPERTY_COORDINATES_VALUE"]);
			$arElements["PROPERTY_COORDINATES_LEFT"] = (float) $arCoordinates[0];
			$arElements["PROPERTY_COORDINATES_RIGHT"] = (float) $arCoordinates[1];
			$arElements["PROPERTY_DRIVE"] = CFile::GetByID($arElements["PROPERTY_DRIVE_VALUE"])->Fetch();
			//$arElements["PROPERTY_DRIVE"]["PATH"] = "/upload/".$arElements["PROPERTY_DRIVE"]["SUBDIR"]."/".$arElements["PROPERTY_DRIVE"]["FILE_NAME"];
			$arElements["PROPERTY_WALK"] = CFile::GetByID($arElements["PROPERTY_WALK_VALUE"])->Fetch();
			//$arElements["PROPERTY_WALK"]["PATH"] = "/upload/".$arElements["PROPERTY_WALK"]["SUBDIR"]."/".$arElements["PROPERTY_WALK"]["FILE_NAME"];
			$arElements["DETAIL_URL"] = CComponentEngine::MakePathFromTemplate($arParams["DETAIL_URL"], Array("SECTION_CODE" => $arSection["CODE"], "ELEMENT_CODE" =>$arElements["CODE"] ));
			$arResult["ELEMENTS"][$arSection["ID"]][] = $arElements;
		}
	}
}

//$this->__component->__parent->arResult["SECTION_LIST_RESULT"] = $arResult;
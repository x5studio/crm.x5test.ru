<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arGroups = Array();
foreach($arResult['ITEMS'] AS $keyItem => $arItem){
	$arGroups[$arItem['DISPLAY_PROPERTIES']['GROUP']['VALUE'] ?: '~NO'][] = $arItem;
}
$arResult['ITEMS'] = Array();
foreach($arGroups AS $itemGroup){
	$arResult['ITEMS'] = array_merge($arResult['ITEMS'], $itemGroup);
}
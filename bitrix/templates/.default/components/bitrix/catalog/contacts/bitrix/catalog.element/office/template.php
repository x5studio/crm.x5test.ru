<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<a href="?PRINT=Y" class="print j-print contact-print"><span class="dotted">Распечатать</span></a>

<div class="clearfix content-region" itemscope itemtype="http://schema.org/LocalBusiness">
	<div class="col-l-contact">
		<meta itemprop="legalName" content="Первый Бит" />
		<meta itemprop="name" content="<?=$arResult["NAME"]?>" />
		<ol class="contact-list">
			<li>
				<div class="width100">
				<?
				$nameSeparator = " - ";
				$arName = explode($nameSeparator, $arResult["NAME"]);
				if(count($arName) > 1):?>
					<strong><?=array_shift($arName)?></strong>
				<?endif?>
					<div<?if(!empty($arResult["PROPERTIES"]["ICON_METRO"]["VALUE"])){ echo ' class="metro metro-'.$arResult["PROPERTIES"]["ICON_METRO"]["VALUE"].'"';}?>><?=implode($nameSeparator, $arName)?></div>
				</div>
				<?if(!empty($arResult["DISPLAY_PROPERTIES"]["phone"]["VALUE"])):?>
				<div class="width100"><div class="phone <?=($arResult["CODE"]=="chelyabinsk")?'call_phone_1':''?>" itemprop="telephone"><?=$arResult["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]?></div></div>
				<?endif?>
				<?if(!empty($arResult["DISPLAY_PROPERTIES"]["hotline"]["VALUE"])):?>
					<div class="width100"><span style="margin-left: 20px; margin-bottom: -7px;">Линия консультаций:</span><div class="phone"><?=$arResult["DISPLAY_PROPERTIES"]["hotline"]["DISPLAY_VALUE"]?></div></div>
				<?endif?>
				<div class="phone"><span>Круглосуточно</span></div>
				<?if(!empty($arResult["DISPLAY_PROPERTIES"]["ADD_PHONE"]["VALUE"])):?>
					<div class="width100">
						<?if(!empty($arResult["DISPLAY_PROPERTIES"]["ADD_PHONE_NAME"]["VALUE"])):?>
							<span style="margin-left: 20px; margin-bottom: -7px;"><?=$arResult["DISPLAY_PROPERTIES"]["ADD_PHONE_NAME"]["VALUE"]?></span>
						<?endif?>
						<div class="phone"><?=$arResult["DISPLAY_PROPERTIES"]["ADD_PHONE"]["DISPLAY_VALUE"]?></div>
					</div>
				<?endif?>
				<?if(!empty($arResult["DISPLAY_PROPERTIES"]["email"]["VALUE"])):?>
					<div class="width100"><div class="mail" itemprop="email"><a href="mailto:<?=$arResult["DISPLAY_PROPERTIES"]["email"]["DISPLAY_VALUE"]?>"><?=$arResult["DISPLAY_PROPERTIES"]["email"]["DISPLAY_VALUE"]?></a></div></div>
				<?endif?>
				<div class="width100">
					<?if(!empty($arResult["DISPLAY_PROPERTIES"]["address"]["VALUE"])):?>
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<meta itemprop="addressCountry" content="<?=$arResult["SECTION"]["PATH"][0]["NAME"]?>" />
							<meta itemprop="addressRegion" content="<?=$arResult["SECTION"]["PATH"][1]["NAME"]?>" />
							<div class="place" itemprop="streetAddress"><?=$arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]?></div>
						</div>
					<?endif?>
				</div>

				<?if(!empty($arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["VALUE"])):?>
					<div>
						<div class="time">
							<?if(is_array($arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DISPLAY_VALUE"])):?>
								<?foreach($arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DISPLAY_VALUE"] AS $keyWT => $wtItem):?>
									<div><?if($arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DESCRIPTION"][$keyWT]):?><?=$arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DESCRIPTION"][$keyWT]?>: <?endif?><?=$wtItem?></div>
								<?endforeach?>
							<?else:?>
								<?if($arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DESCRIPTION"]):?>
									<?=$arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DESCRIPTION"][0]?>
								<?endif?>
								<?=$arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DISPLAY_VALUE"]?>
							<?endif?>
						</div>
					</div>
				<?endif?>
				<div class="width100">
					<?if(!empty($arResult["DISPLAY_PROPERTIES"]["DRIVE"]["VALUE"])):?>
						<div><div class="place"><a href="<?=$arResult["DISPLAY_PROPERTIES"]["DRIVE"]["FILE_VALUE"]["SRC"]?>" class="show-way">Как проехать</a></div></div>
					<?endif?>
					<?if(!empty($arResult["DISPLAY_PROPERTIES"]["WALK"]["VALUE"])):?>
						<div><div class="place"><a href="<?=$arResult["DISPLAY_PROPERTIES"]["WALK"]["FILE_VALUE"]["SRC"]?>" class="show-way">Как пройти</a></div></div>
					<?endif?>
				</div>
				<?if(!empty($arResult["PREVIEW_TEXT"]) || !empty($arResult["DETAIL_TEXT"])):?>
				<div class="contact-transport">
				<?if(!empty($arResult["PREVIEW_TEXT"])):?>
					<div class="preview"><?=$arResult["PREVIEW_TEXT"]?></div>
				<?endif?>
				<?if(!empty($arResult["DETAIL_TEXT"])):?>
					<div class="detail"><?=$arResult["DETAIL_TEXT"]?></div>
				<?endif?>
				</div>
				<?endif?>
			</li>
		</ol>
	</div>
	<div class="col-r-contact">
		<div class="map-content">
			<div id="contacts-map-top"></div>
		</div>
	</div>
</div>
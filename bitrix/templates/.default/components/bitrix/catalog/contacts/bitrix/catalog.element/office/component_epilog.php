<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$PARENT = $this->__parent;
if ($PARENT->arParams["SET_TITLE"] && !empty($arResult["SECTION"]["NAME"])){
	$APPLICATION->SetTitle($arResult["SECTION"]["NAME"]." - ".$arResult["NAME"]);
}

$APPLICATION->AddChainItem($arResult["SECTION_NAME"],$arResult["SECTION_URL"]);
$APPLICATION->AddChainItem($arResult["NAME"]);

?>
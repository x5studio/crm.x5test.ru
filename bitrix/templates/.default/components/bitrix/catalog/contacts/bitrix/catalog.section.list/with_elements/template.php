<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$PARENT = $this->__component->__parent;?>
<?
if($PARENT->arParams["SHOW_OFFICE_IN_LIST_IF_HE_ONE"] != "Y" || $arResult["SECTIONS_COUNT"] > 0 || $arResult["SECTION"]["ELEMENT_CNT"] > 0 )
{
	if($arParams['USE_FILTER'] == 'Y' && !empty($arParams['FILTER_NAME']) )
	{
		global $$arParams['FILTER_NAME'];
		$arFilter = &$$arParams['FILTER_NAME'];
		$arFilter['PROPERTY_HIDE_IN_CITY_LIST'] = 'N';
	}
}
?>
<?if($arResult["SECTIONS_COUNT"] === 0 && $arResult["SECTION"]["ELEMENT_CNT"] === "0" ):?>
	<p class="more-info">В <?=$arResult["SECTION"]["UF_NAME_DATIVE"]?:$arResult["SECTION"]["NAME"]?> офисы не обнаружены.</p>
<?elseif($PARENT->arParams["SHOW_OFFICE_IN_LIST_IF_HE_ONE"] == "Y" && $arResult["SECTIONS_COUNT"] === 0 && $arResult["SECTION"]["ELEMENT_CNT"] === "1" ):?>
	<?$ElementID=$APPLICATION->IncludeComponent(
		"bitrix:catalog.element",
		"office",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
			"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
			"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
			"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SET_TITLE" => "N",
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
			"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
			"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
			"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],

			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],

			"ELEMENT_ID" => $arResult["ELEMENTS"][$arResult["SECTION"]["ID"]][0]["ID"],
			"ELEMENT_CODE" => $arResult["ELEMENTS"][$arResult["SECTION"]["ID"]][0]["CODE"],
			"SECTION_ID" => $arResult["SECTION"]["ID"],
			"SECTION_CODE" => $arResult["SECTION"]["CODE"],
			"SECTION_URL" => $arParams["SECTION_URL"],
			"DETAIL_URL" => $arParams["DETAIL_URL"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
            "ADD_SECTIONS_CHAIN" => "N",
		),
		$PARENT
	);?>
<?elseif($arResult["SECTIONS_COUNT"] === 0 && $arResult["SECTION"]["ELEMENT_CNT"] > 0 ):?>
	<?
	if($PARENT->arParams["DETAIL_BLANK_SECTION"] == "Y"){
		$DETAIL_URL = ($arResult["SECTION"]["ELEMENT_CNT"] != "1") ? $PARENT->arResult["FOLDER"].$PARENT->arResult["URL_TEMPLATES"]["element"] : $PARENT->arResult["FOLDER"].$PARENT->arResult["URL_TEMPLATES"]["section_blank"];
	} else {
		$DETAIL_URL = ($arResult["SECTION"]["ELEMENT_CNT"] != "1") ? $PARENT->arResult["FOLDER"].$PARENT->arResult["URL_TEMPLATES"]["element"] : $PARENT->arResult["FOLDER"].$PARENT->arResult["URL_TEMPLATES"]["section"];
	}
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"offices-list",
		Array(
			"IBLOCK_TYPE" => $PARENT->arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $PARENT->arParams["IBLOCK_ID"],
			"ELEMENT_SORT_FIELD" => $PARENT->arParams["ELEMENT_SORT_FIELD"],
			"ELEMENT_SORT_ORDER" => $PARENT->arParams["ELEMENT_SORT_ORDER"],
			"PROPERTY_CODE" => $PARENT->arParams["LIST_PROPERTY_CODE"],
			"SECTION_USER_FIELDS" => $PARENT->arParams["SECTION_USER_FIELDS"],
			"META_KEYWORDS" => $PARENT->arParams["LIST_META_KEYWORDS"],
			"META_DESCRIPTION" => $PARENT->arParams["LIST_META_DESCRIPTION"],
			"BROWSER_TITLE" => $PARENT->arParams["LIST_BROWSER_TITLE"],
			"INCLUDE_SUBSECTIONS" => $PARENT->arParams["INCLUDE_SUBSECTIONS"],
			"BASKET_URL" => $PARENT->arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $PARENT->arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $PARENT->arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $PARENT->arParams["SECTION_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $PARENT->arParams["PRODUCT_QUANTITY_VARIABLE"],
			"FILTER_NAME" => $PARENT->arParams["FILTER_NAME"],
			"CACHE_TYPE" => $PARENT->arParams["CACHE_TYPE"],
			"CACHE_TIME" => $PARENT->arParams["CACHE_TIME"],
			"CACHE_FILTER" => $PARENT->arParams["CACHE_FILTER"],
			"CACHE_GROUPS" => $PARENT->arParams["CACHE_GROUPS"],
			"SET_TITLE" => "N",
			"SET_STATUS_404" => $PARENT->arParams["SET_STATUS_404"],
			"DISPLAY_COMPARE" => $PARENT->arParams["USE_COMPARE"],
			"PAGE_ELEMENT_COUNT" => $PARENT->arParams["PAGE_ELEMENT_COUNT"],
			"LINE_ELEMENT_COUNT" => $PARENT->arParams["LINE_ELEMENT_COUNT"],
			"PRICE_CODE" => $PARENT->arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $PARENT->arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $PARENT->arParams["SHOW_PRICE_COUNT"],

			"PRICE_VAT_INCLUDE" => $PARENT->arParams["PRICE_VAT_INCLUDE"],
			"USE_PRODUCT_QUANTITY" => $PARENT->arParams['USE_PRODUCT_QUANTITY'],

			"DISPLAY_TOP_PAGER" => $PARENT->arParams["DISPLAY_TOP_PAGER"],
			"DISPLAY_BOTTOM_PAGER" => $PARENT->arParams["DISPLAY_BOTTOM_PAGER"],
			"PAGER_TITLE" => $PARENT->arParams["PAGER_TITLE"],
			"PAGER_SHOW_ALWAYS" => $PARENT->arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_TEMPLATE" => $PARENT->arParams["PAGER_TEMPLATE"],
			"PAGER_DESC_NUMBERING" => $PARENT->arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME" => $PARENT->arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $PARENT->arParams["PAGER_SHOW_ALL"],

			"OFFERS_CART_PROPERTIES" => $PARENT->arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $PARENT->arParams["LIST_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $PARENT->arParams["LIST_OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $PARENT->arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $PARENT->arParams["OFFERS_SORT_ORDER"],
			"OFFERS_LIMIT" => $PARENT->arParams["LIST_OFFERS_LIMIT"],

			"SECTION_ID" => $arResult["SECTION"]['ID'],
			"SECTION_CODE" => $arResult["SECTION"]["SECTION_CODE"],
			"SECTION_URL" => $PARENT->arResult["FOLDER"].$PARENT->arResult["URL_TEMPLATES"]["section"],
			"DETAIL_URL" => $DETAIL_URL,
			'CONVERT_CURRENCY' => $PARENT->arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $PARENT->arParams['CURRENCY_ID'],
			'LIMIT_ELEMENTS_LEFT_COLUMN' => $PARENT->arParams['LIMIT_ELEMENTS_LEFT_COLUMN'],
			'SHOW_CITY_OFFICES_HOURS' => $PARENT->arParams['SHOW_CITY_OFFICES_HOURS'],
			'SHOW_CITY_MAPS' => $PARENT->arParams['SHOW_CITY_MAPS'],
			'OFFICE_LIST_DATA_TABLE' => $PARENT->arParams['OFFICE_LIST_DATA_TABLE'],
            "ADD_SECTIONS_CHAIN" => "Y",
		),
		$PARENT
	);?>
<?else:?>

<div id="contacts-map-top" style="width:100%; height:500px"></div>
<div class="catalog-section-list">
<?
$TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
$CURRENT_DEPTH = $TOP_DEPTH;

$SECTIONS = !empty($arResult["SECTIONS"]) ? $arResult["SECTIONS"] : Array($arResult["SECTION"]);
foreach($SECTIONS as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
	if($CURRENT_DEPTH < $arSection["DEPTH_LEVEL"])
		echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH),"<ul>";
	elseif($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"])
		echo "</li>";
	else
	{
		while($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"])
		{
			echo "</li>";
			echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
			$CURRENT_DEPTH--;
		}
		echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</li>";
	}

	echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH);
	?><li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><h<?=$CURRENT_DEPTH+1?>><?=$arSection["NAME"]?><?if($arParams["COUNT_ELEMENTS"]):?>&nbsp;(<?=$arSection["ELEMENT_CNT"]?>)<?endif;?></h<?=$CURRENT_DEPTH+1?>></a>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"",
	Array(
		"IBLOCK_TYPE" => $PARENT->arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $PARENT->arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => $PARENT->arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $PARENT->arParams["ELEMENT_SORT_ORDER"],
		"PROPERTY_CODE" => $PARENT->arParams["LIST_PROPERTY_CODE"],
		"META_KEYWORDS" => $PARENT->arParams["LIST_META_KEYWORDS"],
		"META_DESCRIPTION" => $PARENT->arParams["LIST_META_DESCRIPTION"],
		"BROWSER_TITLE" => $PARENT->arParams["LIST_BROWSER_TITLE"],
		"INCLUDE_SUBSECTIONS" => $PARENT->arParams["INCLUDE_SUBSECTIONS"],
		"BASKET_URL" => $PARENT->arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $PARENT->arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $PARENT->arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $PARENT->arParams["SECTION_ID_VARIABLE"],
		"PRODUCT_QUANTITY_VARIABLE" => $PARENT->arParams["PRODUCT_QUANTITY_VARIABLE"],
		"FILTER_NAME" => $PARENT->arParams["FILTER_NAME"],
		"CACHE_TYPE" => $PARENT->arParams["CACHE_TYPE"],
		"CACHE_TIME" => $PARENT->arParams["CACHE_TIME"],
		"CACHE_FILTER" => $PARENT->arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $PARENT->arParams["CACHE_GROUPS"],
		"SET_TITLE" => "N",
		"SET_STATUS_404" => $PARENT->arParams["SET_STATUS_404"],
		"DISPLAY_COMPARE" => $PARENT->arParams["USE_COMPARE"],
		"PAGE_ELEMENT_COUNT" => $PARENT->arParams["PAGE_ELEMENT_COUNT"],
		"LINE_ELEMENT_COUNT" => $PARENT->arParams["LINE_ELEMENT_COUNT"],
		"PRICE_CODE" => $PARENT->arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $PARENT->arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $PARENT->arParams["SHOW_PRICE_COUNT"],

		"PRICE_VAT_INCLUDE" => $PARENT->arParams["PRICE_VAT_INCLUDE"],
		"USE_PRODUCT_QUANTITY" => $PARENT->arParams['USE_PRODUCT_QUANTITY'],

		"DISPLAY_TOP_PAGER" => $PARENT->arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $PARENT->arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $PARENT->arParams["PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => $PARENT->arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_TEMPLATE" => $PARENT->arParams["PAGER_TEMPLATE"],
		"PAGER_DESC_NUMBERING" => $PARENT->arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $PARENT->arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $PARENT->arParams["PAGER_SHOW_ALL"],

		"OFFERS_CART_PROPERTIES" => $PARENT->arParams["OFFERS_CART_PROPERTIES"],
		"OFFERS_FIELD_CODE" => $PARENT->arParams["LIST_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $PARENT->arParams["LIST_OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $PARENT->arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $PARENT->arParams["OFFERS_SORT_ORDER"],
		"OFFERS_LIMIT" => $PARENT->arParams["LIST_OFFERS_LIMIT"],

		"SECTION_ID" => $arSection['ID'],
		"SECTION_CODE" => $arSection["SECTION_CODE"],
		"SECTION_URL" => $PARENT->arResult["FOLDER"].$PARENT->arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => ($arSection["ELEMENT_CNT"] !== "1") ? $PARENT->arResult["FOLDER"].$PARENT->arResult["URL_TEMPLATES"]["element"] : $PARENT->arResult["FOLDER"].$PARENT->arResult["URL_TEMPLATES"]["section"],
		'CONVERT_CURRENCY' => $PARENT->arParams['CONVERT_CURRENCY'],
		'CURRENCY_ID' => $PARENT->arParams['CURRENCY_ID'],
	),
	$PARENT
);?>
	<?

	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
endforeach;

while($CURRENT_DEPTH > $TOP_DEPTH)
{
	echo "</li>";
	echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
	$CURRENT_DEPTH--;
}
?>
</div>
<?endif?>
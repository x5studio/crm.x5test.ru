<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/*
$arResult["ELEMENTS"] = Array();
foreach($arResult["SECTIONS"] AS $keySection => $arSection){
	$rsElements = CIBlockElement::GetList(Array('SORT' => 'ASC'), Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arSection["ID"],
		"ACTIVE" => "Y"
	), false, false, Array(
		"ID", "NAME", "SORT", "PROPERTY_phone", "PROPERTY_address", "PROPERTY_email", "PROPERTY_DRIVE", "PROPERTY_WALK", "PROPERTY_COORDINATES", "PROPERTY_COORDINATES_CENTER", "PROPERTY_ZOOM"
	));
	while($arElements = $rsElements->GetNext()){
		$arElements["PROPERTY_DRIVE"] = CFile::GetByID($arElements["PROPERTY_DRIVE_VALUE"])->Fetch();
		//$arElements["PROPERTY_DRIVE"]["PATH"] = "/upload/".$arElements["PROPERTY_DRIVE"]["SUBDIR"]."/".$arElements["PROPERTY_DRIVE"]["FILE_NAME"];
		$arElements["PROPERTY_WALK"] = CFile::GetByID($arElements["PROPERTY_WALK_VALUE"])->Fetch();
		//$arElements["PROPERTY_WALK"]["PATH"] = "/upload/".$arElements["PROPERTY_WALK"]["SUBDIR"]."/".$arElements["PROPERTY_WALK"]["FILE_NAME"];
		$arResult["ELEMENTS"][$arSection["ID"]][] = $arElements;
	}
}

$this->__component->__parent->arResult["SECTION_LIST_RESULT"] = $arResult;
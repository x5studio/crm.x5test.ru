<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$section = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"])->GetNext();
$arResult["SECTION_NAME"] = $section["NAME"];
$arResult["SECTION_URL"] = $section["SECTION_PAGE_URL"];

global $APPLICATION;
$cp = $this->__component;
$cp->SetResultCacheKeys(array('SECTION_NAME','SECTION_URL'));

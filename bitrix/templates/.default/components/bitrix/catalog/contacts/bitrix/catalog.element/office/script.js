$(function() {
	var links = $('.place').find('.show-way');
	links.click(function(e) {
		e.preventDefault();
		var src = $(this).attr('href');
		$.fancybox.open($(this), {
			afterLoad: function(current) {
				current.wrap.append('<button class="road-print btn btn-mini">Распечатать</button>');
				$('.road-print').click(function() {
					$('body').html('<img src="' + src + '">');
					window.print();
				})
			}
		});
	});
});
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if($_SESSION["REGION"]["CODE"]=="chelyabinsk") {
	$call_phone = "call_phone_1";
}
if(empty($arResult["ITEMS"])) return;?>
<?$ELEMENT_CNT = count($arResult["ITEMS"]);?>
<?if($ELEMENT_CNT > $arParams["LIMIT_ELEMENTS_LEFT_COLUMN"]):?>
	<?if($arParams["SHOW_CITY_MAPS"] == "Y" || $arParams["SHOW_CITY_OFFICES_HOURS"] == "Y"):?>
	<div class="clearfix" style="position: relative">
		<div class="left">
			<?if($arParams["SHOW_CITY_MAPS"] == "Y"):?>
			<div class="contact-switch j-switch2">
				<a href="#contact-list" class="switch-active">Списком</a>
				<div class="switch3"><div class="switch-handle"></div></div>
				<a href="#contact-map">На карте</a>
			</div>
			<?endif?>
		</div>
		<div class="right">
		<?if($arParams["SHOW_CITY_OFFICES_HOURS"] == "Y" && $arResult["UF_OFFICES_HOURS"]):?>
			<div class="contact-office-schedule">
				<strong>Время работы</strong>
				<?=$arResult["~UF_OFFICES_HOURS"]?>
			</div>
		<?endif?>
		</div>
	</div>
	<div id="contact-list"></div>
	<div id="contact-map" class="map-hidden">
		<div class="map-content">
			<div id="contacts-map-top"></div>
		</div>
	</div>
	<?endif?>
<?else:?>
	<div class="clearfix" style="position: relative">
		<div class="right">
			<?if($arParams["SHOW_CITY_OFFICES_HOURS"] == "Y" && $arResult["UF_OFFICES_HOURS"]):?>
				<div class="contact-office-schedule" style="top: -50px">
					<strong>Время работы офисов</strong>
					<?=$arResult["~UF_OFFICES_HOURS"]?>
				</div>
			<?endif?>
		</div>
	</div>
<div class="clearfix content-region">
	<div class="col-l-contact">
<?endif?>
	<?/*<ol class="contact-list">*/?>
	<?$GROUP_NAME = false;
	$openList = false;?>
	<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>
		<?if(!$openList || $GROUP_NAME != $arElement['DISPLAY_PROPERTIES']['GROUP']['VALUE']):?>
			<?if($openList):?>
				</ol>
			<?endif?>
			<?if(!empty($arElement['DISPLAY_PROPERTIES']['GROUP']['VALUE'])):?>
				<br>
				<h2><?=$arElement['DISPLAY_PROPERTIES']['GROUP']['VALUE']?></h2>
<br>
			<?endif?>
			<ol class="contact-list">
			<?$openList = true;
			$GROUP_NAME = $arElement['DISPLAY_PROPERTIES']['GROUP']['VALUE'];?>
		<?endif?>
		<li id="<?=$arElement['ID']?>">
			<?foreach($arParams["OFFICE_LIST_DATA_TABLE"] AS $keyColumSettings => $columSettings):?>
				<div class="contact-item <?if(!empty($columSettings["CLASS"])) echo $columSettings["CLASS"]?>">
					<?foreach($columSettings["ITEMS"] AS $itemColumn):?>
						<?switch($itemColumn):?><?
							case "NAME": ?>
								<?
								$nameSeparator = " - ";
								$arName = explode($nameSeparator, $arElement["NAME"]);
								if(count($arName) > 1):?>
									<strong><?=array_shift($arName)?></strong>
								<?endif?>
								<div<?if(!empty($arElement["PROPERTIES"]["ICON_METRO"]["VALUE"])){ echo ' class="metro metro-'.$arElement["PROPERTIES"]["ICON_METRO"]["VALUE"].'"';}?>><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=implode($nameSeparator, $arName)?></a></div>
							<?break; case "phone":?>
								<div class="phone <?=($cell==0)?$call_phone:''?>"><?=$arElement["DISPLAY_PROPERTIES"]["phone"]["DISPLAY_VALUE"]?></div>
							<?break; case "address":?>
								<div class="place"><?=$arElement["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]?></div>
							<?break; case "email":?>
								<div class="mail"><a href="mailto:<?=$arElement["DISPLAY_PROPERTIES"]["email"]["DISPLAY_VALUE"]?>"><?=$arElement["DISPLAY_PROPERTIES"]["email"]["DISPLAY_VALUE"]?></a></div>
							<?break; case "WORK_TIME":?>
								<?if($arElement["DISPLAY_PROPERTIES"]["WORK_TIME"]["DISPLAY_VALUE"]):?>
								<div class="time">
									<?if(is_array($arElement["DISPLAY_PROPERTIES"]["WORK_TIME"]["DISPLAY_VALUE"])):?>
										<?foreach($arElement["DISPLAY_PROPERTIES"]["WORK_TIME"]["DISPLAY_VALUE"] AS $keyWT => $wtItem):?>
											<div><?=$arElement["DISPLAY_PROPERTIES"]["WORK_TIME"]["DESCRIPTION"][$keyWT]?>: <?=$wtItem?></div>
										<?endforeach?>
									<?else:?>
										<?if($arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DESCRIPTION"]):?>
											<?=$arResult["DISPLAY_PROPERTIES"]["WORK_TIME"]["DESCRIPTION"][0]?>
										<?endif?>
										<?=$arElement["DISPLAY_PROPERTIES"]["WORK_TIME"]["DISPLAY_VALUE"]?>
									<?endif?>
								</div>
								<?endif?>
							<?break;?>
						<?endswitch?>
					<?endforeach?>
				</div>
			<?endforeach?>
		</li>
	<?endforeach?>
	</ol>
<?if($ELEMENT_CNT <= $arParams["LIMIT_ELEMENTS_LEFT_COLUMN"]):?>
	</div>
	<div class="col-r-contact">
		<div class="map-content">
			<div id="contacts-map-top"></div>
		</div>
	</div>
</div>
<?endif?>

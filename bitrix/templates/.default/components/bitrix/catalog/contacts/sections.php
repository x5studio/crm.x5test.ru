<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(!$componentPage)
    {
        $componentPage = "sections";

        if($arParams["SET_STATUS_404"]==="Y")
        {
            $folder404 = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
            if ($folder404 != "/")
                $folder404 = "/".trim($folder404, "/ \t\n\r\0\x0B")."/";
            if (substr($folder404, -1) == "/")
                $folder404 .= "index.php";

             if($folder404 != $APPLICATION->GetCurPage(true)){
                @define("ERROR_404", "Y");
                CHTTP::SetStatus("404 Not Found");}
        }
    }

$arResult["VARIABLES"]["SECTION_ID"] =  $_SESSION["REGION"]["ID"];
$arResult["VARIABLES"]["SECTION_CODE"] =  $_SESSION["REGION"]["CODE"];
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"with_elements",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		"DETAIL_PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		"DETAIL_META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
		"DETAIL_META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
		"DETAIL_BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
		"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
		"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
		"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],

		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"DETAIL_OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
		"DETAIL_OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],

		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
		"LIST_PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"SECTION_USER_FIELDS" => $arParams["SECTION_USER_FIELDS"],
		"USE_FILTER" => $arParams["USE_FILTER"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
        "ADD_SECTIONS_CHAIN" => "N",
	),
	$component
);?>
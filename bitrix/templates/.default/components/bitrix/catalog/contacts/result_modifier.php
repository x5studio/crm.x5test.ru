<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$PARENT = $this->__component->__parent;?>
<!-- Выпадающий список городов -->
<?$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_simple",
	Array(
		"IBLOCK_TYPE" => "company",
		"IBLOCK_ID" => "33",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "1",
		"SECTION_FIELDS" => "",
		"SECTION_USER_FIELDS" => "",
		"ADD_SECTIONS_CHAIN" =>  "N",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_NOTES" => $arParams["CACHE_NOTES"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"LINK_IN_BASEURL" => "Y",
		"LINK_WITH_SUBDOMAIN" => "N",
		"SEF_MODE" => $arParams["SEF_MODE"],
		"SEF_FOLDER" => $arParams["SEF_FOLDER"],
		"SEF_URL_TEMPLATES" =>  $arParams["SEF_URL_TEMPLATES"],
		"VARIABLE_ALIASES" =>  $arParams["VARIABLE_ALIASES"],
		"HTML_LIST_ID" => "contacts-city-list",
		"HTML_TITLE" => $arParams["~CITY_DROPDOWN_HTML_TITLE"],
		"OFFICE_FILTER_NAME" => $arParams["FILTER_NAME"],
		"BY_CACHE_SUBDOMAIN_PAGE" => $arResult["VARIABLES"]["SECTION_CODE"] ?: $_SESSION["REGION"]["CODE"],
		"HIDE_EMPTY_CITY" => $arParams["HIDE_EMPTY_CITY"],
	),
	$PARENT
);?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="product4">
    <ul>
        <?foreach($arResult["ITEMS"] as $k=>$arItem){
            if($k>3) continue;
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	$img=CFile::ResizeImageGet($arItem[DETAIL_PICTURE],array("width" => 196, "height" => 150),BX_RESIZE_IMAGE_PROPORTIONAL, true);
            ?>
            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="javascript:void(0)" class='product4_a_box'>
                    <div class="compare_active"></div>
                    <div class="product4_hide">
                        <div class="product4_description"><?=$arItem[PREVIEW_TEXT]?></div>
                        <button id='<?=$arItem['ID']?>' class="btn">Сравнить</button>
                    </div>
                    <span class="product4_img" style="background: url(<?=$img[src]?>) no-repeat center center;"></span>
                    
                </a>
            </li>
        <?}?>
        
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "pod_product4_compare", array(
	"IBLOCK_TYPE" => "IB_PRODUCTS",
		"IBLOCK_ID" => "14",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "LINK",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
        
        
        
        
    </ul>
    <div class="cl"></div>
</div>



<script>
    $(document).ready(function(){
        var compare_widget=false;
        var compare_first='';
        var compare_second='';
        
        $('.compare_product button').click(function(){
        
            var q=$(this);
            var id=q.attr('id');
            var img=q.parents('.product4_a_box').find('.product4_img').attr('style');
            var bg=q.parents('.product4_a_box').find('.compare_active');
            
            if(id==compare_first || id==compare_second) return false;
            
            
            
            if(!compare_widget){
                //создаем виджет
                $('.compare_product').append("<div class='compare_box'><div class='compare_close'></div>"+
                                                "<div class='compare_first'>"+
                                                    "<div class='del_compare'></div>"+
                                                "</div>"+
                                                "<div class='compare_separate'>и</div>"+
                                                "<div class='compare_second'>"+
                                                    "<div class='del_compare'></div>"+
                                                "</div>"+
                                                "<div class='compare_button'>"+
                                                    "<button class='btn'>Сравнить</button>"+
                                                "</div>"+
                                                "<div class='cl'></div>"+
                                             "</div>");
                //обработчик на compare_close
                $('.compare_close').click(function(){
                    compare_widget=false;
                    $('.compare_box').slideUp(500,function(){
                        $(this).remove();
                        $('.compare_product button[id='+compare_first+']').parents('.product4_a_box').find('.compare_active').hide();
                        $('.compare_product button[id='+compare_second+']').parents('.product4_a_box').find('.compare_active').hide();
                        compare_first='';
                        compare_second='';
                    })
                });
                //---
                $('.del_compare').click(function(){//удаление из сравнения
                      var q=$(this);
                      if(q.parent().attr("class")=="compare_first"){
                            $('.compare_product button[id='+compare_first+']').parents('.product4_a_box').find('.compare_active').hide();
                            compare_first='';
                            $('.compare_first').hide();
                      } else{
                            $('.compare_product button[id='+compare_second+']').parents('.product4_a_box').find('.compare_active').hide();
                            compare_second='';
                            $('.compare_second').hide();
                      } 
                      if((compare_first && compare_second=='') ||(compare_first=='' && compare_second)) $('.compare_separate').html("Выберите продукт для сравнения");
                });
                //---
                    $('.compare_button button').click(function(){
                       if(compare_first && compare_second) {
                         $.get("/.ajax/get_compare.php?first="+compare_first+"&second="+compare_second,function(d){
                            //модалка
                            var top=$(window).scrollTop();
                            top=top+20;
                            $('body').prepend("<div class='overlay'></div>");
                            $('body').prepend("<div class='modal compare_modal'><div class='modal_close'></div><div class='modal_content'></div></div>");
                            $('.modal_content').html(d);
                            $('.compare_modal').css('top',top+'px');
                            // обработчики модалки
                            $('.overlay,.modal_close').click(function(){
                                $('.overlay,.modal').remove();
                            });
                         });
                       }
                    });
                //---
                
                
                $('.compare_box').slideDown(500);
                compare_widget=true;
                
                
            }
            
            if(compare_first && compare_second==''){
                $('.compare_second').attr('style',img).show();
                compare_second=id;
                $('.compare_separate').html("и");
                bg.show();
                
            }
            if(compare_first=='' && compare_second){
                $('.compare_first').attr('style',img).show();
                compare_first=id;
                $('.compare_separate').html("и");
                bg.show();
            }
            if(compare_first=='' && compare_second==''){
                $('.compare_first').attr('style',img).show();
                compare_first=id;
                $('.compare_separate').html("Выберите продукт для сравнения");
                bg.show();
            }
            
            
            
        });
    });
</script>









































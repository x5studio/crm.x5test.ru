<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="title">Мероприятия</div>
    <ul>
        <?foreach($arResult["ITEMS"] as $arItem){
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="event_date"><?=FormatDate("d F Y, H:i",strtotime($arItem[PROPERTIES][DATETIME_START][VALUE]))?></div>
                <div class="event_name"><?=$arItem[NAME]?></div>
                <div class="event_type"><?=$arItem[PROPERTIES][TYPE][VALUE]?></div>
                <a href="<?=$arItem[DETAIL_PAGE_URL]?>"><button class="btn event_reg">записаться</button></a>
            </li>
        <?}?>
    </ul>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
    <ul class="event_list">
        <?foreach($arResult["ITEMS"] as $arItem){
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	$img=CFile::ResizeImageGet($arItem[PROPERTIES][LEADING][VALUE],array("width" => 200, "height" => 150),BX_RESIZE_IMAGE_PROPORTIONAL, true); 
            ?>
        	<li class="event_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        		<div class="event_info fl">
                    <div class="event_tip"><?=$arItem[PROPERTIES][EVENT][VALUE]?></div>
                    <table class="event_place">
				<tr>
					<td>Место:</td>
					<td><div><?=$arItem[PROPERTIES][EVENT_PLACE][VALUE]?></div></td>
				</tr>
			</table>
			<table class="event_dat">
				<tr>
					<td>Дата:</td>
					<td><div><?=date("d.m.Y",strtotime($arItem[PROPERTIES][DATETIME_START][VALUE]))?></div></td>
				</tr>
			</table>
			<table class="event_time">
				<tr>
					<td>Время:</td>
					<td><div><?=date("H:i",strtotime($arItem[PROPERTIES][DATETIME_START][VALUE]))?></div></td>
				</tr>
			</table>
                    <div class="event_liading">
                        <?if($img[src]){?>
                            <img src="<?=$img[src]?>"/>
                            <div><?=$arItem[PROPERTIES][LEADING_NAME][VALUE]?></div>
                            <?=$arItem[PROPERTIES][LEADING_GRADE][VALUE]?>
                        <?}?>
                    </div>
                </div>
                <div class="event_description fr">
                    <div class="event_cost"><?=$arItem[PROPERTIES][TYPE][VALUE]?></div>
                    <div class="event_title"><?=$arItem[NAME]?></div>
                    <div class="event_preview"><?=$arItem[PREVIEW_TEXT]?></div>
                    <button class="btn" onclick="form10('<?=$arItem[NAME]?>')">записаться</button><br />
                    <a class="event_more" href="<?=$arItem[DETAIL_PAGE_URL]?>">Подробнее &raquo;</a>
                </div>
                <div class="cl"></div>
        	</li>
        <?}?>
    </ul>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]) print $arResult["NAV_STRING"]?>
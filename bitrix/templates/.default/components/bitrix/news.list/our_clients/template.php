<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<a href="/clients/" class="our_clients_more all_our_clients ">все клиенты &raquo;</a>
<div class="our_clients">
    <ul>
        <?foreach($arResult["ITEMS"] as $arItem){
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	$img=CFile::ResizeImageGet($arItem[PREVIEW_PICTURE],array("width" => 400, "height" => 300),BX_RESIZE_IMAGE_PROPORTIONAL, true);
            ?>
            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="our_clients_img fl" style="background: url(<?=$img[src]?>) no-repeat center center;"></div>
                <div class="our_clients_rightbox fr">
                    <div class="our_clients_title"><?=$arItem[NAME]?></div>
                    <div class="our_clients_text"><?=$arItem[PROPERTIES][TEXT_MAIN]["~VALUE"][TEXT]?></div>
                    <a class="our_clients_more" href="<?=$arItem[DETAIL_PAGE_URL]?>">Читать далее &raquo;</a>
                </div>
                <div class="cl"></div>
            </li>
        <?}?>
    </ul>
</div>
<div class="our_clients_prev"></div>
<div class="our_clients_next"></div>
<div class="cl"></div>
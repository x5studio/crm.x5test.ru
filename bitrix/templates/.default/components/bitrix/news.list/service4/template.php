<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="product4 service4">
    <ul>
        <?foreach($arResult["ITEMS"] as $k=>$arItem){
            if($k>3) continue;
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	$img=CFile::ResizeImageGet($arItem[DETAIL_PICTURE],array("width" => 196, "height" => 150),BX_RESIZE_IMAGE_PROPORTIONAL, true);
            ?>
            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?=$arItem[DETAIL_PAGE_URL]?>" class='product4_a_box'>
                    <span  class="product4_img" style="background: url(<?=$img[src]?>) no-repeat center top;"></span>
                    <div class="product4_description"><?=$arItem[PREVIEW_TEXT]?></div>
                    <span href="<?=$arItem[DETAIL_PAGE_URL]?>" class="product4_more">Подробнее &raquo;</span>
                </a>
            </li>
        <?}?>
    </ul>
    <div class="cl"></div>
</div>
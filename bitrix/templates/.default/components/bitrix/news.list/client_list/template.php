<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="filter">
    <form method="get">
        <select name="branch">
            <option value="">Выберите отрасль</option>
            <?$res=CIBlockPropertyEnum::GetList(array(), Array("IBLOCK_ID"=>$arParams, "CODE"=>"BRANCH"));
              while($tmp=$res->GetNext()){
                if($_GET[branch]==$tmp[ID]) $s="selected"; else $s="";
                ?>
              <option <?=$s?> value="<?=$tmp[ID]?>"><?=$tmp[VALUE]?></option>
              <?}?>
        </select>
        <select name="product">
            <option value="">Выберите продукт</option>
            <?$res=CIBlockElement::GetList(
                 Array("SORT"=>"ASC"),
                 Array("IBLOCK_ID"=>1,"ACTIVE"=>"Y"),
                 false,
                 false,
                 Array("ID","NAME")
                );
              while($tmp=$res->GetNext()){
                if($_GET[product]==$tmp[ID]) $s="selected"; else $s="";
                ?>
              <option <?=$s?> value="<?=$tmp[ID]?>"><?=$tmp[NAME]?></option>
              <?}?>
        </select>
    </form>
</div>

<ul class="client_list">
    <?foreach($arResult["ITEMS"] as $arItem){
    	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
 	    $img=CFile::ResizeImageGet($arItem[PREVIEW_PICTURE],array("width" => 300, "height" => 180),BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ?>
    	<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="client_list_img fl" style="background:url(<?=$img[src]?>) no-repeat center center;"></div>
            <div class="client_list_description fr">
                <div class="client_list_title"><?=$arItem[NAME]?></div>
                <div class="client_list_text"><?=$arItem[PROPERTIES][PREVIEW_TEXT]["~VALUE"][TEXT]?></div>
                <div class="client_list_control">
                    <a href="<?=$arItem[DETAIL_PAGE_URL]?>">Описание</a>
                    <a href="<?=$arItem[DETAIL_PAGE_URL]?>">Внедрение</a>
                    <!-- a href="<?=$arItem[DETAIL_PICTURE][SRC]?>" class="fancybox">Отзыв</a -->
                </div>
            </div>
            <div class="cl"></div>
    	</li>
    <?}?>
</ul>



<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
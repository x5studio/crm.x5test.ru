<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="product4">
    <ul>
        <?foreach($arResult["ITEMS"] as $k=>$arItem){
            if($k>3) continue;
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	$img=CFile::ResizeImageGet($arItem[DETAIL_PICTURE],array("width" => 196, "height" => 150),BX_RESIZE_IMAGE_PROPORTIONAL, true);
            ?>
            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?=$arItem[DETAIL_PAGE_URL]?>" class='product4_a_box'>
                    <div class="product4_hide">
                        <div class="product4_description"><?=$arItem[PREVIEW_TEXT]?></div>
                        <span href="<?=$arItem[DETAIL_PAGE_URL]?>" class="product4_more">Подробнее &raquo;</span>
                        <button class="btn" onclick="form3('<?=$arItem[NAME]?>')">попробовать бесплатно</button>
                    </div>
                    <span class="product4_img" style="background: url(<?=$img[src]?>) no-repeat center center;"></span>
                    
                </a>
            </li>
        <?}?>
        
        <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"pod_product4", 
	array(
		"COMPONENT_TEMPLATE" => "pod_product4",
		"IBLOCK_TYPE" => "IB_PRODUCTS",
		"IBLOCK_ID" => "14",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "LINK",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>
        
        
        
        
    </ul>
    <div class="cl"></div>
    <br />
    <div style='text-align:right'>
        <?/*?><a href="/about-crm/" class="our_clients_more">Сравнить CRM-системы &raquo;</a><?*/?>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.product4 .btn').hover(function(){
            $(this).parents('.product4_a_box').click(function(){
                return false;
            });
        },function(){
             $(this).parents('.product4_a_box').unbind("click");
        });
    });
</script>
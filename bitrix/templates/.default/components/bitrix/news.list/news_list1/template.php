<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
    <ul class="event_list">
        <?foreach($arResult["ITEMS"] as $arItem){
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	$img=CFile::ResizeImageGet($arItem[PROPERTIES][LEADING][VALUE],array("width" => 200, "height" => 130),BX_RESIZE_IMAGE_PROPORTIONAL, true); 
            ?>
        	<li class="event_item news_list_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?=$arItem[DETAIL_PAGE_URL]?>">
            	   <div class="event_info news_info fl">
                        
                        <div class="event_liading" style="margin:0px;">
                            <?if($img[src]){?>
                                <img src="<?=$img[src]?>"/>
                                <div><?=$arItem[PROPERTIES][LEADING_NAME][VALUE]?></div>
                                <?=$arItem[PROPERTIES][LEADING_GRADE][VALUE]?>
                            <?}?>
                        </div>
                    </div>
                    <div class="event_description news_description fr">
        
                        <div class="event_title"><?=$arItem[NAME]?></div>
                        <div class="event_preview"><?=$arItem[PROPERTIES][PREVIEW_TEXT]["~VALUE"][TEXT]?></div>
                        <span class="event_more" href="<?=$arItem[DETAIL_PAGE_URL]?>">Подробнее »</span>
                    </div>
                    <div class="cl"></div>
                </a>
        	</li>
        <?}?>
    </ul>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]) print $arResult["NAV_STRING"];?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
        <?foreach($arResult["ITEMS"] as $k=>$arItem){
            if($k>3) continue;
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        	$img=CFile::ResizeImageGet($arItem[DETAIL_PICTURE],array("width" => 196, "height" => 150),BX_RESIZE_IMAGE_PROPORTIONAL, true);
            ?>
            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?=$arItem[PROPERTIES][LINK][VALUE]?>" class='product4_a_box' <?=(!$arItem[PROPERTIES][LINK][VALUE])?"onclick='return false' style='cursor:default'":"";?>>
                    <div class="product4_hide">
                        <div class="product4_description product4_description_more"><?=$arItem[PREVIEW_TEXT]?></div>
			<?if(trim($arItem[PROPERTIES][LINK][VALUE])!=''){?>
                        	<span href="<?=$arItem[PROPERTIES][LINK][VALUE]?>" class="product4_more">Подробнее »</span>
			<?}?>
                    </div>
                    <span class="product4_img" style="background: url(<?=$img[src]?>) no-repeat center center;"></span>
                    
                </a>
            </li>
        <?}?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$img=CFile::ResizeImageGet($arItem[PREVIEW_PICTURE],array("width" => 180, "height" => 90),BX_RESIZE_IMAGE_PROPORTIONAL, true);
    ?>
	<a class="clients_logos" href="<?=$arItem[DETAIL_PAGE_URL]?>" >
        <img src="<?=$img[src]?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"/> 
    </a>
<?}?>

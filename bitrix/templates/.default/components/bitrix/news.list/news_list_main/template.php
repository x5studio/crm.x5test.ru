<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?>
<div class="title">Эксперты о CRM</div>
    <ul>
        <?foreach($arResult["ITEMS"] as $arItem){
        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="news_date"><?=FormatDate("d F Y",strtotime($arItem[DATE_CREATE]))?></div>
                <div class="news_preview_title"><?=$arItem[NAME]?></div>
                <div class="news_preview"><?=$arItem[PREVIEW_TEXT]?></div>
                <a href="<?=$arItem[DETAIL_PAGE_URL]?>" class="our_clients_more">Подробнее &raquo;</a>
            </li>
        <?}?>
    </ul>
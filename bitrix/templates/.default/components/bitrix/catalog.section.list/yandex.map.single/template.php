<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript">

if(typeof mapInit === 'undefined'){
    mapInit = true;
    if($("#<?=($arParams["CONTAINER_YMAPS_ID"] ?: 'contacts-map-top')?>").size()){
        ymaps.ready(init);
    }
}


function init () {
	
	var o = this;

	var map = new ymaps.Map("<?=($arParams["CONTAINER_YMAPS_ID"] ?: 'contacts-map-top')?>", {
		center: [52.680913, 65.741902], 
		zoom: 3.5,
		behaviors: ['default', 'scrollZoom']
		}, {
		maxZoom: 16,
		geoObjectIconImageHref: "/bitrix/templates/main/img/first.png",
		geoObjectIconImageSize: [32,35],
		geoObjectIconImageOffset: [0, -35]
	});

    require(['main'], function() {
        require(['Controls/Responsive'], function(obResponsive){
            obResponsive.one(['xsmall', 'small', 'medium'], 'in', function(){

                map.behaviors.disable('scrollZoom');

            });
        });
    });

	map.controls
	// Кнопка изменения масштаба
	.add('zoomControl', { left: 5, top: 5 })

	// Список типов карты
	.add('typeSelector')

	// Кнопка изменения масштаба - компактный вариант
	// Расположим её справа
	//.add('smallZoomControl', { right: 5, top: 75 })

	// Стандартный набор кнопок
	//.add('mapTools', { left: 35, top: 5 });
	
    var trafficControl = new ymaps.control.TrafficControl();
	/*map.controls
        .add(trafficControl)
        .add(new ymaps.control.MiniMap({
            type: 'yandex#publicMap'
        }));
	*/

	var myCollection = new ymaps.GeoObjectCollection();

	<?foreach($arResult["ELEMENTS"] AS $keySection => $arSection):?>
	myGeoObjects<?=$keySection?> = [];
		<?foreach($arSection AS $arElement):?>
	curGeoObject = new ymaps.GeoObject({
		geometry: {type: "Point", coordinates: [<?=$arElement["PROPERTY_COORDINATES_LEFT"]?>, <?=$arElement["PROPERTY_COORDINATES_RIGHT"]?>]},
		properties: {
			clusterCaption: '<?=$arElement["NAME"]?>',
			balloonContentHeader: "<?="<a href='".$arElement["DETAIL_URL"]."' style='font-size: 18px;'>".$arElement["NAME"]."</a>"?>",
			//balloonContentBody: "<?="<b>Адрес:</b>&nbsp;".$arElement["PROPERTY_ADDRESS_VALUE"]."<br /><b>Телефон:</b>&nbsp;".$arElement["PROPERTY_PHONE_VALUE"]."<br/><b>Координаты для GPS:</b>&nbsp;".$arElement["PROPERTY_COORDINATES_LEFT"].", ".$arElement["PROPERTY_COORDINATES_RIGHT"]."<br /><br /><img src='/contacts_old/office_fasad_photo/msk-main.jpg' width='250' height='187' alt='Москва - Центральный офис 1С:Бухучет и Торговля (БИТ)' style='border: #555555 1px solid;' />"?>",
            //balloonContentFooter: "",
            hintContent: "<?=$arElement["NAME"]?>"
		}
	});

	myGeoObjects<?=$keySection?>.push(curGeoObject);
	myCollection.add(curGeoObject);
		<?endforeach?>
	<?endforeach?>

	// Добавляем коллекцию на карту.
	map.geoObjects.add(myCollection);
	// Устанавливаем карте центр и масштаб так, чтобы охватить коллекцию целиком.
	map.setBounds(myCollection.getBounds());
	//map.geoObjects.remove(myCollection);

	ymaps.getZoomRange('yandex#map', map.getCenter()).then(function (result) {
		if(map.getZoom() > result[1]){
			map.setZoom(result[1]);
		}
	});
	

	this.SetDetailIcon = function(){
		map.options.set({ 
			geoObjectIconImageHref: "/bitrix/templates/main/img/bit-map-marker.png",
			geoObjectIconImageSize: [167,93],
			geoObjectIconImageOffset: [-12,-93],
			
			geoObjectIconShadow: true,
			geoObjectIconShadowImageHref: "/bitrix/templates/main/img/bit-map-marker-shadow.png",
			geoObjectIconShadowImageOffset: [-5, -36],
			geoObjectIconShadowImageSize: [177, 36]
		});
	};
	
	map.events.add('boundschange', function (event) {
		if(event.get('newZoom') != event.get('oldZoom')){
			if (event.get('newZoom') > 13) {
				o.SetDetailIcon();
			} else {
				map.options.set({ 
					geoObjectIconImageHref: "/bitrix/templates/main/img/first.png",
					geoObjectIconImageSize: [32,35],
					geoObjectIconImageOffset: [0, -37],
					
					geoObjectIconShadow: false
				});
			}
		}		
		
	});
	
	if(map.getZoom() > 13){
		o.SetDetailIcon();
	}



}



/*
function distance(lat1, lng1, lat2, lng2) {
    var pi80 = Math.PI / 180;
    lat1 *= pi80;
    lng1 *= pi80;
    lat2 *= pi80;
    lng2 *= pi80;

    r = 6372.797; // mean radius of Earth in km
    dlat = lat2 - lat1;
    dlng = lng2 - lng1;
    a = Math.sin(dlat / 2) * Math.sin(dlat / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlng / 2) * Math.sin(dlng / 2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    km = r * c;

    return km;
}*/
</script>


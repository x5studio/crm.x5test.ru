<?
$MESS["PB_MAIN_SEARCH_TITLE_FORM_PAGE"] = "Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)";
$MESS["PB_MAIN_SEARCH_TITLE_NUM_CATEGORIES"] = "Количество категорий поиска";
$MESS["PB_MAIN_SEARCH_TITLE_NUM_CATEGORY"] = "Категория ##NUM#";
$MESS["PB_MAIN_SEARCH_TITLE_CATEGORY_TITLE"] = "Название категории";
$MESS["PB_MAIN_SEARCH_TITLE_TOP_COUNT"] = "Количество результатов в каждой категории";
$MESS["PB_MAIN_SEARCH_TITLE_CHECK_DATES"] = "Искать только в активных по дате документах";
$MESS["PB_MAIN_SEARCH_TITLE_SHOW_OTHERS"] = "Показывать категорию \"прочее\"";
$MESS["PB_MAIN_SEARCH_TITLE_OTHERS_CATEGORY"] = "Категория \"прочее\"";
$MESS["PB_MAIN_SEARCH_TITLE_USE_LANGUAGE_GUESS"] = "Включить автоопределение раскладки клавиатуры";
$MESS["PB_MAIN_SEARCH_TITLE_ORDER"] = "Сортировка результатов";
$MESS["PB_MAIN_SEARCH_TITLE_ORDER_BY_DATE"] = "по дате";
$MESS["PB_MAIN_SEARCH_TITLE_ORDER_BY_RANK"] = "по релевантности";
?>
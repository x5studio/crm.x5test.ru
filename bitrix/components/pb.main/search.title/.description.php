<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => \PB\Main\Loc::getMessage("PB_MAIN_SEARCH_TITLE_SEMINAR_NAME"),
	"DESCRIPTION" => \PB\Main\Loc::getMessage("PB_MAIN_SEARCH_TITLE_SEMINAR_DESCRIPTION"),
	"ICON" => "/images/search_title.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "utility",
		"CHILD" => array(
			"ID" => "search",
			"NAME" => \PB\Main\Loc::getMessage("PB_MAIN_SEARCH_TITLE_SEMINAR_SEARCH")
		)
	),
);

?>
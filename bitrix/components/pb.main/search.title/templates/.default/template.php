<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
    $INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
    $CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

$SUBMIT_ID = trim($arParams['~SUBMIT_ID']);
if(strlen($SUBMIT_ID) <= 0)
    $SUBMIT_ID = "header-search-submit";
$SUBMIT_ID = CUtil::JSEscape($SUBMIT_ID);

$RESULT_ID = trim($arParams['~RESULT_ID']);
if(strlen($RESULT_ID) <= 0)
    $RESULT_ID = "search-result";
$RESULT_ID = CUtil::JSEscape($RESULT_ID);

$SEARCH_WRAP_ID = trim($arParams['~SEARCH_WRAP_ID']);
if(strlen($SEARCH_WRAP_ID) <= 0)
    $SEARCH_WRAP_ID = "search-wrap";
$SEARCH_WRAP_ID = CUtil::JSEscape($SEARCH_WRAP_ID);


if($arParams["SHOW_INPUT"] !== "N"):?>
    <div id="<?= $SEARCH_WRAP_ID ?>"   data-ajax-page='<?=$arParams["AJAX_PAGE"]?>'
         data-container_id='<?echo $CONTAINER_ID?>' data-input_id='<?echo $INPUT_ID?>' data-min_query_len='2'
         data-wait_image='/bitrix/themes/.default/images/wait.gif' data-submit_id='<?=$SUBMIT_ID?>'
         data-result_id='<?=$RESULT_ID?>' data-search_wrap_id='<?=$SEARCH_WRAP_ID?>'>
        <div id="<?echo $CONTAINER_ID?>" class="header__top-search top-search" _lpchecked="1">
            <button class="top-search__button" type="submit" id="<?=$SUBMIT_ID?>">
                <svg class="icon icon-search icon_blue">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/sprite.svg#search"></use>
                </svg><span class="visually-hidden">Поиск</span>
            </button>

            <label class="visually-hidden" for="top-search">Поиск по сайту</label>
            <input class="top-search__input" id="<?echo $INPUT_ID?>" type="text" name="q" value="" maxlength="50" autocomplete="off"
                   placeholder="<?=\PB\Main\Loc::getMessage('PB_MAIN_SEARCH_TITLE_CT_DEF_1C')?>" required="" />

        </div>
        <div id="<?echo $RESULT_ID?>" class="search-result"></div>
    </div>

<?endif?>

<script>
    $(document).ready(function () {
        BX.ready(function(){
            new SearchPage($("#<?=$SEARCH_WRAP_ID ?>"));
        });
    });
</script>

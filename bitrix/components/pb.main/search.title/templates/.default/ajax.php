<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!empty($arResult["CATEGORIES"])):?>
		<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
			<h3 class="form-title"><?if(!empty($arCategory['URL'])):?><a href="<?=$arCategory['URL']?>"><?endif;?><?=$arCategory["TITLE"]?><?if(!empty($arCategory['URL'])):?></a><?endif;?></h3>
			<ul>
				<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
					<li><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></a></li>
				<?endforeach;?>
			</ul>
		<?endforeach;?>
<?else:?>
    <p class="empty-result">По вашему запросу ничего не найдено. <a href="/contacts/">Позвоните нам</a>, мы поможем.</p><?endif;
?>

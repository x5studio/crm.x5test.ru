function JCTitleSearch(arParams)
{
    var _this = this;

    this.arParams = {
        'AJAX_PAGE': arParams.AJAX_PAGE,
        'CONTAINER_ID': arParams.CONTAINER_ID,
        'INPUT_ID': arParams.INPUT_ID,
        'MIN_QUERY_LEN': parseInt(arParams.MIN_QUERY_LEN)
    };
    if(arParams.WAIT_IMAGE)
        this.arParams.WAIT_IMAGE = arParams.WAIT_IMAGE;
    this.SUBMIT_ID = document.getElementById(arParams.SUBMIT_ID);
    if(arParams.MIN_QUERY_LEN <= 0)
        arParams.MIN_QUERY_LEN = 1;

    this.cache = [];
    this.cache_key = null;

    this.startText = '';
    this.running = false;
    this.currentRow = -1;
    this.RESULT = null;
    this.CONTAINER = null;
    this.INPUT = null;
    this.WAIT = null;

    this.ShowResult = function(result)
    {
        if(BX.type.isString(result))
        {
            _this.RESULT.innerHTML = result;
        }
        _this.RESULT.style.display = _this.RESULT.innerHTML !== '' ? 'block' : 'none';
    };

    this.onKeyPress = function(keyCode)
    {
        var tbl = BX.findChild(_this.RESULT, {'tag':'table','class':'title-search-result'}, true);
        if(!tbl)
            return false;

        var i;
        var cnt = tbl.rows.length;

        switch (keyCode)
        {
            case 27: // escape key - close search div
                _this.RESULT.style.display = 'none';
                _this.currentRow = -1;
                _this.UnSelectAll();
                return true;

            case 40: // down key - navigate down on search results
                if(_this.RESULT.style.display == 'none')
                    _this.RESULT.style.display = 'block';

                var first = -1;
                for(i = 0; i < cnt; i++)
                {
                    if(!BX.findChild(tbl.rows[i], {'class':'title-search-separator'}, true))
                    {
                        if(first == -1)
                            first = i;

                        if(_this.currentRow < i)
                        {
                            _this.currentRow = i;
                            break;
                        }
                        else if(tbl.rows[i].className == 'title-search-selected')
                        {
                            tbl.rows[i].className = '';
                        }
                    }
                }

                if(i == cnt && _this.currentRow != i)
                    _this.currentRow = first;

                tbl.rows[_this.currentRow].className = 'title-search-selected';
                return true;

            case 38: // up key - navigate up on search results
                if(_this.RESULT.style.display == 'none')
                    _this.RESULT.style.display = 'block';

                var last = -1;
                for(i = cnt-1; i >= 0; i--)
                {
                    if(!BX.findChild(tbl.rows[i], {'class':'title-search-separator'}, true))
                    {
                        if(last == -1)
                            last = i;

                        if(_this.currentRow > i)
                        {
                            _this.currentRow = i;
                            break;
                        }
                        else if(tbl.rows[i].className == 'title-search-selected')
                        {
                            tbl.rows[i].className = '';
                        }
                    }
                }

                if(i < 0 && _this.currentRow != i)
                    _this.currentRow = last;

                tbl.rows[_this.currentRow].className = 'title-search-selected';
                return true;

            case 13: // enter key - choose current search result
                if(_this.RESULT.style.display == 'block')
                {
                    for(i = 0; i < cnt; i++)
                    {
                        if(_this.currentRow == i)
                        {
                            if(!BX.findChild(tbl.rows[i], {'class':'title-search-separator'}, true))
                            {
                                var a = BX.findChild(tbl.rows[i], {'tag':'a'}, true);
                                if(a)
                                {
                                    window.location = a.href;
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
        }

        return false;
    };

    this.onTimeout = function()
    {
        _this.onChange(function(){
            setTimeout(_this.onTimeout, 500);
        });
    };

    this.onChange = function(callback)
    {
        if (_this.running)
            return;
        _this.running = true;

        if(_this.INPUT.value != _this.oldValue && _this.INPUT.value != _this.startText)
        {
            _this.oldValue = _this.INPUT.value;
            if(_this.INPUT.value.length >= _this.arParams.MIN_QUERY_LEN)
            {
                _this.cache_key = _this.arParams.INPUT_ID + '|' + _this.INPUT.value;
                if(_this.cache[_this.cache_key] == null)
                {
                    if(_this.WAIT)
                    {
                        var pos = BX.pos(_this.INPUT);
                        var height = (pos.bottom - pos.top)-2;
                        require(['main'], function() {
                            require(['Controls/Responsive'], function(obResponsive) {
                                obResponsive.on(['xsmall', 'small', 'medium'], 'in', function () {
                                    _this.WAIT.style.top = (pos.top+15) + 'px';
                                });
                                obResponsive.on(['large'], 'in', function () {
                                    _this.WAIT.style.top = (pos.top+1) + 'px';
                                });
                            });
                        });
                        _this.WAIT.style.height = '19px';
                        _this.WAIT.style.width = '19px';
                        _this.WAIT.style.left = (pos.right - height + 2) + 'px';
                        _this.WAIT.style.display = 'block';
                        _this.SUBMIT_ID.style.display = 'none';
                        $('body').addClass('over-hidden');
                    }

                    BX.ajax.post(
                        _this.arParams.AJAX_PAGE,
                        {
                            'ajax_call':'y',
                            'INPUT_ID':_this.arParams.INPUT_ID,
                            'q':_this.INPUT.value,
                            'l':_this.arParams.MIN_QUERY_LEN
                        },
                        function(result)
                        {

                            if (typeof ga == 'function') {
                                ga('send', 'pageview', '?q=' + _this.INPUT.value);
                            }

                            _this.cache[_this.cache_key] = result;
                            _this.ShowResult(result);
                            _this.currentRow = -1;
                            _this.EnableMouseEvents();
                            if(_this.WAIT) {
                                _this.WAIT.style.display = 'none';
                                _this.SUBMIT_ID.style.display = 'block';
                            }
                            if (!!callback)
                                callback();
                            _this.running = false;
                        }
                    );
                    return;
                }
                else
                {
                    _this.ShowResult(_this.cache[_this.cache_key]);
                    _this.currentRow = -1;
                    _this.EnableMouseEvents();
                }
            }
            else
            {
                _this.RESULT.style.display = 'none';
                _this.currentRow = -1;
                _this.UnSelectAll();
                $('body').removeClass('over-hidden');
            }
        }
        if (!!callback)
            callback();
        _this.running = false;
    };

    this.UnSelectAll = function()
    {
        var tbl = BX.findChild(_this.RESULT, {'tag':'table','class':'title-search-result'}, true);
        if(tbl)
        {
            var cnt = tbl.rows.length;
            for(var i = 0; i < cnt; i++)
                tbl.rows[i].className = '';
        }
    };

    this.EnableMouseEvents = function()
    {
        var tbl = BX.findChild(_this.RESULT, {'tag':'table','class':'title-search-result'}, true);
        if(tbl)
        {
            var cnt = tbl.rows.length;
            for(var i = 0; i < cnt; i++)
                if(!BX.findChild(tbl.rows[i], {'class':'title-search-separator'}, true))
                {
                    tbl.rows[i].id = 'row_' + i;
                    tbl.rows[i].onmouseover = function (e) {
                        if(_this.currentRow != this.id.substr(4))
                        {
                            _this.UnSelectAll();
                            this.className = 'title-search-selected';
                            _this.currentRow = this.id.substr(4);
                        }
                    };
                    tbl.rows[i].onmouseout = function (e) {
                        this.className = '';
                        _this.currentRow = -1;
                    };
                }
        }
    };

    this.onFocusLost = function(hide)
    {
        setTimeout(function(){
            _this.RESULT.style.display = 'none';
            $('body').removeClass('over-hidden');
        }, 250);
    };

    this.onFocusGain = function()
    {
        if(_this.RESULT.innerHTML.length)
            _this.ShowResult();
    };

    this.onKeyDown = function(e)
    {
        if(!e)
            e = window.event;

        if (_this.RESULT.style.display == 'block')
        {
            if(_this.onKeyPress(e.keyCode))
                return BX.PreventDefault(e);
        }
    };

    this.Init = function()
    {
        this.CONTAINER = document.getElementById(this.arParams.CONTAINER_ID);
        this.RESULT = document.getElementById(arParams.RESULT_ID).appendChild(document.createElement("DIV"));
        this.RESULT.className = 'title-search-result';
        this.INPUT = document.getElementById(this.arParams.INPUT_ID);
        console.log(this.INPUT);

        this.startText = this.oldValue = this.INPUT.value;
        BX.bind(this.INPUT, 'focus', function() {_this.onFocusGain()});
        BX.bind(this.INPUT, 'blur', function() {_this.onFocusLost()});

        if(BX.browser.IsSafari() || BX.browser.IsIE())
            this.INPUT.onkeydown = this.onKeyDown;
        else
            this.INPUT.onkeypress = this.onKeyDown;

        if(this.arParams.WAIT_IMAGE)
        {
            this.WAIT = document.body.appendChild(document.createElement("DIV"));
            this.WAIT.style.backgroundImage = "url('" + this.arParams.WAIT_IMAGE + "')";
            if(!BX.browser.IsIE())
                this.WAIT.style.backgroundRepeat = 'none';
            this.WAIT.style.display = 'none';
            this.WAIT.style.position = 'absolute';
            this.WAIT.style.zIndex = '1100';
            this.SUBMIT_ID.style.display = 'block';
        }

        BX.bind(this.INPUT, 'input', function() {
            setTimeout(_this.onChange, 1000);
        });
        BX.bind(this.INPUT, 'bxchange', function() {
            setTimeout(_this.onChange, 1000);
        });
    };
    BX.ready(function (){_this.Init(arParams)});
}

function SearchPage(container) {
    this.container = $(container);
    this.ajaxPage = this.container.data('ajax-page');
    this.containerId = this.container.data('container_id');
    this.inputId = this.container.data('input_id');
    this.minQueryLen = this.container.data('min_query_len');
    this.waitImage = this.container.data('wait_image');
    this.submitId = this.container.data('submit_id');
    this.resultId = this.container.data('result_id');
    this.mobileBox = $('#mobile-search');
    this.prevBox = this.container.prev();
    this.init();
}
SearchPage.prototype.init = function () {
    var o = this;

    new JCTitleSearch({
        'AJAX_PAGE' : o.ajaxPage,
        'CONTAINER_ID': o.containerId,
        'INPUT_ID': o.inputId,
        'MIN_QUERY_LEN': o.minQueryLen,
        'WAIT_IMAGE': o.waitImage,
        'SUBMIT_ID': o.submitId,
        'RESULT_ID': o.resultId
    });

};

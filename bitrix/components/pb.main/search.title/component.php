<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!IsModuleInstalled("search"))
{
	ShowError(\PB\Main\Loc::getMessage("PB_MAIN_SEARCH_TITLE_MODULE_NOT_INSTALLED"));
	return;
}

if(!isset($arParams["PAGE"]) || strlen($arParams["PAGE"])<=0)
	$arParams["PAGE"] = "#SITE_DIR#search/index.php";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
	$arFILTERCustom = array();
else
{
	$arFILTERCustom = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arFILTERCustom))
		$arFILTERCustom = array();
}
	
$arResult["CATEGORIES"] = array();

$query = ltrim($_POST["q"]);
if(
	!empty($query)
	&& $_REQUEST["ajax_call"] === "y"
	&& (
		!isset($_REQUEST["INPUT_ID"])
		|| $_REQUEST["INPUT_ID"] == $arParams["INPUT_ID"]
	)
	&& CModule::IncludeModule("search")
)
{
	CUtil::decodeURIComponent($query);

	$arResult["alt_query"] = "";
	if($arParams["USE_LANGUAGE_GUESS"] !== "N")
	{
		$arLang = CSearchLanguage::GuessLanguage($query);
		if(is_array($arLang) && $arLang["from"] != $arLang["to"])
			$arResult["alt_query"] = CSearchLanguage::ConvertKeyboardLayout($query, $arLang["from"], $arLang["to"]);
	}

	$arResult["query"] = $query;
	$arResult["phrase"] = stemming_split($query, LANGUAGE_ID);

	$arParams["NUM_CATEGORIES"] = intval($arParams["NUM_CATEGORIES"]);
	if($arParams["NUM_CATEGORIES"] <= 0)
		$arParams["NUM_CATEGORIES"] = 1;

	$arParams["TOP_COUNT"] = intval($arParams["TOP_COUNT"]);
	if($arParams["TOP_COUNT"] <= 0)
		$arParams["TOP_COUNT"] = 5;

	$arOthersFilter = array("LOGIC"=>"OR");
	$str_query = $arResult["alt_query"]? $arResult["alt_query"]: $arResult["query"];
	$searchParams = array("QUERY"=>$str_query, "SITE_ID" => $arParams['SITE_ID']);
	if($arParams["CHECK_DATES"] === "Y") {
		$searchParams["CHECK_DATES"] = "Y";
	}
        
        $arSortSearch = array();
        for ($i = 1; $i <= 5; $i++) {
            if (isset($arParams["ORDER{$i}"])) {
                $arSortSearch[$arParams["ORDER{$i}"]] = $arParams["SORT{$i}"] ?: "ASC";
            }
        }

	for($i = 0; $i < $arParams["NUM_CATEGORIES"]; $i++)
	{
		$category_title = trim($arParams["CATEGORY_".$i."_TITLE"]);
		if(empty($category_title))
		{
			if(is_array($arParams["CATEGORY_".$i]))
				$category_title = implode(", ", $arParams["CATEGORY_".$i]);
			else
				$category_title = trim($arParams["CATEGORY_".$i]);
		}
		if(empty($category_title))
			continue;
	
		$arResult["CATEGORIES"][$i] = array(
			"TITLE" => htmlspecialchars($category_title,ENT_HTML5,LANG_CHARSET),
			"URL" => $arParams["CATEGORY_".$i."_URL"],
			"ITEMS" => array()
		);
		
		if(strlen($arParams["CATEGORY_".$i."_ANCHOR"]) > 1)
			$arResult["CATEGORIES"][$i]["ANCHOR"] = $arParams["CATEGORY_".$i."_ANCHOR"];

		$exFILTER = array(
			0 => CSearchParameters::ConvertParamsToFilter($arParams, "CATEGORY_".$i)
		);
		$exFILTER[0]["LOGIC"] = "OR";

		if($arParams["CHECK_DATES"] === "Y") {
			$exFILTER["CHECK_DATES"] = "Y";
			$searchParams["CHECK_DATES"] = "Y";
		}
		$temp = $exFILTER;
		$exFILTER = array();
		if(count($arFILTERCustom) > 0) {
			foreach ($arFILTERCustom as $index => $filterItem) {
				$exFILTER[$index] = array_merge($temp, $filterItem);
			}
		}
		if(!empty($arParams["CATEGORY_".$i."_PARAMS_FILTER"])) {
			foreach ($exFILTER as $index => $filterItem) {
				$exFILTER[$index]['PARAMS'] = array_merge($exFILTER[$index]['PARAMS'], $arParams["CATEGORY_".$i."_PARAMS_FILTER"]);
			}
		}

		$arOthersFilter[] = $exFILTER;

		$j = 0;
		$obTitle = new CSearch;
                $obTitle->SetOptions(array("ERROR_ON_EMPTY_STEM" => false));
		$obTitle->Search($searchParams, $arSortSearch, $exFILTER);
		$findUrls = array();
		while($ar = $obTitle->Fetch())
		{
			if($j >= $arParams["TOP_COUNT"])
			{
				break;
			}
			else
			{
				if (!empty($arResult["CATEGORIES"][$i]["ANCHOR"])) {
					$resc = CIBlockElement::GetByID($ar["ITEM_ID"]);
					if($ar_resc = $resc->GetNext()) {
						$anchorCode = $ar_resc['CODE'] ? $ar_resc['CODE'] : Cutil::translit($ar_resc['NAME'],'ru',['replace_space' => '-','replace_other' => '-']);
						$ar["URL"] .= $arResult["CATEGORIES"][$i]["ANCHOR"].$anchorCode;
					}
				}

				if(!in_array(htmlspecialchars($ar["URL"]), $findUrls)) {
                                        $j++;
                                        
                                        if (in_array(IBT_EVENTS, $ar) && in_array(IB_EVENTS, $ar)) {
                                            $arEventFilter = array("IBLOCK_ID" => IB_EVENTS, "ID" => $ar["ITEM_ID"]);
                                            $arEventSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_LINK", "PROPERTY_event_type");
                                            $resEvent = CIBlockElement::GetList(array(), $arEventFilter, false, false, $arEventSelect);
                                            while ($obEvent = $resEvent->GetNextElement()) {
                                                $arProps = $obEvent->GetProperties();
                                                if ($arProps["event_type"]["VALUE"] == \PB\Main\Loc::getMessage('PB_MAIN_SEARCH_TITLE_SEMINAR') || $arProps["event_type"]["VALUE_XML_ID"] == "sem") {
                                                    $linkId = current($arProps["LINK"]["VALUE"]);
                                                    $resLink = CIBlockElement::GetProperty(IB_EVENTS_PLACE, $linkId, "sort", "asc", array("CODE" => "CITY"));
                                                    if ($arItemLink = $resLink->GetNext()) {
                                                        $cityId = $arItemLink["VALUE"];
                                                        if ($cityId != $_SESSION["REGION"]["ID"]) {
                                                            --$j;
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                        }

					$findUrls[] = htmlspecialchars($ar["URL"]);
					$arResult["CATEGORIES"][$i]["ITEMS"][] = array(
						"NAME" => $ar["TITLE"],
						"URL" => htmlspecialchars($ar["URL"]),
						"MODULE_ID" => $ar["MODULE_ID"],
						"PARAM1" => $ar["PARAM1"],
						"PARAM2" => $ar["PARAM2"],
						"ITEM_ID" => $ar["ITEM_ID"],
					);
				}
			}
		}
		unset($findUrls);
		if(!$j)
		{
			unset($arResult["CATEGORIES"][$i]);
		}
	}

	if($arParams["SHOW_OTHERS"] === "Y")
	{
		$arResult["CATEGORIES"]["others"] = array(
			"TITLE" => htmlspecialchars($arParams["CATEGORY_OTHERS_TITLE"]),
			"ITEMS" => array(),
		);

		$j = 0;
		$obTitle = new CSearch;	
		$obTitle->SetOptions(array("ERROR_ON_EMPTY_STEM" => false));
		$obTitle->Search($searchParams, $arSortSearch, $arFILTERCustom);
			$findUrls = array();
			while($ar = $obTitle->Fetch())
			{
				if($j >= $arParams["TOP_COUNT"])
				{
					//it's really hard to make it working
					break;
				}
				else
				{
					if(!in_array(htmlspecialchars($ar["URL"]), $findUrls)) {
						$j++;
						$findUrls[] = htmlspecialchars($ar["URL"]);
						$arResult["CATEGORIES"]["others"]["ITEMS"][] = array(
							"NAME" => $ar["NAME"],
							"URL" => htmlspecialchars($ar["URL"]),
							"MODULE_ID" => $ar["MODULE_ID"],
							"PARAM1" => $ar["PARAM1"],
							"PARAM2" => $ar["PARAM2"],
							"ITEM_ID" => $ar["ITEM_ID"],
						);
					}
				}
			}
			unset($findUrls);

		if(!$j)
		{
			unset($arResult["CATEGORIES"]["others"]);
		}

	}
}

$arResult["FORM_ACTION"] = htmlspecialchars(str_replace("#SITE_DIR#", SITE_DIR, $arParams["PAGE"]));

if (
    $_REQUEST["ajax_call"] === "y"
    && (
		!isset($_REQUEST["INPUT_ID"])
		|| $_REQUEST["INPUT_ID"] == $arParams["INPUT_ID"]
    )
)
{
	$APPLICATION->RestartBuffer();

	if  (!empty($query)) {
            $this->IncludeComponentTemplate('ajax');
        }
	die();
}
else
{
	$APPLICATION->AddHeadScript($this->GetPath().'/script.js');
	CUtil::InitJSCore(array('ajax'));
	$this->IncludeComponentTemplate();
}
?>
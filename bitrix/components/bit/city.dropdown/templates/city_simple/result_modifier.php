<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["HTML_TITLE"] = htmlspecialcharsBack(CComponentEngine::MakePathFromTemplate($arParams["HTML_TITLE"],Array(
	"HTML_LIST_ID" => $arParams["HTML_LIST_ID"],
	"NAME" => $arResult["SELECT"][key($arResult["SELECTED"])]["CITY"][reset($arResult["SELECTED"])]["NAME"] ?: $arResult["SELECTED"]["NAME"],
	"NAME_DATIVE" => ($arResult["SELECT"][key($arResult["SELECTED"])]["CITY"][reset($arResult["SELECTED"])]["NAME_DATIVE"] ?: $arResult["SELECT"][key($arResult["SELECTED"])]["CITY"][reset($arResult["SELECTED"])]["NAME"]) ?: ($arResult["SELECTED"]["UF_NAME_DATIVE"] ?: $arResult["SELECTED"]["NAME"])
)));

if($_SESSION['REGION']['COUNTRY_CODE'] == 'ua')
{
	$arResult['SELECT'][$_SESSION['REGION']['COUNTRY_ID']]['SORT'] = 1;
	uasort($arResult['SELECT'], function($a, $b){
		if ($a['SORT'] == $b['SORT']) {
			return 0;
		}
		return ($a['SORT'] > $b['SORT']) ? 1 : -1;
	});
}

?>
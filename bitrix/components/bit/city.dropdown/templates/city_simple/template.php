<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $APPLICATION;?>
<?=$arResult["HTML_TITLE"]?>
<div id="<?=$arParams["HTML_LIST_ID"]?>" class="city-list j-city-list">
	<div class="city-list-close j-city-close" title="Закрыть"></div>
	<div class="h2">Выберите регион:</div>
	<?foreach($arResult['SELECT'] as $country):?>
		<?if(empty($country['CITY'])) continue?>
		<div class="h3"><?=$country['NAME']?></div>
		<?
		$strong = array(
			2, // Москва
			3, // Санкт-Петербург
		);
		$country['CITY'] = array_filter($country['CITY'], function($item){
			return empty($item['UF_HIDE_IN_CITY_LIST']) ? true : false;
		});
		$chunks = array_chunk($country['CITY'], split2cols($country['CITY'], 3));
		foreach($chunks as $CITY):?>
			<div class="city-list-col">
				<?foreach($CITY as $city):?>
					<?$s = false; if(in_array($city['ID'], $strong)){ $s = true; }?>
					<?
					$href = "";
					if($arParams["LINK_LOCAL_CHOOSE"] == "Y"){
						$href = 'javascript:void(0)" class="j-new-choose';
					} else {
						$city['SUB_DOMAIN'] = in_array($city['CODE'], Array("msk")) ? "www" : $city['CODE'];
						if($arParams["LINK_WITH_SUBDOMAIN"] == "Y"){
							$arHost = explode(".", $_SESSION['REGION']['HOST']);
							end($arHost);
							$d1Key = key($arHost);
							switch($country["CODE"]){
								default: $arHost[$d1Key] = "ru";
							}
							$newHost = implode(".", $arHost);
							$href = "http://".$city['SUB_DOMAIN'].".".$newHost;
						}
						if($arParams["LINK_IN_BASEURL"] == "Y"){
							$href .= CComponentEngine::MakePathFromTemplate($arParams["SEF_FOLDER"].$arParams["SEF_URL_TEMPLATES"]["section"],Array(
								$arResult["ALIASES"]["CITY_CODE"] => $city['CODE']
							));
						}
					}
					?>

					<p><?=$s==true?'<b>':'';?><a item-id="<?=$city['ID'];?>" href="<?=$href?>"><?=$city['NAME'];?></a><?=$s==true?'</b>':'';?></p>
					<?=47==$city['ID'] ? '<p>&nbsp;</p>' : '';?>
				<?endforeach;?>
			</div>
		<?endforeach;?>
	<?endforeach;?>
</div>

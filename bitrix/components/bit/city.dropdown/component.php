<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);

$arParams["SECTION_URL"]=trim($arParams["SECTION_URL"]);

$arParams["TOP_DEPTH"] = intval($arParams["TOP_DEPTH"]);
if($arParams["TOP_DEPTH"] <= 0)
	$arParams["TOP_DEPTH"] = 2;
$arParams["COUNT_ELEMENTS"] = $arParams["COUNT_ELEMENTS"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default

$arParams["HIDE_EMPTY_CITY"] = $arParams["HIDE_EMPTY_CITY"]?:"N";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global $$arParams["FILTER_NAME"];
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
}

if(strlen($arParams["OFFICE_FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["OFFICE_FILTER_NAME"]))
{
	$officeFilter = array();
}
else
{
	global $$arParams["OFFICE_FILTER_NAME"];
	$officeFilter = ${$arParams["OFFICE_FILTER_NAME"]};
	if(!is_array($officeFilter))
		$officeFilter = array();
}

$arDefaultUrlTemplates404 = array(
	"sections" => "",
	"section" => "#SECTION_ID#/",
	"element" => "#SECTION_ID#/#ELEMENT_ID#/",
	"compare" => "compare.php?action=COMPARE",
);
$arDefaultVariableAliases404 = array(
	"CITY_CODE" => "CITY_CODE"
);


$arResult["SELECT"]=array();

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, array($arrFilter, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation)))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	/*********************************************************************/

	// Получаем страны из инфоблока, в цикле к каждой стране получаем города
	$arFilter1 = Array(
		'IBLOCK_TYPE' => $arParams["IBLOCK_TYPE"],
		'IBLOCK_ID' => $arParams["IBLOCK_ID"],
		'GLOBAL_ACTIVE'=>'Y',
		'DEPTH_LEVEL' => 1
	);
	$db_list1 = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter1);
	$active_flag = false;
	while($res1 = $db_list1->GetNext()) {

		// Собираем страны для формы "Выбрать другой город"
		$arResult['SELECT'][$res1['ID']]['NAME'] = $res1['NAME'];
		$arResult['SELECT'][$res1['ID']]['CODE'] = $res1['CODE'];
		$arResult['SELECT'][$res1['ID']]['SORT'] = $res1['SORT'];

		// Получаем города из инфоблока
		$arFilter2 = Array(
			'IBLOCK_TYPE' => $arParams["IBLOCK_TYPE"],
			'IBLOCK_ID' => $arParams["IBLOCK_ID"],
			'GLOBAL_ACTIVE'=>'Y',
			'SECTION_ID' => $res1['ID'],
			'DEPTH_LEVEL' => 2
		);

		if($arParams["HIDE_EMPTY_CITY"] == "Y"){
			$arFilter2["CNT_ACTIVE"] = "Y";
		}
		$bIncCnt = $arParams["HIDE_EMPTY_CITY"] == "Y" ? true : false;

		$db_list2 = CIBlockSection::GetList(Array("SORT"=>"ASC"), $arFilter2, $bIncCnt, Array("ID", "CODE", "NAME", "UF_NAME_DATIVE", "UF_HIDE_IN_CITY_LIST"));
		while($cty = $db_list2->GetNextElement()) {
			$res2 = $cty->GetFields();

			if($arParams["HIDE_EMPTY_CITY"] == "Y" && $res2["ELEMENT_CNT"] == 0){
				continue;
			}

			if(!empty($arParams["OFFICE_FILTER_NAME"])){
				$arFilter3["ACTIVE"] = "Y";
				$arFilter3["ACTIVE_DATE"] = "Y";
				$arFilter3["SECTION_ID"] = $res2["ID"];
				$cntOffice = CIBlockElement::GetList(Array("SORT"=>"ASC"), array_merge($arFilter2, $arFilter3, $officeFilter), Array());
				if($cntOffice == 0){
					if($res2["CODE"] == $_SESSION["REGION"]["CODE"]){
						$arResult["SELECTED"] = $res2;
					}
					continue;
				}
			}

			// Собираем города для формы "Выбрать другой город"
			$arResult['SELECT'][$res1['ID']]['CITY'][$res2['ID']]['ID'] = $res2['ID'];
			$arResult['SELECT'][$res1['ID']]['CITY'][$res2['ID']]['NAME'] = $res2['NAME'];
			$arResult['SELECT'][$res1['ID']]['CITY'][$res2['ID']]['CODE'] = $res2['CODE'];
			$arResult['SELECT'][$res1['ID']]['CITY'][$res2['ID']]['NAME_DATIVE'] = $res2['UF_NAME_DATIVE'];
			$arResult['SELECT'][$res1['ID']]['CITY'][$res2['ID']]['UF_HIDE_IN_CITY_LIST'] = $res2['UF_HIDE_IN_CITY_LIST'];

			if(!$active_flag && $arParams["SEF_MODE"] == "Y"){
				$arVariables = array();
				$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
				$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);
				CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);
				$arResult["ALIASES"] = $arVariableAliases;

				if($arVariables[$arVariableAliases["CITY_CODE"]] == $res2['CODE']){
					$arResult['SELECT'][$res1['ID']]['CITY'][$res2['ID']]["SELECTED"] = "Y";
					$arResult["SELECTED"][$res1['ID']] = $res2['ID'];
					$active_flag = true;
				}
			}

		}
	}
	if(!$active_flag){
		foreach($arResult['SELECT'] AS $cityKey => $cityItem){
			foreach($cityItem["CITY"] AS $officeKey => $officeItem){
				if(!$active_flag && ($officeItem["CODE"] == $_SESSION["REGION"]["CODE"])){
					$arResult['SELECT'][$cityKey]['CITY'][$officeKey]["SELECTED"] = "Y";
					$arResult["SELECTED"][$cityKey] = $officeKey;
					$active_flag = true;
					break;
				}
			}
		}
	}

	/*********************************************************************/

	$this->SetResultCacheKeys(array(
		"SELECT",
	));

	$this->IncludeComponentTemplate();
}

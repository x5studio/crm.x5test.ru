<?
$MESS ['CACHE_TYPE_TIP'] = "<i>Automatisch</i>: Der Cache ist gьltig gemдЯ Definition in den Cache-Einstellungen;<br /><i>Cache</i>: immer cachen fьr den Zeitraum, der im nдchsten Feld definiert wird;<br /><i>Nicht cachen</i>: es wird kein Caching ausgefьhrt.";
$MESS ['DISPLAY_PANEL_TIP'] = "Wenn diese Option aktiv ist, werden Buttons im Modus \"Inhalte\" auf dem administrativen Pannel und in der Zusammenstellung des Bearbeitungsbereichs der Komponente angezeigt.";
$MESS ['ADD_SECTIONS_CHAIN_TIP'] = "Wenn diese Option aktiv ist und im Informationsblock Bereiche erstellt sind, werden bei einem Bereichsьbergang ihre Namen zur Navigationskette hinzugefьgt.";
$MESS ['IBLOCK_ID_TIP'] = "Wдhlen Sie ein Informationsblock aus. Wenn der Punkt (andere)-> gewдhlt ist, mьssen Sie im Feld daneben die ID des Informationsblocks eingeben.";
$MESS ['IBLOCK_TYPE_TIP'] = "Wдhlen aus der angezeigten Liste einen Informationsblocktyp. Nachdem Sie <b><i>ok</i></b> gedrьckt haben, werden Informationsblцcke vom ausgewдhlten Typ geladen.";
$MESS ['TOP_DEPTH_TIP'] = "Die Zahl bestimmt die Menge der angezeigten Bereichsebenen des Katalogs.";
$MESS ['COUNT_ELEMENTS_TIP'] = "Anzahl der Bereichselemente anzeigen";
$MESS ['CACHE_TIME_TIP'] = "Geben Sie die Cache-Laufzeit in Sekunden an.";
$MESS ['SECTION_URL_TIP'] = "Geben Sie die URL zur Seite mit dem Bereichsinhalt an.";
$MESS ['SECTION_ID_TIP'] = "Das Feld enthдlt den Code in dem die Bereichs ID ьbergeben wird. Als Standard enthдlt das Feld ={\$_REQUEST[\"SECTION_ID\"]}.";
?>
<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
Processing of received parameters
 *************************************************************************/

global $obOfficeFilter;

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global $$arParams["FILTER_NAME"];
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
}

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

if(!isset($arParams["COMPONENT_MARKER"]))
	$arParams["COMPONENT_MARKER"] = rand(99, 999);

$arParams["CACHE_FILTER"]=$arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

/*************************************************************************
Work with cache
 *************************************************************************/

if($this->StartResultCache(false, array($arrFilter, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()))))
{

	$contMng = ContactManager::getInstance();

	$arResult['ITEMS'] = $contMng->getOfficeList($obOfficeFilter, $arrFilter);

	foreach($arResult["ITEMS"] as $keyItem => $arItem) {
		if($arItem["CODE"] == "lomonosovskaya") {
			$arItem[$keyItem]["NAME"] = "м. Ломоносовская	(центр. офис)";
		}
	}

	/* handler */
	$CNT = count($arResult["ITEMS"])-2;
	if($CNT > 2) {
		$arResult['OFFICE_TEXT'] = 'Ещё '.declOfNum($CNT, array('офис', 'офиса', 'офисов'));
	}

// Если указан офис в параметре STOP_RANDOM, отключаем рандом жд.
	if($arParams['STOP_RANDOM'] != 'all')
	{
		$STOP_RANDOM = 'N';
		foreach($arResult["ITEMS"] as $OFFICE) {
			if(is_array($arParams['STOP_RANDOM'])) {
				if(in_array($OFFICE['IBLOCK_SECTION_ID'], $arParams['STOP_RANDOM'])) {
					$STOP_RANDOM = 'Y'; break;
				}
			}
			if(intval($OFFICE['PROPERTIES']['POWER']['VALUE']) > 0) {
				$_sorts[] = $OFFICE['PROPERTIES']['POWER']['VALUE'];
			}
		}
		if($STOP_RANDOM == 'N' && count($_sorts) <= 0) {
			$STOP_RANDOM = 'Y';
		}
	}
	else
	{
		$STOP_RANDOM = 'Y';
	}

// Если все офисы в городе имеют один вес, отключаем рандом.
	/*
	if($STOP_RANDOM == 'N') {
		$_offices = array_unique($_sorts);
		if(count($_offices) == 1) {
			$STOP_RANDOM = 'Y';
		}
	}
	*/

// Получаем случайный офис, с учетом веса
// т.е. если у офиса вес больше других, он будет показываться чаще других
	if(count($arResult["ITEMS"]) >= 2 && $STOP_RANDOM == 'N') {

		/**
		 * Получаем случайный офис, с учетом веса
		 * @param $nodes
		 * @return mixed
		 */
		function generateWeighedRandomValue($nodes) {
			$weights = array_values($nodes);
			$values = array_keys($nodes);
			$count = count($values);
			$i = 0;
			$n = 0;
			$num = mt_rand(0, array_sum($weights));
			while($i < $count) {
				$n += $weights[$i];
				if($n >= $num) {
					break;
				}
				$i++;
			}
			return $values[$i];
		}

		/**
		 * Фильтр дублей офисов
		 * @param $arr
		 * @param $id_compare
		 *
		 * @return mixed
		 */
		function second_office($arr, $id_compare) {
			$id = generateWeighedRandomValue($arr);
			if($id == $id_compare) {
				$id = second_office($arr, $id_compare);
			}
			return $id;
		}

		// START
		$reArr = $arr = array();
		foreach($arResult["ITEMS"] as $arItem) {
			$reArr[$arItem['ID']] = $arItem;
			$arr[$arItem['ID']] = $arItem['PROPERTIES']['POWER']['VALUE'] ? $arItem['PROPERTIES']['POWER']['VALUE'] : 1000;
		}
		$arResult['ITEMS'] = $reArr;

		$first_office['ID'] = generateWeighedRandomValue($arr);
		$second_office['ID'] = second_office($arr, $first_office['ID']);

		$first_office = $arResult['ITEMS'][$first_office['ID']];
		$second_office = $arResult['ITEMS'][$second_office['ID']];

		unset($arResult['ITEMS'][$first_office['ID']]);
		unset($arResult['ITEMS'][$second_office['ID']]);

		$office_block = array(
			$first_office,
			$second_office
		);

		$sort = array();
		array_push($sort, $first_office['SORT']);
		array_push($sort, $second_office['SORT']);

		array_unshift($arResult['ITEMS'], $second_office);
		array_unshift($arResult['ITEMS'], $first_office);

		// ПРОВЕРКА ФОРМУЛЫ - НЕ УДАЛЯТЬ!
		if($_REQUEST['STAT_OFFICE'] == 'Y') {
			foreach($arResult["ITEMS"] as $arItem) {
				$names[$arItem['ID']] = $arItem['NAME'];
			}

			for ($i = 0; $i < 10000; ++$i) {
				$values[$names[generateWeighedRandomValue($arr)]]++;
			}
			arsort($values);
			__($values);
		}
	}


	$this->IncludeComponentTemplate();

}
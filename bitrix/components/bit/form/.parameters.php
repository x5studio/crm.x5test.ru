<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"FORM_NAME" => array(
			"PARENT" => "BASE",
			"NAME" => "атрибут name формы",
			"TYPE" => "STRING",
			"DEFAULT" => "webform1",
		),
		"TITLE" => array(
			"PARENT" => "BASE",
			"NAME" => "Заголовок формы",
			"TYPE" => "STRING",
		),
		"DESCRIPTION" => array(
			"PARENT" => "BASE",
			"NAME" => "Описание формы",
			"TYPE" => "STRING",
		),
        	"BUTTON_NAME" => array(
			"PARENT" => "BASE",
			"NAME" => "Текст кнопки",
			"TYPE" => "STRING",
		),
		"TYPE_CEVENT" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип почтового события",
			"TYPE" => "STRING",
		),
        	"TYPE_CEVENT" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип почтового события",
			"TYPE" => "STRING",
		),
		"MESSAGE_TRUE" => array(
			"PARENT" => "BASE",
			"NAME" => "Сообщение об успешной отправки",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"MESSAGE_FALSE" => array(
			"PARENT" => "BASE",
			"NAME" => "Сообщение об ошибки отправки",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"SHOW_LINE" => array(
			"PARENT" => "BASE",
			"NAME" => "Показывать линюю снизу",
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
	),
);
?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?/*
    [FORM_NAME] => webform1
    [TITLE] => Бесплатный расчет стоимости проекта
    [DESCRIPTION] => В течение 2 часов в рабочее время
    [BUTTON_NAME] => отправить заявку
    [TYPE_CEVENT] => ----
    [MESSAGE_TRUE] => ----
    [MESSAGE_FALSE] => ---
    [CACHE_TYPE] => A
    [~FORM_NAME] => webform1
    [~TITLE] => Бесплатный расчет стоимости проекта
    [~DESCRIPTION] => В течение 2 часов в рабочее время
    [~BUTTON_NAME] => отправить заявку
    [~TYPE_CEVENT] => ----
    [~MESSAGE_TRUE] => ----
    [~MESSAGE_FALSE] => ---
    [~CACHE_TYPE] => A
    [SHOW_LINE]=>Y
*/?>






<div class="center formcall qwerty">
	<div class="form_title"><?=$arParams[TITLE]?></div>
	<div class="form_description"><?=$arParams[DESCRIPTION]?></div>
	<form name="<?=$arParams[FORM_NAME]?>" >
		<div class='form_input'>
            <div class="error">Не указано имя</div>
			<input type="text" placeholder="Имя*" name="name"/>
		</div>
		<div class='form_input'>
            <div class="error">Не указан телефон</div>
			<input type="text" placeholder="Телефон*" name="phone"/>
		</div>
		<div class='form_input'>
			<button class='btn'><?=$arParams[BUTTON_NAME]?></button>
		</div>
		<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("include_areas/privacy_policy.php"),Array(),Array("MODE"=>"html"));?>
	</form>
</div>
<?if($arParams[SHOW_LINE]=="Y"){?>
	<div style='border-bottom:1px solid #454545'></div>

<?}?>
<script>
    $(document).ready(function(){
        $('form[name=<?=$arParams[FORM_NAME]?>] input[type=text]').click(function(){
            $(this).removeClass("error_input").prev().hide();
        });
        $('form[name=<?=$arParams[FORM_NAME]?>]').submit(function(){
            
            if($("input[name=name]",this).val()=='') $("input[name=name]",this).addClass("error_input").prev().css("display","inline-block");
            if($("input[name=phone]",this).val()=='') $("input[name=phone]",this).addClass("error_input").prev().css("display","inline-block");
            
            if($("input[name=name]",this).val()!='' && $("input[name=phone]",this).val()!='')
                $.post("/.ajax/form.php?name=form5",$('form[name=<?=$arParams[FORM_NAME]?>]').serialize(),function(){
                    endform("менеджер скоро с вами свяжется");
                });
            
            return false;
        });
    });
</script>
$(function() {

	function initializeGeoConfirm()
	{
		new GeoConfirm({
			root: '#geo_confirm',
			confirm: '.confirm',
			close: '.close',
			select: '#header-city-list-confirm'
		});
	}

	if(typeof frameCacheVars == 'undefined')
	{
		setTimeout(function(){
			initializeGeoConfirm();
		}, 500);
	}

	BX.addCustomEvent("onFrameDataReceived", function(arEventParams){
		setTimeout(function(){
			initializeGeoConfirm();
		}, 300);
	});

});

function GeoConfirm(p) {

	var self = this;
	self.root = $(p.root);
	self.confirm = self.root.find(p.confirm);
	self.close = self.root.find(p.close);
	self.select = $(p.select);
	self.isSmartConfirm = self.root.data('smartConfirm');

	self.root.slideDown('fast');

	self.confirm.click(function(e) {
		e.preventDefault();
		self.send({
			'TYPE': 'CONFIRM',
			'CITY': 'CURRENT'
		}, function(){});
		self.root.hide('fast');
	});

	self.close.click(function(e) {
		e.preventDefault();
		self.send({
			'TYPE': 'CONFIRM',
			'CITY': 'STAY_HERE'
		}, function(){});
		self.root.hide('fast');
	});

	self.select.find('a').click(function(e) {
		e.preventDefault();
		var a = $(this);
		self.send({
			'TYPE': 'CONFIRM',
			'CITY': a.attr('item-id')
		}, function(xhr) {
			self.select.hide('fast', function() {
				self.root.hide('fast');
				if(self.isSmartConfirm)
				{
					window.location.reload();
				}
				else
				{
					window.location.href = a.attr('href');
				}
			});
		});
	});

	self.send = function(data, callback) {
		$.ajax({
			type: 'POST',
			url: "/bitrix/modules/geo/geo/ajax.php",
			data: data,
			success: function (content) {
				try {
					if(content.status == 'ok') {
						if(!self.isSmartConfirm)
						{
							window.location.href = content.path + window.location.pathname + window.location.search;
						}
					}
				} catch (e) {
					//console.log(content);
					//console.log(e);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(textStatus);
			},
			beforeSend: function () {},
			complete: function (xhr) {
				callback(xhr);
			}
		});
	}
}
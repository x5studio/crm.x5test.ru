<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div id="geo-confirm-wrapp">
	<div id="geo_confirm" <?if(COption::GetOptionString('geo', 'SMART_REDIRECT', 'N') == 'Y'){?>data-smart-confirm="true"<?}?>>
		<div class="inner-shadow"></div>
		<div class="close j-close"></div>
		<p>Ваш регион определился как:<br>
		<b class="name"><?=$_SESSION['GEOIP']['city_name']?></b></p>
		<button class="btn btn-mini confirm">Верно</button><span>или</span>

		<!-- Выпадающий список городов -->
		<?
		global $cityFilter;
		$cityFilter = Array(
			"CITY_CODE" => $_SESSION["REGION"]["CODE"],
		);
		$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_simple",
			Array(
				"IBLOCK_TYPE" => IBT_CONTACTS,
				"IBLOCK_ID" => IB_CONTACTS,
				"SECTION_CODE" => "",
				"SECTION_URL" => "",
				"COUNT_ELEMENTS" => "Y",
				"TOP_DEPTH" => "1",
				"SECTION_FIELDS" => "",
				"FILTER_NAME" => "cityFilter",
				"SECTION_USER_FIELDS" => "",
				"ADD_SECTIONS_CHAIN" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_NOTES" => "",
				"CACHE_GROUPS" => "N",
				"CACHE_FILTER" => "Y",
				"LINK_IN_BASEURL" => "Y",
				"LINK_WITH_SUBDOMAIN" => "Y",
				"SEF_MODE" => "N",
				"SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
				"SEF_URL_TEMPLATES" => array(),
				"HTML_LIST_ID" => "header-city-list-confirm",
				"HTML_TITLE" => '<a href="##HTML_LIST_ID#" class="header-city j-city"><strong>выбрать регион</strong></a>',
				"SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
				"COUNTRY_CODE" => $_SESSION['GEOIP']['country_code'],
				'LINK_LOCAL_CHOOSE' => COption::GetOptionString('geo', 'SMART_REDIRECT', 'N'),
			), false,
			Array("HIDE_ICONS"=>"Y")
		);
		?>
	</div>
</div>
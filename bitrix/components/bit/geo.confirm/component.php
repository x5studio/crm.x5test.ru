<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentName */
/** @var string $componentPath */
/** @var string $componentTemplate */
/** @var string $parentComponentName */
/** @var string $parentComponentPath */
/** @var string $parentComponentTemplate */
Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("geo_confirm");

// Если человек не подтвердил город и не переходил в другой, то просим потвердить определившийся город
if(!$APPLICATION->get_cookie("CONFIRM") && empty($_SESSION['EVENTS']['SET_COOKIE']['CONFIRM']) && $_SESSION['SHOW_FREE_REGION'] != 'Y' && $_SESSION['CLOSE_CONFIRM_POPUP'] != "Y") {
	$this->IncludeComponentTemplate();
} else {
	$this->IncludeComponentTemplate('empty');
}
Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("geo_confirm", null);
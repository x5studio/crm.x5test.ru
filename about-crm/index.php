<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
$APPLICATION->SetPageProperty("description", "CRM — это система управления взаимоотношениями с клиентами, направленная на автоматизацию взаимодействия с ними, повышение уровня продаж, оптимизацию маркетинговых усилий, а также улучшение качества обслуживания.");
$APPLICATION->SetPageProperty("title", "Что такое CRM");

?><section>
<div class="case_header about-case-header">
	<div class="container clearfix">
		<div class="row margin-0-xs">
			<h1><?=$APPLICATION->ShowTitle();?></h1>
			<div class="row clearfix">
				<p class="col-sm-8 col-xs-12">
					 CRM — это система управления взаимоотношениями с клиентами, направленная на автоматизацию взаимодействия с ними, повышение уровня продаж, оптимизацию маркетинговых усилий, а также улучшение качества обслуживания. Это достигается с помощью упорядоченного хранения подробной информации о клиентах, истории взаимоотношений с ними, формирования и оптимизация бизнес-процессов и дальнейшего анализа результатов.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row margin-0-xs">
		<div class="about-crm clearfix">
			<h2>Основные функции CRM</h2>
			<div class="row clearfix">
				<div class="col-sm-8 col-xs-12">
					<p>
						 В состав CRM могут входить различные компоненты. Чем их больше — тем более полнофункциональным является решение. Среди основных функция можно выделить:
					</p>
				</div>
			</div>
			<div class="row main-functions-wrap clearfix">
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Управление контактами (информацией о клиентах)
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Управление продажами
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Управление задачами и временем
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Поддержка и обслуживание клиентов
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Управление маркетингом
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Отчетность в различных разрезах
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Интеграция с внешними системами
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Управление электронной торговлей
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="single-main-function">
						 Работа с мобильных устройств
					</div>
				</div>
			</div>
			<h2>Когда нужна CRM?</h2>
			<div class="row clearfix">
				<div class="reasons_crm_wrap col-sm-8 col-xs-12">
					<div class="single_reason">
 <span class="reason_title">Нет единой клиентской базы</span>
						<p>
							 Все данные о клиентах (а также о поставщиках, партнёрах и других контрагентах компании) не структурированы и хранятся в разных источниках. Данные невозможно получить оперативно, они теряются при увольнении менеджеров, а также отсутствует возможность их совместного анализа.
						</p>
					</div>
					<div class="single_reason">
 <span class="reason_title">История общения с клиентами не структурирована или не хранится</span>
						<p>
							 История общения, договоренностей и взаимодействия с клиентом (коммерческие предложения, договоры) не структурирована или не хранится вообще. Нет возможности поднять переписку для решения возникших конфликтов или организации дополнительных продаж.
						</p>
					</div>
					<div class="single_reason">
 <span class="reason_title">Бизнес-процессы не автоматизированы и не исполняются</span>
						<p>
							 На исполнение бизнес-процессов затрачивается очень много ресурсов. Бизнес-процессы периодически не исполняются, что приводит к отсутствию роста продаж или даже снижению его уровня.
						</p>
					</div>
					<div class="single_reason">
 <span class="reason_title">Необходим инструмент анализа и прогнозирования продаж</span>
						<p>
							 Компании нужны инструменты для активной работы с продажами — анализа клиентской базы, построения отчетности по разным показателям, а также составления прогнозов и планов по продажам.
						</p>
					</div>
					<div class="single_reason">
 <span class="reason_title">Отсутствие системы, большое количество рутины</span>
						<p>
							 Сотрудники тратят очень много времени на простые и рутинные задачи, договоры и коммерческие предложения составляются долго, а руководитель вынужден тратить существенную часть своего времени на контроль работы сотрудников.
						</p>
					</div>
				</div>
			</div>
			<h2>Соберите всю информацию воедино с помощью CRM</h2>
			<div class="row collecting_info_with_crm clearfix">
				<div class="col-sm-6 col-xs-12">
					<div class="single_collection_feature">
						 Структурируйте и соберите в единую базу всю информацию о ваших контрагентах
					</div>
					<div class="single_collection_feature">
						 Сведите к минимуму задержки и потерю информации при передачи между сотрудниками и подразделениями компании
					</div>
					<div class="single_collection_feature">
						 Сохраняйте всю историю взаимоотношений с клиентами и поставщиками, фиксируйте переговоры и договоренности
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<div class="single_collection_feature">
						 Получите инструменты для планирования и управления маркетингом в компании
					</div>
					<div class="single_collection_feature">
						 Стройте отчетность в разных разрезах и анализируйте показатели компании, чтобы своевременно принимать правильные управленческие решения
					</div>
					<div class="single_collection_feature">
						 Формируйте собственную «базу знаний» внутри компании
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"launching_product",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "20",
		"IBLOCK_TYPE" => "main",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "6",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "LINK",
            1 => "",
        ),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?> <br><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
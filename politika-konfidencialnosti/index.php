<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Политика конфинденциальности");
?><div class="container">
<h1>Политика конфиденциальности</h1>
<br>
<p>Мы прилагаем все достаточные и необходимые технические и организационные меры для защиты персональной информации Пользователей сайта от несанкционированного либо неправомерного использования и распространения.</p><br>
<p>Мы соблюдаем права Пользователей сайта на получение информации, касающейся обработки их персональных данных, в соответствии с Федеральными законами РФ «О персональных данных» (от 27.07.2006 г. № 152-ФЗ).</p><br>
<p>Мы обеспечиваем неограниченный доступ к документам, определяющим политику Компании в отношении обработки персональных данных, к сведениям о реализуемых требованиях к защите персональных данных.</p> <br>
<p>С запросом таких документов, а так же по всем вопросам использования и обработки, отзыва разрешения на обработку персональных данных и их уничтожение вы можете обратиться по адресу <a href="mailto:bitweb@1cbit.ru">bitweb@1cbit.ru</a><p>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Центр аккредитации МГМУ им. И. М. Сеченова | Портфолио веб-студии Первый БИТ");
$APPLICATION->SetPageProperty("keywords", "сайт недорого, разработка сайта, создание сайта, сайт битрикс, битрикс");
$APPLICATION->SetPageProperty("description", "Разработка сайта центра аккредитации медицинского персонала на базе МГМУ им. И. М. Сеченова");
$APPLICATION->SetAdditionalCSS("/clients/servisy-dlya-edinogo-tsentra-akkreditatsii-na-baze-pervogo-mgmu-im-i-m-sechenova/style.css");
\Bitrix\Main\Page\Asset::getInstance()->addJs(Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPageDirectory() . "/js/jquery.animateNumber.min.js", true);
?>
<div class="p-page">

	<div class="p-header">
	<img src="images/logo.jpg" class="p-logo">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="p-title">Сервисы для Единого Центра Аккредитации на базе Первого МГМУ им. И.М. Сеченова</h1>
						<i class="i-mouse"></i>
					</div>
				</div>
			</div>
	</div>

	 <section class="client">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="subtitle">Заказчик</h2>
					<ul class="client-list">
						<li class="client-item">– Российчкий медицинский ВУЗ №1, история которого началась в 1759 году.</li>
						<li class="client-item">– На базе «Первого МГМУ» им. И.М. Сеченова был запущен «Единый Центр Аккредитации Медицинского Персонала» в Российской Федерации.</li>
					</ul>
				</div>
			</div>
		</div>
	 </section>

	 <section class="task bg-color">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="subtitle">Задачи</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<ul class="task-list">
						<li class="task-item">– Разработать официальный сайт «Центра Аккредитации Медицинского Персонала».</li>
					</ul>
				</div>
				<div class="col-sm-6 col-xs-12">
					<ul class="task-list">
						<li class="task-item">– Разработка системы тестирования для всероссийского центра аккредитации медицинских работников.</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="task-img-wrapper">
	 				<img src="images/t.png" class="img-responsive task-img">
				</div>
			</div>
		</div>
	 </section>

	 <section class="counters">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<div class="counter">
	 					<span class="counter-val cv1">100+</span>
						<p class="counter-descr">
							Разработанных прототипов
						</p>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<div class="counter bg-color2">
	 					<span class="counter-val cv2">3</span>
						<p class="counter-descr">
							Разработанных <br>дизайн-концепции
						</p>
					</div>
				</div>
			</div>
			<div class="row shadow">
				<div class="col-xs-12">
					<div class="counters-descr1">
						<p>
							Был выбран программный комплекс Moodle в качестве платформы для разработки системы тестировнаия.
						</p>
					</div>
				</div>
			</div>
		</div>
	 </section>

	 <section class="about">
		<div class="about-sect1">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="about-descr">
							<h2 class="subtitle">О проекте</h2>
							<p>
								Был разработан официальный сайт «Центра Аккредитации на базе Первого Московского государственного медицинского университета им И.М. Сеченова».
							</p>
						</div>
						<div class="about-img-wrapper">
	 						<img src="images/a1.jpg" class="img-responsive about-img">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="about-sect2 bg-color">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="about-img-wrapper">
	 						<img src="images/a2.jpg" class="img-responsive about-img1">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="about-sect3">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="about-descr">
							<h2 class="subtitle2">Система личных кабинетов</h2>
							<p>
								Были реализованы системы личных кабинетов для специалистов, организаций, кабинетов ВУЗов, которым необходимо пройти аккредитацию, продавать групповые заявки и отслеживать результаты и самостоятельно выставлять оценки за локальные этапы.
							</p>
						</div>
						<div class="about-img-wrapper1">
	 						<img src="images/a3.png" class="img-responsive about-img2"> <img src="images/a4.png" class="img-responsive about-img3">
						</div>
					</div>
				</div>
			</div>
		</div>
	 </section>

	 <section class="bg-image"></section>

	 <section class="sert">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="sert-descr">
						<h2 class="subtitle2">Система сертификации</h2>
						<p>
							Разработано программное обеспечение для реализации процесса автоматизированной аккредитации медицинского персонала на базе специализированного программного обеспечения.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<div class="sert-img-wrapper">
	 					<img src="images/s1.jpg" class="img-responsive sert-img">
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<div class="sert-img-wrapper">
	 					<img src="images/s2.jpg" class="img-responsive sert-img">
					</div>
				</div>
			</div>
		</div>
	 </section>

	 <section class="terminal bg-color">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="terminal-descr">
						<h2 class="subtitle1">Терминалы записи в электронную очередь</h2>
						<p>
							Было разработано программное обеспечение и интерфейсы для терминалов записи в электронную очередь.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="terminal-img-wrapper">
	 					<img src="images/te1.jpg" class="img-responsive terminal-img">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<div class="terminal-img-wrapper1">
	 					<img src="images/te2.jpg" class="img-responsive terminal-img1"> <img src="images/te3.jpg" class="img-responsive terminal-img1">
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<div class="terminal-img-wrapper1">
	 					<img src="images/te4.jpg" class="img-responsive terminal-img1"> <img src="images/te5.jpg" class="img-responsive terminal-img1">
					</div>
				</div>
			</div>
		</div>
	 </section>

	 <section class="result">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="title-b">Результаты</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="res-list">
						<li class="res-item">
							<i class="res-icon1"></i>
							<div class="res-txt">
								<p>
									Запущен официальный сайт Центра Аккредитации.
								</p>
							</div>
	 					</li>
						<li class="res-item">
							<i class="res-icon2"></i>
							<div class="res-txt">
								<p>
									Личные кабинеты специалистов позволяют дистанционно подать заявку на аккредитацию, отслеживать статусы этой заявки, узнать результаты тестирования, подать апелляцию.
								</p>
							</div>
	 					</li>
						<li class="res-item">
							<i class="res-icon3"></i>
							<div class="res-txt">
								<p>
									Личные кабинеты ВУЗов позволяют дистанционно подать групповую заявку на аккредитацию, отслеживать статусы этой заявки, узнать результаты тестирования, проводить аккредитацию на местах и вносить оценки.
								</p>
							</div>
	 					</li>
						<li class="res-item">
							<i class="res-icon4"></i>
							<div class="res-txt">
								<p>
									Разработаны средства коммуникации со специалистами центра.
								</p>
							</div>
	 					</li>
						<li class="res-item">
							<i class="res-icon5"></i>
							<div class="res-txt">
								<p>
									Реализованы возможности для прохождения репетиционных экзаменов на сайте Центра.
								</p>
							</div>
	 					</li>
					</ul>
				</div>
			</div>
		</div>
	</section>


	</div>





		<script>
			$(window).load(function() {
				var run = true;
				var h = ($(window).height()) * 0.85;
				$(window).scroll(function() {
					if ( $(this).scrollTop() > $('.task-img-wrapper').offset().top - h) {
						$('.task-img-wrapper').addClass('s');
				  	}
					if ( $(this).scrollTop() > $('.counters').offset().top - h) {
						if (run) {
					    	$('.cv1').animateNumber({ number: 42 });
							$('.cv2').animateNumber({ number: 270 });
							run = false;
						}
				  	}
				  	if ( $(this).scrollTop() > $('.about-img-wrapper').offset().top - h) {
				  		$('.about-img-wrapper').addClass('s');
				  	}
				  	if ( $(this).scrollTop() > $('.about-img-wrapper1').offset().top - h) {
				  		$('.about-img-wrapper1').addClass('s');
				  	}
				  	if ( $(this).scrollTop() > $('.bg-image').offset().top - h) {
				  		$('.bg-image').addClass('s');
				  	}
				  	if ( $(this).scrollTop() > $('.sert-img-wrapper').offset().top - h) {
				  		$('.sert-img-wrapper').addClass('s');
				  	}
				  	if ( $(this).scrollTop() > $('.terminal-img-wrapper').offset().top - h) {
				  		$('.terminal-img-wrapper').addClass('s');
				  	}
				  	if ( $(this).scrollTop() > $('.terminal-img-wrapper1').offset().top - h) {
				  		$('.terminal-img-wrapper1').addClass('s');
				  	}
				  	if ( $(this).scrollTop() > $('.res-list').offset().top - h) {
				  		$('.res-list').addClass('s');
				  	}

				});
			});
		</script>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

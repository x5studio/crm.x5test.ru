<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Внедрение Битрикс24 для «Группы компаний ИНСТАМ»");
$APPLICATION->SetPageProperty("keywords", "портфолио сайтов, веб студия примеры работ, качественные сайты, корпоративный портал, битрикс 24");
$APPLICATION->SetPageProperty("title", "Битрикс24 для «Группы компаний ИНСТАМ» | Портфолио веб-студии Первый БИТ");
$APPLICATION->SetTitle("Внедрение Битрикс24 для «Группы компаний ИНСТАМ»");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/block2.css" type="text/css"  rel="stylesheet" />
</head>
<body>

<div class="nb_block2">
	<div class="nb_block2_t">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="wr">
						<div class="logo"><img src="images/block2_logo.png"></div>
						<div class="title">Кейc по внедрению Битрикс24 для<br /> «Группы компаний ИНСТАМ»</div>
                        <div class="panel">
                            <a href="instam.pdf" target="_blank" class="pdf">Скачать кейс</a>
                        </div>
						<div class="icon--mouse"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block2_tt">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-7">
						<div class="z">ЗАКАЗЧИК</div>
						<p>Группа компаний ИНСТАМ — ведущий игрок в российском сегменте мобильного маркетинга, DIGITAL и лидогенерации. Компания работает на рынке более 10 лет, и является не только поставщиком услуг, но и разработчик собственных решений.</p>
						<p>www.instam.ru</p>
					</div>
					<div class="col-md-5">
						<img src="images/nb_block2_tt.png" class="fl_img1">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block2_t3">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="z">Цели и задачи проекта</div>
					</div>
					<div class="col-md-4">
						<div class="i">
							<span>01.</span>
							<img src="images/nb_block2_t31.png"  class="fl_img2">
						</div>
						<div class="t">
							Реализовать возможность полноценной работы с почтой в рамках корпоративного портала;
						</div>
					</div>
					<div class="col-md-4">
						<div class="i">
							<span>02.</span>
							<img src="images/nb_block2_t32.png"  class="fl_img2">
						</div>
						<div class="t">
							Сформировать бизнес-процесс продажи;
						</div>
					</div>
					<div class="col-md-4">
						<div class="i">
							<span>03.</span>
							<img src="images/nb_block2_t33.png"  class="fl_img2">
						</div>
						<div class="t">
							Разработать дополнительные инструменты для оптимизации внутренних процессов компании.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block2_t4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="z">Основной функционал</div>
					</div>
					<div class="col-md-6">
						<div class="zz">Почтовый клиент</div>
						<p>В рамках корпоративного портала был реализован полноценный почтовый клиент, поддерживающий синхронизацию со всеми современными почтовыми протоколами (в частности, Microsoft Exchange). Почтовый клиент обладает широкой функциональностью, позволяющей полноценно работать с электронной почтой без использования других почтовых клиентов, а также имеет ряд особенностей:</p>
						<ul>
							<li>Автоматическое распределение писем по клиентам;</li>
							<li>Возможность конвертации письма в задачу или в лид;</li>
							<li>Отчеты по входящим письмам в рамках каждого клиента;</li>
							<li>Использование нескольких почтовых аккаунтов в рамках одного пользователя;</li>
							<li>Поддержка фильтров и другого функционала почтовых программ.</li>
						</ul>
					</div>
					<div class="col-md-6">
						<img src="images/nb_block2_t41.png"  class="fl_img3">
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="nb_block2_bg">
	<div class="nb_block2_t4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="zz">Бизнес-процесс продажи</div>
						<p><strong>Специалисты компании «Первый БИТ» в рамках корпоративного портала реализовали четкий бизнес-процесс ведения сделки от лида до контракта:</strong></p>
					</div>
					<div class="col-md-6">
						<ul>
							<li>Разработана карточка клиента с уникальным набором полей и поддержкой автоматического заполнения данных по ИНН (интеграция с сервисом DaData);</li>
							<li>Внедрена цепочка шагов, по которой движется менеджер во время проработки сделки, причем для каждой услуги реализован свой конкретный бизнес-процесс;</li>
							<li>Автоматическое создание задачи для менеджера при переходе на следующий этап воронки;</li>
						</ul>
					</div>
					<div class="col-md-6">
						<ul>
							<li>Автоматическое завершение задач при движении по воронке сделки — менеджер не может завершить задачу самостоятельно без перехода на следующий шаг воронки;</li>
							<li>Автоматический расчет стоимости коммерческого предложения (КП) в зависимости от различных условий;</li>
							<li>Автоматическое формирование КП по заданному шаблону.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block2_t4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="bg">
						<div class="col-md-7">
							<p>Отправка КП клиенту осуществляется прямо из интерфейса корпоративного портала Битрикс24. После отправки КП происходит автоматический переход на следующий этап воронки сделки.</p>
							<p>В случае положительной реакции клиента на коммерческое предложение и согласия на сотрудничество с компанией происходит автоматическое формирование договора исходя из данных клиента и выставленного ранее КП.</p>
						</div>
						<div class="col-md-5 r">
							<img src="images/nb_block2_t42.png"  class="fl_img4">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block2_t4 fl">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="images/nb_block2_t43.png" class="l fl_img5">
					<p class="p18">Кроме того, был реализован бизнес-процесс согласования договора — внесения в него корректировок, а также прикрепления новых версий сотрудниками компании. В корпоративном портале можно также отметить признак получения оригинала по данному договору.</p>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="nb_block2_t4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<div class="zz">Бизнес-процесс продажи</div>
						<div class="img_f"><img src="images/nb_block2_t44.png"  class="fl_img6"></div>
						<p>В качестве дополнительного функционала была разработана возможность автоматического формирования и печати конвертов для отправки бумажной корреспонденции, а также ее учета.</p>
					</div>
					<div class="col-md-6">
						<div class="zz">Настройка прав</div>
						<div class="img_f"><img src="images/nb_block2_t45.png"  class="fl_img7"></div>
						<p>Важной доработкой корпоративного портала для компании Instam явилось разграничение прав на уровне полей объектов — это позволяет регулировать доступ сотрудников компании к различным типам информации</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block2_tw">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="z">Результаты проекта</div>
					<div class="padd">
						Повысилась скорость работы сотрудников за счет выполнения  задач в рамках одного интерфейса.
						<span class="ico"></span>
					</div>
					<div class="padd">
						Появилась возможность построения отчетов и анализа эффективности взаимодействия менеджеров с клиентами компании.
						<span class="ico"></span>
					</div>
					<div class="padd">
						Увеличилось количество продаж за счет выстроенного бизнес-процесса сделки и четкой работы менеджеров по воронке в Битрикс24.
						<span class="ico"></span>
					</div>
					<div class="padd">
						Сократилось время выполнения ряда операций сотрудниками компании.
						<span class="ico"></span>
					</div>
					<div class="padd">
						Вырос уровень информационной безопасности за счет хранения данных в Битрикс24 и четкого разграничения доступа к ним.
						<span class="ico"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
<script>
var h = ($(window).height()) * 0.92;
$(window).scroll(function() {				
if ( $(this).scrollTop() > $('.fl_img1').offset().top - h) {   
	$('.fl_img1').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img2').offset().top - h-430) {   
	$('.fl_img2').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img3').offset().top - h) {   
	$('.fl_img3').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img4').offset().top - h) {   
	$('.fl_img4').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img5').offset().top - h) {   
	$('.fl_img5').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img6').offset().top - h) {   
	$('.fl_img6').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img7').offset().top - h) {   
	$('.fl_img7').addClass('s');					
}
});	
</script>
</div>
</body>
</html>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
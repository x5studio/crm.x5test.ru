<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Внедрение Битрикс24 для медиа-офиса правительства Дубая");
$APPLICATION->SetPageProperty("keywords", "портфолио сайтов, веб студия примеры работ, качественные сайты, корпоративный портал, битрикс 24");
$APPLICATION->SetPageProperty("title", "Корпоративный портал Битрикс 24 для медиа-офиса правительства Дубая | Портфолио веб-студии Первый БИТ");
$APPLICATION->SetTitle("Внедрение Битрикс24 для медиа-офиса правительства Дубая");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/block1.css" type="text/css"  rel="stylesheet" />
</head>
<body>

<div class="nb_block1">
	<div class="nb_block1_t">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="wr">
						<div class="logo"><img src="images/block1_logo.png"></div>
						<div class="title">«Первый БИТ» обеспечил ускорение взаимодействия<br />сотрудников в медиа-офисе правительства Дубая</div>
                        <div class="panel">
                            <a href="dubay.pdf" target="_blank" class="pdf">Скачать кейс</a>
                        </div>
						<div class="icon--mouse"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_ta">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-3">
						<div class="left">
							<img src="images/block1_ico1.png">
						</div>
						<div class="right">
							<strong>Заказчик проекта</strong>
							The Government of Dubai Media Office (GDMO)
						</div>
					</div>
					<div class="col-md-3">
						<div class="left">
							<img src="images/block1_ico2.png">
						</div>
						<div class="right">
							<strong>Отрасль</strong>
							Государственное управление
						</div>
					</div>
					<div class="col-md-3">
						<div class="left">
							<img src="images/block1_ico3.png">
						</div>
						<div class="right">
							<strong>Специфика</strong>
							СМИ, коммуникации
						</div>
					</div>
					<div class="col-md-3">
						<div class="left">
							<img src="images/block1_ico4.png">
						</div>
						<div class="right">
							<strong>Центральный офис</strong>
							ОАЭ, Дубай
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_tb">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-9">
						<strong>Масштаб деятельности</strong>
						<div class="t">Медиа-офис занимается стратегическим планированием коммуникаций для правительства Дубая. В сотрудничестве с государственными органами медиа-офис распространяет новости о деятельности правительства. GDMO также организует контакты правительства с местными и мировыми СМИ.</div>
					</div>
					<div class="col-md-3">
						<div class="b">
							<strong>Штат</strong>
							<div class="tb"><span>120</span>человек</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_tc">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<div class="bg bg1">
							<div class="z">Бизнес-задачи</div>
							<div class="a">Государственное управление</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="bg bg2">
							<div class="z">Программный продукт</div>
							<div class="a"><a href="#">Битрикс 24 Коробочное Решение</a></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="bg bg3">
							<div class="z">Автоматизировано рабочих мест</div>
							<div class="a">120 мест</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="bg bg4">
							<div class="z">Продолжительность проекта</div>
							<div class="a">2,5 месяца</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_td">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-2">
						<img src="images/nb_block1_td.png">
					</div>
					<div class="col-md-10">
						<strong>Автоматизированные структурные единицы</strong>
						<div class="t">Отдел стратегического управления, медиийные службы, отдел по обеспечению ведения бизнеса, центр подготовки информационных материалов,  отдел стратегических медийных вопросов, Пресс-клуб Дубая, офис генерального директора, отдел Brand Dubai</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_tf">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12"><div class="z">ЗАКАЗЧИК</div></div>
					<div class="col-md-6">
						<p>Медиа-офис правительства Дубая был учрежден в 2010 г. Указ о создании организации подписал вице-президент и премьер-министр ОАЭ, правитель Дубая Его Высочество шейх Мухаммед бин Рашид Аль Мактум.</p>
						<p>Главная задача GDMO — распространять материалы, отражающие процесс развития Дубая, освещать ключевые события и достижения города. Для этого офис взаимодействует с местными и международными СМИ.</p>
					</div>
					<div class="col-md-6">
						<p>GDMO управляет медиа-сделками правителя Дубая Его Высочества шейха Мухаммеда бин Рашид Аль Мактума, а также заместителя правителя Дубая Его Высочества шейха Мактума бин Мохаммеда бин Рашид аль-Мактума.</p>
						<p>Подразделения «Пресс-клуб Дубая» (DPC) и «Brand Dubai» занимаются развитием отношений правительства Дубая со средствами массовой информации.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_tg">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="z">Цели и задачи проекта</div>
					<div class="col-md-4">
						<div class="i"><img src="images/nb_block1_tg1.png"></div>
						<div class="t">Повысить скорость реагирования медиа-офиса на информационные поводы</div>
					</div>
					<div class="col-md-4">
						<div class="i"><img src="images/nb_block1_tg2.png"></div>
						<div class="t">Упростить планирование сотрудничества со СМИ и мониторинг публикаций</div>
					</div>
					<div class="col-md-4">
						<div class="i"><img src="images/nb_block1_tg3.png"></div>
						<div class="t">Упростить и ускорить обмен данными внутри организации</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_th">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<img src="images/nb_block1_th.png" class="r">
					<div class="z">Описание проекта</div>
					<p>Медиа-офис правительства Дубая за 6 лет развития вырос в организацию с несколькими подразделениями и более чем сотней сотрудников. В 2016 году руководство компании приняло решение оптимизировать внутренние коммуникации.</p>
					<p>Ранее сотрудники офиса ежедневно держали связь через несколько программ-мессенджеров и почтовый клиент. Чтобы ускорить рабочий процесс, нужна была одна программа, в которой сотрудни и получали бы информацию и задачи, а руководство –  следило за ходом работ и управляло мероприятиями.</p>
					<p>В качестве такой системы заказчик выбрал коробочное решение «Битрикс24». Программа объенияет возможности CRM, планировщика задач, системы учета рабочего времени и контроля исполнительской дисциплины. Программа позволяет в любой момент анализировать рабочий процесс и принимать решения как на операционном, так и на стратегическом уровне.</p>
					<div class="c"></div>
					<img src="images/nb_block1_th2.png" class="l">
					<p class="p20">«Первый БИТ» является «Золотым» партнером компании «Битрикс» и регулярно становится лидером продаж системы «Битрикс24». Накопленный опыт внедрений продукта позволяет использовать его возможности по максимуму. Эти факторы стали определяющими для заказчика при выборе партнера по автоматизации.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_ts bg">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<img src="images/nb_block1_ts1.png" class="fl_img1">
					</div>
					<div class="col-md-6">
						<p>На встрече специалистов компании «Первый БИТ» с заказчиком был согласован необходимый функционал системы. Модульная структура «Битрикс24» позволила внедрить только те компоненты, которые соответствовали бизнес-процессам заказчика:</p>
						<ul>
							<li>Модуль управления коммуникациями,</li>
							<li>Модуль обмена информацией</li>
							<li>Календари</li>
							<li>Хранилище данных «Битрикс Диск»</li>
							<li>Модуль управления встречами и бронирования переговорных комнат</li>
						</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_ts">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<p>Внедрение заняло всего 2,5 месяца и позволило исключить из рабочего процесса лишние операции. Изменилась сама модель взаимодействия сотрудников. Отпала необходимость в дополнительном контроле выполнения поручений с помощью звонков и писем. Руководитель ставит задачи и следит за их выполнением в «Битрикс24».</p>
						<p>Сотрудники получают уведомления о новых задачах и важных событиях автоматически. Для общения с коллегами не нужно переключаться между  приложениями – все коммуникации происходят внутри системы. Простой и удобный интерфейс программы экономит рабочее время.</p>
					</div>
					<div class="col-md-6">
						<img src="images/nb_block1_ts2.png" class="fl_img2">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_ts">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<img src="images/nb_block1_ts3.png" class="fl_img3">
					</div>
					<div class="col-md-6">
						<p>При оперативном анализе публикаций и разработке планов по сотрудничеству со СМИ быстрый обмен новостями критически важен. Живая Лента «Битрикс24» упростила этот процесс. Сотрудник может мгновенно делиться обзорами ИТ-новинок и важными объявлениями со всей компанией. Публикация не теряется в общем потоке – заинтересованные читатели могут добавить ее в «Избранное».</p>
						<p>Оповещая коллег о важном событии, можно отслеживать, кто прочел текст. Это еще один инструмент контроля сотрудников. </p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_ts bg npad">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<div class="pad">
							<p>Благодаря дополнительному модулю «Битрикс24» у медиа-офиса появился удобный инструмент для бронирования переговорных и управления встречами. Раньше специальной программы для этих задач у клиента не было. Информация  о наличии свободной комнаты запрашивалась после назначения встречи. Из-за этого могли возникать ситуации, когда свободного помещения вовремя не оказывалось.</p>
						</div>
					</div>
					<div class="col-md-6">
						<img src="images/nb_block1_ts4.png" class="fl_img4">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_ts">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<img src="images/nb_block1_ts5.png" class="fl_img5">
					</div>
					<div class="col-md-6">
						<p>Еще одна важная для заказчика возможность, которую удалось реализовать в «Битрикс24» – обмен документами через корпоративное хранилище. Пользователи могут загружать файлы, устанавливать права доступа к ним и создавать собственные директории хранилища.</p>
						<p>Благодаря этому при общении в программе можно быстро делиться ссылками на документы.</p>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_ts bg7">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<p>На финальном этапе внедрения специалисты «Первого БИТа» обучили сотрудников медиа-офиса работе с новым программным обеспечением. Были подготовлены два набора учебных материалов: для пользователей и для администраторов. А чтобы заказчик мог оперативно консультироваться по работе с «Битрикс24», «Первый БИТ» обеспечивает информационно-техническое сопровождение программы.</p>
						<p>Медиа-офис правительства Дубая уже наметил для компании «Первый БИТ» новую задачу. Планируется расширить возможности внедренной системы за счет установки модулей  для управления бизнес-процессами (BPM) и управления отношениями с клиентами (CRM).</p>
					</div>
					<div class="col-md-6">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block1_tw">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="z">Результаты проекта</div>
					<div class="col-md-4">
						<div class="padd">
							Офис быстрее реагирует на информационные поводы. Упростилось информирование сотрудников о текущих событиях.
							<span class="ico"></span>
						</div>
					</div>
					<div class="col-md-4">
						<div class="padd">
							Ускорился анализ СМИ с целью поиска новых партнеров.
							<span class="ico"></span>
						</div>
					</div>
					<div class="col-md-4">
						<div class="padd">
							Исключены задержки деловых встреч, связанные с нехваткой свободных переговорных и залов для совещаний. Отлажено бронирование помещений и управление встречами.
							<span class="ico"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var h = ($(window).height()) * 0.92;
$(window).scroll(function() {				
if ( $(this).scrollTop() > $('.nb_block1_tg .i img').offset().top - h) {   
	$('.nb_block1_tg .i img').addClass('s');					
}
if ( $(this).scrollTop() > $('.nb_block1_th img.r').offset().top - h) {   
	$('.nb_block1_th img.r').addClass('s');					
}
if ( $(this).scrollTop() > $('.nb_block1_th img.l').offset().top - h) {   
	$('.nb_block1_th img.l').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img1').offset().top - h-430) {   
	$('.fl_img1').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img2').offset().top - h) {   
	$('.fl_img2').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img3').offset().top - h) {   
	$('.fl_img3').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img4').offset().top - h) {   
	$('.fl_img4').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img5').offset().top - h) {   
	$('.fl_img5').addClass('s');					
}
});	
</script>
</body>
</html>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
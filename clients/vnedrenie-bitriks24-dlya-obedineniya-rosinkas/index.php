<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Внедрение Битрикс24 для Объединения «РОСИНКАС»");
$APPLICATION->SetPageProperty("keywords", "портфолио сайтов, веб студия примеры работ, качественные сайты, корпоративный портал, битрикс 24");
$APPLICATION->SetPageProperty("title", "Битрикс24 для Объединения «РОСИНКАС» | Портфолио веб-студии «Первый БИТ»");
$APPLICATION->SetTitle("Внедрение Битрикс24 для компании «РОСИНКАС»");
?>
<link href="css/block4.css" type="text/css"  rel="stylesheet" />
<div class="nb_block4">
	<div class="nb_block4_t">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="wr">
						<div class="logo"><img src="images/block4_logo.png"></div>
						<div class="title">Внедрение Битрикс24<br />для Объединения «РОСИНКАС»</div>
                        <div class="panel">
                            <a href="rosinkas.pdf" target="_blank" class="pdf">Скачать кейс</a>
                        </div>
						<div class="icon--mouse"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="z">ЗАКАЗЧИК</div>
					</div>
					<div class="col-md-6">
						<div class="nit41">
							<div class="i"><img src="images/nit41.png" class="fl_img1"></div>
							<div class="t">Объединение «РОСИНКАС» - один из крупнейших в стране перевозчиков наличных денег и других ценностей, имеет статус юридического лица с особыми уставными задачами.</div>
						</div>
						<div class="nit41">
							<div class="i"><img src="images/nit42.png" class="fl_img1"></div>
							<div class="t">Объединение «РОСИНКАС» является надёжным партнёром многих крупных российских и международных компаний, в том числе большинства банков, работающих на территории России.</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="nit41">
							<div class="i"><img src="images/nit43.png"  class="fl_img1"></div>
							<div class="t">Объединение «РОСИНКАС» осуществляет перевозки ценностей автомобильным, воздушным, железнодорожным и водным видами транспорта.</div>
						</div>
						<div class="nit41">
							<div class="i"><img src="images/nit44.png" class="fl_img1"></div>
							<div class="t">Объединение «РОСИНКАС» обеспечивает деятельность Банка России по организации наличного денежного обращения и входит в единую централизованную систему Банка России.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="nb_block4_tt">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<div class="nit42">
							<span class="t">Общая протяженность маршрутов инкассации</span>
							<span class="b">834 362 км</span>
						</div>
						<div class="nit42">
							<span class="t">Общая сумма коммерческих перевозок</span>
							<span class="b">7 513,6 млрд руб</span>
						</div>
						<div class="nit42">
							<span class="t">Общая численность участков инкассации</span>
							<span class="b">420 шт.</span>
						</div>
						<div class="nit42">
							<span class="t">Количество объектов обслуживания</span>
							<span class="b">174 436 шт.</span>
						</div>
					</div>
					<div class="col-md-6">
						<img src="images/nit45.png" class="l50 fl_img2">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt bg bg1">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="zz">Цели и задачи проекта</div>
					</div>
					<div class="col-md-4">
						<div class="nit43">
							<strong>01</strong>
							<div class="t">Внедрить функционал структурирования и хранения информации о клиентах</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="nit43">
							<strong>02</strong>
							<div class="t">Реализовать возможность управления сделками — от лида до выставления коммерческого предложения и заключения договора</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="nit43">
							<strong>03</strong>
							<div class="t">Предоставить возможность руководителям смотреть отчетность по компании в различных разрезах</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt nit44">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="zz">Основной функционал</div>
					</div>
					<div class="col-md-2">
						<img src="images/nit46.png" class="fl_img9">
					</div>
					<div class="col-md-10">
						<strong>Карточка клиента</strong>
						<div class="t">
							<p>В соответствии со спецификой работы компании была внедрена карточка клиента с уникальным набором полей в зависимости от того, физическим или юридическим лицом является клиент. Поля имеют функцию автозаполнения данных о компании по ИНН или названию, благодаря синхронизации с сервисом DaData.</p>
							<p>Так как компания «РОСИНКАС» часто сотрудничает с клиентами, имеющими разветвленную структуру (с большим количеством подразделений внутри), то было реализовано создание и отображение этой структуры в рамках карточки клиента.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<img src="images/nit47.png" class="sh fl_img3">
					</div>
					<div class="col-md-6">
						<strong class="color">Управление сделками</strong>
						<div class="t">
							<p>Был разработан и реализован алгоритм управления сделками с помощью корпоративного портала Битрикс24:</p>
							<ul>
								<li>Фиксирование источника, откуда в компанию пришел клиент;</li>
								<li>Отображение прогресса сделки — в зависимости от прогресса рассчитывается вероятность успешного ее завершения. </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="nb_block4_tt bg bg2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="b">
					<div class="col-md-12">
						<p>Отдельно стоит отметить обработку точек инкассации (их может быть очень много, до 2000 на одного клиента) — от них напрямую зависит расчет стоимости оказываемых услуг и формирование коммерческого предложения (КП). Корпоративный портал Битрикс24 позволяет загружать точки и работать с ними: </p>
					</div>
					<div class="col-md-6">
						<div class="t">
							<ul>
								<li>Добавление каждой точки вручную или массовый импорт;</li>
								<li>Проверка точек по Яндекс и Google-картам;</li>
								<li>Ручная проверка точек при наличии каких-либо расхождений;</li>
								<li>Согласование точек с различными отделами для выставления КП;</li>
								<li>Формирование калькуляции в рамках каждой точки — каждую точку просчитывает конкретный отдел/филиал;</li>
								<li>Сверка точек по регионам в рамках всей структуры продавцами и коммерческими директорами соответствующих филиалов;</li>
								<li>Формирование индивидуального КП с учетом просчета всех точек или общего КП, исходя из фиксированных тарифов. </li>
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<img src="images/nit48.png" class="sh r60 fl_img4">
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt nit44 bor">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-2">
						<img src="images/nit49.png" class="fl_img10">
					</div>
					<div class="col-md-10">
						<div class="t">
							<p>В системе существует проверка карточек на дубликаты — сделок с одной компанией может быть множество, но менеджеры не могут завести компании с одним ИНН и КПП несколько раз (с одним ИНН, но с разными КПП — могут). Этот метод позволяет не плодить множество одинаковых компаний в системе. Если менеджер при заведении ИНН и КПП получает совпадение, то он просит права доступа к этой компании и совершает все манипуляции в рамках одного клиента. В таком случае пользователям не приходится искать информацию по разным карточкам клиента при дальнейшей работе.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt nit44 bor">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-2">
						<img src="images/nit50.png" class="fl_img11">
					</div>
					<div class="col-md-10">
						<div class="t">
							<p>Также были предусмотрены различные сценарии работы со сделками. Менеджер по продажам в регионе, получивший сделку на проработку, может отправить ее в производственный отдел на просчет, может отправить в финансовый отдел на проверку расчета, а может сформировать расчет сам, если условия являются простыми.</p>
							<p>Для оперативного контроля за ходом сделки была настроена система уведомлений о задачах и статусе сделки.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt nit44">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-2">
						<img src="images/nit51.png" class="fl_img12">
					</div>
					<div class="col-md-10">
						<div class="t">
							<p>Готовый расчет перед окончательным формированием КП можно вернуть на доработку по бизнес-процессу или отправить коммерческому директору на согласование. После согласования коммерческим директором происходит автоматическое формирование КП по всем точкам в формате Excel. КП можно сразу отправить клиенту прямо из интерфейса корпоративного портала Битрикс24.</p>
							<p>Также предусмотрена возможность формирования общего простого КП с автоматическим расчетом стоимости исходя из фиксированных общих тарифов.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt bg bg3">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<div class="paddr">
							<p class="p20 b">После согласования КП с клиентом и подтверждения сделки в системе формируется договор на работу с ним. В системе добавлены более 10-и различных шаблонов договоров и приложений.</p>
							<p class="p16 b">В договорах поддерживается автоматическая конвертация полей в определенный падеж. Есть возможность заключения сложных 3-х сторонних договоров, а также договоров с несколькими услугами. Все версии договоров хранятся на внутреннем диске в Битрикс24 — можно посмотреть историю изменения, а также прикрепить скан. Также есть возможность отобразить связанные договоры.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="paddl">
							<p class="p20">Все изменения в действующих сделках осуществляются через оформление дополнительных соглашений. <br /> &nbsp;</p>
							<p class="p16">Заключение ДС так же реализовано в системе с помощью задач бизнес-процесса и формирования документов (как и в случае с договорами).</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt nit44">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-2">
						<img src="images/nit52.png" class="fl_img13">
					</div>
					<div class="col-md-10">
						<strong>Account team</strong>
						<div class="t">
							<p>Большое внимание было уделено вопросу безопасности и реализовано разграничение прав на уровне полей объектов. Например, сделку, компанию или лид могут увидеть только те пользователи, которые являются ответственными за нее или соисполнителями. Также можно предоставлять доступ к конкретным данным сделки только определенным сотрудникам. Доступ можно предоставлять всем сотрудникам в системе.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<img src="images/nit53.png" class="sh mtop50 fl_img5">
					</div>
					<div class="col-md-6">
						<strong class="color">Отчеты</strong>
						<div class="t">
							<p>Специалистами компании «Первый БИТ» был доработан функционал отчетов, для того, чтобы руководство «РОСИНКАС» могло оценивать эффективность работы компании и принимать верные управленческие решения. Есть возможность формирования не стандартных отчетов по сделкам в любых разрезах — выручка, затраты, прибыль, рентабельность. Также можно формировать отчеты с любым полями, которые есть в системе — например, вывести отчеты по задачам и встречам менеджеров. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<strong class="color">Структура компании</strong>
						<div class="t">
							<p>Компания имеет очень сложную внутреннюю структуру, которая была отображена в корпоративном портале Битрикс24. Это позволяет проводить работы по оптимизации бизнес-процессов компаний, а также упрощает взаимодействие между сотрудниками.</p>
						</div>
					</div>
					<div class="col-md-6">
						<img src="images/nit54.png" class="sh fl_img6">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block4_tt bg bg4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="z">Результаты проекта</div>
					</div>
					<div class="col-md-6 left bgb">
						<div class="item">Сократилось время подготовки коммерческих предложений за счет удобного интерфейса управления сделкой и взаимодействия между различными отделами и филиалами, а также автоматизации ряда операций</div>
						<div class="item">Повысилась эффективность работы сотрудников, задействованных в бизнес-процессе продажи</div>
					</div>
					<div class="col-md-6 right">
						<img src="images/nit55.png" class="fl_img7">
					</div>
					<div class="clr"></div>
					<div class="col-md-6 left">
						<img src="images/nit56.png" class="fl_img8">
					</div>
					<div class="col-md-6 right bgb">
						<div class="item">Стали более прозрачными внутренние бизнес-процессы компании</div>
						<div class="item">Появилась возможность более глубокого анализа деятельности компании для ее дальнейшей оптимизации</div>
					</div>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>

<script>
var h = ($(window).height()) * 0.92;
$(window).scroll(function() {				
if ( $(this).scrollTop() > $('.fl_img1').offset().top - h) {   
	$('.fl_img1').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img2').offset().top - h-430) {   
	$('.fl_img2').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img3').offset().top - h) {   
	$('.fl_img3').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img4').offset().top - h) {   
	$('.fl_img4').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img5').offset().top - h) {   
	$('.fl_img5').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img6').offset().top - h) {   
	$('.fl_img6').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img7').offset().top - h) {   
	$('.fl_img7').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img8').offset().top - h) {   
	$('.fl_img8').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img9').offset().top - h-430) {   
	$('.fl_img9').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img10').offset().top - h) {   
	$('.fl_img10').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img11').offset().top - h) {   
	$('.fl_img11').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img12').offset().top - h) {   
	$('.fl_img12').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img13').offset().top - h) {   
	$('.fl_img13').addClass('s');					
}
});	
</script>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Програмное обеспечение финансовой группы \"Лайф\" | Портфолио веб-студии Первый БИТ");
$APPLICATION->SetPageProperty("keywords", "битрикс, 1с, сайт битрикс, разработка сайтов, портфолио студий");
$APPLICATION->SetPageProperty("description", "Разработка программного обеспечения для проекта \"LifePad\" для финансовой группы \"Лайф\"");
$APPLICATION->SetTitle("Портфолио");
$APPLICATION->SetAdditionalCSS("/clients/razrabotka-programmnogo-obespecheniya-dlya-proekta-lifepad-finansovoy-gruppy-layf/style.css");
?>
<link href="/local/templates/main/assets/css/css1.css" type="text/css"  rel="stylesheet" />
<div class="p-page">
	<div class="p-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="p-title">Разработка программного обеспечения для проекта «LifePad» Финансовой Группы Лайф</h1>
 					<i class="i-fly1 hidden-xs"></i> <i class="i-fly2 hidden-xs"></i> <i class="i-fly3 hidden-xs"></i> <i class="i-fly4 hidden-xs"></i> <i class="i-fly5 hidden-xs"></i> <i class="i-fly6 hidden-xs"></i> <i class="i-fly7 hidden-xs"></i> <i class="i-fly8 hidden-xs"></i> <i class="i-fly9 hidden-xs"></i> <i class="i-fly10 hidden-xs"></i> <i class="i-mouse"></i>
				</div>
			</div>
		</div>
	</div>
 <section class="client">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="subtitle">Заказчик</h2>
				<ul class="client-list">
					<li class="client-item">– Финансовая Группа Лайф - холдинг независимых региональных банков, активно развивающихся на рынке банковских услуг России с 1993 года.</li>
					<li class="client-item">– Группа работает в 75 регионах и 200 городах.</li>
				</ul>
			</div>
		</div>
	</div>
 </section> <section class="about bg-color">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="subtitle">О проекте</h2>
				<ul class="about-list">
					<li class="about-item">– Проект «LifePAD» - новое слово в банковской сфере. LifePAD - это планшетное устройство с набором предустановленных приложений от Финансовой Группы</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="about-icon-line">
		<div class="about-icon">
			<i class="i-about i-about-1"></i>
		</div>
		<div class="about-icon">
			<i class="i-about i-about-2"></i>
		</div>
		<div class="about-icon">
			<i class="i-about i-about-3"></i>
		</div>
		<div class="about-icon">
			<i class="i-about i-about-4"></i>
		</div>
		<div class="about-icon hidden-xs">
			<i class="i-about i-about-1"></i>
		</div>
		<div class="about-icon hidden-xs">
			<i class="i-about i-about-2"></i>
		</div>
		<div class="about-icon hidden-xs">
			<i class="i-about i-about-3"></i>
		</div>
		<div class="about-icon hidden-xs">
			<i class="i-about i-about-4"></i>
		</div>
		<div class="about-icon hidden-xs">
			<i class="i-about i-about-1"></i>
		</div>
	</div>
 </section> 

 <section class="work">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-push-6 col-xs-12">
				<div class="work-img-wrapper">
 					<img src="images/w1.png" class="img-responsive work-img">
				</div>
			</div>
			<div class="col-sm-6 col-sm-pull-6 col-xs-12">
				<ul class="work-list">
					<li class="work-item">– Разработанный проект прошёл нагрузочное тестирование под требования плановой нагрузки в 1 000 000 устройств.</li>
					<li class="work-item">– Реализовано программное обеспечение для управления всеми услугами финансовой группы, которыми пользуется клиент - обладатель LifePAD.</li>
					<li class="work-item">– Планшет позволяет управлять вкладами, кредитными продуктами, инвестиционными портфелями, страховыми услугами.</li>
				</ul>
			</div>
		</div>
	</div>
 </section> 

 <section class="services">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<div class="services-item">
 					<img src="images/s1.png" class="services-img">
					<p>
						В рамках проекта запущен LifeMessenger - приложение, для прямой коммуникации с менеджером банка.
					</p>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="services-item bg-color">
 					<img src="images/s2.png" class="services-img">
					<p>
						В рамках проекта и части CRM был внедрён корпоративный портал на базе «Bitrix24».
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6 col-xs-12">
				<div class="services-item">
 					<img src="images/s4.png" class="services-img">
					<p>
						В рамках внедрения CRM реализован набор бизнес - процессов, позволяющих оптимизировать коммуникацию сотрудников банка с клиентами - обладателями LifePAD.
					</p>
				</div>
			</div>
			<div class="col-sm-6 col-sm-pull-6 col-xs-12">
				<div class="services-item bg-color">
 					<img src="images/s3.png" class="services-img">
					<p>
						Произведена интеграция CRM в направлении формирования заявок на основании обращений, полученных банком через Life Mesenger
					</p>
				</div>
			</div>
		</div>
	</div>
 </section> 
<section class="bg-image"></section>
		<script>
			var h = ($(window).height()) * 0.85;
			$(window).load(function() {	
				setTimeout(function() { $('.p-header').addClass('s') }, 800);

			});
			$(window).scroll(function() {				
				if ( $(this).scrollTop() > $('.work-img-wrapper').offset().top - h) {   
					$('.work-img-wrapper').addClass('s');					
			  	}
			  	if ( $(this).scrollTop() > $('.services-item').offset().top - h) { 
			  		$('.services-item').addClass('s');
			  	}
			  	if ( $(this).scrollTop() > $('.bg-image').offset().top - h) { 
			  		$('.bg-image').addClass('s');
			  	}
			  	if ( $(this).scrollTop() > $('.about').offset().top - h) { 
			  		$('.about').addClass('s');
			  	}
			});	
		</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
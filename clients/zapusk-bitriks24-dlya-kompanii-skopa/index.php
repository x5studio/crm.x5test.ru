<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Запуск Битрикс24 для компании «Скопа»");
$APPLICATION->SetPageProperty("keywords", "портфолио сайтов, веб студия примеры работ, качественные сайты, корпоративный портал, битрикс 24");
$APPLICATION->SetPageProperty("title", "Битрикс24 для компании «Скопа» | Портфолио веб-студии Первый БИТ");
$APPLICATION->SetTitle("Запуск Битрикс24 для компании «Скопа»");
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/block3.css" type="text/css"  rel="stylesheet" />
</head>
<body>

<div class="nb_block3">
	<div class="nb_block3_t">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="wr">
						<div class="logo"><img src="images/block3_logo.png"></div>
						<div class="title">Кейc по внедрению Битрикс24 для<br />агентства недвижимости «Скопа»</div>
                        <div class="panel">
                            <a href="skopa.pdf" target="_blank" class="pdf">Скачать кейс</a>
                        </div>
						<div class="icon--mouse"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block3_tt">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-7">
						<div class="z">ЗАКАЗЧИК</div>
						<p>«Скопа недвижимость» работает с 2006 года и занимается продажей квартир непосредственно от Застройщика. Также компания оказывает широкий спектр услуг: разъезд, расселение, срочный выкуп квартиры, юридическое сопровождение сделок, подготовка и проведение государственной регистрации возникновения и перехода права собственности.</p>
						<p>www.sell.skopa.ru</p>
					</div>
					<div class="col-md-5">
						<img src="images/nb_block3_tt.png" class="fl_img1">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block3_t4 bg bg1">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="zz">Цели и задачи проекта</div>
					</div>
					<div class="col-md-6 left">
						<div class="blue">
							<div class="it it1">
								<strong>01</strong><span>Упростить процесс наполнения веб-сайта информацией об объектах, а также реализовать механизм поддержания этой информации в актуальном состоянии.</span>
							</div>
							<div class="it it2">
								<strong>02</strong><span>Оптимизировать бизнес-процесс продажи.</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 right">
						<img src="images/nb_block3_t41.png" class="fl_img2">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block3_t4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="z">Основной функционал</div>
						<div class="zz">Интеграция веб-сайта и портала Битрикс24</div>
					</div>
					<div class="col-md-6">
						<img src="images/nb_block3_t42.png" class="sh fl_img3">
					</div>
					<div class="col-md-6">
						<p>Компания «Первый БИТ» не только внедрила корпоративный портал Битрикс24, но и разработала веб-сайт для клиента.</p>
						<p>В качестве главной особенности можно выделить тесную интеграцию между сайтом и корпоративным порталом.</p>
						<p>Шахматка объектов в Битрикс24 синхронизирована непосредственно с веб-сайтом.</p>
						<p>Веб-сайт отображает информацию и описание объектов, данные  о том, какие квартиры являются свободными, а какие уже проданы, а также сведения об их планировке и стоимости.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block3_t4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<p>Кроме того, для покупателя доступна проектная документация по объекту, которую он может скачать с сайта. </p>
						<p>Все данные заполняются в рамках портала Битрикс24 и автоматически выгружаются на сайт. </p>
						<p>Подобная система реализована не только для категории жилья в новостройках, но и для вторичного жилья, а также нежилых помещений (например, отображение машиномест в паркингах).</p>
					</div>
					<div class="col-md-6">
						<img src="images/nb_block3_t43.png" class="sh fl_img4">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block3_t4 bg bg2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6">
						<p><strong>Бизнес-процесс продажи</strong></p>
						<p>Вся работа менеджера по продажам строится по определенному, заранее выстроенному алгоритму. </p>
						<p>Ведение сделок связано с базой объектов. Таким образом, каждый менеджер видит всю необходимую информацию об объекте: площадь, детали квартиры, планировку, а также статус продажи. Есть авторасчет полей — например, стоимости.</p>
						<p>Создана уникальная карточки сделки, учитывающая конкретные задачи.</p>
						<p>Настроено автоматическое формирование задач после перехода на следующий этап по воронке сделки. Таким образом, у менеджера есть четкая последовательность шагов, по которой происходит отработка каждой сделки.</p>
					</div>
					<div class="col-md-6">
						<img src="images/nb_block3_t444.png" class="fl_img5">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block3_t4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-12">
						<div class="zz">Дополнительный функционал</div>
					</div>
					<div class="col-md-6">
						<img src="images/nb_block3_t45.png" class="fl_img6">
					</div>
					<div class="col-md-6">
						<p>Кроме других инструментов, входящих в состав корпоративного портала Битрикс24, также был реализован дополнительный функционал: </p>
						<ul>
							<li>Формирование печатной версии договора;</li>
							<li>Возможность проведения email и SMS рассылок по клиентам прямо из интерфейса портала.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nb_block3_tw">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="z">Результаты проекта</div>
					<div class="col-md-4">
						<div class="padd">
							Существенно сократилось время заполнения сайта.
							<span class="ico"></span>
						</div>
					</div>
					<div class="col-md-4">
						<div class="padd">
							Повысилась актуальность представленных на сайте данных об объектах, находящихся в продаже.
							<span class="ico"></span>
						</div>
					</div>
					<div class="col-md-4">
						<div class="padd">
							Увеличилось количество результативных сделок за счет четкого и контролируемого процесса их отработки.
							<span class="ico"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script>
var h = ($(window).height()) * 0.92;
$(window).scroll(function() {				
if ( $(this).scrollTop() > $('.fl_img1').offset().top - h) {   
	$('.fl_img1').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img2').offset().top - h-430) {   
	$('.fl_img2').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img3').offset().top - h) {   
	$('.fl_img3').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img4').offset().top - h) {   
	$('.fl_img4').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img5').offset().top - h) {   
	$('.fl_img5').addClass('s');					
}
if ( $(this).scrollTop() > $('.fl_img6').offset().top - h) {   
	$('.fl_img6').addClass('s');					
}
});	
</script>
</div>
</body>
</html>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
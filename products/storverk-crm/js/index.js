$(document).ready( function(){
    require(['mCustomScrollbar'], function () {
        $(".adding-modules-wrap .modules-list").mCustomScrollbar();
    });

    $('body').on('click', '.modules-list .single-module:not(".active")', function(){
        $('.modules-list .single-module').removeClass('active');
        $('.module-desc').addClass('hidden');

        $(this).addClass('active');
        $('.module-desc[data-module="' + $(this).attr('data-module') + '"]').removeClass('hidden');
    });

    $('body').on('click', '.mobile-modules-list .single-module', f_acc);

} );

function f_acc(){

    $('.mobile-modules-list .module-desc').not($(this).next()).slideUp(250);
    $(this).next().slideToggle(500);
}
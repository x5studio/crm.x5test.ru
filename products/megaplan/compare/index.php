<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сравнение тарифов Мегаплан");
?>
<?
$APPLICATION->IncludeComponent(
	"studiobit:product.compare",
	"megaplan",
	array(
		"IBLOCK_TYPE" => "products",
		"IBLOCK_ID" => "30",
		"SECTION" => "megaplan",
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
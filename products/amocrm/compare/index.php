<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("DESCRIPTION", "Полное сравнение тарифов Amocrm - подберем для Вас самый выгодный тариф.");
$APPLICATION->SetPageProperty("TITLE", "Сравнение тарифов Amocrm");
$APPLICATION->SetTitle("");
?>
<?
$APPLICATION->IncludeComponent(
	"studiobit:product.compare",
	"amocrm",
	array(
		"IBLOCK_TYPE" => "products",
		"IBLOCK_ID" => "30",
		"SECTION" => "amocrm",
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
$(document).ready( function(){
    require(['mCustomScrollbar'], function () {
        $(".adding-modules-wrap .modules-list").mCustomScrollbar();
    });

    $('body').on('click', '.modules-list .single-module:not(".active")', function(){
        $('.modules-list .single-module').removeClass('active');
        $('.module-desc').addClass('hidden');

        $(this).addClass('active');
        $('.module-desc[data-module="' + $(this).attr('data-module') + '"]').removeClass('hidden');
    });

    $('body').on('click', '.mobile-modules-list .single-module', f_acc);

    var $tab = $('.j-simple-tabs', document);
    if ($tab.length > 0) {
        require(['Controls/SimpleTabs'], function (SimpleTabs) {
            new SimpleTabs({
                root: $tab,
                tabNav: '.j-tab-nav'
            });
        });
    }

    (function () {
        var $popupButtons = $('.js-fopen');
        var $body = $('body, html');

        if($popupButtons.length) {
            require(['fancybox'], function (){
                $popupButtons.fancybox({
                      autoSize: true,
                    openEffect: 'none',
                    closeEffect: 'none',
                    helpers: {
                        overlay: {
                            locked: false
                        }},
                    beforeShow:  function () {
                        var currentScroll = $(window).scrollTop();
                        sessionStorage.setItem('scroll', currentScroll);
                        $body.css('overflow','hidden');
                    },
                    afterClose:  function () {
                        var currentScroll = Number(sessionStorage.getItem('scroll'));
                        sessionStorage.removeItem('scroll');
                        $body.removeAttr('style');
                        $(window).scrollTop(currentScroll);
                    }

                });
            });

        }
    })();



    (function () {
        var youtube = document.querySelectorAll(".youtube");
        for (var i = 0; i < youtube.length; i++) {
            var screen = $(i).attr
            var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/sddefault.jpg";
            var image = new Image();
            image.src = source;
            image.addEventListener("load", function () {
                youtube[i].appendChild(image);
            }(i));

            youtube[i].addEventListener("click", function () {
                var iframe = document.createElement("iframe");
                iframe.setAttribute("frameborder", "0");
                iframe.setAttribute("allow", "autoplay");
                iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?showinfo=1&autoplay=1&rel=0");
                this.innerHTML = "";
                this.appendChild(iframe);
            });
        };
    })();

} );

function f_acc(){

    $('.mobile-modules-list .module-desc').not($(this).next()).slideUp(250);
    $(this).next().slideToggle(500);
}
<?
require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");
$APPLICATION->SetTitle("Ошибка 404. Такой страницы не существует");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
    <section>
        <div class="container">
            <div class="row">
                <div class="page404">
                    <div class="text-content">
                        <h1>Ошибка 404. Такой страницы не существует</h1>
                        <p>Возможно, вы ошиблись набирая адрес, или данная страница удалена.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?
include($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php');
?>